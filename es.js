/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Contacto seleccionado",
      notSelected: "Seleccione un contacto",
      sections: {
        cell: " Móvil",
        home: "Hogar",
        homeFax: "Enviar fax a casa",
        work: "Trabajo",
        workFax: "Enviar fax al trabajo"
      },
      selectContacts: "Contactos seleccionados",
      selectedInfo: "Seleccione un contacto de la lista para ver los detalles."
    },
    contactsList: {
      emptyContacts: "Esta agenda está vacía",
      emptyContactsInfo: "Agregar un contacto a la agenda",
      foundContacts: "Encontrado",
      notFound: "No hay contactos",
      notFoundInfo:
        "La búsqueda no produjo ningún resultado, inténtalo de nuevo utilizando criterios distintos",
      totalContacts: "Contactos"
    },
    contextualMenuLabels: {
      address: "Dirección de correo",
      birthday: "Fecha de nacimiento",
      cancel: "Cancelar",
      delete: "Borrar",
      downloadVCard: "Descargar vCard",
      email: "Email",
      newContact: "Nuevo contacto",
      newList: "Nueva lista",
      note: "Nota",
      phone: "Teléfono",
      shareVCard: "Compartir",
      showVCard: "Mostrar VCard",
      sendAsAttachment: "Enviar por correo electrónico",
      sendMail: "Enviar correo electrónico",
      url: "URL"
    },
    deleteContactDsc:
      "Al eliminar el contacto <code>{{contact}}</code> toda la información relacionada con este contacto se eliminará también.<br/><b>The operation is not reversible</b>.",
    deleteContactsDsc:
      "Al eliminar estos contactos, toda la información relacionada con estos contactos se eliminarán también.<br/><b>The operation is not reversible</b>.",
    deleteContactTitle: "Eliminar contacto",
    deleteContactDscList:
      "Al eliminar estos contactos, toda la información relacionada con estos contactos se eliminarán también.<br/><b>The operation is not reversible</b>.",
    deleteContactsTitle: "Eliminar contactos",

    deleteListDsc:
      "Al eliminar la lista <code>{{contact}}</code> toda la información relacionada con esta lista se eliminará también.<br/><b>The operation is not reversible</b>.",
    deleteListTitle: "Eliminar lista",

    editContactModal: {
      title: "Editar contacto"
    },
    editListModal: {
      title: "Editar lista"
    },
    exportAddressBookModal: {
      desc:
        "Está exportando la agenda: <code>{{addressbookExport}}</code><br/>Por favor, seleccione el formato para exportar todos los contactos presentes",
      title: "Exportar agenda"
    },
    import: {
      contactsInvalid: "No es válido:",
      contactsNr: "Contactos totales a importar:",
      contactsValid: "Válido:",
      desc:
        "Vas a importar contactos a la agenda: <code>{{addressbookImport}}</code><br/>Vas a importar contactos a la agenda. Puedes importar contactos de otras aplicaciones de correo electrónico utilizando un formato vCard (<strong>.vcf</strong>) o <strong>.ldif</strong> archivo.\u000APor ejemplo, puedes exportar contactos desde Gmail o desde Outlook e importarlos a Qboxmail.\u000ALos contactos importados no van a sobreescribir a ninguno de los contactos existentes.",
      dropFiles: "Subir o soltar aquí el archivo que desea importar",
      error: "No pudieron importarse los contactos",
      importing:
        "Estamos importando <b>{{contactsNumber}}</b> evento en el calendario: <code>{{addressbookImport}}</code>",
      loading: "Cargando...",
      success: "Importación exitosa!",
      title: "Importar contactos",
      uploadedFile: "Subido",
      uploadingFile: "Subiendo"
    },
    list: {
      desc:
        "Es posible añadir hasta <b>150 miembros</b> a la lista escogiendo los contactos en la agenda <code>{{addressbookName}}</code>.",
      members: "Lista de miembros"
    },
    messages: {
      newContactAddressBookBody:
        "No se puedo agregar contacto <b>Todos los contactos</b> o <b>Contactos corporativos</b>.<br/>Seleccione una agenda personal o compartida.",
      newContactAddressBookTitle: "Seleccionar agenda",
      resetListBody:
        "Miembros de listas estan relacionados con la agenda seleccionada. Este cambio va a reiniciar la lista de miembros. ¿Continuar?",
      resetListTitle: "Eliminación de miembros"
    },
    newAddressBookModal: {
      desc: "",
      title: "Add / Edit address book"
    },
    newContactModal: {
      title: "Add contact"
    },
    newListModal: {
      title: "Añadir lista"
    },
    shareModal: {
      desc:
        "Estás compartiendo la agenda: <code>{{addressbookShare}}</code><br/>Selecciona los contactos con los que deseas compartir la agenda y permiteles editarla.",
      title: "Compartir agenda"
    },
    sidebar: {
      allContacts: "Todos los contactos",
      deleteAddressbookDsc:
        "Borrando la agenda <code>{{addressbook}}</code> se eliminarán todos los contactos en ella.<br/><b>La operación no es reversible.</b>.",
      deleteAddressbookTitle: "Eliminar agenda",
      globalAddressbook: "Contactos corporativos",
      personalAddressbook: "Contactos personales",
      sharedAddressbooks: "Agendas compartidas",
      userAddressbooks: "Agenda del usuario"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "No hay agendas a las que subscribirse",
      title: "Suscribirse a la agenda"
    }
  },

  app: {
    addressbook: "Agenda",
    calendar: "Calendario",
    chat: "Chat",
    meeting: "Reunión",
    of: "de",
    webmail: "Webmail"
  },

  avatarEditorModal: {
    photoTitle: "Tomar una foto",
    title: "Editar avatar",
    zoom: "Zoom -/+"
  },

  buttons: {
    accept: "Aceptar",
    add: "Añadir",
    addAction: "Agregar acción",
    addAddressbook: "Agenda",
    addAsAttachment: "Adjuntar",
    addCalendar: "Calendario",
    addField: "Añadir campo",
    addHolidayCalendar: "Holiday calendar",
    addLabel: "Agregar etiqueta",
    addInline: "Añadir en línea",
    addParameter: "Añadir parámetro",
    addSignature: "Añadir firma",
    addToList: "Añadir a la lista",
    apply: "Aplicar",
    backTo: "Atrás",
    backAddressbook: "Cerrar",
    cancel: "Cancelar",
    changeImage: "Cambiar imagen",
    changePassword: "Cambiar la contraseña",
    checkFreeBusy: "Ver disponibilidad",
    close: "Cerrar",
    createFolder: "Nueva carpeta",
    comeBackLater: "Vuelve mas tarde",
    confirm: "Confirmar",
    copyLink: "Copiar link",
    decline: "Declinar",
    delete: "Eliminar",
    deleteDraft: "Eliminar borrador",
    details: "Detalles",
    disabled: "Desactivado",
    disableOTP: "Desactivar OTP",
    download: "Descargar",
    editLabel: "Editar etiqueta",
    editSignature: "Editar firma",
    empty: "Vacío",
    enabled: "Activado",
    event: "Evento",
    export: "Exportar",
    goToMail: "Volver al correo",
    goToOldWebmail: "Go to old webmail",
    import: "Importar",
    hide: "Esconder",
    label: "Etiqueta",
    leftRotate: "Girar 90 grados a la izquierda",
    loadImage: "Subir imagen",
    logout: "Cerrar sesión",
    makePhoto: "Tomar una foto",
    markRead: "Leer",
    markSeen: "Marcar",
    markUnread: "No leído",
    message: "Mensaje",
    modify: "Editar",
    new: "Nuevo",
    newContact: "Nuevo contacto",
    newEvent: "Nuevo evento",
    newMessage: "Nuevo mensaje",
    occurrence: "Ocurrencia",
    openMap: "Abrir en mapa",
    reloadPage: "Recargar página",
    removeImage: "Quita la imagen",
    rename: "Renombrar agenda",
    reorder: "Reordenar",
    reorderEnd: "Finalizar reorden",
    report: "Reportar",
    retryImport: "Reintentar",
    save: "Guardar",
    saveDraft: "Guardar borrador",
    saveNextAvailability: "Guardar disponibilidad sugerida",
    search: "Buscar",
    send: "Enviar",
    series: "Serie",
    setOtp: "Establecer OTP",
    share: "Compartir",
    show: "Mostrar",
    showMore: "Otras opciones",
    showUrlsAddressbook: "Mostrar URLS de la agenda",
    showUrlsCalendar: "Mostrar URLS del calendario",
    snooze: "Silenciar",
    starred: "Estrella",
    subscribe: "Suscribir",
    subscribeAddressbook: "Subscribir agenda",
    subscribeCalendar: "Suscribir calendario",
    tentative: "Tal vez",
    unsetOtp: "Desactivar OTP",
    unsubscribe: "Darse de baja",
    update: "Actualizar",
    updateFilter: "Actualizar filtro",
    viewMap: "Ver el mapa"
  },

  buttonIcons: {
    activeSync: "Activar ActiveSync",
    activeSyncNot: "Desactivar ActiveSync",
    alignCenter: "Alinear al centro",
    alignLeft: "Alinear a la izquierda",
    alignRight: "Alinear a la derecha",
    backgroundColor: "Color de fondo",
    backTo: "Atrás",
    bold: "Negrita",
    confirm: "Confirmar",
    close: "Cerrar",
    delete: "Eliminar",
    download: "Descargar",
    downloadVCard: "Descargar vCard",
    edit: "Editar",
    emoji: "Emoji",
    forward: "Reenviar",
    fullscreen: "Pantalla completa",
    fullscreenNot: "Modo ventana",
    imageAdd: "Añadir imagen",
    indent: "Aumentar sangría",
    italic: "Cursiva",
    maintenance: "Mantenimiento",
    maxmized: "Agrandar",
    minimized: "Reducir",
    more: "Más",
    moveDown: "Descender",
    moveUp: "Ascender",
    next: "Siguiente",
    orderList: "Lista de pedidos",
    outdent: "Disminuir sangría",
    pin: "Marcar",
    previous: "Anterior",
    print: "Imprimir",
    removeFormatting: "Eliminar formato",
    reply: "Contestar",
    replyAll: "Contestar a todos",
    saveDraft: "Guardar borrador",
    sendDebug: "Enviar mensaje que no se puede visualizar",
    sendMail: "Enviar correo",
    share: "Compartir",
    showSidebar: "Mostrar / ocultar la barra lateral",
    showSourceCode: "Mostrar código fuente",
    showVCard: "Mostrar VCard",
    spam: "Marcar como spam",
    spamNot: "Marcar como no spam",
    startChat: "Comenzar chat",
    startMeeting: "Comenzar reunión",
    strike: "Tachar",
    subscribe: "Suscribir",
    textColor: "Color del texto",
    thanks: "Gracias",
    underline: "Subrayar",
    unorderList: "Desordenar lista",
    unSubscribe: "Darse de baja"
  },

  calendar: {
    deleteEvent: "Borrar evento",
    deleteEventDsc: "¿Proceder a la eliminación del evento seleccionado?",
    event: {
      deleteEventOccurrenceTitle: "¿Eliminar evento o serie?",
      deleteEventOccurrenceDesc:
        "El evento que desea borrar tiene varios incidentes. Eliminar un incidente dejará inalterados los demás",
      fromDay: "de",
      maxLabelsNumberTitle: "¡Demasiadas etiquetas!",
      maxLabelsNumberDsc:
        "Has alcanzado el máximo número de etiquetas. Para añadir una nueva etiqueta debes cambiar primero una de las ya presentes.",
      organizerTitle: "Creado por",
      pendingMsg: "Evento pendiente de confirmación",
      repeatEvent: "Este evento es parte de eventos recurrentes",
      saveEventOccurrenceTitle: "¿Mostrar evento o eventos recurrentes?",
      saveEventOccurrenceDesc:
        "El evento que deseas ver o editar esta repetido varias veces. Editar este evento dejará inalterado el resto",
      toDay: "para"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "Primero",
      WeekDayNumberInMonth_2: "Segundo",
      WeekDayNumberInMonth_3: "Tercero",
      WeekDayNumberInMonth_4: "Cuarto",
      WeekDayNumberInMonth_5: "Quinto",
      never: "Nunca",
      after: "Después;repeticiones",
      inDate: "A fecha",
      end: "Fin de la repetición"
    },
    filterView: {
      listmonth: "Agenda",
      day: "Día",
      month: "Mes",
      week: "Semana",
      workWeek: "Semana de trabajo",
      year: "Año"
    },
    import: {
      contactsInvalid: "No es válido:",
      contactsNr: "Eventos totales a importar:",
      contactsValid: "Válido:",
      dropFiles: "Subir o soltar aquí el archivo que desea importar",
      error: "Los eventos no pudieron ser importados",
      importing:
        "Estamos importando <b>{{eventsNumber}}</b> evento en el calendario: <code>{{calendarImport}}</code>",
      info:
        "Estas importando eventos en el calendário: <code>{{calendarImport}}</code><br/>Puedes importar eventos de otras aplicaciones de correo electrónico usando un formato de archivo ICS. Por ejemplo, puedes importar eventos desde Gmail o desde Outlook en formato ICS e importarlos a Qboxmail.<br/><b>La importación no reemplazará ninguno de los ecentos existentes</b>.",
      loading: "Cargando...",
      success: "¡Todos tus eventos han sido importados con éxito!",
      title: "Importar eventos",
      uploadedFile: "Subido",
      uploadingFile: "Subiendo"
    },
    newCalendarModal: {
      desc: "",
      title: "Agregar / Editar calendario"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Archivos adjuntos",
        placeholderList: " "
      },
      desc: "",
      dropFiles: "Subir o soltar aquí el archivo que deseas adjuntar",
      freeBusy: {
        legend: {
          event: "Evento",
          busy: "Ocupado",
          waiting: "Esperando",
          availability: "Siguiente disponibilidad"
        }
      },
      hideDetails: "Ocultar detalles",
      participantsList: {
        headerItem: "Participantes",
        answeredHeaderItem: "Respondido",
        notAnsweredHeaderItem: "Esperando una respuesta",
        placeholderList: " "
      },
      showDetails: "Mostrar detalles",
      tabAttachments: "Archivos adjuntos",
      tabDetails: "Detalles",
      tabParticipants: "Participantes",
      tabSeries: "Repeticiones",
      tabSettings: "Ajustes",
      title: "Crear evento",
      titleEdit: {
        event: "Evento",
        series: "Serie"
      },
      titleFreebusy: "Asistentes libres/ocupados"
    },
    nextAvailability: "Siguiente disponibilidad",
    searchEvents: {
      notFound: "No se han encontrado eventos",
      notFoundInfo:
        "La búsqueda no ha dado ningún resultado, inténtalo de nuevo utilizando diferentes criterios"
    },
    shareModal: {
      desc:
        "Estás compartiendo el calendario: <code>{{calendarShare}}</code><br/>Selecciona los contactos con los que quieres compartir el calendario y dejarles editar.",
      title: "Compartir calendario"
    },
    sidebar: {
      labels: "Etiquetas",
      sharedCalendars: "Calendarios compartidos",
      userCalendars: "Calendarios de usuarios",
      deleteCalendarDsc:
        "Eliminar el calendario <code>{{calendar}}</code> borrará todos los eventos en el mismo.<br/><b>Esta operación no es reversible</b>.",
      deleteCalendarTitle: "Eliminar calendario"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "No hay calendarios a los que suscribirse",
      title: "Suscribirse al calendario"
    },
    today: "Hoy"
  },

  circularLoopBar: {
    uploading: "Subiendo"
  },

  comingSoon: "Desarrollo en progreso...",

  contactImport: {
    title: "Buscando contactos",
    desc:
      "Estamos importando tus antiguos contactos. La operación puede tardar algo de tiempo, espera, por favor"
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Respondido",
      "\\\\Deleted": "Supreso",
      "\\\\Draft": "Sequía",
      "\\\\Flagged": "Marcado",
      "\\\\Seen": "Leer"
    },
    antispamFilterTypes: {
      whitelist_from: "Permitir a partir del",
      whitelist_to: "Permitir"
    },
    calendarMinutesInterval: {
      "15": "15 minutos",
      "30": "30 minutos",
      "60": "60 minutos"
    },
    calendarNewEventFreeBusy: {
      busy: "Ocupado",
      free: "Disponible",
      outofoffice: "Fuera de la oficina",
      temporary: "Temporal"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Aceptar",
      DECLINED: "Rechazar",
      TENTATIVE: "Quizás"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Presidente",
      "REQ-PARTICIPANT": "Solicitado",
      "OPT-PARTICIPANT": "No solicitado",
      "NON-PARTICIPANT": "Con fines de información"
    },
    calendarNewEventPriority: {
      "0": "Ninguna",
      "1": "Alta",
      "5": "Media",
      "9": "Baja"
    },
    calendarNewEventTypes: {
      public: "Público",
      confidential: "Confidencial",
      private: "Privado"
    },
    calendarStartWeekDay: {
      saturday: "Sábado",
      sunday: "Domingo",
      monday: "Lunes"
    },
    calendarView: {
      year: "Año",
      month: "Mes",
      week: "Semana",
      workWeek: "Semana de trabajo",
      day: "Día",
      listmonth: "Lista"
    },
    calendarEventFilterBy: {allEvents: "Todos", nextEvents: "Todos los próximos eventos"},
    calendarEventSearchIn: {title: "Título/etiqueta/lugar", all: "Contenido completo"},
    cancelSendTimer: {
      "0": "Disabled",
      "5000": "5 seconds",
      "10000": "10 seconds",
      "20000": "20 seconds",
      "30000": "30 seconds"
    },
    contactNameFormat: {
      firstName: "Nombre, Apellidos",
      lastName: "Apellidos, nombre"
    },
    contactsOrder: {
      firstName: "Nombre",
      lastName: "Apellido"
    },
    countries: {
      AD: "Andorra",
      AE: "United Arab Emirates",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "American Samoa",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Åland Islands",
      AZ: "Azerbaijan",
      BA: "Bosnia and Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgium",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius and Saba",
      BR: "Brasil",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Belarus",
      BZ: "Belize",
      CA: "Canada",
      CC: "Cocos (Keeling) Islands",
      CD: "Democratic Republic of the Congo",
      CF: "Central African Republic",
      CG: "Republic of the Congo",
      CH: "Switzerland",
      CL: "Chile",
      CM: "Cameroun",
      CN: "China",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Cabo Verde",
      CW: "Curaçao",
      CX: "Christmas Island",
      CY: "Cyprus",
      CZ: "Czech Republic",
      DE: "Germany",
      DK: "Denmark",
      DM: "Dominica",
      DO: "Dominican Republic",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spain",
      ET: "Ethiopia",
      FI: "Finland",
      FO: "Faroe Islands",
      FR: "France",
      GA: "Gabon",
      GB: "United Kingdom",
      GD: "Grenada",
      GF: "French Guiana",
      GG: "Guernsey",
      GI: "Gibraltar",
      GL: "Greenland",
      GP: "Guadeloupe",
      GQ: "Equatorial Guinea",
      GR: "Greece",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croatia",
      HT: "Haiti",
      HU: "Hungary",
      IE: "Ireland",
      IM: "Isle of Man",
      IS: "Island",
      IT: "Italy",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Japan",
      KE: "Kenya",
      KR: "South Korea",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lithuania",
      LU: "Luxembourg",
      LV: "Latvia",
      MC: "Monaco",
      MD: "Moldova",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "North Macedonia",
      MQ: "Martinique",
      MT: "Malta",
      MW: "Malawi",
      MX: "Mexico",
      MZ: "Mozambique",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Netherlands",
      NO: "Norway",
      NZ: "New Zealand",
      PA: "Panama",
      PE: "Perú",
      PH: "Philippines",
      PL: "Poland",
      PT: "Portugal",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Sweden",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovakia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "South Sudan",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turkey",
      TZ: "Tanzania",
      UA: "Ukraine",
      UG: "Uganda",
      US: "United States of America",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "South Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Pedir siempre confirmación de entrega",
      never: "Nunca pedir confirmación de entrega"
    },
    eventAlarmPostponeTime: {
      "60": "Durante 1 minuto",
      "300": "Durante 5 minutos",
      "900": "Durante 15 minutos",
      "1800": "Durante 30 minutos",
      "3600": "Durante 1 hora",
      "7200": "Durante 2 horas",
      "86400": "Durante 1 días",
      "604800": "Durante 1 semana"
    },

    calendarTaskStatus: {
      "needs-action": "Necesita una accion",
      completed: "Terminado",
      "in-process": "En progreso",
      cancelled: "Cancelado"
    },

    feedbackAreas: {
      addressbookSidebar: "Lista de la agenda",
      contactList: "Lista de contactos",
      contactView: "Mostrar contactos",
      mailSearch: "Buscar mensajes",
      mailSidebar: "Lista de carpetas",
      messageList: "Lista de mensajes",
      messageView: "Mostrar mensajes",
      newMessage: "Componer un nuevo mensaje",
      other: "Más",
      settings: "Ajustes"
    },
    filterMail: {
      all: "Todos",
      byDate: "Fecha",
      byFrom: "De",
      bySize: "Tamaño",
      bySubject: "Asunto",
      flagged: "Marcado",
      filter: "Filtros",
      orderBy: "Ordenar por",
      seen: "Leer",
      unseen: "No leído"
    },
    filtersActions: {
      discard: "Eliminar",
      addflag: "Marcar el mensaje como",
      setkeyword: "Agregar etiqueta",
      fileinto: "Mover el mensaje",
      fileinto_copy: "Copiar mensaje",
      keep: "Mantener mensaje",
      redirect: "Redirigir mensaje a",
      redirect_copy: "Enviar copia a",
      reject: "Rechazar con mensaje",
      stop: "Dejar de procesar filtros"
    },
    filtersDateOperators: {
      gt: "Después",
      is: "Es igual a",
      lt: "Antes de",
      notis: "No es igual a"
    },
    filtersEnvelopeSubtypes: {From: "De", To: "Para"},
    filtersDateSubTypes: {date: "Fecha", time: "Hora", weekday: "Día laborable"},
    filtersDateWeekdays: {
      monday: "Lunes",
      tuesday: "Martes",
      wednesday: "Miércoles",
      thursday: "Jueves",
      friday: "Viernes",
      saturday: "Sábado",
      sunday: "Domingo"
    },
    filtersRules: {
      Bcc: "CCO",
      body: "Cuerpo",
      Cc: "Cc",
      currentdate: "Fecha de llegada",
      envelope: "Mensaje",
      From: "Remitente",
      other: "Otra cabecera",
      size: "Tamaño",
      Subject: "Asunto",
      To: "Destinatario"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "es mayor que", under: "es menor que"},
    filtersStringOperators: {
      contains: "contiene",
      exists: "existe",
      is: "es igual a",
      notcontains: "no contiene",
      notexists: "no existe",
      notis: "no es igual a"
    },
    languages: {
      de: "Alemán",
      en: "Inglés",
      it: "Italiano",
      nl: "Holandés",
      sv: "Sueco",
      es: "Español",
      pt: "Portugués"
    },
    matchTypes: {
      allof: "Satisface todos los parámetros",
      anyof: "Satisface al menos un parámetro"
    },
    newAllDayEventAlert: {
      never: "Nunca",
      eventDate: "Fecha del evento",
      "-PT17H": "Un día antes, a las 9 am",
      "-P6DT17H": "Una semana antes, a las 9 am"
    },
    newEventAlert: {
      never: "Nunca",
      eventHour: "El tiempo de inicio del evento",
      //P[n]Y[n]M[n]DT[n]H[n]M[n]S
      "-PT5M": "5 minutos antes ",
      "-PT15M": "15 minutos antes",
      "-PT30M": "30 minutos antes",
      "-PT1H": "1 hora antes ",
      "-PT2H": "2 horas antes",
      "-PT12H": "12 horas antes",
      "-P1D": "1 día antes",
      "-P7D": "1 semana antes"
    },
    newEventRepeatDayWeek: {
      MO: "Lunes",
      TU: "Martes",
      WE: "Miércoles",
      TH: "Jueves",
      FR: "Viernes",
      SA: "Sábado",
      SU: "Domingo"
    },
    newEventRepeatFrequency: {
      NEVER: "Nunca",
      DAILY: "Diariamente",
      WEEKLY: "Semanalmente",
      MONTHLY: "Mensualmente",
      YEARLY: "Anualmente"
    },
    newEventRepeatMonths: {
      "1": "Enero",
      "2": "Febrero",
      "3": "Marzo",
      "4": "Abril",
      "5": "Mayo",
      "6": "Junio",
      "7": "Julio",
      "8": "Agosto",
      "9": "Setiembre",
      "10": "Octubre",
      "11": "Noviembre",
      "12": "Diciembre"
    },
    orderSelector: {
      byLastname: "Apellido",
      byMail: "Email",
      byName: "Nombre",
      byOrg: "Empresa",
      orderBy: "Ordenar por",
      showOnlyLists: "Mostrar solamente las listas"
    },
    readConfirmations: {
      always: "Pedir siempre confirmación de lectura",
      never: "Nunca pedir la confirmación de lectura"
    },
    readingConfirmation: {
      always: "Siempre",
      ask: "Preguntar siempre",
      never: "Nunca"
    },
    selectMail: {
      all: "Seleccionar todo",
      cancel: "Cancelar",
      modify: "Editar",
      none: "Deseleccionar todo"
    },
    shareOptionsCalendar: {
      None: "Ninguna",
      DAndTViewer: "Ver la fecha y hora",
      Viewer: "Ver todo",
      Responder: "Responder a las",
      Modifier: "Modificar"
    },
    showImages: {
      always: "Siempre",
      contacts: "Contactos y de confianza",
      never: "Nunca"
    },
    themes: {
      dark: "Oscuro",
      light: "Claro"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Diseño de 3 columnas",
      viewHalf: "Diseño con vista previa",
      viewFull: "Diseño de lista"
    },
    viewModeCompact: {
      viewColumns: "Columnas",
      viewHalf: "Vista previa",
      viewFull: "Lista"
    }
  },

  dropdownMenu: {
    activesync: "ActiveSync",
    delete: "Eliminar",
    download: "Descargar",
    edit: "Editar",
    exportContacts: "Exportar contactos",
    importContacts: "Importar contactos",
    importEvents: "Importar eventos",
    remove: "Eliminar",
    rename: "Renombrar agenda",
    share: "Compartir",
    syncNo: "No sincronizar",
    syncYes: "Sincronizar",
    unsubscribe: "Darse de baja"
  },

  dropdownUser: {
    feedback: "Comentarios",
    logout: "Cerrar sesión",
    settings: "Ajustes"
  },

  errors: {
    emailDomainAlreadyPresent: "Dirección de correo electrónico o dominio ya presente",
    emptyField: "Campo vacío",
    invalidEmail: "Correo electrónico no válido",
    invalidEmailDomain: "Dirección de correo electrónico o dominio no válido",
    invalidPassword: "Contraseña no válida",
    invalidPasswordFormat: "Formato no válido, introduzca al menos 8 caracteres alfanuméricos",
    invalidValue: "No es un valor válido",
    mandatory: "Campo requerido",
    notAllowedCharacter: "Caracter no permitido",
    notChanged: "Campo no modificado",
    permissionDenied: "Permiso denegado",
    tooLong: "Campo demasiado largo"
  },

  feedback: {
    disclaimer:
      "Tu opinión es importante! ayudarnos a resolver los problemas y mejorar el servicio",
    labels: {
      feedbackAreas: "¿Sobre que área quieres dejar un comentario?",
      note: "Deja un mensaje (opcional)"
    },
    msgError: {
      title: "Sus comentarios no pudieron ser enviados",
      desc:
        "Por favor, inténtalo de nuevo más adelante, es importante para nosotros saber lo que piensas de nuestro webmail. Tus comentarios nos ayudarán a crear una mejor experiencia para ti y todos nuestros usuarios"
    },
    msgSuccess: {
      title: "¡Gracias por su respuesta!",
      desc:
        "Apreciamos de forma sincera el tiempo que has usado para ayudar a mejorar nuestro correo web. Tus comentarios nos ayudarán a crear una mejor experiencia para ti y para todos nuestros usuarios"
    },
    privacy:
      "Los datos recogidos serán tratados de conformidad con la legislación vigente en materia de privacidad",
    title: "Comentarios"
  },

  inputs: {
    labels: {
      addAddress: "Añadir dirección",
      addDomain: "Agregar dirección o dominio",
      addLabel: "Nombre de etiqueta",
      addMembers: "Añadir miembros",
      addParticipant: "Añada participante",
      addressbookName: "Agenda",
      addUser: "Agregar usuario",
      alert: "Alerta",
      alert2: "Segunda alerta",
      at: "A",
      calendar: "Calendario",
      calendarBusinessDayEndHour: "Final del día de trabajo",
      calendarBusinessDayStartHour: "Al comienzo del día de trabajo",
      calendarColor: "Color del calendario",
      calendarMinutesInterval: "Intervalo de tiempo del calendario",
      calendarName: "Nombre del calendario",
      calendarView: "Vista predeterminada",
      company: "Empresa",
      confidentialEvents: "Eventos confidenciales",
      confirmNewPassword: "Confirmar contraseña",
      contactNameFormat: "Formato de nombre de contacto",
      contactsOrder: "Orden de los contactos",
      country: "Country",
      dateFormat: "Formato de fecha",
      defaultAddressbook: "Agenda predeterminada",
      defaultCalendar: "Calendario predeterminado",
      displayName: "Disp. Nombre",
      email: "Email",
      endDate: "Fecha final",
      event: "Evento",
      eventDailyRepetition: "Todos;los dias",
      eventDailyWorkdayRepetition: "Cada día de la semana",
      eventMonthlyLastDayRepetition: "El último día del mes, cada;mes",
      eventMonthlyLastWeekDayRepetition: "Último {{WeekDay}}, cada;mes",
      eventMonthlyRepetition: "En {{DayNumber}}, cada;mes",
      eventMonthlyWeekDayRepetition: "En {{WeekDayNumber}}, cada;mes",
      eventPartecipation: "Estado de partecipacion",
      eventPriority: "Prioridad del evento",
      eventType: "Tipo de evento",
      eventWeeklyRepetition: "Cada;semana, en:",
      eventYearlyLastDayRepetition: "Último día de {{Month}}, cada;año",
      eventYearlyLastWeekDayRepetition: "Último {{WeekDay}} de {{Month}}, cada;año",
      eventYearlyRepetition: "El {{DayNumber}} de {{Month}}, cada;año ",
      eventYearlyWeekDayRepetition: "En {{WeekDayNumber}} de {{Month}}, cada;año",
      exportAsVcard: "Exportar en formato vCard",
      exportAsLdiff: "Exportar en formato LDIF",
      faxHome: "Enviar fax a casa",
      faxWork: "Enviar fax al trabajo",
      filter: "Filtrar por",
      filterName: "Filtrar por nombre",
      firstName: "Nombre",
      folderName: "Nombre de la carpeta",
      freeBusy: "Mostrar como",
      from: "De",
      fromDate: "Desde la fecha",
      home: "Casa",
      hour: "Hora",
      includeFreeBusy: "Incluir en el libre-ocupado",
      label: "Etiqueta",
      labelColor: "Color de la etiqueta",
      language: "Idioma",
      lastName: "Apellido",
      listName: "Nombre de la lista",
      location: "Ubicación",
      loginAddressbook: "Agenda inicial",
      mailAccount: "Cuenta",
      mobile: "Móvil",
      newAddressBook: "Nueva agenda",
      newAllDayEventAlert: "Nueva alerta a todos los eventos del día",
      newCalendar: "Nuevo calendario",
      newEventAlert: "Nueva alerta de eventos",
      newForward: "Añadir destinatario a la lista",
      newPassword: "Nueva contraseña",
      nickname: "Apodo",
      object: "Asunto",
      oldPassword: "Contraseña actual",
      parentFolder: "Camino",
      phone: "Teléfono",
      privateEvents: "Eventos privados",
      publicEvents: "Eventos públicos",
      readingConfirmation: "Enviar confirmación de lectura",
      recoveryEmail: "Recuperación de correo",
      renameAddressbook: "Renombrar agenda",
      repeatEvery: "Repetir cada",
      reportBugNote: "Descripción del problema",
      reportBugPhoneNumber: "Número de teléfono",
      searchAddressbookToSubscribe: "Buscar una agenda para suscribirse",
      searchCalendarToSubscribe: "Buscar un calendario al que suscribirse",
      searchIn: "Buscar en",
      selectCalendar: "Seleccionar el calendario",
      selectIdentity: "Seleccionar la identidad",
      selectList: "Seleccionar la lista",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "¿Con quien deseas compartir la agenda?",
      shareToCalendar: "¿Con quien deseas compartir el calendario?",
      shareToFolder: "¿Con quien deseas compartir la carpeta?",
      showImages: "Mostrar imagenes",
      signatureName: "Nombre a mostrar",
      signatureText: "Texto de la firma",
      startDate: "Fecha de inicio",
      team: "Equipo",
      theme: "Tema",
      timeFormat: "Formato de tiempo",
      timezone: "Zona horaria",
      title: "Título",
      to: "Para",
      untilDate: "Hasta el día",
      weekStart: "Primer día de la semana",
      work: "Trabajo"
    },
    placeholder: {
      addressbookName: "Nombre de la agenda",
      birthday: "Fecha de nacimiento",
      city: "Ciudad",
      country: "País",
      company: "Empresa",
      email: "Email",
      firstName: "Nombre",
      lastName: "Apellido",
      members: "Miembros",
      newFolderName: "Nuevo nombre de carpeta",
      newNameAddressbook: "Nuevo nombre de la agenda",
      newPassword: "Al menos 8 caracteres alfanuméricos",
      note: "Nota",
      phone: "Teléfono",
      postCode: "Código postal",
      region: "Región",
      role: "Papel",
      select: "Seleccionar",
      search: "Buscar en:",
      searchActivity: "Buscar actividad",
      searchEvent: "Buscar eventos",
      snooze: "Silenciar",
      street: "Calle"
    },
    hint: {
      cancelSendMessage:
        "Select the time available to retract a message during sending, if you decide you don't want to send the email."
    }
  },

  labels: {
    attendees: "Los asistentes",
    default: "Defecto",
    forDomain: "On domain",
    withAllUsers: "Con todos los usuarios",
    calendarAdvancedSearch: "¿Dónde quieres buscar?",
    clearAdvancedFilters: "Eliminar filtros",
    clearFields: "Borrar campos",
    holidayCalendar: "Holiday",
    searchResults: "Resultados de la búsqueda"
  },

  loaderFullscreen: {
    description: "Los contenidos están llegando, por favor espere",
    descriptionFolder: "Las carpetas están llegando, por favor espere",
    title: "Cargando"
  },

  mail: {
    confirmMailLabel: "OK, confirmo!",
    contextualMenuLabels: {
      addAddressBook: "Agregar a la agenda",
      addEvent: "Añadir evento",
      alarm: "Alarma",
      backTo: "Atrás",
      cancel: "Cancelar",
      close: "Cerrar",
      confirm: "Confirmar",
      copyIn: "Copiar en",
      copyThreadIn: "Copiar conversación",
      delete: "Eliminar",
      deletePerm: "Borrar permanentemente",
      deleteThread: "Eliminar conversación",
      deliveryNotification: "Confirmacion de envio",
      download: "Descargar",
      editAsNew: "Editar como nuevo",
      emptySpam: "Marcar como spam",
      emptyTrash: "Papelera vacía",
      forward: "Reenviar",
      import: "Importar correo",
      label: "Etiqueta",
      labelsManagement: "Administrar etiquetas",
      labelThread: "Hilo de etiquetas",
      markAllAsRead: "Marcar todo como leido",
      markAs: "Marcar como",
      markThreadAs: "Marcar conversación",
      manageFolders: "Administrar carpetas",
      modify: "Editar",
      more: "Más",
      moveThreadTo: "Mover conversación",
      moveTo: "Mover a",
      newSubFolder: "Agregar subcarpeta",
      plainText: "El modo de texto sin formato",
      print: "Impresión",
      priorityHigh: "Alta prioridad",
      readed: "Leer",
      readedNot: "No leído",
      readingNotification: "Confirmación de lectura",
      rename: "Renombrar agenda",
      reply: "Responder",
      replyToAll: "Responder a todos",
      restore: "Restaurar",
      restoreThreadIn: "Restaurar hilo",
      restoreIn: "Restaurar en",
      sendAsAttachment: "Reenviar como adjunto",
      showSourceCode: "Mostrar código fuente",
      spam: "Spam",
      spamNot: "No spam",
      starred: "Destacado",
      starredNot: "No destacado",
      thanks: "Gracias",
      thread: "Acciones en hilo"
    },
    deleteMailDsc:
      "Si no hay ningún sistema de almacenamiento activo, se eliminará permanentemente el mensaje.<br/><b>La operación no es reversible</b>.",
    deleteMailTitle: "Borrar mensaje",
    deleteMailsDsc:
      "Si no hay ningún sistema de almacenamiento activo, se eliminarán permanentemente los mensajes.<br/><b>La operación no es reversible<b>.",
    deleteMailsTitle: "Borrar mensajes seleccionados",
    dropFiles: "Soltar aquí los archivos a adjuntar",
    emptyTrashDsc:
      "Si no existe ningún sistema de almacenamiento activo al vaciar la papelera, todos los mensajes serán eliminados <b>de forma irreversible</b>.<br/>Después de <b>30 dìas</b> en la papelera, los mensajes se eliminan automáticamente.",
    emptyTrashTitle: "Papelera vacía",
    import: {
      messagesInvalid: "No es válido:",
      messagesNr: "Total de mensajes a importar:",
      messagesValid: "Válido:",
      desc:
        "Esta importando mensajes en la carpeta: <code>{{folderImport}}</code><br/>Puedes importar mensajes de otras aplicaciones de correo electrónico usando un formato de archivo eml (<strong>.eml</strong>).",
      dropFiles: "Subir o soltar aquí el archivo que desea importar",
      error: "Los mensajes no pudieron importarse",
      importing:
        "Estamos importando <b>{{messagesNumber}}</b> mensajes en la carpeta: <code>{{folderImport}}</code>",
      loading: "Cargando...",
      success: "Importación realizada con éxito!",
      title: "Importar mensajes",
      uploadedFile: "Subido",
      uploadingFile: "Subiendo"
    },
    markSpamReadDsc:
      "Marcar todos los mensajes en la carpeta como leídos.<br/>Despudés de <b>30 dìas</b>, los mensajes se eliminan automáticamente.",
    markSpamReadTitle: "Marcar mensajes como leídos",
    maxLabelsNumberTitle: "¡Demasiadas etiquetas!",
    maxLabelsNumberDsc:
      "Has alcanzado el máximo de etiquetas que pueden asociarse a un mensaje. Para añadir una etiqueta nueva, debes quitar primero una de las ya presentes.",
    message: {
      add: "Añadir",
      addToAddressBook: "Añadir contacto a la agenda",
      attachedMessage: "Mensaje adjunto",
      attachments: "Archivos adjuntos",
      bugMsg: "¿No ve el mensaje?",
      cc: "Cc:",
      date: "Fecha:",
      draftsSelectedInfo: "Seleccionar un mensaje de la lista y continuar escribiendo",
      downloadAll: "Descargar todo",
      downloadAttachments: "Descargar archivos",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Editar estado de participación",
      eventActionAccept: "Aceptar",
      eventActionDecline: "Rechazar",
      eventActionTentative: "Quizás",
      eventAddToCalendar: "Este evento no está presente en su calendario",
      eventAttendees: "Los asistentes",
      eventAttendeeStatusAccepted: "Evento aceptado",
      eventAttendeeStatusDeclined: "Evento rechazado",
      eventAttendeeStatusTentative: "Tentativa de participación",
      eventCounterProposal: "Este mensaje contiene una contrapropuesta al evento",
      eventDeleted: "Evento eliminado",
      eventInvitation: "Event invitation:",
      eventLocationEmpty: "No especificado",
      eventModify: "Editar",
      eventNeedAction: "Confirmar participación",
      eventNotNeedAction: "Evento ya procesado",
      eventOutDated: "El evento se ha modificado",
      eventStatusAccepted: "Aceptado",
      eventStatusDeclined: "Rechazado",
      eventStatusTentative: "Tentativa",
      eventUpdate: "Actualizar evento",
      folderSelectedInfo: "Seleccione un mensaje de la lista y descubrir su contenido.",
      from: "De:",
      hideAllAttachments: "Esconder",
      hugeMessage:
        "Contenido demasiado grande para mostrar completamente, haga clic en el enlace a continuación para ver el código fuente del mensaje.",
      hugeMessageNotice: "Mensaje truncado",
      inboxSelectedInfo: "Seleccione un mensaje de la lista y averiguar quién lo escribió.",
      loadImages: "Descarga imágenes",
      localAttachments: "There are additional attachments inside the event",
      message: "Mensaje",
      messageForwarded: "Mensaje reenviado",
      messages: "Mensajes",
      messagesQuota: "Cuota de mensajes usados:",
      newMessageinThread: "Hay un mesaje nuevo",
      notSelected: "Seleccione un mensaje",
      oneSelectMessages: "Mensaje seleccionado",
      oneSelectThreads: "Seleccionar conversación",
      readNotificationIgnore: "Ignorar",
      readNotificationLabel: "{{Sender}} ha solicitado confirmación de lectura para el mesaje.",
      readNotificationLabelSimple: "Confirmación de lectura del mensaje solicitada.",
      readNotificationResponse:
        "<P>Message<BR><BR>&nbsp;&nbsp;&nbsp; A:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Subject:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Sent:&nbsp; %SENDDATE%<BR><BR>it was read the day %READDATE%.</P>",
      readNotificationSend: "Confirmar",
      readNotificationSubjectPrefix: "Leer:",
      replyTo: "Responder a:",
      sender: "Remitente:",
      sentSelectedInfo: "Seleccionar un mensaje de la lista y ver a quien le escribiste.",
      showAllAttachments: "Mostrar otros",
      showMoreThread: "Mostrar más",
      selectMessages: "Los mensajes seleccionados",
      selectThreads: "Conversación seleccionada",
      spamNotSelected: "Mantener la carpeta en orden",
      spamSelectedInfo: "Comprobar mensajes y marcar spam como leído.",
      subject: "Asunto:",
      to: "Para:",
      trashNotSelected: "Compruebar la papelera",
      trashSelectedInfo: "Puede recuperar los mensajes de la papelera durante 30 días.",
      updating: "Edición en curso",
      yesterday: "Ayer",
      wrote: "escribió:"
    },
    messagesList: {
      allMessagesLoaded: "Todos los mensajes se han subido",
      ctaSpamInfo: "Marcar todo como leido",
      ctaTrashInfo: "Papelera vacía",
      folderEmpty: "Esta carpeta está vacía",
      folderInfo: "Iniciar una conversación o mover un mensaje.",
      foundMessages: "Encontrado",
      moveMessage: "Mover el mensaje",
      moveMessages: "Mover {{NumMessages}} mensajes",
      notFound: "No se han encontrado mensajes",
      notFoundInfo:
        "La búsqueda no ha dado ningún resultado, inténtalo de nuevo utilizando diferentes criterios",
      pullDownToRefresh: "Arrastra para actualizar",
      releaseToRefresh: "Suelta para actualizar",
      searchInfo: "Mensajes encontrados",
      selectMessages: "Seleccionados",
      showMoreThread: "Mostrar más",
      spamEmpty: "¡Todo el spam se ha escapado!",
      spamInfo: "Los mensajes de spam se eliminan automáticamente después de 30 días",
      trashEmpty: "La papelera está vacía",
      trashInfo: "Los mensajes de la papelera se eliminan automáticamente después de 30 días",
      totalMessages: "Mensajes"
    },
    newMessage: {
      addAttach: "Añadir un adjunto",
      addImgeDsc:
        "Puede insertar imágenes directamente en línea, en el cuerpo del mensaje o adjuntarlas al mensaje.",
      addImgeTitle: "Insertar imágenes",
      advanced: "Avanzado",
      bcc: "CCO",
      cc: "Cc",
      cancelSend: "Envío de correo cancelado con éxito",
      delete: "Eliminar",
      deleted: "Mensaje eliminado correctamente",
      deleteInProgress: "Eliminación de mensaje en curso",
      discardChanges: "Descartar los cambios",
      fontSize: {
        smaller: "Smaller",
        small: "Small",
        normal: "Normal",
        medium: "Medium",
        big: "Big",
        bigger: "Bigger"
      },
      from: "De",
      hide: "Esconder",
      invalidMails:
        "Dirección de correo electrónico no válida, comprueba las direcciones y vuelve a intentarlo",
      moreAttachments: "Otros",
      noDraftChanged: "No hay cambios para guardar",
      object: "Asunto",
      saveDraftDsc:
        "El mensaje no se ha enviado y contiene cambios no guardados. Puedes guardarlo como borrador y reanudar la escritura posteriormente",
      saveDraftTitle: "¿Guardar el mensaje como borrador?",
      savedDraft: "¡Borrador guardado!",
      savingDraft: "Guardar en curso",
      sendInProgress: "Enviar mensaje en curso",
      sent: "Mensaje enviado con éxito",
      sizeExceeded:
        "Adjuntos demasiado grandes. El tamaño máximo de envío es de {{messageMaxSize}} MB.",
      to: "Para",
      toMore: "Otros",
      windowSubject: "Nuevo mensaje"
    },
    reportBugMessage:
      "Si no ves el mensaje correctamente, puedes ayudarnos a solucionar el problema y mejorar el servicio. Es posible que necesitemos contactarte, por favor, instera tu número de teléfono y describe el problema al que te estás enfrentando.<br/><br/>Procediendo con tus comentarios, tu mensaje podrá ser visto por nuestro equipo de desarrollo con tal de mejorar el servicio.<br/><b>La información NO será compartida con terceros y tus datos serán procesados de acuerdo a la legislación sobre privacidad vigente.</b>.",
    reportBugTitle: "Denunciar mensaje",
    searchMail: {
      attachments: "Archivos adjuntos",
      cleanFilters: "Borrar los filtros",
      marked: "Destacado"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Crear etiqueta"
      },
      createFolder: {
        desc: "",
        title: "Crear carpeta"
      },
      modifyFolder: {
        desc: "",
        title: "Editar carpeta"
      },
      restoreFolder: {
        desc: "",
        title: "Restaurar carpeta"
      },
      folder: "Carpetas",
      folders: "Carpetas personales",
      labels: "Etiquetas",
      newFoderName: "Nuevo nombre de carpeta",
      occupiedSpace: "Espacio usado",
      sharedFolders: "Carpetas compartidas",
      unlimitedSpace: "Espacio ilimitado"
    },
    thanksMailLabel: "¡OK, gracias!",
    tooManyCompositionsDsc:
      "Has alcanzado el máximo numero de mensajes nuevos que puedes redactar al mismo tiempo. Completa al menos uno de los mensajes que estás escribiendo para empezar a componer uno nuevo.",
    tooManyCompositionsTitle: "¡Termina el escrito!"
  },

  mailboxes: {
    drafts: "Borradores",
    inbox: "Bandeja de entrada",
    more: "Más",
    sent: "Enviado el",
    shared: "Carpetas compartidas",
    spam: "Spam",
    trash: "Papelera"
  },

  navbar: {
    addressbook: "Contactos",
    calendar: "Calendario",
    chat: "Chat",
    mail: "Correo",
    meeting: "Reunión",
    news: "Noticias"
  },

  newsTags: {
    news: "Noticias",
    suggestions: "Consejos",
    fix: "Reparar",
    comingSoon: "Próximamente",
    beta: "Beta"
  },

  pageLogout: {
    title: "Salida en curso",
    desc: ""
  },

  pageIndexing: {
    title: "Tu cuenta está siendo indexada",
    desc: "La operación puede tardar algún tiempo, por favor, espera."
  },

  pageNotFound: {
    pageNotFound: "Algo salió mal",
    pageNotFoundInfo: "La página que estás buscando no está disponible. ¡No pierdas la esperanza!"
  },

  powered: "Impulsado por Qboxmail",
  poweredCollaborationWith: "en colaboración con ",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Confirmación de la entrega de mensajes",
      askReadConfirmation: "Confirmación de lectura del mensaje",
      defaultView: "Diseño predeterminado",
      newEventType: "Tipo del nuevo evento",
      newEventFreeBusy: "Mostrar la disponibilidad de nuevos eventos"
    }
  },

  shareOptions: {
    delete: "Eliminación",
    read: "Leyendo",
    update: "Editando",
    write: "Escribiendo"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Permitir insertar eventos en este calendario",
    canEraseObjects: "Permitir eliminar eventos en este calendario"
  },

  shortcuts: {
    addressbook: {
      newContact: "Nuevo contacto",
      editContact: "Editar contacto",
      deleteContact: "Borrar contacto"
    },
    calendar: {
      newEvent: "Nuevo evento",
      editEvent: "Editar evento",
      deleteEvent: "Borrar evento",
      moveRangeNext: "Ver siguiente intervalo",
      moveRangePrevious: "Ver intervalo previo"
    },
    general: {
      switchModuls: "Switch modules",
      refresh: "Actualizar página",
      tabForward: "Avanzar entre los elementos en los que se puede hacer clic",
      tabBack: "Retroceder entre los elementos en los que se puede hacer clic",
      closeModal: "Cerrar módulo",
      openSettings: "Configuración abierta",
      copy: "Copiar elemento seleccionado",
      past: "Pegar elemento seleccionado",
      cut: "Cortar el elemento seleccionado"
    },
    mail: {
      confirm: "Confirmar",
      deleteMessage: "Borrar mensaje",
      deleteSelectMessage: "Eliminar mensajes seleccionados",
      editAsNew: "Editar como nuevo",
      empityTrash: "Papelera vacía",
      expandThread: "Expand thread",
      extendSelectDown: "Extender selección hacia abajo en la lista de mensajes",
      extendSelectDown: "Extender selección hacia abajo en la lista de mensajes",
      extendSelectUp: "Extender selección hacia arriba en la lista de mensajes",
      forward: "Reenviar",
      markRead: "Marcar como leído / no leído",
      moveDown: "Mover hacia abajo en la lista de mensajes",
      moveUp: "Mover hacia arriba en la lista de mensajes",
      newMessage: "Nuevo mensaje",
      openSelectMessage: "Abrir mensaje seleccionado",
      printMessage: "Imprimir mensaje",
      reduceThread: "Reducir el hilo",
      reply: "Responder",
      replyAll: "Responder a todos",
      thanks: "Gracias"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Guardado automático"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Agenda",
          placeholderList: " ",
          title: "Agenda personal"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Agenda subscrita",
          placeholderList: " ",
          title: "Agenda compartida"
        },
        title: "Agendas"
      },
      title: "Contactos",
      view: {
        desc: "",
        title: "Ver"
      }
    },
    calendar: {
      activities: {title: "Actividades"},
      events: {title: "Eventos", desc: "", alertTitle: "Alerts", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Gestión de invitaciones",
        msgFromHeader: "Permitir invitaciones unicamente provenientes de",
        placeholderList: " "
      },
      labels: {title: "Etiquetas", desc: ""},
      labelsTask: {title: "Etiquetas de tareas", desc: ""},
      management: {
        title: "Calendarios",
        holidayCalendars: {
          title: "Calendarios vacaciones",
          desc: "",
          headerItem: "Calendarios",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Calendarios personales",
          desc: "",
          headerItem: "Calendarios",
          placeholderList: " "
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Calendarios suscritos",
          placeholderList: " ",
          title: "Calendarios compartidos"
        },
        includeInFreeBusy: "Incluir en libre/ocupado",
        showActivities: "Mostrar actividades",
        showAlarms: "Mostrar alarmas",
        syncWithActivesync: "Sincronizar con ActiveSync",
        sendMeMailOnMyEdit: "Envíame un correo electrónico cuando modifique el calendario",
        sendMeMailOnOtherEdit:
          "Envíame un correo electrónico cuando alguien modifique el calendario",
        sendMailOnEditTo: "Cuando modifique envía un correo electrónico a"
      },
      view: {
        showBusyOutsideWorkingHours: "Mostrar como ocupado fuera de horário laboral",
        showWeekNumbers: "Mostrar número de la semana",
        title: "Ver"
      },
      title: "Calendario"
    },
    chat: {
      title: "Chat",
      view: {
        title: "Ver"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "Correo electrónico del usuario",
        generalSettings: {
          desc: "",
          title: "Configuración general"
        },
        personalInfo: {
          desc: "",
          title: "Informacion personal"
        },
        title: "Perfil"
      },
      security: {
        changepassword: {
          info: "Contraseña de al menos 8 caracteres alfanuméricos",
          desc: "",
          resetpassword: "Crear una contraseña nueva",
          resetpassworddisabled: "Cambio de contraseña desactivado en esta cuenta",
          resetpasswordexpired: "Su contraseña ha caducado, establecer una nueva",
          title: "Cambiar la contraseña"
        },
        otp: {
          cantDisableOtp: "Autenticación de dos factores no se puede desactivar en esta cuenta",
          desc: "",
          disableOtp: "Desactivar autentificación en dos factores",
          hideParams: "Ocultar parámetros",
          instructionDisableOtp:
            "Ingrese el código generado por la aplicación en el siguiente formulario para deshabilitar la autenticación de dos factores para esta cuenta.",
          instructionsSetOtp:
            "Escanea el Código QR con la aplicación e introduzca el código generado en el formulario. Alternativamente, introduzca manualmente los parámetros para la activación.",
          labelEnterCode: "Introducir código generado por la aplicación",
          paramAccount: "Cuenta",
          paramCode: "Código",
          paramType: "Basada en el tiempo",
          paramTypeVal: "Activa",
          params: "Introduce el código manualmente",
          paramsInstruction: "Introduzca los siguientes parámetros en la aplicación",
          setOtp: "Habilitar la autenticación de dos factores",
          title: "Establecer OTP",
          titleDisableOtp: "Desactivar OTP",
          titleSetOtp: "Habilitar OTP"
        },
        sessions: {
          browser: "Navegador:",
          currentSession: "Actual",
          device: "Dispositivo:",
          desc: "",
          ip: "IP:",
          os: "OS:",
          removeAll: "Eliminar todo",
          removeSession: "Eliminar",
          startdate: "Comenzar:",
          title: "Sesiones abiertas"
        },
        title: "Seguridad"
      },
      title: "General"
    },
    mail: {
      accounts: {desc: "", title: "Cuentas"},
      antispam: {
        blacklist: {
          desc: "Agregar dirección o dominio a la lista (es. 'nombre@dominio' o '*@dominio')",
          msgNoHeader: "No permitir que los mensajes de",
          title: "Lista negra"
        },
        deleteRuleDsc: "Eliminar conjunto de normas para <code>{{ruleEmail}}</code>.",
        deleteRuleTitle: "Eliminar la norma antispam",
        placeholderList: " ",
        title: "Antispam",
        whitelist: {
          desc: "Agregar dirección o dominio a la lista (es. 'Nombre @ dominio' o '* @ dominio')",
          msgFromHeader: "Permitir mensajes de",
          msgToHeader: "Permitir que los mensajes",
          title: "Lista blanca"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Mensaje",
        placholdeSubject: "Asunto",
        placholderTextarea: "Mensaje de texto",
        title: "Respuesta automática"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Cancel send email"
        },
        confirmations: {
          desc: "",
          title: "Lectura / confirmación de entrega"
        },
        title: "Composición"
      },
      delegations: {desc: "", title: "Delegación"},
      filters: {
        actionsTitle: "Acciones",
        activeFiltersTitle: "Filtros activos",
        desc: "Administrar los filtros en mensajes entrantes",
        headerItem: "Filtros activos",
        parametersTitle: "Parámetros",
        placeholderList: {
          parameter: "Añadir parámetro al filtro",
          action: "Añadir acción al filtro"
        },

        title: "Filtros"
      },
      folders: {
        deleteFolderDsc:
          "La eliminación de la carpeta  <code>{{folderName}}</code> eliminará todos los mensajes contenidos en ella.<br/><b>La operación no es reversible.</b>.",
        deleteFoldersTitle: "Eliminar carpetas",
        desc: "Gestionar y compartir sus carpetas personales",
        descShared: "Administrar las carpetas compartidas",
        headerItem: "Carpetas",
        headerMsg: "Mensajes",
        headerUnread: "No leído",
        modifyFolder: "Editar carpeta",
        newFolder: "Añadir nueva carpeta",
        rootFolder: "- Raíz -",
        subscribe: "Suscribir",
        title: "Carpetas",
        titleShared: "Carpetas compartidas"
      },
      forward: {
        description: "Añadir máximo de {{max}} destinatarios",
        headerItem: "Destinatarios",
        placeholderList: " ",
        title: "Reenviar",
        titleSection: "Reenviar mensajes"
      },
      labels: {
        desc: "",
        headerItem: "Etiquetas",
        deleteLabelTitle: "Eliminar etiqueta",
        deleteLabelTitleDsc: "Al eliminar una etiqueta, será borrada de todos los mensajes.",
        placeholderList: " ",
        title: "Etiquetas"
      },
      reading: {
        layout: {
          desc: "",
          title: "Diseño"
        },
        readingSettings: {
          desc: "",
          title: "Ajustes de lectura"
        },
        title: "Ver"
      },
      signature: {
        title: "Firma",
        default: "Default",
        dropFiles: "Colocar aquí la imagen a añadir",
        management: {
          title: "Signature management"
        },
        onlyNewMessages: "No agregue firma en las respuestas",
        placeholderList: " ",
        senderBcc: "Añadir remitente en CCO",
        setAsDefault: "Set as default sender",
        signatures: {
          desc:
            "Customize your email by choosing the name displayed, who receives the message, and setting a signature to add on all your messages.<br/>You can customize the display name and signature for all identities (alias groups) associated with your account.",
          headerItemSignature: "Nombre a mostrar",
          headerItemIdentity: "Identidad",
          title: "Firmas e identidades"
        }
      },
      title: "Correo"
    },
    meeting: {
      title: "Reunión",
      view: {
        desc: "",
        title: "Ver"
      }
    },
    news: {
      readMore: "Leer mas",
      title: "Noticias Webmail"
    },
    shortcuts: {
      title: "Atajos de teclado"
    },

    title: "Ajustes",
    unsaved: "Hay cambios sin guardar"
  },

  toast: {
    error: {
      alreadyPresent: "Advertencia, el recurso ya existe",
      avatarTooBig: "Advertencia, el tamaño del avatar es demasiado grande",
      compact: "Error al compactar carpeta",
      connectionUnavailable: "No hay conexión disponible",
      delete: "Error en la supresión",
      deleteDraft: "Error eliminando el borrador",
      enqueued: "Actualizando los ajustes, vuelva a intentarlo",
      fetchingAttachments: "Error al buscar archivo adjunto",
      filterActionsMissing: "Acciones que no están presentes",
      filterConditionsMissing: "Los parámetros no presentes",
      filterNameMissing: "Falta el nombre del filtro",
      folderCreation: "Error al crear nueva carpeta",
      folderCreationCharNotValid:
        "Error al crear / cambiar el nombre de la carpeta, caracter no válido (/)",
      folderCreationNoName: "Error al crear la carpeta, necesita especificar un nombre válido",
      folderDelete: "Error al eliminar la carpeta",
      folderNotPresent: "Carpeta no presente",
      folderRename: "Error al cambiar el nombre de la carpeta, ya se encuentra presente",
      folderSubscribe: "Error al suscribirse a la carpeta",
      generic: "Algo salió mal",
      maxMembers: "Alerta, número máximo de miembros alcanzado",
      messageCopy: "Error copiando mensajes",
      messageMove: "Error al mover mensajes",
      messageNotFound: "Mensaje no encontrado",
      messageRemove: "Error al borrar mensajes",
      missingParameters: "Faltan parámetros",
      notifications: "Error al obtener notificaciones",
      notPermitted: "Operación no permitida",
      notValid: "Advertencia, valor no válido",
      onlyOneHomeAddress: "Sólo se puede insertar una dirección de casa",
      onlyOneWorkAddress: "Sólo se puede insertar una dirección de trabajo",
      save: "Error al guardar",
      saveAcl: "Error al guardar los permisos de la carpeta",
      savingDraft: "Guardado de borrador en progreso, por favor, inténtalo de nuevo al acabar",
      sendingMessage: "Error en la entrega de mensajes",
      serverDisconnection:
        "El servidor se está actualizando, por favor, inténtalo de nuevo más tarde",
      sessionExpired: "Ha expirado la sesión",
      smtpBlockedByAntispam: "Mensaje no enviado: bloqueado por antispam",
      smtpBlockedByAntivirus: "Message not sent: blocked by antivirus",
      smtpDisabled: "Mensaje no enviado: SMTP deshabilitado en esta cuenta",
      smtpQuotaExceeded: "Mensaje no enviado: se ha sobrepasados la cuota de mensajes enviados ",
      requestLimit:
        "Se ha excedido el límite de solicitud de números, inténtalo de nuevo más tarde",
      requestError: "Error durante la solicitud, inténtalo de nuevo más tarde",
      updateFolders: "Se ha producido un error en la actualización de carpetas",
      updateMessageEvent: "Error al obtener la actualización del estado del evento",
      updatingParentFlags: "Error al actualizar las alertas de mesaje"
    },
    info: {
      newVersionAvailable: {
        desc: "Esta operación es segura, no se modificarán los ajustes",
        title:
          "Las nuevas características están disponibles, vuelve a cargar la página para actualizar el correo web a la última versión"
      },
      serverReconnected: "Webmail está en línea de nuevo",
      sourceMail: "Fuente del correo copiada al portapapeles"
    },
    progress: {
      newFolder: "Creacion de nueva carpeta en curso",
      operationInProgress: "Operación en curso"
    },
    success: {
      compact: "Carpeta compactada con éxito",
      copiedLink: "Link copiado al portapapeles",
      copiedMail: "Mensaje copiado con éxito",
      copiedMailAddress: "Dirección de correo electrónico copiado al portapapeles",
      createFolder: "Carpeta creada correctamente",
      delete: "Eliminado con éxito",
      disabledOtp: "OTP desactivado con éxito",
      enabledOtp: "OTP activado con éxito",
      movedMail: "Mensaje trasladado con éxito",
      otpDisabled: "OTP deshabilitado con éxito",
      otpEnable: "OTP activado con éxito",
      save: "Guardado con éxito",
      saveAcl: "Permisos guardados correctamente",
      saveDraft: "Borrador guardado correctamente",
      saveFolder: "Carpeta actualizada correctamente",
      sentMail: "Mensaje enviado con éxito",
      subscribeFolder: "Suscripción actualizada correctamente",
      updateMessageEvent: "Estado del evento actualizado"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "Todo el dia",
      autoSaveContact:
        "Guardar automáticamente todos los destinatarios que no están presentes en sus agendas",
      enableAutorespopnd: "Habilitar respuesta automática",
      enableAutorespopndInterval: "Habilitar el rango de fechas",
      keepCopy: "Mantener copia del mensaje a nivel local",
      showPriority: "Mostrar prioridad del mensaje recibido",
      showThreads: "Mostrar mensajes como hilo",
      denyInvites: "Evitar invitaciones a citas",
      enableDateSearch: "Habilitar búsqueda de fecha",
      enableFullBody: "Habilitar la búsqueda en el cuerpo de texto",
      subscribeFolder: "Subscribirse a carpeta"
    }
  },

  wizard: {
    layout: {
      desc: "Personalizar el Webmail eligiendo los parámetros de vista que prefieras",
      title: "Ver opciones"
    },
    wizardData: {
      desc: "Personalizar su perfil añadiendo una imagen y completando los datos que faltan",
      title: "Perfil completo"
    }
  }
};
