/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Vald kontakt",
      notSelected: "Välj en kontakt",
      sections: {
        cell: "Mobil",
        home: "Hem",
        homeFax: "Fax hem",
        work: "Arbete",
        workFax: "Fax arbete"
      },
      selectContacts: "Valda kontakter",
      selectedInfo: "Välj en kontakt från listan för att se detaljerna."
    },
    contactsList: {
      emptyContacts: "Denna adressbok är tom",
      emptyContactsInfo: "Lägg till en kontakt i adressboken",
      foundContacts: "Hittades",
      notFound: "Inga kontakter hittade",
      notFoundInfo: "Sökningen gav inga resultat, försök igen med andra kriterier.",
      totalContacts: "Kontakter"
    },
    contextualMenuLabels: {
      address: "Adress",
      birthday: "Födelsedag",
      cancel: "Avbryt",
      delete: "Radera",
      downloadVCard: "Ladda ner VCard",
      email: "E-post",
      newContact: "Ny kontakt",
      newList: "Ny lista",
      note: "Anteckning",
      phone: "Telefon",
      shareVCard: "Dela",
      showVCard: "Visa VCard",
      sendAsAttachment: "Skicka via e-post",
      sendMail: "Skicka e-post",
      url: "URL"
    },
    deleteContactDsc:
      "Genom att ta bort kontakten <code>{{contact}}</code> raderas all relaterad information också. <br/><b>Åtgärden är inte reversibel</b>.",
    deleteContactsDsc:
      "Genom att radera utvalda kontakter kommer alla deras information att raderas också. <br/><b>Åtgärden är inte reversibel</b>.",
    deleteContactTitle: "Radera kontakt",
    deleteContactDscList:
      "Genom att ta bort vald kontakt kommer alla information att raderas också. <br/><b>Åtgärden är inte reversibel</b>.",
    deleteContactsTitle: "Radera kontakter",

    deleteListDsc:
      "Genom att radera listan <code>{{contact}}</code> raderas all relaterad information också. <br/><b>Åtgärden är inte reversibel</b>.",
    deleteListTitle: "Radera lista",

    editContactModal: {
      title: "Redigera kontakt"
    },
    editListModal: {
      title: "Redigera lista"
    },
    exportAddressBookModal: {
      desc:
        "Du exporterar adressboken: <code>{{addressbookExport}}</code><br/> Välj format för att exportera alla närvarande kontakter",
      title: "Exportera adressbok"
    },
    import: {
      contactsInvalid: "Inte giltig:",
      contactsNr: "Totalt antal kontakter som ska importeras:",
      contactsValid: "Giltig:",
      desc:
        "Du importerar kontakter till adressboken: <code>{{addressbookImport}}</code><br/>Du kan importera kontakter från andra e-postprogram med ett vCard (<strong>.vcf</strong>) eller <strong>.ldif</strong> filformat.\u000AExempel kan du exportera kontakter från Gmail eller från Outlook och importera dem till Qboxmail.\u000AImporten kommer inte att ersätta någon av de befintliga kontakterna.",
      dropFiles: "Ladda upp eller släpp filen som ska importeras, här",
      error: "Kontakter kunde inte importeras",
      importing:
        "Vi importerar <b>{{contactsNumber}}</b> händelsen till kalendern: <code>{{addressbookImport}}</code>",
      loading: "Läser in...",
      success: "Importering framgångsrik!",
      title: "Importera kontakter",
      uploadedFile: "Uppladdad",
      uploadingFile: "Laddar upp"
    },
    list: {
      desc:
        "Det är möjligt att lägga till upp till <b>150 medlemmar</b> till listan genom att välja från kontakterna i adressboken <code>{{addressbookName}}</code>.",
      members: "Lista medlemmar"
    },
    messages: {
      newContactAddressBookBody:
        "Du kan inte lägga till en kontakt i <b>Alla kontakter</b> eller i <b>Företagskontakter</b>. <br/>Välj en personlig eller delad adressbok.",
      newContactAddressBookTitle: "Välj adressbok",
      resetListBody:
        "Listans medlemmar är relaterade till den valda adressboken. Denna ändring återställer medlemslistan. Fortsätt?",
      resetListTitle: "Radering av medlemmar"
    },
    newAddressBookModal: {
      desc: "",
      title: "Lägg till / Redigera adressbok"
    },
    newContactModal: {
      title: "Lägg till kontakt"
    },
    newListModal: {
      title: "Lägg till lista"
    },
    shareModal: {
      desc:
        "Du delar adressboken: <code>{{addressbookShare}}</code><br/>Välj de kontakter du vill dela adressboken med och låta dem hantera.",
      title: "Dela adressbok"
    },
    sidebar: {
      allContacts: "Alla kontakter",
      deleteAddressbookDsc:
        "Genom att ta bort adressboken <code>{{addressbook}}</code> kommer alla delaktiga kontakter att raderas. <br/> <b> Åtgärden är inte reversibel </b>.",
      deleteAddressbookTitle: "Radera adressbok",
      globalAddressbook: "Företagskontakter",
      personalAddressbook: "Personliga kontakter",
      sharedAddressbooks: "Delade adressböcker",
      userAddressbooks: "Användarens adressbok"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "Det finns inga adressböcker som kan prenumereras",
      title: "Prenumerera på adressboken"
    }
  },

  app: {
    addressbook: "Adressbok",
    calendar: "Kalender",
    chat: "Chatt",
    meeting: "Möte",
    of: "av",
    webmail: "Webmail"
  },

  avatarEditorModal: {
    photoTitle: "Ta en bild",
    title: "Redigera avatar",
    zoom: "Zooma -/+"
  },

  buttons: {
    accept: "Acceptera",
    add: "Lägg till",
    addAction: "Lägg till åtgärd",
    addAddressbook: "Adressbok",
    addAsAttachment: "Bifoga",
    addCalendar: "Kalender",
    addField: "Lägg till fält",
    addHolidayCalendar: "Holiday calendar",
    addLabel: "Lägg till etikett",
    addInline: "Lägg till i rad",
    addParameter: "Lägg till parameter",
    addSignature: "Lägg till signatur",
    addToList: "Lägg till i lista",
    apply: "Tillämpa",
    backTo: "Tillbaka",
    backAddressbook: "Stäng",
    cancel: "Avbryt",
    changeImage: "Ändra bild",
    changePassword: "Ändra lösenord",
    checkFreeBusy: "Kontrollera tillgänglighet",
    close: "Stäng",
    createFolder: "Ny mapp",
    comeBackLater: "Kom tillbaka senare",
    confirm: "Bekräfta",
    copyLink: "Kopiera länk",
    decline: "Nekar",
    delete: "Radera",
    deleteDraft: "Radera utkast",
    details: "Detaljer",
    disabled: "Inaktivera",
    disableOTP: "Inaktivera OTP",
    download: "Ladda ner",
    editLabel: "Redigera etikett",
    editSignature: "Redigera signatur",
    empty: "Töm",
    enabled: "Aktiverad",
    event: "Händelse",
    export: "Exportera",
    goToMail: "Återgå till e-post",
    goToOldWebmail: "Go to old webmail",
    import: "Importera",
    hide: "Dölj",
    label: "Etikett",
    leftRotate: "Vänster rotera 90 grader",
    loadImage: "Ladda upp bild",
    logout: "Logga ut",
    makePhoto: "Ta en bild",
    markRead: "Läs",
    markSeen: "Mark",
    markUnread: "Oläs",
    message: "Meddelande",
    modify: "Redigera",
    new: "Ny",
    newContact: "Ny kontakt",
    newEvent: "Ny händelse",
    newMessage: "Nytt meddelande",
    occurrence: "Förekomst",
    openMap: "Öppna i karta",
    reloadPage: "Reload page",
    removeImage: "Ta bort bild",
    rename: "Döp om",
    reorder: "Ändra ordning",
    reorderEnd: "Slutför ordning",
    report: "Rapport",
    retryImport: "Försök igen",
    save: "Spara",
    saveDraft: "Spara utkast",
    saveNextAvailability: "Spara föreslagen tillgänglighet",
    search: "Sök",
    send: "Skicka",
    series: "Serier",
    setOtp: "Ställ in OTP",
    share: "Dela",
    show: "Visa",
    showMore: "Andra alternativ",
    showUrlsAddressbook: "Visa adressboks-webbadresser",
    showUrlsCalendar: "Visa kalender-webbadresser",
    snooze: "Snooze",
    starred: "Stjärnmärk",
    subscribe: "Prenumerera",
    subscribeAddressbook: "Prenumerera på adressbok",
    subscribeCalendar: "Prenumerera på kalender",
    tentative: "Kanske",
    unsetOtp: "Inaktivera OTP",
    unsubscribe: "Avprenumerera",
    update: "Uppdatera",
    updateFilter: "Uppdatera filter",
    viewMap: "Visa karta"
  },

  buttonIcons: {
    activeSync: "Aktivera ActiveSync",
    activeSyncNot: "Inaktivera ActiveSync",
    alignCenter: "Justera centrerat",
    alignLeft: "Justera vänster",
    alignRight: "Justera höger",
    backgroundColor: "Bakgrundsfärg",
    backTo: "Tillbaka",
    bold: "Fet",
    confirm: "Bekräfta",
    close: "Stäng",
    delete: "Radera",
    download: "Ladda ner",
    downloadVCard: "Ladda ner VCard",
    edit: "Redigera",
    emoji: "Emoji",
    forward: "Framåt",
    fullscreen: "Fullskärm",
    fullscreenNot: "Fönsterläge",
    imageAdd: "Lägg till bild",
    indent: "Öka indrag",
    italic: "Kursiv",
    maintenance: "Underhåll",
    maxmized: "Förstora",
    minimized: "Förminska",
    more: "Mer",
    moveDown: "Flytta ner",
    moveUp: "Flytta upp",
    next: "Nästa",
    orderList: "Ordnad lista",
    outdent: "Minska indraget",
    pin: "Fäst",
    previous: "Tidigare",
    print: "Skriv ut",
    removeFormatting: "Ta bort formatering",
    reply: "Svara",
    replyAll: "Svara alla",
    saveDraft: "Spara utkast",
    sendDebug: "Skicka meddelande som inte kan visas",
    sendMail: "Skicka mail",
    share: "Dela",
    showSidebar: "Visa / dölja sidofältet",
    showSourceCode: "Visa källkod",
    showVCard: "Visa VCard",
    spam: "Markera som skräppost",
    spamNot: "Markera som inte skräppost",
    startChat: "Starta chatt",
    startMeeting: "Börja möte",
    strike: "Genomstrykning",
    subscribe: "Prenumerera",
    textColor: "Text färg",
    thanks: "Tack",
    underline: "Understrykning",
    unorderList: "O-ordnad lista",
    unSubscribe: "Avprenumerera"
  },

  calendar: {
    deleteEvent: "Radera händelse",
    deleteEventDsc: "Vill du fortsätta att radera den valda händelsen?",
    event: {
      deleteEventOccurrenceTitle: "Radera händelse eller serie?",
      deleteEventOccurrenceDesc:
        "Den händelse du vill ta bort har återkommande händelser. Borttagning av den enskilda händelsen kommer att göra att påverka dom andra.",
      fromDay: "från",
      maxLabelsNumberTitle: "För många etiketter!",
      maxLabelsNumberDsc:
        "Du har nått det maximala antalet etiketter som kan kopplas till en händelse. För att lägga till en ny etikett måste du först vända en av de som redan finns.",
      organizerTitle: "Skapad av",
      pendingMsg: "Händelse som väntar på bekräftelse",
      repeatEvent: "Denna händelse är en del av återkommande händelser",
      saveEventOccurrenceTitle: "Visa händelsen eller återkommande händelser?",
      saveEventOccurrenceDesc:
        "Den händelse du vill visa eller redigera har återkommande händelser. Att redigera den enskilda händelsen kommer inte att påvera dom andra.",
      toDay: "till"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "Första",
      WeekDayNumberInMonth_2: "Andra",
      WeekDayNumberInMonth_3: "Tredje",
      WeekDayNumberInMonth_4: "Fjärde",
      WeekDayNumberInMonth_5: "Femte",
      never: "Aldrig",
      after: "Efter;återkommanden",
      inDate: "På datum",
      end: "Avsluta repetition"
    },
    filterView: {
      listmonth: "Dagordning",
      day: "Dag",
      month: "Månad",
      week: "Vecka",
      workWeek: "Arbetsvecka",
      year: "År"
    },
    import: {
      contactsInvalid: "Inte giltig:",
      contactsNr: "Totalt antal händelser som ska importeras:",
      contactsValid: "Giltig:",
      dropFiles: "Ladda upp eller släpp filen som ska importeras, här",
      error: "Händelser kunde inte importeras",
      importing:
        "Vi importerar händelsen <b>{{eventsNumber}}</b> i kalendern: <code>{{calendarImport}}</code> ",
      info:
        "Du importerar händelser till kalendern: <code>{{calendarImport}}</code> <br/> Du kan importera händelser från andra e-postprogram med ett ICS-filformat. Du kan till exempel exportera händelser från Gmail eller från Outlook i ICS-format och importera dem till Qboxmail. <br/> <b>Importen kommer inte att ersätta någon av de befintliga händelserna</b>.",
      loading: "Läser in...",
      success: "Alla dina händelser har importerats framgångsrikt!",
      title: "Importera händelser",
      uploadedFile: "Uppladdad",
      uploadingFile: "Uppladdning"
    },
    newCalendarModal: {
      desc: "",
      title: "Lägg till / Redigera kalender"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Bilagor",
        placeholderList: ""
      },
      desc: "",
      dropFiles: "Ladda upp eller släpp filen som ska bifogas, här",
      freeBusy: {
        legend: {
          event: "Händelse",
          busy: "Upptagen",
          waiting: "Väntar",
          availability: "Nästa tillgänglighet"
        }
      },
      hideDetails: "Göm detaljer",
      participantsList: {
        headerItem: "Deltagare",
        answeredHeaderItem: "Besvarad",
        notAnsweredHeaderItem: "Väntar på ett svar",
        placeholderList: ""
      },
      showDetails: "Visa detaljer",
      tabAttachments: "Bilagor",
      tabDetails: "Detaljer",
      tabParticipants: "Deltagare",
      tabSeries: "Upprepningar",
      tabSettings: "Inställningar",
      title: "Skapa händelse",
      titleEdit: {
        event: "Händelse",
        series: "Serier"
      },
      titleFreebusy: "Deltagare tillgänglig/upptagen"
    },
    nextAvailability: "Nästa tillgänglighet",
    searchEvents: {
      notFound: "Inga händelser hittade",
      notFoundInfo: "Sökningen gav inga resultat, försök igen med andra kriterier."
    },
    shareModal: {
      desc:
        "Du delar kalendern: <code>{{calendarShare}}</code> <br/> Välj de kontakter du vill dela kalendern med och låta dem hantera. ",
      title: "Dela kalender"
    },
    sidebar: {
      labels: "Etiketter",
      sharedCalendars: "Delade kalendrar",
      userCalendars: "Användares kalendrar",
      deleteCalendarDsc:
        "Genom att radera kalendern <code>{{calendar}}</code> raderas alla närvarande händelser. <br/> <b>Åtgärden är inte reversibel</b>.",
      deleteCalendarTitle: "Radera kalender"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "Det finns inga kalendrar att prenumerera på",
      title: "Prenumerera på kalendern"
    },
    today: "Idag"
  },

  circularLoopBar: {
    uploading: "Laddar upp"
  },

  comingSoon: "Under utveckling...",

  contactImport: {
    title: "Söker efter kontakter",
    desc: "Vi importerar dina gamla kontakter. Det kan ta lite tid, vänta."
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Besvarad",
      "\\\\Deleted": "Raderad",
      "\\\\Draft": "Utkast",
      "\\\\Flagged": "Flaggad",
      "\\\\Seen": "Läst"
    },
    antispamFilterTypes: {
      whitelist_from: "Tillåt från",
      whitelist_to: "Tillåt till"
    },
    calendarMinutesInterval: {
      "15": "15 minuter",
      "30": "30 minuter",
      "60": "60 minuter"
    },
    calendarNewEventFreeBusy: {
      busy: "Upptagen",
      free: "Ledig",
      outofoffice: "Inte på kontoret",
      temporary: "Tillfällig"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Acceptera",
      DECLINED: "Neka",
      TENTATIVE: "Kanske"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Ordförande",
      "REQ-PARTICIPANT": "Begärt",
      "OPT-PARTICIPANT": "Ej begärt",
      "NON-PARTICIPANT": "För informationsändamål"
    },
    calendarNewEventPriority: {
      "0": "Ingen",
      "1": "Hög",
      "5": "Medium",
      "9": "Låg"
    },
    calendarNewEventTypes: {
      public: "Offentlig",
      confidential: "Konfidentiell",
      private: "Privat"
    },
    calendarStartWeekDay: {
      saturday: "Lördag",
      sunday: "Söndag",
      monday: "Måndag"
    },
    calendarView: {
      year: "År",
      month: "Månad",
      week: "Vecka",
      workWeek: "Arbetsvecka",
      day: "Dag",
      listmonth: "Lista"
    },
    calendarEventFilterBy: {allEvents: "Alla", nextEvents: "Alla nästa händelser"},
    calendarEventSearchIn: {title: "Titel/etiketter/plats", all: "Fullt innehåll"},
    cancelSendTimer: {
      "0": "Disabled",
      "5000": "5 seconds",
      "10000": "10 seconds",
      "20000": "20 seconds",
      "30000": "30 seconds"
    },
    contactNameFormat: {
      firstName: "Förnamn, Efternamn",
      lastName: "Efternamn, Förnamn"
    },
    contactsOrder: {
      firstName: "Förnamn",
      lastName: "Efternamn"
    },
    countries: {
      AD: "Andorra",
      AE: "United Arab Emirates",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "American Samoa",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Åland Islands",
      AZ: "Azerbaijan",
      BA: "Bosnia and Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgium",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius and Saba",
      BR: "Brasil",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Belarus",
      BZ: "Belize",
      CA: "Canada",
      CC: "Cocos (Keeling) Islands",
      CD: "Democratic Republic of the Congo",
      CF: "Central African Republic",
      CG: "Republic of the Congo",
      CH: "Switzerland",
      CL: "Chile",
      CM: "Cameroun",
      CN: "China",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Cabo Verde",
      CW: "Curaçao",
      CX: "Christmas Island",
      CY: "Cyprus",
      CZ: "Czech Republic",
      DE: "Germany",
      DK: "Denmark",
      DM: "Dominica",
      DO: "Dominican Republic",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spain",
      ET: "Ethiopia",
      FI: "Finland",
      FO: "Faroe Islands",
      FR: "France",
      GA: "Gabon",
      GB: "United Kingdom",
      GD: "Grenada",
      GF: "French Guiana",
      GG: "Guernsey",
      GI: "Gibraltar",
      GL: "Greenland",
      GP: "Guadeloupe",
      GQ: "Equatorial Guinea",
      GR: "Greece",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croatia",
      HT: "Haiti",
      HU: "Hungary",
      IE: "Ireland",
      IM: "Isle of Man",
      IS: "Island",
      IT: "Italy",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Japan",
      KE: "Kenya",
      KR: "South Korea",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lithuania",
      LU: "Luxembourg",
      LV: "Latvia",
      MC: "Monaco",
      MD: "Moldova",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "North Macedonia",
      MQ: "Martinique",
      MT: "Malta",
      MW: "Malawi",
      MX: "Mexico",
      MZ: "Mozambique",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Netherlands",
      NO: "Norway",
      NZ: "New Zealand",
      PA: "Panama",
      PE: "Perú",
      PH: "Philippines",
      PL: "Poland",
      PT: "Portugal",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Sweden",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovakia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "South Sudan",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turkey",
      TZ: "Tanzania",
      UA: "Ukraine",
      UG: "Uganda",
      US: "United States of America",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "South Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Be alltid om leveransbekräftelse",
      never: "Be aldrig om leveransbekräftelse"
    },
    eventAlarmPostponeTime: {
      "60": "i 1 minut",
      "300": "i 5 minuter",
      "900": "i 15 minuter",
      "1800": "i 30 minuter",
      "3600": "i 1 timme",
      "7200": "i 2 timmar",
      "86400": "i 1 dag",
      "604800": "i 1 vecka"
    },

    calendarTaskStatus: {
      "needs-action": "Needs action",
      completed: "Completed",
      "in-process": "In progress",
      cancelled: "Cancelled"
    },

    feedbackAreas: {
      addressbookSidebar: "Adressbokslista",
      contactList: "Kontaktlista",
      contactView: "Kontaktvisning",
      mailSearch: "Sök meddelanden",
      mailSidebar: "Mapplista",
      messageList: "Meddelandelista",
      messageView: "Meddelandevisning",
      newMessage: "Nytt meddelande",
      other: "Mer",
      settings: "Inställningar"
    },
    filterMail: {
      all: "Allt",
      byDate: "Datum",
      byFrom: "Från",
      bySize: "Storlek",
      bySubject: "Ämne",
      flagged: "Markerad",
      filter: "Filter",
      orderBy: "Sortera efter",
      seen: "Läst",
      unseen: "Oläst"
    },
    filtersActions: {
      discard: "Ta bort",
      addflag: "Markera meddelande som",
      setkeyword: "Lägg till etikett",
      fileinto: "Flytta meddelande i",
      fileinto_copy: "Kopiera meddelande i",
      keep: "Behåll meddelandet",
      redirect: "Omdirigera meddelande till",
      redirect_copy: "Skicka kopia till",
      reject: "Avvisa med följande meddelande",
      stop: "Sluta bearbeta filter"
    },
    filtersDateOperators: {
      gt: "Efter",
      is: "är lika med",
      lt: "Före",
      notis: "är inte lika med"
    },
    filtersEnvelopeSubtypes: {From: "Från", To: "Till"},
    filtersDateSubTypes: {date: "Datum", time: "Tid", weekday: "Veckodag"},
    filtersDateWeekdays: {
      monday: "Måndag",
      tuesday: "Tisdag",
      wednesday: "Onsdag",
      thursday: "Torsdag",
      friday: "Fredag",
      saturday: "Saturday",
      sunday: "Sunday"
    },
    filtersRules: {
      Bcc: "Dold kopia (Bcc)",
      body: "Kropp",
      Cc: "Kopia (Cc)",
      currentdate: "Incheckningsdatum",
      envelope: "Meddelande",
      From: "Skicka",
      other: "Alternativ rubrik",
      size: "Storlek",
      Subject: "Ämne",
      To: "Mottagare"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "är större än", under: "är mindre än"},
    filtersStringOperators: {
      contains: "Innehåller",
      exists: "existerar",
      is: "är lika med",
      notcontains: "inte innehålla",
      notexists: "finns inte",
      notis: "är inte lika med"
    },
    languages: {
      de: "Tysk",
      en: "Engelska",
      it: "Italienska",
      nl: "Dutch",
      sv: "Svenska",
      es: "Spanska",
      pt: "Portugisiska"
    },
    matchTypes: {
      allof: "Tillfredsställ alla parametrar",
      anyof: "Tillfredsställ minst en parameter"
    },
    newAllDayEventAlert: {
      never: "Aldrig",
      eventDate: "Händelsedatum",
      "-PT17H": "En dag innan, kl. 9",
      "-P6DT17H": "En vecka innan, kl. 9"
    },
    newEventAlert: {
      never: "Aldrig",
      eventHour: "Starttid för evenemang",
      //P[n]Y[n]M[n]DT[n]H[n]M[n]S
      "-PT5M": "5 minuter före",
      "-PT15M": "15 minuter före",
      "-PT30M": "30 minuter före",
      "-PT1H": "1 timme före",
      "-PT2H": "2 timmar innan",
      "-PT12H": "12 timmar före",
      "-P1D": "1 dag innan",
      "-P7D": "1 vecka innan"
    },
    newEventRepeatDayWeek: {
      MO: "Måndag",
      TU: "Tisdag",
      WE: "Onsdag",
      TH: "Torsdag",
      FR: "Fredag",
      SA: "Lördag",
      SU: "Söndag"
    },
    newEventRepeatFrequency: {
      NEVER: "Aldrig",
      DAILY: "Dagligen",
      WEEKLY: "Veckovis",
      MONTHLY: "Månadsvis",
      YEARLY: "Årligen"
    },
    newEventRepeatMonths: {
      "1": "Januari",
      "2": "Februari",
      "3": "Mars",
      "4": "April",
      "5": "Maj",
      "6": "Juni",
      "7": "Juli",
      "8": "Augusti",
      "9": "September",
      "10": "Oktober",
      "11": "November",
      "12": "December"
    },
    orderSelector: {
      byLastname: "Efternamn",
      byMail: "E-post",
      byName: "Förnamn",
      byOrg: "Företag",
      orderBy: "Sortera efter",
      showOnlyLists: "Visa endast listor"
    },
    readConfirmations: {
      always: "Be alltid om läsbekräftelse",
      never: "Be aldrig om läsbekräftelse"
    },
    readingConfirmation: {
      always: "Alltid",
      ask: "Fråga alltid",
      never: "Aldrig"
    },
    selectMail: {
      all: "Välj alla",
      cancel: "Avbryt",
      modify: "Redigera",
      none: "Avmarkera alla"
    },
    shareOptionsCalendar: {
      None: "Ingen",
      DAndTViewer: "Visa datum och tid",
      Viewer: "Visa alla",
      Responder: "Svar på",
      Modifier: "Ändra"
    },
    showImages: {
      always: "Alltid",
      contacts: "Kontakter och betrodda",
      never: "Aldrig"
    },
    themes: {
      dark: "Mörk",
      light: "Ljus"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Layout 3 kolumner",
      viewHalf: "Layout med förhandsvisning",
      viewFull: "Layout som lista"
    },
    viewModeCompact: {
      viewColumns: "Columns",
      viewHalf: "Preview",
      viewFull: "List"
    }
  },

  dropdownMenu: {
    activesync: "ActiveSync",
    delete: "Radera",
    download: "Ladda ner",
    edit: "Redigera",
    exportContacts: "Exportera kontakter",
    importContacts: "Importera kontakter",
    importEvents: "Importera händelser",
    remove: "Avlägsna",
    rename: "Döp om",
    share: "Dela",
    syncNo: "Synkronisera inte",
    syncYes: "Synkronisera",
    unsubscribe: "Avprenumerera"
  },

  dropdownUser: {
    feedback: "Feedback",
    logout: "Logga ut",
    settings: "Inställningar"
  },

  errors: {
    emailDomainAlreadyPresent: "Email address or domain already present",
    emptyField: "Tomt fält",
    invalidEmail: "Ogiltig e-post",
    invalidEmailDomain: "Email address or domain not valid",
    invalidPassword: "Ogiltigt lösenord",
    invalidPasswordFormat: "Ogiltigt format, ange minst 8 alfanumeriska tecken",
    invalidValue: "Ogiltigt värde",
    mandatory: "Obligatoriskt fält",
    notAllowedCharacter: "Otillåtet tecken",
    notChanged: "Oändrat fält",
    permissionDenied: "Åtkomst nekad",
    tooLong: "För långt fält"
  },

  feedback: {
    disclaimer: "Din åsikt är viktig! Hjälp oss att lösa problem och förbättra tjänsten.",
    labels: {
      feedbackAreas: "På vilket område du vill lämna feedback?",
      note: "Lämna ett meddelande (valfritt)"
    },
    msgError: {
      title: "Din feedback kunde inte skickas",
      desc:
        "Försök igen senare, det är viktigt för oss att veta vad du tycker om vår webbmail. Din feedback kommer att hjälpa oss skapa en bättre upplevelse för dig och alla våra användare."
    },
    msgSuccess: {
      title: "Tack för din feedback!",
      desc:
        "Vi uppskattar verkligen tiden du tog för att hjälpa oss förbättra vår webbmail. Din feedback kommer att hjälpa oss skapa en bättre upplevelse för dig och för alla våra användare."
    },
    privacy:
      "De insamlade uppgifterna kommer att behandlas i enlighet med den nuvarande lagstiftningen om integritet.",
    title: "Feedback"
  },

  inputs: {
    labels: {
      addAddress: "Lägg till adress",
      addDomain: "Lägg till adress eller domän",
      addLabel: "Etikettnamn",
      addMembers: "Lägg till medlemmar",
      addParticipant: "Lägg till deltagare",
      addressbookName: "Adressbok",
      addUser: "Lägg till användare",
      alert: "Varna",
      alert2: "Andra varning",
      at: "vid",
      calendar: "Kalender",
      calendarBusinessDayEndHour: "Arbetsdagens slut",
      calendarBusinessDayStartHour: "Arbetsdagens start",
      calendarColor: "Kalenderfärg",
      calendarMinutesInterval: "Kalender tidsintervall",
      calendarName: "Kalendernamn",
      calendarView: "Standardläge",
      company: "Företag",
      confidentialEvents: "Konfidentiella händelser",
      confirmNewPassword: "Bekräfta lösenord",
      contactNameFormat: "Formatera kontaktens namn",
      contactsOrder: "Kontaktordning",
      country: "Country",
      dateFormat: "Datumformat",
      defaultAddressbook: "Standard adressbok",
      defaultCalendar: "Standardkalender",
      displayName: "Visnings Namn",
      email: "E-post",
      endDate: "Slutdatum",
      event: "Händelse",
      eventDailyRepetition: "Varje;dag/ar",
      eventDailyWorkdayRepetition: "Varje veckodag",
      eventMonthlyLastDayRepetition: "Sista dagen i månaden, varje;månad/er",
      eventMonthlyLastWeekDayRepetition: "Senaste {{WeekDay}}, varje;månad/er",
      eventMonthlyRepetition: "Den {{DayNumber}} varje;månad/er",
      eventMonthlyWeekDayRepetition: "Den {{WeekDayNumber}} varje;månad/er",
      eventPartecipation: "Deltagarestatus",
      eventPriority: "Händelseprioritet",
      eventType: "Händelsetyp",
      eventWeeklyRepetition: "Varje;vecka/or, den:",
      eventYearlyLastDayRepetition: "Sista dagen i {{Month}}, varje;år",
      eventYearlyLastWeekDayRepetition: "Sista {{WeekDay}} av {{Month}}, varje;år",
      eventYearlyRepetition: "Den {{DayNumber}} av {{Month}}, varje;år",
      eventYearlyWeekDayRepetition: "Den {{WeekDayNumber}} av {{Month}} varje;år",
      exportAsVcard: "Exportera i VCard-format",
      exportAsLdiff: "Exportera i LDIF-format",
      faxHome: "Fax hem",
      faxWork: "Fax arbete",
      filter: "Filtrera efter",
      filterName: "Filternamn",
      firstName: "Förnamn",
      folderName: "Mapp namn",
      freeBusy: "Visa som",
      from: "Från",
      fromDate: "Från datum",
      home: "Hem",
      hour: "Tid",
      includeFreeBusy: "Inkludera i tillgänglig-upptagen",
      label: "Etikett",
      labelColor: "Etikettfärg",
      language: "Språk",
      lastName: "Efternamn",
      listName: "Namnlista",
      location: "Plats",
      loginAddressbook: "Initiell adressbok",
      mailAccount: "Konto",
      mobile: "Mobil",
      newAddressBook: "Ny adressbok",
      newAllDayEventAlert: "Ny Varna vid heldagshändelse",
      newCalendar: "Ny kalender",
      newEventAlert: "Varning om nya händelser",
      newForward: "Lägg till mottagare i listan",
      newPassword: "Nytt lösenord",
      nickname: "Smeknamn",
      object: "Ämne",
      oldPassword: "Nuvarande lösenord",
      parentFolder: "Sökväg",
      phone: "Telefon",
      privateEvents: "Privata händelser",
      publicEvents: "Offentliga händelser",
      readingConfirmation: "Skicka läsbekräftelse",
      recoveryEmail: "Återställnings e-post",
      renameAddressbook: "Byt namn på adressbok",
      repeatEvery: "Upprepa varje",
      reportBugNote: "Problembeskrivning",
      reportBugPhoneNumber: "Telefonnummer",
      searchAddressbookToSubscribe: "Sök en adressbok som ska prenumereras",
      searchCalendarToSubscribe: "Sök en kalender som ska prenumereras",
      searchIn: "Sök i",
      selectCalendar: "Välj kalender",
      selectIdentity: "Välj identitet",
      selectList: "Välj lista",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "Med vem du vill dela adressboken?",
      shareToCalendar: "Med vem du vill dela kalendern?",
      shareToFolder: "Vem du vill dela mappen med?",
      showImages: "Visa bilder",
      signatureName: "Visningsnamn",
      signatureText: "Signaturtext",
      startDate: "Startdatum",
      team: "Team",
      theme: "Tema",
      timeFormat: "Tidsformat",
      timezone: "Tidszon",
      title: "Titel",
      to: "Till",
      untilDate: "Fram till dagen",
      weekStart: "Veckans första dag",
      work: "Arbete"
    },
    placeholder: {
      addressbookName: "Adressboknamn",
      birthday: "Födelsedag",
      city: "Stad",
      country: "Land",
      company: "Företag",
      email: "E-post",
      firstName: "Förnamn",
      lastName: "Efternamn",
      members: "Medlemmar",
      newFolderName: "Nytt mappnamn",
      newNameAddressbook: "Nytt adressboknamn",
      newPassword: "Minst åtta alfanumeriska tecken",
      note: "Anteckning",
      phone: "Telefon",
      postCode: "Postnummer",
      region: "Område",
      role: "Roll",
      select: "Välj",
      search: "Sök i:",
      searchActivity: "Sökaktivitet",
      searchEvent: "Sök händelse",
      snooze: "Snooze",
      street: "Gata"
    },
    hint: {
      cancelSendMessage:
        "Select the time available to retract a message during sending, if you decide you don't want to send the email."
    }
  },

  labels: {
    attendees: "Deltagare",
    default: "Default",
    forDomain: "On domain",
    withAllUsers: "Med alla användare",
    calendarAdvancedSearch: "Var vill du söka?",
    clearAdvancedFilters: "Rensa filter",
    clearFields: "Rensa fält",
    holidayCalendar: "Holiday",
    searchResults: "Sökresultat"
  },

  loaderFullscreen: {
    description: "Innehållet är på väg, vänta.",
    descriptionFolder: "Dina mappar är på väg, vänta.",
    title: "Läser in"
  },

  mail: {
    confirmMailLabel: "OK, jag bekräftar!",
    contextualMenuLabels: {
      addAddressBook: "Lägg till i adressbok",
      addEvent: "Lägg till händelse",
      alarm: "Larm",
      backTo: "Tillbaka",
      cancel: "Avbryt",
      close: "Stäng",
      confirm: "Bekräfta",
      copyIn: "Kopiera till",
      copyThreadIn: "Kopiera tråd",
      delete: "Radera",
      deletePerm: "Radera permanent",
      deleteThread: "Ta bort tråd",
      deliveryNotification: "Leveransbekräftelse",
      download: "Ladda ner",
      editAsNew: "Redigera som nytt",
      emptySpam: "Mark skräppost läst",
      emptyTrash: "Töm papperskorgen",
      forward: "Vidarebefodra",
      import: "Importera e-post",
      label: "Etikett",
      labelsManagement: "Hantera etiketter",
      labelThread: "Etiketttråd",
      markAllAsRead: "Markera allt som läst",
      markAs: "Markera som",
      markThreadAs: "Markera tråd",
      manageFolders: "Hantera mappar",
      modify: "Redigera",
      more: "Mer",
      moveThreadTo: "Flytta tråd",
      moveTo: "Flytta till",
      newSubFolder: "Lägg till undermapp",
      plainText: "Vanligt textläge",
      print: "Skriv ut",
      priorityHigh: "Hög prioritet",
      readed: "Läs",
      readedNot: "Oläs",
      readingNotification: "Bekräftelse av läsning",
      rename: "Döp om",
      reply: "Svara",
      replyToAll: "Svara alla",
      restore: "Återställ",
      restoreThreadIn: "Återställ tråd",
      restoreIn: "Återställ till",
      sendAsAttachment: "Vidarebefordra som bilaga",
      showSourceCode: "Visa källkod",
      spam: "Spam",
      spamNot: "Ej spam",
      starred: "Stjärnmärk",
      starredNot: "Ta bort stjärna",
      thanks: "Tacka",
      thread: "Trådåtgärder"
    },
    deleteMailDsc:
      "Om ett lagringssystem inte är aktivt kommer meddelandet att raderas permanent.<br/><b>Åtgärden är inte reversibel</b>.",
    deleteMailTitle: "Radera meddelande",
    deleteMailsDsc:
      "Om ett lagringssystem inte är aktivt raderas meddelandena permanent. <br/><b>Åtgärden är inte reversibel <b>.",
    deleteMailsTitle: "Radera valda meddelanden",
    dropFiles: "Släpp filerna att bifoga här",
    emptyTrashDsc:
      "Om ett lagringssystem inte är aktivt vid tömning av papperskorgen, kommer alla meddelanden att tas bort <b>oåterkallerligt</b>.<br/> Efter<b>30 dagar</b> raderas meddelandena automatiskt. ",
    emptyTrashTitle: "Töm papperskorgen",
    import: {
      messagesInvalid: "Inte giltig:",
      messagesNr: "Totalt antal meddelande som ska importeras:",
      messagesValid: "Giltig:",
      desc:
        "Du importerar meddelanden i mappen: <code>{{folderImport}}</code><br/> Du kan importera meddelanden från andra e-postprogram med en eml-fil (<strong>.eml</strong>).",
      dropFiles: "Ladda upp eller släpp här filen som ska importeras",
      error: "Meddelanden kunde inte importeras",
      importing:
        "Vi importerar <b>{{messagesNumber}}</b> meddelanden i mappen: <code>{{folderImport}}</code> ",
      loading: "Läser in...",
      success: "Importera framgångsrik!",
      title: "Importera meddelanden",
      uploadedFile: "Uppladdad",
      uploadingFile: "Laddar upp"
    },
    markSpamReadDsc:
      "Markera alla meddelanden i mappen som lästa. <br/>Efter<b>30 dagar</b> raderas meddelandena automatiskt. ",
    markSpamReadTitle: "Markera meddelanden som lästa",
    maxLabelsNumberTitle: "För många etiketter!",
    maxLabelsNumberDsc:
      "Du har nått det maximala antalet etiketter som kan kopplas till ett meddelande. För att lägga till en ny etikett måste du först ta bort en av de som redan finns.",
    message: {
      add: "Lägg till",
      addToAddressBook: "Lägg till kontakt i adressboken",
      attachedMessage: "Bifogat meddelande",
      attachments: "bilagor",
      bugMsg: "Ser du inte meddelandet?",
      cc: "Cc",
      date: "Datum:",
      draftsSelectedInfo: "Välj ett meddelande från listan och fortsätt att skriva.",
      downloadAll: "Ladda ner allt",
      downloadAttachments: "Ladda ner bilagor",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Redigera deltagarestatus",
      eventActionAccept: "Acceptera",
      eventActionDecline: "Neka",
      eventActionTentative: "Preliminär",
      eventAddToCalendar: "Den här händelsen finns inte i din kalender",
      eventAttendees: "Deltagare:",
      eventAttendeeStatusAccepted: "Händelse accepterad",
      eventAttendeeStatusDeclined: "Händelse avvisad",
      eventAttendeeStatusTentative: "Preliminär för deltagande",
      eventCounterProposal: "Det här meddelandet innehåller ett motförslag för händelsen",
      eventDeleted: "Händelse raderad",
      eventInvitation: "Händelseinbjudan:",
      eventLocationEmpty: "Ej angivet",
      eventModify: "Redigera",
      eventNeedAction: "Det här meddelandet innehåller en ny händelse",
      eventNotNeedAction: "Det här meddelandet innehåller en händelse som du redan har behandlat",
      eventOutDated: "Den här händelsen har ändrats",
      eventStatusAccepted: "Accepterad",
      eventStatusDeclined: "Nekad",
      eventStatusTentative: "Preliminär",
      eventUpdate: "Uppdatera händelse",
      folderSelectedInfo: "Välj ett meddelande från listan och upptäck innehållet.",
      from: "Från:",
      hideAllAttachments: "Dölj",
      hugeMessage:
        "Content too big to display entirely, click the link below to view the message source code.",
      hugeMessageNotice: "Message truncated",
      inboxSelectedInfo: "Välj ett meddelande från listan och ta reda på vem som har skrivit dig.",
      loadImages: "Ladda ner bilder",
      localAttachments: "There are additional attachments inside the event",
      message: "meddelande",
      messageForwarded: "Vidarebefordrat meddelande",
      messages: "meddelanden",
      messagesQuota: "Used messages quota:",
      newMessageinThread: "Det finns ett nytt meddelande",
      notSelected: "Välj ett meddelande",
      oneSelectMessages: "Valt meddelande",
      oneSelectThreads: "Vald tråd",
      readNotificationIgnore: "Ignorera",
      readNotificationLabel: "{{Sender}} frågade efter läsbekräftelse för meddelandet.",
      readNotificationLabelSimple: "Frågade om läsningsbekräftelse för meddelande.",
      readNotificationResponse:
        "<P>Meddelande<BR><BR>&nbsp;&nbsp;&nbsp; A:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Ämne:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Skickat:&nbsp; %SENDDATE%<BR><BR>det lästes den %READDATE%.</P>",
      readNotificationSend: "Bekräfta",
      readNotificationSubjectPrefix: "Läs:",
      replyTo: "Svara till:",
      sender: "Sender:",
      sentSelectedInfo: "Välj ett meddelande från listan och se vem du skrev till.",
      showAllAttachments: "Visa andra",
      showMoreThread: "Visa mer",
      selectMessages: "Valda meddelanden",
      selectThreads: "Vald tråd",
      spamNotSelected: "Håll mappen ren",
      spamSelectedInfo: "Kontrollera meddelanden och markera skräppost som läst.",
      subject: "Ämne:",
      to: "Till:",
      trashNotSelected: "Kontrollera papperskorgen",
      trashSelectedInfo: "Du kan återställa meddelanden från papperskorgen inom 30 dagar.",
      updating: "Redigering pågår",
      yesterday: "Igår",
      wrote: "skrev:"
    },
    messagesList: {
      allMessagesLoaded: "Alla meddelanden har laddats upp",
      ctaSpamInfo: "Markera allt som läst",
      ctaTrashInfo: "Töm papperskorgen",
      folderEmpty: "Denna mapp är tom",
      folderInfo: "Starta en konversation eller flytta ett meddelande.",
      foundMessages: "Hittades",
      moveMessage: "Flytta meddelande",
      moveMessages: "Flytta {{NumMessages}} meddelanden",
      notFound: "Inga meddelanden hittades",
      notFoundInfo: "Sökningen gav inte resultat, försök igen med olika kriterier.",
      pullDownToRefresh: "Drag för att uppdatera",
      releaseToRefresh: "Släpp för att uppdatera",
      searchInfo: "meddelanden hittade",
      selectMessages: "Vald",
      showMoreThread: "Visa mer",
      spamEmpty: "All skräppost har sprungit bort!",
      spamInfo: "Meddelanden i skräppost raderas automatiskt efter 30 dagar.",
      trashEmpty: "Papperskorgen är tom",
      trashInfo: "Meddelanden i papperskorgen raderas automatiskt efter 30 dagar.",
      totalMessages: "Meddelanden"
    },
    newMessage: {
      addAttach: "Bifoga fil",
      addImgeDsc:
        "Du kan infoga bilder direkt inline, i meddelandets kropp eller bifoga dem till meddelandet.",
      addImgeTitle: "Infoga bilder",
      advanced: "Avancerad",
      bcc: "Bcc",
      cc: "Cc",
      cancelSend: "Sending mail successfully canceled",
      delete: "Radera",
      deleted: "Meddelandet har raderats framgångsrikt",
      deleteInProgress: "Radering av meddelandet pågår",
      discardChanges: "Discard changes",
      fontSize: {
        smaller: "Smaller",
        small: "Small",
        normal: "Normal",
        medium: "Medium",
        big: "Big",
        bigger: "Bigger"
      },
      from: "Från",
      hide: "Dölj",
      invalidMails: "Ogiltig e-postadress finns, kolla adresser och försök igen",
      moreAttachments: "Övriga",
      noDraftChanged: "Ingen ändring att spara",
      object: "Ämne",
      saveDraftDsc:
        "Meddelandet har inte skickats och innehåller ändringar som inte är sparade. Du kan spara det som ett utkast och fortsätta skriva senare.",
      saveDraftTitle: "Spara meddelande som utkast?",
      savedDraft: "Utkast sparat!",
      savingDraft: "Sparar utkast...",
      sendInProgress: "Skickar meddelande...",
      sent: "Meddelandet skickades utan problem",
      sizeExceeded: "Bilagor för stora. Den maximala sändningsstorleken är {{messageMaxSize}} MB.",
      to: "Till",
      toMore: "Övriga",
      windowSubject: "Nytt meddelande"
    },
    reportBugMessage:
      "Om du inte ser meddelandet korrekt kan du hjälpa oss att lösa problemet och förbättra tjänsten. Vi kan behöva kontakta dig, sätt in ditt telefonnummer och beskriv det problem du står inför. <br/><br/> När du fortsätter med feedbacken kan ditt meddelande ses av vårt utvecklingsteam för att förbättra tjänsten. <br/> <b>Informationen kommer INTE att delas med tredje parter och informationen kommer att behandlas i enlighet med gällande lagar om integritet</b>.",
    reportBugTitle: "Rapportera meddelande",
    searchMail: {
      attachments: "Bilagor",
      cleanFilters: "Rengör filter",
      marked: "Stjärnmärkt"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Skapa etikett"
      },
      createFolder: {
        desc: "",
        title: "Skapa mapp"
      },
      modifyFolder: {
        desc: "",
        title: "Redigera mapp"
      },
      restoreFolder: {
        desc: "",
        title: "Återställ mapp"
      },
      folder: "Mappar",
      folders: "Personliga mappar",
      labels: "Etiketter",
      newFoderName: "Nytt mappnamn",
      occupiedSpace: "Använt utrymme",
      sharedFolders: "Delade mappar",
      unlimitedSpace: "Obegränsat utrymme"
    },
    thanksMailLabel: "OK tack!",
    tooManyCompositionsDsc:
      "Du har nått det maximala antalet nya meddelanden som du kan skriva samtidigt. Fyll i minst ett meddelande från de du skriver för att starta en ny komposition.",
    tooManyCompositionsTitle: "Slutför skrivning!"
  },

  mailboxes: {
    drafts: "Utkast",
    inbox: "Inkorg",
    more: "Mer",
    sent: "Skickat",
    shared: "Delade mappar",
    spam: "Spam",
    trash: "Skräp"
  },

  navbar: {
    addressbook: "Kontakter",
    calendar: "Kalender",
    chat: "Chatt",
    mail: "E-Post",
    meeting: "Möte",
    news: "News"
  },

  newsTags: {
    news: "News",
    suggestions: "Tips",
    fix: "Fix",
    comingSoon: "Coming soon",
    beta: "Beta"
  },

  pageLogout: {
    title: "Loggar ut...",
    desc: ""
  },

  pageIndexing: {
    title: "Ditt konto indexeras",
    desc: "Åtgärden kan ta lite tid, vänta."
  },

  pageNotFound: {
    pageNotFound: "Något gick fel",
    pageNotFoundInfo: "Sidan du letar efter är inte tillgänglig, tappa inte hoppet!"
  },

  powered: "drivs av Qboxmail",
  poweredCollaborationWith: "i samarbete med ",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Bekräftelse av leveransmeddelande",
      askReadConfirmation: "Bekräftelse av läsningsmeddelande",
      defaultView: "Standardlayout",
      newEventType: "Ny händelsetyp",
      newEventFreeBusy: "Visa tillgänglighet för nya evenemang"
    }
  },

  shareOptions: {
    delete: "Radera",
    read: "Läsa",
    update: "Redigera",
    write: "Skriva/Ändra"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Tillåt att infoga händelser i den här kalendern",
    canEraseObjects: "Tillåt att ta bort händelser i den här kalendern"
  },

  shortcuts: {
    addressbook: {
      newContact: "Ny kontakt",
      editContact: "Redigera kontakt",
      deleteContact: "Radera kontakt"
    },
    calendar: {
      newEvent: "Ny händelse",
      editEvent: "Redigera händelse",
      deleteEvent: "Radera händelse",
      moveRangeNext: "Visa nästa intervall",
      moveRangePrevious: "Visa tidigare intervall"
    },
    general: {
      switchModuls: "Växla moduler",
      refresh: "Uppdatera sida",
      tabForward: "Gå framåt bland de klickbara elementen",
      tabBack: "Flytta tillbaka bland de klickbara elementen",
      closeModal: "Stäng modulär ruta",
      openSettings: "Öppna Inställningar",
      copy: "Kopiera valt objekt",
      past: "Klistra in valt objekt",
      cut: "Klipp ut valt objekt"
    },
    mail: {
      confirm: "Bekräfta",
      deleteMessage: "Radera meddelande",
      deleteSelectMessage: "Radera valda meddelanden",
      editAsNew: "Redigera som nytt",
      empityTrash: "Töm papperskorgen",
      expandThread: "Expandera tråd",
      extendSelectDown: "Utöka valet nedåt i meddelandelistan",
      extendSelectDown: "Utöka valet nedåt på meddelandelistan",
      extendSelectUp: "Utöka valet upp på meddelandelistan",
      forward: "Framåt",
      markRead: "Markera som läst/oläst",
      moveDown: "Flytta ner på meddelandelistan",
      moveUp: "Gå upp på meddelandelistan",
      newMessage: "Nytt meddelande",
      openSelectMessage: "Öppna valt meddelande",
      printMessage: "Skriv ut meddelande",
      reduceThread: "Reducera tråd",
      reply: "Svara",
      replyAll: "Svara alla",
      thanks: "Tack"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Spara Automatiskt"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Adressbok",
          placeholderList: "",
          title: "Personlig adressbok"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Prenumererad adressbok",
          placeholderList: "",
          title: "Delad adressbok"
        },
        title: "Adressböcker"
      },
      title: "Kontakter",
      view: {
        desc: "",
        title: "Visning"
      }
    },
    calendar: {
      activities: {title: "Aktiviteter"},
      events: {title: "Händelser", desc: "", alertTitle: "Larm", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Inbjudningshantering",
        msgFromHeader: "Tillåt bara inbjudningar från",
        placeholderList: ""
      },
      labels: {title: "Etiketter", desc: ""},
      labelsTask: {title: "Task labels", desc: ""},
      management: {
        title: "Kalendrar",
        holidayCalendars: {
          title: "Festlighet kalendrar",
          desc: "",
          headerItem: "Kalendrar",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Personliga kalendrar",
          desc: "",
          headerItem: "Kalendrar",
          placeholderList: ""
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Prenumererade kalendrar",
          placeholderList: "",
          title: "Delade kalendrar"
        },
        includeInFreeBusy: "Inkludera i tillgänglig/upptagen",
        showActivities: "Visa aktiviteter",
        showAlarms: "Visa larm",
        syncWithActivesync: "Synkronisera med ActiveSync",
        sendMeMailOnMyEdit: "Skicka ett e-postmeddelande när jag ändrar kalendern",
        sendMeMailOnOtherEdit: "Skicka mig ett e-postmeddelande när någon ändrar kalendern",
        sendMailOnEditTo: "När jag ändrar skicka ett e-postmeddelande till"
      },
      view: {
        showBusyOutsideWorkingHours: "Visa som upptagen utanför arbetstid",
        showWeekNumbers: "Visa veckonummer",
        title: "Visning"
      },
      title: "Kalender"
    },
    chat: {
      title: "Chatt",
      view: {
        title: "Visning"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "Användare e-post",
        generalSettings: {
          desc: "",
          title: "Allmänna Inställningar"
        },
        personalInfo: {
          desc: "",
          title: "Personlig information"
        },
        title: "Profil"
      },
      security: {
        changepassword: {
          info: "Lösenord som är minst 8 alfanumeriska tecken",
          desc: "",
          resetpassword: "Ställ in nytt lösenord",
          resetpassworddisabled: "Lösenordsändring är inaktiverad på det här kontot",
          resetpasswordexpired: "Ditt lösenord har löpt ut, ställ in ett nytt",
          title: "Ändra lösenord"
        },
        otp: {
          cantDisableOtp: "Tvåfaktorsautentisering kan inte inaktiveras på det här kontot",
          desc: "",
          disableOtp: "Inaktivera tvåfaktorautentisering",
          hideParams: "Dölj parametrar",
          instructionDisableOtp:
            "Ange koden som genereras av applikationen i formuläret nedan för att inaktivera tvåfaktorautentisering för det här kontot.",
          instructionsSetOtp:
            "Skanna QRCode med applikationen och ange koden som genereras i formuläret nedan. Eller ange parametrarna för manuell aktivering.",
          labelEnterCode: "Ange kod genererad av applikationen",
          paramAccount: "Konto",
          paramCode: "Kod",
          paramType: "Tidsbaserad",
          paramTypeVal: "Aktiv",
          params: "Ange kod manuellt",
          paramsInstruction: "Ange följande parametrar i applikationen",
          setOtp: "Aktivera tvåfaktorautentisering",
          title: "Ställ in OTP",
          titleDisableOtp: "Inaktivera OTP",
          titleSetOtp: "Aktivera OTP"
        },
        sessions: {
          browser: "Webbläsare",
          currentSession: "Aktuell",
          device: "Enhet:",
          desc: "",
          ip: "IP:",
          os: "OS",
          removeAll: "Ta bort alla",
          removeSession: "Ta bort",
          startdate: "Start:",
          title: "Öppna sessioner"
        },
        title: "Säkerhet"
      },
      title: "Allmän"
    },
    mail: {
      accounts: {desc: "", title: "Konton"},
      antispam: {
        blacklist: {
          desc: "Lägg till adress eller domän till listan (ex. 'namn@domän' or '*@domän')",
          msgNoHeader: "Tillåt inte meddelanden från",
          title: "Svartlista"
        },
        deleteRuleDsc: "Radera regel inställd till <code>{{ruleEmail}}</code>.",
        deleteRuleTitle: "Radera antispamregel",
        placeholderList: "",
        title: "Anti-Spam",
        whitelist: {
          desc: "Lägg till adress eller domän till listan (ex. 'namn@domän' or '*@domän')",
          msgFromHeader: "Tillåt meddelanden från",
          msgToHeader: "Tillåt meddelanden till",
          title: "Vitlista"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Meddelande",
        placholdeSubject: "Ämne",
        placholderTextarea: "Meddelandetext",
        title: "Autosvar"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Cancel send email"
        },
        confirmations: {
          desc: "",
          title: "Läsning/leverans bekräftelse"
        },
        title: "Komposition"
      },
      delegations: {desc: "", title: "Delegation"},
      filters: {
        actionsTitle: "Åtgärder",
        activeFiltersTitle: "Aktiva filter",
        desc: "Hantera filter på inkommande meddelanden",
        headerItem: "Aktiva filter",
        parametersTitle: "Parametrar",
        placeholderList: {
          parameter: "Lägg till parameter i filtret",
          action: "Lägg till åtgärder i filtret"
        },

        title: "Filter"
      },
      folders: {
        deleteFolderDsc:
          "Radering av mappen <code>{{folderName}}</code> kommer att radera alla meddelanden i den. <br/><b>Åtgärden är inte reversibel</b>.",
        deleteFoldersTitle: "Radera mappar",
        desc: "Hantera och dela dina personliga mappar",
        descShared: "Hantera dina delade mappar",
        headerItem: "Mappar",
        headerMsg: "Meddelanden",
        headerUnread: "Oläst",
        modifyFolder: "Redigera mapp",
        newFolder: "Lägg till ny mapp",
        rootFolder: "- Rot -",
        subscribe: "Prenumerera",
        title: "Mappar",
        titleShared: "Delade mappar"
      },
      forward: {
        description: "Lägg till max {{max}} mottagare.",
        headerItem: "Mottagare",
        placeholderList: "",
        title: "Vidarebefordra",
        titleSection: "Vidarebefordra meddelanden"
      },
      labels: {
        desc: "",
        headerItem: "Etiketter",
        deleteLabelTitle: "Radera etikett",
        deleteLabelTitleDsc:
          "Borttagning av etiketter kommer att ta bort dem från alla meddelanden.",
        placeholderList: "",
        title: "Etiketter"
      },
      reading: {
        layout: {
          desc: "",
          title: "Layout"
        },
        readingSettings: {
          desc: "",
          title: "Läsnings inställningar"
        },
        title: "Visning"
      },
      signature: {
        title: "Signatur",
        default: "Default",
        dropFiles: "Släpp bilden här för att lägga till",
        management: {
          title: "Signature management"
        },
        onlyNewMessages: "Don't add signature on replies",
        placeholderList: " ",
        senderBcc: "Lägg till avsändare i blind kopia (Bcc)",
        setAsDefault: "Set as default sender",
        signatures: {
          desc:
            "Customize your email by choosing the name displayed, who receives the message, and setting a signature to add on all your messages.<br/>You can customize the display name and signature for all identities (alias groups) associated with your account.",
          headerItemSignature: "Visningsnamn",
          headerItemIdentity: "Identitet",
          title: "Signaturer och identiteter"
        }
      },
      title: "E-Post"
    },
    meeting: {
      title: "Möte",
      view: {
        desc: "",
        title: "Visa"
      }
    },
    news: {
      readMore: "Läs mer",
      title: "Webmail nyheter"
    },
    shortcuts: {
      title: "Tangentbordsgenvägar"
    },
    title: "Inställningar",
    unsaved: "Det finns ej sparade ändringar"
  },

  toast: {
    error: {
      alreadyPresent: "Varning, resursen finns redan",
      avatarTooBig: "Varning, avatarstorlek för stor",
      compact: "Fel vid komprimering av mapp",
      connectionUnavailable: "Ingen anslutning tillgänglig",
      delete: "Fel vid radering",
      deleteDraft: "Fel vid utkast radering",
      enqueued: "Uppdatera inställningar, försök igen",
      fetchingAttachments: "Fel vid hämtning av bilagor",
      filterActionsMissing: "Åtgärder inte närvarande",
      filterConditionsMissing: "Parametrar inte närvarande",
      filterNameMissing: "Filternamn saknas",
      folderCreation: "Fel vid skapande av ny mapp",
      folderCreationCharNotValid: "Fel vid skapande/namn byte av mapp, tecken är inte giltigt (/)",
      folderCreationNoName: "Fel vid skapande av mapp, du måste ange ett giltigt namn",
      folderDelete: "Fel vid radering av mapp",
      folderNotPresent: "Mapp inte närvarande",
      folderRename: "Fel vid namnbyte av mapp, redan närvarande",
      folderSubscribe: "Fel vid prenumerations av mapp",
      generic: "Något gick fel",
      maxMembers: "Varning, maximalt antal uppnådda medlemmar",
      messageCopy: "Fel vid kopiering av meddelanden",
      messageMove: "Fel vid flyttning av meddelanden",
      messageNotFound: "Meddelandet hittades inte",
      messageRemove: "Fel vid borttagning av meddelanden",
      missingParameters: "Parametrar saknas",
      notifications: "Fel vid aviseringar",
      notPermitted: "Åtgärden inte tillåten",
      notValid: "Varning, ogiltigt värde",
      onlyOneHomeAddress: "Du kan bara infoga en hemadress",
      onlyOneWorkAddress: "Du kan bara infoga en arbetsadress",
      save: "Fel vid sparande",
      saveAcl: "Fel spara mappbehörigheter",
      savingDraft: "Utkast till sparning pågår, försök igen i slutet",
      sendingMessage: "Fel vid leverans av meddelanden",
      serverDisconnection: "Server uppdateras, försök igen senare!",
      sessionExpired: "Sessionen löpte ut",
      smtpBlockedByAntispam: "Message not sent: blocked by antispam",
      smtpBlockedByAntivirus: "Message not sent: blocked by antivirus",
      smtpDisabled: "Meddelande inte skickat: SMTP inaktiverat på det här kontot",
      smtpQuotaExceeded:
        "Meddelandet har inte skickats: meddelandekvoten för skickade meddelanden överskreds",
      requestLimit: "Efterfrågningsgräns (request limit) har överskridits, försök igen senare",
      requestError: "Efterfråningfel har inträffat, försök igen senare",
      updateFolders: "Ett fel uppstod vid uppdatering av mappar",
      updateMessageEvent: "Fel vid uppdatering av händelsestatus",
      updatingParentFlags: "Fel vid uppdatering av meddelandeflaggor"
    },
    info: {
      newVersionAvailable: {
        desc: "Denna åtgärd är säker, dina inställningar ändras inte.",
        title:
          "Nya funktioner är tillgängliga, ladda om sidan för att uppdatera till den senaste versionen."
      },
      serverReconnected: "Webmailen är tillgänglig igen",
      sourceMail: "E-postkälla kopierad till urklipp"
    },
    progress: {
      newFolder: "Skapande av ny mapp pågår...",
      operationInProgress: "Operation pågår..."
    },
    success: {
      compact: "Mapp komprimerad framgångsrikt",
      copiedLink: "Länk kopierad till urklipp",
      copiedMail: "Meddelandet har kopierats.",
      copiedMailAddress: "E-postadress kopierad till urklipp",
      createFolder: "Mapp skapad framgångsrik",
      delete: "Raderad framgångsrikt",
      disabledOtp: "OTP inaktiverad framgångsrikt",
      enabledOtp: "OTP aktiverat framgångsrikt",
      movedMail: "Meddelandet flyttades framgångsrikt",
      otpDisabled: "OTP har avaktiverats",
      otpEnable: "OTP har aktiverats",
      save: "Sparades framgångsrikt",
      saveAcl: "Behörigheter sparade framgångsrikt",
      saveDraft: "Utkastet sparats framgångsrikt",
      saveFolder: "Mapp har uppdaterades framgångsrikt",
      sentMail: "Meddelandet skickades framgångsrikt",
      subscribeFolder: "Prenumerationen har uppdaterats framgångsrikt",
      updateMessageEvent: "Händelsestatus uppdaterad"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "Hela dagen",
      autoSaveContact: "Spara automatiskt alla mottagare som inte finns i dina adressböcker",
      enableAutorespopnd: "Aktivera autosvar",
      enableAutorespopndInterval: "Aktivera datumintervall",
      keepCopy: "Behåll kopia av meddelandet lokalt",
      showPriority: "Visa mottagen meddelandeprioritet",
      showThreads: "Visa meddelanden som tråd",
      denyInvites: "Förhindra inbjudningar till möten",
      enableDateSearch: "Aktivera datumssökning",
      enableFullBody: "Aktivera fulltextsökning",
      subscribeFolder: "Prenumerera på mapp"
    }
  },

  wizard: {
    layout: {
      desc: "Anpassa din Webmail genom att välja inställningar som du föredrar.",
      title: "Visa alternativ"
    },
    wizardData: {
      desc: "Anpassa din profil med en bild och komplettera med detaljer som saknas",
      title: "Slutför profil"
    }
  }
};
