/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Geselecteerde contacten",
      notSelected: "Selecteer een contact",
      sections: {
        cell: " Mobiel",
        home: "Thuis",
        homeFax: "Fax thuis",
        work: "Werk",
        workFax: "Fax werk"
      },
      selectContacts: "Geselecteerde contacten",
      selectedInfo: "Selecteer een contact uit de lijst om de details te bekijken."
    },
    contactsList: {
      emptyContacts: "Dit adresboek is leeg",
      emptyContactsInfo: "Voeg een contact aan het adresboek",
      foundContacts: "Gevonden",
      notFound: "Geen contact gevonden",
      notFoundInfo:
        "De zoekopdracht heeft geen resultaat opgeleverd, probeer het opnieuw met andere criteria.",
      totalContacts: "Contacten"
    },
    contextualMenuLabels: {
      address: "Adres",
      birthday: "Verjaardag",
      cancel: "Annuleren",
      delete: "Verwijderen",
      downloadVCard: "VCard Downloaden",
      email: "E-mail",
      newContact: "Nieuw contact",
      newList: "Nieuwe lijst",
      note: "Notitie",
      phone: "Telefoon",
      shareVCard: "Delen",
      showVCard: "Vcard weergeven",
      sendAsAttachment: "Verzend per e-mail",
      sendMail: "E-mail versturen",
      url: "URL"
    },
    deleteContactDsc:
      "Door het verwijderen van het contact <code>{{contact}}</code> worden alle gerelateerde gegevens ook verwijderd. <br/><b>Dit is onomkeerbaar</b>.",
    deleteContactsDsc:
      "Door het verwijderen van de geselecteerde contacten worden alle gerelateerde gegevens ook verwijderd. <br/><b>Dit is onomkeerbaar</b>.",
    deleteContactTitle: "Verwijder contact",
    deleteContactDscList:
      "Door het verwijderen van het geselecteerde contact worden alle gerelateerde gegevens ook verwijderd. <br/><b>Dit is onomkeerbaar</b>.",
    deleteContactsTitle: "Verwijder contacten",

    deleteListDsc:
      "Door het verwijderen van de lijst <code>{{contact}}</code> worden alle gerelateerde gegevens ook verwijderd. <br/><b>Dit is onomkeerbaar</b>.",
    deleteListTitle: "Verwijder lijst",

    editContactModal: {
      title: "Contact bewerken"
    },
    editListModal: {
      title: "Lijst bewerken"
    },
    exportAddressBookModal: {
      desc:
        "Je exporteert het adresboek: <code>{{addressbookExport}}</code><br/>In welk formaat wil je de lijst exporteren?",
      title: "Exporteer adresboek"
    },
    import: {
      contactsInvalid: "Niet geldig:",
      contactsNr: "Totaal aantal te importeren contacten:",
      contactsValid: "Geldig:",
      desc:
        "Je importeert contacten in het adresboek:: <code>{{addressbookImport}}</code><br/>Je kan contacten uit andere e-mailprogramma's importeren met behulp van een vCard (<strong>.vcf</strong>) of <strong>.ldif</strong> bestandsformaat.\u000AJe kunt bijvoorbeeld contactpersonen exporteren vanuit Gmail of vanuit Outlook en ze importeren.\u000AImporteren vervangt géén bestaande contacten.",
      dropFiles: "Upload or drop here the file to be imported",
      error: "Contacten konden niet worden geïmporteerd",
      importing:
        "We importeren <b>{{contactsNumber}}</b> gebeurtenis in de agenda: <code>{{addressbookImport}}</code>",
      loading: "Laden...",
      success: "Import succesvol!",
      title: "Contacten importeren",
      uploadedFile: "Geüpload",
      uploadingFile: "Uploaden..."
    },
    list: {
      desc:
        "Het is mogelijk om tot <b>150 leden</b> aan de lijst toe te voegen door te kiezen uit de contacten in het adresboek <code>{{{addressbookName}}</code>.",
      members: "Lijst leden"
    },
    messages: {
      newContactAddressBookBody:
        "Je kan geen contact toevoegen in <b>Alle contacten</b> of in <b>Bedrijfscontacten</b>. Kies een persoonlijk of gedeeld adresboek.",
      newContactAddressBookTitle: "Kies een adresboek",
      resetListBody:
        "De leden van de lijst zijn gerelateerd aan het geselecteerde adresboek. Deze wijziging zal de ledenlijst resetten. Wil je doorgaan?",
      resetListTitle: "Leden verwijderen"
    },
    newAddressBookModal: {
      desc: "",
      title: "Adresboek toevoegen/bewerken"
    },
    newContactModal: {
      title: "Contact toevoegen"
    },
    newListModal: {
      title: "Lijst toevoegen"
    },
    shareModal: {
      desc:
        "Je deelt het adresboek: <code>{{addressbookShare}}</code><br/>Selecteer de contacten waarmee je het adresboek wil delen en laat ze het adresboek beheren.",
      title: "Adresboek delen"
    },
    sidebar: {
      allContacts: "Alle contacten",
      deleteAddressbookDsc:
        "Door het verwijderen van het adresboek <code>{{addressbook}}</code> worden alle contacten die in de lijst staan verwijderd.<br/><b>Dit is onomkeerbaar</b>.",
      deleteAddressbookTitle: "Adresboek verwijderen",
      globalAddressbook: "Zakelijke contacten",
      personalAddressbook: "Persoonlijke contacten",
      sharedAddressbooks: "Gedeelde adresboeken",
      userAddressbooks: "Adresboek van de gebruiker"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "Er zijn geen adresboeken gevonden",
      title: "Abonneren op het adresboek"
    }
  },

  app: {
    addressbook: "Adresboek",
    calendar: "Agenda",
    chat: "Chat",
    meeting: "Vergadering",
    of: "of",
    webmail: "Webmail"
  },

  avatarEditorModal: {
    photoTitle: "Maak een foto",
    title: "Avatar wijzigen",
    zoom: "Zoom -/+"
  },

  buttons: {
    accept: "Accepteren",
    add: "Toevoegen",
    addAction: "Actie toevoegen",
    addAddressbook: "Addresboek",
    addAsAttachment: "Toevoegen",
    addCalendar: "Agenda",
    addField: "Veld toevoegen",
    addHolidayCalendar: "Holiday calendar",
    addLabel: "Label toevoegen",
    addInline: "In de regel toevoegen",
    addParameter: "Parameter toevoegen",
    addSignature: "Handtekening toevoegen",
    addToList: "Aan lijst toevoegen",
    apply: "Toepassen",
    backTo: "Vorige",
    backAddressbook: "Sluiten",
    cancel: "Annuleren",
    changeImage: "Afbeelding wijzigen",
    changePassword: "Wachtwoord wijzigen",
    checkFreeBusy: "Beschikbaarheid controleren",
    close: "Sluiten",
    createFolder: "Nieuwe map",
    comeBackLater: "Kom later terug",
    confirm: "Bevestigen",
    copyLink: "Link kopiëren",
    decline: "Afwijzen",
    delete: "Verwijderen",
    deleteDraft: "Concept verwijderen",
    details: "Details",
    disabled: "Uitgeschakeld",
    disableOTP: "OTP uitschakelen",
    download: "Downloaden",
    editLabel: "Label wijzigen",
    editSignature: "Handtekening wijzigen",
    empty: "Leeg",
    enabled: "Actief",
    event: "Gebeurtenis",
    export: "Exporteren",
    goToMail: "Terug naar e-mail",
    goToOldWebmail: "Go to old webmail",
    import: "Importeren",
    hide: "Verbergen",
    label: "Label",
    leftRotate: "90 graden naar links draaien",
    loadImage: "Afbeelding uploaden",
    logout: "Uitloggen",
    makePhoto: "Maak een foto",
    markRead: "Gelezen",
    markSeen: "Gezien",
    markUnread: "Ongelezen",
    message: "Bericht",
    modify: "Bewerken",
    new: "Nieuw",
    newContact: "Nieuw contact",
    newEvent: "Nieuwe gebeurtenis",
    newMessage: "Nieuw bericht",
    occurrence: "Gebeurtenis",
    openMap: "Openen",
    reloadPage: "Reload page",
    removeImage: "Afbeelding verwijderen",
    rename: "Hernoemen",
    reorder: "Opnieuw rangschikken",
    reorderEnd: "Einde",
    report: "Rapport",
    retryImport: "Opnieuw",
    save: "Opslaan",
    saveDraft: "Concept opslaan",
    saveNextAvailability: "Voorgestelde beschikbaarheid opslaan",
    search: "Zoeken",
    send: "Verzenden",
    series: "Series",
    setOtp: "OTP instellen",
    share: "Delen",
    show: "Weergeven",
    showMore: "Overige opties",
    showUrlsAddressbook: "Adresboek URL's tonen",
    showUrlsCalendar: "Agenda URL's tonen",
    snooze: "Uitstellen",
    starred: "Markeren",
    subscribe: "Abonneren",
    subscribeAddressbook: "Abonneren op adresboek",
    subscribeCalendar: "Abonneren op agenda",
    tentative: "Misschien",
    unsetOtp: "OTP uitschakelem",
    unsubscribe: "Uitschrijven",
    update: "Bijwerken",
    updateFilter: "Filter bijwerken",
    viewMap: "Kaart bekijken"
  },

  buttonIcons: {
    activeSync: "ActiveSync aanzetten",
    activeSyncNot: "ActiveSync uitzetten",
    alignCenter: "Midden centreren",
    alignLeft: "Links uitlijnen",
    alignRight: "Rechts uitlijnen",
    backgroundColor: "Achtergrondkleur",
    backTo: "Terug",
    bold: "Dikgedrukt",
    confirm: "Bevestigen",
    close: "Sluiten",
    delete: "Verwijderen",
    download: "Downloaden",
    downloadVCard: "Download VCard",
    edit: "Bewerken",
    emoji: "Emoji",
    forward: "Doorsturen",
    fullscreen: "Volledig scherm",
    fullscreenNot: "Niet volledig scherm",
    imageAdd: "Afbeelding toevoegen",
    indent: "Meer inspringen",
    italic: "Schuingedrukt",
    maintenance: "Onderhoud",
    maxmized: "Vergroten",
    minimized: "Verkleinen",
    more: "Meer",
    moveDown: "Naar beneden verplaatsen",
    moveUp: "Naar boven verplaatsen",
    next: "Volgende",
    orderList: "Lijst rankschikken",
    outdent: "Minder inspringen",
    pin: "Vastzetten",
    previous: "Vorige",
    print: "Afdrukken",
    removeFormatting: "Opmaak verwijderen",
    reply: "Reageren",
    replyAll: "Reageren op iedereen",
    saveDraft: "Concept opslaan",
    sendDebug: "Bericht verzenden dat niet kan worden bekeken",
    sendMail: "Bericht versturen",
    share: "Delen",
    showSidebar: "Toon / verberg zijbalk",
    showSourceCode: "Broncode tonen",
    showVCard: "Vcard tonen",
    spam: "Markeren als spam",
    spamNot: "Markeren als veilig",
    startChat: "Chat starten",
    startMeeting: "Afspraak starten",
    strike: "Doorstrepen",
    subscribe: "Inschrijven",
    textColor: "Tekstkleur",
    thanks: "Bedankt",
    underline: "Onderstrepen",
    unorderList: "Lijst niet ranschikken",
    unSubscribe: "Uitschrijven"
  },
  calendar: {
    deleteEvent: "Gebeurtenis verwijderen",
    deleteEventDsc: "Wil je doorgaan met het verwijderen van deze gebeurtenis?",
    event: {
      deleteEventOccurrenceTitle: "Alleen dit evenement verwijderen of de hele reeks?",
      deleteEventOccurrenceDesc:
        "De gebeurtenis die je wil verwijderen heeft verschillende gebeurtenissen. Het verwijderen van een enkele gebeurtenis laat andere gebeurtenissen ongewijzigd.",
      fromDay: "van",
      maxLabelsNumberTitle: "Te veel labels",
      maxLabelsNumberDsc:
        "Je hebt het maximale aantal labels bereikt dat aan een evenement kan worden gekoppeld. Om een nieuw label toe te voegen, moet je eerst een van de reeds aanwezige labels verwijderen.",
      organizerTitle: "Gemaakt door",
      pendingMsg: "Wachtend op bevestiging",
      repeatEvent: "Deze gebeurtenis is deel van een reeks",
      saveEventOccurrenceTitle: "Deze gebeurtenis of hele reeks tonen?",
      saveEventOccurrenceDesc:
        "De gebeurtenis die je wil bekijken of bewerken heeft verschillende gebeurtenissen. Het bewerken van deze gebeurtenis laat andere gebeurtenissen ongewijzigd.",
      toDay: "tot"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "Eerste",
      WeekDayNumberInMonth_2: "Tweede",
      WeekDayNumberInMonth_3: "Derde",
      WeekDayNumberInMonth_4: "Vierde",
      WeekDayNumberInMonth_5: "Vijfde",
      never: "Nooit",
      after: "Na;herhalingen",
      inDate: "Op datum",
      end: "Einde herhaling"
    },
    filterView: {
      listmonth: "Agenda",
      day: "Dag",
      month: "Maand",
      week: "Week",
      workWeek: "Werkweek",
      year: "Jaar"
    },
    import: {
      contactsInvalid: "Niet geldig:",
      contactsNr: "Totaal aantal evenementen om te importeren:",
      contactsValid: "Goedgekeurd:",
      dropFiles: "Upload bestanden of sleep ze hiernaartoe",
      error: "Gebeurtenissen konden niet worden geïmporteerd",
      importing:
        "We importeren <b>{{eventsNumber}}</b> gebeurtenissen naar: <code>{{calendarImport}}</code>",
      info:
        "Je importeert gebeurtenissen naar de agenda: <code>{{calendarImport}}</code><br/>Je kan gebeurtenissen importeren uit andere e-mailapps in een ICS-bestandsformaat. <br/><b>De import vervangt geen bestaande gebeurtenissen</b>.",
      loading: "Laden...",
      success: "Al je gebeurtenissen zijn succesvol geïmporteerd!",
      title: "Gebeurtenissen importeren",
      uploadedFile: "Geüpload",
      uploadingFile: "Uploaden..."
    },
    newCalendarModal: {
      desc: "",
      title: "Maak/bewerk agenda"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Bijlagen",
        placeholderList: " "
      },
      desc: "",
      dropFiles: "Upload bestanden of sleep ze hiernaartoe",
      freeBusy: {
        legend: {
          event: "Gebeurtenis",
          busy: "Bezig",
          waiting: "Wachten",
          availability: "Volgende beschikbaarheid"
        }
      },
      hideDetails: "Deelnemers verbergen",
      participantsList: {
        headerItem: "Deelnemers",
        answeredHeaderItem: "Beantwoord",
        notAnsweredHeaderItem: "Wachten op een antwoord...",
        placeholderList: " "
      },
      showDetails: "Details tonen",
      tabAttachments: "Bijlagen",
      tabDetails: "Details",
      tabParticipants: "Deelnemers",
      tabSeries: "Herhalingen",
      tabSettings: "Instellingen",
      title: "Gebeurtenis maken",
      titleEdit: {
        event: "Gebeurtenis",
        series: "Series"
      },
      titleFreebusy: "Deelnemers beschikbaar/bezig"
    },
    nextAvailability: "Volgende beschikbaarheid",
    searchEvents: {
      notFound: "Geen gebeurtenissen gevonden",
      notFoundInfo:
        "De zoekopdracht heeft geen resultaten opgeleverd, probeer het opnieuw andere zoekwoorden."
    },
    shareModal: {
      desc:
        "Je deelt de agenda: <code>{{{{calendarShare}}}}</code><br/>Selecteer de contactpersonen waarmee je de agenda wil delen en sta ze toe deze te beheren.",
      title: "Agenda delen"
    },
    sidebar: {
      labels: "Labels",
      sharedCalendars: "Gedeelde agenda's",
      userCalendars: "Gebruikersagenda's",
      deleteCalendarDsc:
        "Door de agenda <code>{{calendar}}</code> te verwijderen worden alle gebeurtenissen ook verwijderd. <br/><b>Dit is inomkeerbaar</b>.",
      deleteCalendarTitle: "Agenda verwijderen"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "Er zijn geen agenda's om op te abonneren",
      title: "Abonneren in agenda"
    },
    today: "Vandaag"
  },

  circularLoopBar: {
    uploading: "Uploaden..."
  },

  comingSoon: "Ontwikkeling in uitvoering...",

  contactImport: {
    title: "Contacten zoeken",
    desc: "We importeren je oude contacten. Dat kan even duren..."
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Beantwoord",
      "\\\\Deleted": "Verwijderd",
      "\\\\Draft": "Concept",
      "\\\\Flagged": "Gemarkeerd",
      "\\\\Seen": "Gelezen"
    },
    antispamFilterTypes: {
      whitelist_from: "Toestaan van",
      whitelist_to: "Toestaan naar"
    },
    calendarMinutesInterval: {
      "15": "15 minuten",
      "30": "30 minuten",
      "60": "60 minuten"
    },
    calendarNewEventFreeBusy: {
      busy: "Bezet",
      free: "Beschikbaar",
      outofoffice: "Buiten kantoor",
      temporary: "Tijdelijk"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Accepteren",
      DECLINED: "Afwijzen",
      TENTATIVE: "Misschien"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Voorzitter",
      "REQ-PARTICIPANT": "Gevraagd",
      "OPT-PARTICIPANT": "Niet gevraagd",
      "NON-PARTICIPANT": "Ter informatie"
    },
    calendarNewEventPriority: {
      "0": "Geen",
      "1": "Hoog",
      "5": "Middel",
      "9": "Laag"
    },
    calendarNewEventTypes: {
      public: "Openbaar",
      confidential: "Vertrouwelijk",
      private: "Privé"
    },
    calendarStartWeekDay: {
      saturday: "Zaterdag",
      sunday: "Zondag",
      monday: "Maandag"
    },
    calendarView: {
      year: "Jaar",
      month: "Maand",
      week: "Week",
      workWeek: "Werkweek",
      day: "Dag",
      listmonth: "Lijst"
    },
    calendarEventFilterBy: {allEvents: "Alle", nextEvents: "Alle volgende gebeurtenissen"},
    calendarEventSearchIn: {title: "Titel/label/plaats", all: "Gehele inhoud"},
    cancelSendTimer: {
      "0": "Disabled",
      "5000": "5 seconds",
      "10000": "10 seconds",
      "20000": "20 seconds",
      "30000": "30 seconds"
    },
    contactNameFormat: {
      firstName: "Voornaam, Achternaam",
      lastName: "Achternaam, Voornaam"
    },
    contactsOrder: {
      firstName: "Voornaam",
      lastName: "Achternaam"
    },
    countries: {
      AD: "Andorra",
      AE: "United Arab Emirates",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "American Samoa",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Åland Islands",
      AZ: "Azerbaijan",
      BA: "Bosnia and Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgium",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius and Saba",
      BR: "Brasil",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Belarus",
      BZ: "Belize",
      CA: "Canada",
      CC: "Cocos (Keeling) Islands",
      CD: "Democratic Republic of the Congo",
      CF: "Central African Republic",
      CG: "Republic of the Congo",
      CH: "Switzerland",
      CL: "Chile",
      CM: "Cameroun",
      CN: "China",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Cabo Verde",
      CW: "Curaçao",
      CX: "Christmas Island",
      CY: "Cyprus",
      CZ: "Czech Republic",
      DE: "Germany",
      DK: "Denmark",
      DM: "Dominica",
      DO: "Dominican Republic",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spain",
      ET: "Ethiopia",
      FI: "Finland",
      FO: "Faroe Islands",
      FR: "France",
      GA: "Gabon",
      GB: "United Kingdom",
      GD: "Grenada",
      GF: "French Guiana",
      GG: "Guernsey",
      GI: "Gibraltar",
      GL: "Greenland",
      GP: "Guadeloupe",
      GQ: "Equatorial Guinea",
      GR: "Greece",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croatia",
      HT: "Haiti",
      HU: "Hungary",
      IE: "Ireland",
      IM: "Isle of Man",
      IS: "Island",
      IT: "Italy",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Japan",
      KE: "Kenya",
      KR: "South Korea",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lithuania",
      LU: "Luxembourg",
      LV: "Latvia",
      MC: "Monaco",
      MD: "Moldova",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "North Macedonia",
      MQ: "Martinique",
      MT: "Malta",
      MW: "Malawi",
      MX: "Mexico",
      MZ: "Mozambique",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Netherlands",
      NO: "Norway",
      NZ: "New Zealand",
      PA: "Panama",
      PE: "Perú",
      PH: "Philippines",
      PL: "Poland",
      PT: "Portugal",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Sweden",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovakia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "South Sudan",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turkey",
      TZ: "Tanzania",
      UA: "Ukraine",
      UG: "Uganda",
      US: "United States of America",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "South Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Vraag altijd om een leveringsbevestiging",
      never: "Vraag nooit om een leveringsbevestiging"
    },
    eventAlarmPostponeTime: {
      "60": "voor 1 minuut",
      "300": "voor 5 minuten",
      "900": "voor 15 minuten",
      "1800": "voor 30 minuten",
      "3600": "voor 1 uur",
      "7200": "voor 2 uur",
      "86400": "voor 1 dag",
      "604800": "voor 1 week"
    },

    calendarTaskStatus: {
      "needs-action": "Needs action",
      completed: "Completed",
      "in-process": "In progress",
      cancelled: "Cancelled"
    },

    feedbackAreas: {
      addressbookSidebar: "Lijst met adresboeken",
      contactList: "Contactenlijst",
      contactView: "Contactweergave",
      mailSearch: "Zoek berichten",
      mailSidebar: "Mappenlijst",
      messageList: "Berichtenlijst",
      messageView: "Berichtweergave",
      newMessage: "Nieuw bericht opstellen",
      other: "Meer",
      settings: "Instellingen"
    },
    filterMail: {
      all: "Alles",
      byDate: "Datum",
      byFrom: "Van",
      bySize: "Grootte",
      bySubject: "Onderwerp",
      flagged: "Gemarkeerd",
      filter: "Filters",
      orderBy: "Sorteer op",
      seen: "Gelezen",
      unseen: "Ongelezen"
    },
    filtersActions: {
      discard: "Weggooien",
      addflag: "Markeer bericht als",
      setkeyword: "Label toevoegen",
      fileinto: "Verplaats het bericht naar",
      fileinto_copy: "Kopieer bericht naar",
      keep: "Bericht bewaren",
      redirect: "Bericht doorverwijzen naar",
      redirect_copy: "Stuur een kopie naar",
      reject: "Weigeren met het volgende bericht",
      stop: "Stop de verwerking van filters"
    },
    filtersDateOperators: {
      gt: "Na",
      is: "Is gelijk aan",
      lt: "Voor",
      notis: "is niet gelijk aan"
    },
    filtersEnvelopeSubtypes: {From: "Van", To: "Tot"},
    filtersDateSubTypes: {date: "Datum", time: "Tijd", weekday: "Weekdag"},
    filtersDateWeekdays: {
      monday: "Maandag",
      tuesday: "Dinsdag",
      wednesday: "Woensdag",
      thursday: "Donderdag",
      friday: "Vrijdag",
      saturday: "Zaterdag",
      sunday: "Zondag"
    },
    filtersRules: {
      Bcc: "Bcc",
      body: "Body",
      Cc: "Cc",
      currentdate: "Datum",
      envelope: "Bericht",
      From: "Afzender",
      other: "Andere",
      size: "Grootte",
      Subject: "Onderwerp",
      To: "Ontvanger"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "is groter dan", under: "is kleiner dan"},
    filtersStringOperators: {
      contains: "bevat",
      exists: "bestaat",
      is: "is gelijk aan",
      notcontains: "bevat niet",
      notexists: "bestaat niet",
      notis: "is niet gelijk aan"
    },
    languages: {
      de: "Duits",
      en: "Engels",
      it: "Italiaans",
      nl: "Dutch",
      sv: "Zweeds",
      es: "Spaans",
      pt: "Portugees"
    },
    matchTypes: {
      allof: "Match op alle parameters",
      anyof: "Match op ten minste één parameter"
    },
    newAllDayEventAlert: {
      never: "Nooit",
      eventDate: "Gebeurtenis datum",
      "-PT17H": "1 dag van tevoren, om 9 uur",
      "-P6DT17H": "1 week van tevoren, om 9 uur"
    },
    newEventAlert: {
      never: "Nooit",
      eventHour: "Begintijd",
      //P[n]Y[n]M[n]DT[n]H[n]M[n]S
      "-PT5M": "5 minuten voor",
      "-PT15M": "15 minuten voor",
      "-PT30M": "30 minuten voor",
      "-PT1H": "1 uur voor",
      "-PT2H": "2 uur voor",
      "-PT12H": "12 uur voor",
      "-P1D": "1 dag voor",
      "-P7D": "1 week voor"
    },
    newEventRepeatDayWeek: {
      MO: "Maandag",
      TU: "Donderdag",
      WE: "Woensdag",
      TH: "Donderdag",
      FR: "Vrijdag",
      SA: "Zaterdag",
      SU: "Zondag"
    },
    newEventRepeatFrequency: {
      NEVER: "Nooit",
      DAILY: "Dagelijks",
      WEEKLY: "Wekelijks",
      MONTHLY: "Maandelijks",
      YEARLY: "Jaarlijks"
    },
    newEventRepeatMonths: {
      "1": "januari",
      "2": "februari",
      "3": "maart",
      "4": "april",
      "5": "mei",
      "6": "juni",
      "7": "juli",
      "8": "augustus",
      "9": "september",
      "10": "october",
      "11": "november",
      "12": "december"
    },
    orderSelector: {
      byLastname: "Achternaam",
      byMail: "E-mailadres",
      byName: "Voornaam",
      byOrg: "Bedrijf",
      orderBy: "Sorteren op",
      showOnlyLists: "Toon alleen lijsten"
    },
    readConfirmations: {
      always: "Altijd om leesbevestiging vragen",
      never: "Nooit om leesbevestiging vragen"
    },
    readingConfirmation: {
      always: "Altijd",
      ask: "Altijd vragen",
      never: "Nooit"
    },
    selectMail: {
      all: "Alles selecteren",
      cancel: "Annuleren",
      modify: "Bewerken",
      none: "Alles deselecteren"
    },
    shareOptionsCalendar: {
      None: "Nooit",
      DAndTViewer: "Datum en tijd tonen",
      Viewer: "Alles weergeven",
      Responder: "Antwoord bij",
      Modifier: "Aanpassen"
    },
    showImages: {
      always: "Altijd",
      contacts: "Contacten en vertrouwden",
      never: "Nooit"
    },
    themes: {
      dark: "Donker",
      light: "Licht"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Layout 3 kolommen",
      viewHalf: "Weergave met voorvertoning",
      viewFull: "Lijstweergave"
    },
    viewModeCompact: {
      viewColumns: "Columns",
      viewHalf: "Preview",
      viewFull: "List"
    }
  },

  dropdownMenu: {
    activesync: "ActiveSync",
    delete: "Verwijderen",
    download: "Downloaden",
    edit: "Bewerken",
    exportContacts: "Contacten exporteren",
    importContacts: "Contacten importeren",
    importEvents: "Gebeurtenissen importeren",
    remove: "Verwijderen",
    rename: "Hernoemen",
    share: "Delen",
    syncNo: "Niet synchroniseren",
    syncYes: "Synchroniseren",
    unsubscribe: "Uitschrijven"
  },
  dropdownUser: {
    feedback: "Feedback",
    logout: "Uitloggen",
    settings: "Instellingen"
  },

  errors: {
    emailDomainAlreadyPresent: "Email address or domain already present",
    emptyField: "Veld is leeg",
    invalidEmail: "Geen geldig e-mailadres",
    invalidEmailDomain: "Email address or domain not valid",
    invalidPassword: "Ongeldig wachtwoord",
    invalidPasswordFormat: "Ongeldig wachtwoord, voer ten minste 8 alfanumerieke tekens in.",
    invalidValue: "Geen geldige waarde",
    mandatory: "Verlpicht veld",
    notAllowedCharacter: "Niet toegestaan karakter",
    notChanged: "Niet gewijzigd veld",
    permissionDenied: "Toestemming geweigerd",
    tooLong: "Te lang veld"
  },

  feedback: {
    disclaimer:
      "Je mening is belangrijk! Help ons bij het oplossen van problemen en het verbeteren van de service.",
    labels: {
      feedbackAreas: "Waar wil je feedback op achterlaten?",
      note: "Laat een bericht achter (optioneel)"
    },
    msgError: {
      title: "Je feedback kan niet verzonden worden.",
      desc:
        "Probeer het later nog eens, het is belangrijk voor ons om te weten wat je van onze webmail vindt. Je feedback zal ons helpen om een betere ervaring te creëren voor jou en al onze gebruikers. "
    },
    msgSuccess: {
      title: "Bedankt voor je feedback!",
      desc:
        "Wij waarderen de tijd die je hebt genomen om ons te helpen onze webmail te verbeteren. Je feedback zal ons helpen om een betere ervaring voor jou en voor al onze gebruikers te creëren."
    },
    privacy:
      "De verzamelde gegevens zullen worden verwerkt in overeenstemming met de huidige privacywetgeving.",
    title: "Feedback"
  },

  inputs: {
    labels: {
      addAddress: "Adres toevoegen",
      addDomain: "Adres of domein toevoegen",
      addLabel: "Label naam",
      addMembers: "Leden toevoegen",
      addParticipant: "Deelnemer toevoegen",
      addressbookName: "Adresboek",
      addUser: "Gebruiker toevoegen",
      alert: "Herinnering",
      alert2: "Tweede herinnering",
      at: "om",
      calendar: "Agenda",
      calendarBusinessDayEndHour: "Einde werkdag",
      calendarBusinessDayStartHour: "Start werkdag",
      calendarColor: "Agenda kleur",
      calendarMinutesInterval: "Agenda interval",
      calendarName: "Agenda naam",
      calendarView: "Standaardweergave",
      company: "Bedrijf",
      confidentialEvents: "Vertrouwelijke gebeurtenissen",
      confirmNewPassword: "Wachtwoord bevestigen",
      contactNameFormat: "Opmaak contacten",
      contactsOrder: "Volgorde contacten",
      country: "Country",
      dateFormat: "Datumindeling",
      defaultAddressbook: "Standaard adresboek",
      defaultCalendar: "Standaard agenda",
      displayName: "Weergavenaam",
      email: "E-mail",
      endDate: "Einddatum",
      event: "Gebeurtenis",
      eventDailyRepetition: "Iedere;dag/en",
      eventDailyWorkdayRepetition: "Elke dag van de week",
      eventMonthlyLastDayRepetition: "Laatste dag van de maand, iedere;maand/en",
      eventMonthlyLastWeekDayRepetition: "Laatste {{WeekDay}}, iedere;maand/en",
      eventMonthlyRepetition: "Op {{DayNumber}}, iedere;maand/en",
      eventMonthlyWeekDayRepetition: "Op {{WeekDayNumber}}, iedere;maand/en",
      eventPartecipation: "Aanwezigheidsstatus",
      eventPriority: "Gebeurtenis prioriteit",
      eventType: "Gebeurtenis categorie",
      eventWeeklyRepetition: "Iedere;week, op:",
      eventYearlyLastDayRepetition: "Laatste dag van {{Month}}, ieder;jaar",
      eventYearlyLastWeekDayRepetition: "Laatste {{WeekDay}} van {{Month}}, ieder;jaar",
      eventYearlyRepetition: "Op {{DayNumber}} van {{Month}}, ieder;year",
      eventYearlyWeekDayRepetition: "Op {{WeekDayNumber}} van {{Month}}, ieder;jaar",
      exportAsVcard: "Exporteren in VCard formaat",
      exportAsLdiff: "Exporteren in LDIF formaat",
      faxHome: "Fax thuis",
      faxWork: "Fax werk",
      filter: "Filteren op",
      filterName: "Filter naam",
      firstName: "Voornaam",
      folderName: "Mapnaam",
      freeBusy: "Weergeven als",
      from: "Van",
      fromDate: "Van datum",
      home: "Thuis",
      hour: "Tijd",
      includeFreeBusy: "Laat beschikbaar/bezet zien",
      label: "Label",
      labelColor: "Label kleur",
      language: "Taal",
      lastName: "Achternaam",
      listName: "Lijst naam",
      location: "Locatie",
      loginAddressbook: "Standaard adresboek",
      mailAccount: "Account",
      mobile: "Mobiel",
      newAddressBook: "Nieuw adresboek",
      newAllDayEventAlert: "Nieuwe gebeurtenis voor de hele dag",
      newCalendar: "Nieuwe agenda",
      newEventAlert: "Nieuwe gebeurtenis melding",
      newForward: "Ontvanger toevoegen aan lijst",
      newPassword: "Nieuw wachtwoord",
      nickname: "Roepnaam",
      object: "Onderwerp",
      oldPassword: "Huidig wachtwoord",
      parentFolder: "Pad",
      phone: "Telefoon",
      privateEvents: "Privé gebeurtenissen",
      publicEvents: "Openbare geeurtenissen",
      readingConfirmation: "Leesbevestiging versturen",
      recoveryEmail: "Herstel e-mailadres",
      renameAddressbook: "Adresboek hernoemen",
      repeatEvery: "Herhalen iedere",
      reportBugNote: "Probleem omschrijving",
      reportBugPhoneNumber: "Telefoonnummer",
      searchAddressbookToSubscribe: "Zoek een adresboek om op in te schrijven",
      searchCalendarToSubscribe: "Zoek een agenda om op in te schrijven",
      searchIn: "Zoeken",
      selectCalendar: "Agenda kiezen",
      selectIdentity: "Identiteit kiezen",
      selectList: "Lijst kiezen",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "Met wie wil je het adresboek delen?",
      shareToCalendar: "Met wie wil je de agenda delen?",
      shareToFolder: "Met wie wil je de map delen?",
      showImages: "Laat afbeeldingen zien",
      signatureName: "Weergavenaam",
      signatureText: "Handtekening tekst",
      startDate: "Startdatum",
      team: "Team",
      theme: "Thema",
      timeFormat: "Tijdsindeling",
      timezone: "Tijdzone",
      title: "Titel",
      to: "To",
      untilDate: "Einddatum",
      weekStart: "Eerste dag van de week",
      work: "Werk"
    },
    placeholder: {
      addressbookName: "Naam van het adresboek",
      birthday: "Verjaardag",
      city: "Stad",
      country: "Land",
      company: "Bedrijf",
      email: "E-mail",
      firstName: "Voornaam",
      lastName: "Achternaam",
      members: "Leden",
      newFolderName: "Nieuwe mapnaam",
      newNameAddressbook: "Nieuwe naam voor het adresboek",
      newPassword: "Ten minste 8 alfanumerieke tekens",
      note: "Notitie",
      phone: "Telefoon",
      postCode: "Postcode",
      region: "Regio",
      role: "Rol",
      select: "Selecteren",
      search: "Zoeken in:",
      searchActivity: "Activiteit zoeken",
      searchEvent: "Gebeurtenis zoeken",
      snooze: "Uitstellen",
      street: "Straat"
    },
    hint: {
      cancelSendMessage:
        "Select the time available to retract a message during sending, if you decide you don't want to send the email."
    }
  },

  labels: {
    attendees: "Deelnemers",
    default: "Default",
    forDomain: "On domain",
    withAllUsers: "Met alle gebruikers",
    calendarAdvancedSearch: "Waar wil je zoeken?",
    clearAdvancedFilters: "Filters leegmaken",
    clearFields: "Velden leegmaken",
    holidayCalendar: "Holiday",
    searchResults: "Zoekresultaten"
  },

  loaderFullscreen: {
    description: "De inhoud wordt geladen...",
    descriptionFolder: "Je mappen worden geladen...",
    title: "Laden..."
  },
  mail: {
    confirmMailLabel: "Bevestigen",
    contextualMenuLabels: {
      addAddressBook: "Toevoegen aan adresboek",
      addEvent: "Gebeurtenis toevoegen",
      alarm: "Melding",
      backTo: "Terug",
      cancel: "Annuleren",
      close: "Sluiten",
      confirm: "Bevestigen",
      copyIn: "Kopiëren naar",
      copyThreadIn: "Conversatie kopiëren ",
      delete: "Verwijderen",
      deletePerm: "Permanent verwijderen",
      deleteThread: "Conversatie verwijderen",
      deliveryNotification: "Leveringsbevestiging",
      download: "Downloaden",
      editAsNew: "Als nieuw bericht bewerken",
      emptySpam: "Markeren als spam",
      emptyTrash: "Prullenbak legen",
      forward: "Doorsturen",
      import: "Mail importeren",
      label: "Label",
      labelsManagement: "Labels beheren",
      labelThread: "Conversatie labelen",
      markAllAsRead: "Alles markeren als gelezen",
      markAs: "Markeren als",
      markThreadAs: "Conversatie markeren",
      manageFolders: "Mappen beheren",
      modify: "Bewerken",
      more: "Meer",
      moveThreadTo: "Conversatie verplaatsen",
      moveTo: "Verplaatsen naar",
      newSubFolder: "Submap toevoegen",
      plainText: "Platte tekst",
      print: "Afdrukken",
      priorityHigh: "Hoge prioriteit",
      readed: "Gelezen",
      readedNot: "Ongelezen",
      readingNotification: "Leesbevestiging",
      rename: "Hernoemen",
      reply: "Antwoorden",
      replyToAll: "Allen beantwoorden",
      restore: "Herstellen",
      restoreThreadIn: "Conversatie herstellen",
      restoreIn: "Herstellen in",
      sendAsAttachment: "Doorsturen als bijlage",
      showSourceCode: "Broncode tonen",
      spam: "Spam",
      spamNot: "Geen spam",
      starred: "Gemarkeerd",
      starredNot: "Niet gemarkeerd",
      thanks: "Bedankt",
      thread: "Acties"
    },
    deleteMailDsc: "Als je geen ander opslagsysteem hebt wordt het bericht permanent verwijderd.",
    deleteMailTitle: "Bericht verwijderen",
    deleteMailsDsc: "Na 30 dagen worden de berichten in deze map automatisch verwijderd.",
    deleteMailsTitle: "Geselecteerde berichten verwijderen",
    dropFiles: "Sleep bestanden om toe te voegen hiernaartoe",
    emptyTrashDsc: "Na 30 dagen worden de berichten in deze map automatisch verwijderd",
    emptyTrashTitle: "Prullenbak legen",
    import: {
      messagesInvalid: "Niet geldig:",
      messagesNr: "Totaal aantal berichten om te importeren:",
      messagesValid: "Goedgekeurd:",
      desc:
        "Je importeert berichten in de map: <code>{{folderImport}}</code><br/>Je kunt berichten van andere mailprogramma's importeren door een .eml bestand te gebruiken (<strong>.eml</strong>).",
      dropFiles: "Upload or drop here the file to be imported",
      error: "Berichten konden niet worden geimporteerd",
      importing:
        "We zijn <b>{{messagesNumber}}</b> berichten aan het importeren in de map folder: <code>{{folderImport}}</code>",
      loading: "Laden...",
      success: "Importeren gelukt",
      title: "Berichten importeren",
      uploadedFile: "Geupload",
      uploadingFile: "Uploaden..."
    },
    markSpamReadDsc:
      "Alle berichten in deze map markeren als gelezen. Berichten in deze map worden na 30 dagen automatisch verwijderd.",
    markSpamReadTitle: "Berichten markeren als gelezen",
    maxLabelsNumberTitle: "Te veel labels!",
    maxLabelsNumberDsc:
      "Je hebt het maximale aantal labels bereikt dat aan een bericht kan worden gekoppeld. Om een nieuw label toe te voegen moet je eerst een van de reeds aanwezige labels verwijderen.",
    message: {
      add: "Add",
      addToAddressBook: "Contact toevoegen aan adresboek",
      attachedMessage: "Toegevoegd bericht",
      attachments: "bijlagen",
      bugMsg: "Kun je het bericht niet zien?",
      cc: "Cc:",
      date: "Date:",
      draftsSelectedInfo: "Kies een bericht en ga door met schrijven.",
      downloadAll: "Alles downloaden",
      downloadAttachments: "Bijlagen downloaden",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Aanwezigheid veranderen",
      eventActionAccept: "Accepteren",
      eventActionDecline: "Weigeren",
      eventActionTentative: "Misschien",
      eventAddToCalendar: "Deze gebeurtenis staat niet in je agenda",
      eventAttendees: "Aanwezigen:",
      eventAttendeeStatusAccepted: "Geaccepteerd",
      eventAttendeeStatusDeclined: "Geweigerd",
      eventAttendeeStatusTentative: "Voorlopig aanwezig",
      eventCounterProposal: "Dit bericht bevat een tegenvoorstel voor de gebeurtenis",
      eventDeleted: "Gebeurtenis verwijderd",
      eventInvitation: "Uitnodiging:",
      eventLocationEmpty: "Niet bekend",
      eventModify: "Bewerken",
      eventNeedAction: "Dit bericht bevat een nieuwe gebeurtenis",
      eventNotNeedAction:
        "Dit bericht bevat een uitnodiging voor een gebeurtenis die je al hebt beantwoord",
      eventOutDated: "Deze gebeurtenis is bewerkt",
      eventStatusAccepted: "Geaccepteerd",
      eventStatusDeclined: "Geweigerd",
      eventStatusTentative: "Voorlopig",
      eventUpdate: "Gebeurtenis bijwerken",
      folderSelectedInfo: "Kies een bericht en zie de inhoud.",
      from: "From:",
      hideAllAttachments: "Verbergen",
      hugeMessage:
        "Content too big to display entirely, click the link below to view the message source code.",
      hugeMessageNotice: "Message truncated",
      inboxSelectedInfo: "Selecteer een bericht uit de lijst en zie wie de afzender is.",
      loadImages: "Afbeeldingen downloaden",
      localAttachments: "There are additional attachments inside the event",
      message: "bericht",
      messageForwarded: "Doorgestuurd bericht",
      messages: "berichten",
      messagesQuota: "Used messages quota:",
      newMessageinThread: "Er is een nieuw bericht",
      notSelected: "Selecteer een bericht",
      oneSelectMessages: "Geselecteerd bericht",
      oneSelectThreads: "Geselecteerde conversatie",
      readNotificationIgnore: "Negeren",
      readNotificationLabel: "{{Sender}} vraagt een leesbevestiging voor dit bericht.",
      readNotificationLabelSimple: "Leesbevestiging gevraagd",
      readNotificationResponse:
        "<P>Bericht<BR><BR>&nbsp;&nbsp;&nbsp; A:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Onderwerp:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Verzonden:&nbsp; %SENDDATE%<BR><BR>is gelezen op %READDATE%.</P>",
      readNotificationSend: "Bevestigen",
      readNotificationSubjectPrefix: "Lees:",
      replyTo: "Reageren op:",
      sender: "Sender:",
      sentSelectedInfo: "Kies een bericht uit de lijst en zie wie de afzender is.",
      showAllAttachments: "Meer tonen",
      showMoreThread: "Meer tonen",
      selectMessages: "Geselecteerde berichten",
      selectThreads: "Geselecteerde conversaties",
      spamNotSelected: "Map controleren",
      spamSelectedInfo: "Berichten controleren en spam als gelezen markeren.",
      subject: "Subject:",
      to: "Aan:",
      trashNotSelected: "Controleer de prullenbak",
      trashSelectedInfo: "Je kan binnen 30 dagen berichten uit de prullenbak halen.",
      updating: "Bezig met bewerken",
      yesterday: "Gisteren",
      wrote: "wrote:"
    },
    messagesList: {
      allMessagesLoaded: "Alle berichten zijn ingeladen",
      ctaSpamInfo: "Alles als gelezen markeren",
      ctaTrashInfo: "Prullenbak is leeg",
      folderEmpty: "Dez map is leeg",
      folderInfo: "Start een gesprek of verplaats een bericht.",
      foundMessages: "Gevonden",
      moveMessage: "Bericht verplaatsen",
      moveMessages: "{{NumMessages}} berichten verplaatsen",
      notFound: "Geen berichten gevonden",
      notFoundInfo:
        "De zoekopdracht heeft geen resultaten opgeleverd, probeer het opnieuw op met andere criteria.",
      pullDownToRefresh: "Sleep om te verversen",
      releaseToRefresh: "Laat los om te verversen",
      searchInfo: "berichten gevonden",
      selectMessages: "Geselecteerd",
      showMoreThread: "Meer tonen",
      spamEmpty: "Geen spam aanwezig!",
      spamInfo: "Berichten gemarkeerd als spam worden na 30 dagen automatisch verwijderd.",
      trashEmpty: "De prullenbak is leeg.",
      trashInfo: "Berichten in de prullenbak worden na 30 dagen automatisch verwijderd.",
      totalMessages: "Berichten"
    },
    newMessage: {
      addAttach: "Bijlagen toevoegen",
      addImgeDsc:
        "Je kan afbeeldingen direct inline invoegen, in de tekst van het bericht of ze bij het bericht voegen als bijlage.",
      addImgeTitle: "Afbeelding toevoegen",
      advanced: "Geavanceerd",
      bcc: "Bcc",
      cc: "Cc",
      cancelSend: "Sending mail successfully canceled",
      delete: "Verwijderen",
      deleted: "Bericht verwijderd",
      deleteInProgress: "Bericht aan het verwijderen...",
      discardChanges: "Discard changes",
      fontSize: {
        smaller: "Smaller",
        small: "Small",
        normal: "Normal",
        medium: "Medium",
        big: "Big",
        bigger: "Bigger"
      },
      from: "Van",
      hide: "Verbergen",
      invalidMails: "Ongeldig e-mailadres gevonden. Controleer het adres en probeer het opnieuw.",
      moreAttachments: "Overigen",
      noDraftChanged: "Geen wijzigingen om op te slaan",
      object: "Onderwerp",
      saveDraftDsc:
        "Het bericht is nog niet verzonden en bevat niet-opgeslagen wijzigingen. Je kan het bericht als concept opslaan en later verder schrijven.",
      saveDraftTitle: "Wil je het bericht opslaan als concept?",
      savedDraft: "Concept opgeslagen",
      savingDraft: "Aan het opslaan...",
      sendInProgress: "Aan het verzenden...",
      sent: "Bericht succesvol verzonden",
      sizeExceeded: "Bijlagen zijn te groot. Het maximum is {{messageMaxSize}} MB.",
      to: "Aan",
      toMore: "Anderen",
      windowSubject: "Nieuw berichten"
    },
    reportBugMessage:
      "Wanneer je het bericht niet correct ziet kun je ons helpen het probleem op te lossen. Mogelijk moeten we daarvoor contact met je opnemen. Vul hieronder je telefoonnummer in en beschijf het probleem.",
    reportBugTitle: "Report message",
    searchMail: {
      attachments: "Bijlages",
      cleanFilters: "Filters uitschakelen",
      marked: "Gemarkeerd"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Label maken"
      },
      createFolder: {
        desc: "",
        title: "Map maken"
      },
      modifyFolder: {
        desc: "",
        title: "Map bewerken"
      },
      restoreFolder: {
        desc: "",
        title: "Map herstellen"
      },
      folder: "Mappen",
      folders: "Persoonlijke mappen",
      labels: "Labels",
      newFoderName: "Nieuwe map",
      occupiedSpace: "Gebruikte ruimte",
      sharedFolders: "Gedeelde mappen",
      unlimitedSpace: "Onbeperkte ruimte"
    },
    thanksMailLabel: "Oké, bedankt!",
    tooManyCompositionsDsc:
      "Je hebt het maximale aantal nieuwe berichten dat je tegelijkertijd kunt schrijven bereikt. Verstuur ten minste één van de berichten om een nieuw bericht te kunnen starten.",
    tooManyCompositionsTitle: "Maak een bericht af"
  },

  mailboxes: {
    drafts: "Concepten",
    inbox: "Inbox",
    more: "Meer",
    sent: "Verzonden",
    shared: "Gedeelde mappen",
    spam: "Spam",
    trash: "Prullenbak"
  },

  navbar: {
    addressbook: "Contacten",
    calendar: "Agenda",
    chat: "Chat",
    mail: "Mail",
    meeting: "Vergadering",
    news: "News"
  },

  newsTags: {
    news: "News",
    suggestions: "Tips",
    fix: "Fix",
    comingSoon: "Coming soon",
    beta: "Beta"
  },

  pageLogout: {
    title: "Bezig met uitloggen...",
    desc: ""
  },

  pageIndexing: {
    title: "Je account wordt geïndexeerd",
    desc: "Dit kan even duren..."
  },

  pageNotFound: {
    pageNotFound: "Er ging iets mis",
    pageNotFoundInfo: "De pagina die je zoekt kan niet worden gevonden"
  },

  powered: "powered by MailKing24",
  poweredCollaborationWith: "in samenwerking met",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Ontvangstbevestiging vragen",
      askReadConfirmation: "Leesbevestiging vragen",
      defaultView: "Standaard lay-out",
      newEventType: "Nieuw gebeurtenis categorie",
      newEventFreeBusy: "Toon beschikbaarheid voor nieuwe gebeurtenissen"
    }
  },

  shareOptions: {
    delete: "Verwijderen",
    read: "Lezen",
    update: "Bewerken",
    write: "Schrijven"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Sta het invoegen van gebeurtenissen in deze agenda toe",
    canEraseObjects: "Sta het verwijderen van gebeurtenissen in deze agenda toe"
  },

  shortcuts: {
    addressbook: {
      newContact: "Nieuw contact",
      editContact: "Contact bewerken",
      deleteContact: "Contact verwijderen"
    },
    calendar: {
      newEvent: "Nieuwe gebeurtenis",
      editEvent: "Gebeurtenis bewerken",
      deleteEvent: "Gebeurtenis verwijderen",
      moveRangeNext: "Volgende periode bekijken",
      moveRangePrevious: "Vorige periode bekijken"
    },
    general: {
      switchModuls: "Wissel modules",
      refresh: "Pagina verversen",
      tabForward: "Ga vooruit tussen de aanklikbare elementen",
      tabBack: "Verplaats terug tussen de aanklikbare elementen",
      closeModal: "Venster sluiten",
      openSettings: "Open instellingen",
      copy: "Geselecteerd item kopiëren",
      past: "Geselecteerd item plakken",
      cut: "Geselecteerd item knippen"
    },
    mail: {
      confirm: "Bevestigen",
      deleteMessage: "Bericht verwijderen",
      deleteSelectMessage: "Gekozen bericht verwijderen",
      editAsNew: "Bewerken als nieuw bericht",
      empityTrash: "Prullenbak leegmaken",
      expandThread: "Gesprek uitklappen",
      extendSelectDown: "Selectie naar onder uitbreiden",
      extendSelectUp: "Selectie naar boven uitbreiden",
      forward: "Doorsturen",
      markRead: "Markeren als gelezen/ongelezen",
      moveDown: "Omlaag verplaatsen",
      moveUp: "Omhoog verplaatsen",
      newMessage: "Nieuw bericht",
      openSelectMessage: "Open gekozen bericht",
      printMessage: "Bericht afdrukken",
      reduceThread: "Gesprek verkleinen",
      reply: "Beantwoorden",
      replyAll: "Beantwoord alle",
      thanks: "Bedankt"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Automatisch opslaan"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Adresboek",
          placeholderList: " ",
          title: "Persoonlijk adresboek"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Geabonneerd adresboek",
          placeholderList: " ",
          title: "Gedeeld adresboek"
        },
        title: "Adresboeken"
      },
      title: "Contacten",
      view: {
        desc: "",
        title: "Weergeven"
      }
    },
    calendar: {
      activities: {title: "Activiteiten"},
      events: {title: "Gebeurtenissen", desc: "", alertTitle: "Meldingen", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Uitnodigingen beheren",
        msgFromHeader: "Alleen uitnodigen toestaan van",
        placeholderList: " "
      },
      labels: {title: "Labels", desc: ""},
      labelsTask: {title: "Task labels", desc: ""},
      management: {
        title: "Agenda's",
        holidayCalendars: {
          title: "Vakantie agenda's",
          desc: "",
          headerItem: "Agenda's",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Persoonlijke agenda's",
          desc: "",
          headerItem: "Agenda's",
          placeholderList: " "
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Agenda inschrijvingen",
          placeholderList: " ",
          title: "Gedeelde agenda's"
        },
        includeInFreeBusy: "Laat beschikbaar/bezet zien",
        showActivities: "Laat activiteiten zien",
        showAlarms: "Laat alarmen zien",
        syncWithActivesync: "Synchroniseren met behulp van ActiveSync",
        sendMeMailOnMyEdit: "Stuur mij een e-mail als ik de kalender wijzig",
        sendMeMailOnOtherEdit: "Stuur mij een e-mail wanneer iemand de kalender wijzigt",
        sendMailOnEditTo: "Wanneer ik wijziginen aanbreng een mail sturen naar"
      },
      view: {
        showBusyOutsideWorkingHours: "Toon als bezet buiten werktijd",
        showWeekNumbers: "Laat weeknummer zien",
        title: "Weergeven"
      },
      title: "Agenda"
    },
    chat: {
      title: "Chat",
      view: {
        title: "Weergeven"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "E-mail van de gebruiker",
        generalSettings: {
          desc: "",
          title: "Algemene instellingen"
        },
        personalInfo: {
          desc: "",
          title: "Persoonlijke gegevens"
        },
        title: "Profiel"
      },
      security: {
        changepassword: {
          info: "Wachtwoord van ten minste 8 alfanumerieke tekens",
          desc: "",
          resetpassword: "Nieuw wachtwoord instellen",
          resetpassworddisabled: "Je kunt het wachtwoord voor dit account niet wijzigen",
          resetpasswordexpired: "Je wachtwoord is verlopen, maak een nieuwe",
          title: "Wachtwoord wijzigen"
        },
        otp: {
          cantDisableOtp:
            "Tweevoudige authenticatie kan niet uitgeschakeld worden voor dit account",
          desc: "",
          disableOtp: "Tweevoudige authenticatie uitschakelen",
          hideParams: "Parameters verbergen",
          instructionDisableOtp:
            "Voer de gegenereerde code in om tweevoudige authenticatie uit te schakelen.",
          instructionsSetOtp:
            "Scan de QRcode met de applicatie en voer de gegenereerde code hieronder in. Of voer de parameters voor handmatige activering in.",
          labelEnterCode: "Vul code in die gegenereerd is door de applicatie",
          paramAccount: "Account",
          paramCode: "Code",
          paramType: "Tijdgevoelig",
          paramTypeVal: "Actief",
          params: "Code handmatig invullen",
          paramsInstruction: "Voer de volgende waardes in",
          setOtp: "Tweevoudige authenticatie aanzetten",
          title: "OTP instellen",
          titleDisableOtp: "OTP uitzetten",
          titleSetOtp: "OTP aanzetten"
        },
        sessions: {
          browser: "Browser:",
          currentSession: "Huidig",
          device: "Apparaat:",
          desc: "",
          ip: "IP:",
          os: "OS:",
          removeAll: "Alles verwijderen",
          removeSession: "Verwijderen",
          startdate: "Start:",
          title: "Actieve sessies"
        },
        title: "Beveiliging"
      },
      title: "Algemeen"
    },
    mail: {
      accounts: {desc: "", title: "Accounts"},
      antispam: {
        blacklist: {
          desc: "Voeg adres of domein toe aan de lijst (es. 'name@domain' or '*@domain')",
          msgNoHeader: "Berichten wijgeren van",
          title: "Blacklist"
        },
        deleteRuleDsc: "Regel verwijderen: <code>{{ruleEmail}}</code>.",
        deleteRuleTitle: "Antispam-regel verwijderen",
        placeholderList: " ",
        title: "Antispam",
        whitelist: {
          desc: "Voeg adres of domein toe aan de lijst (es. 'name@domain' or '*@domain')",
          msgFromHeader: "Berichten toestaan van",
          msgToHeader: "Berichten toestaan naar",
          title: "Whitelist"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Bericht",
        placholdeSubject: "Onderwerp",
        placholderTextarea: "Bericht inhoud",
        title: "Autoresponder"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Cancel send email"
        },
        confirmations: {
          desc: "",
          title: "Lees/ontvangst bevestiging"
        },
        dropFiles: "Sleep de afbeelding hiernaartoe om de afbeelding toe te voegen",
        placeholderList: " ",
        senderBcc: "Zender toevoegen in BCC",
        signatures: {
          desc: "",
          headerItemSignature: "Weergavenaam",
          headerItemIdentity: "Identiteit",
          title: "Handtekeningen"
        },
        title: "Samenstelling"
      },
      delegations: {desc: "", title: "Delegeren"},
      filters: {
        actionsTitle: "Acties",
        activeFiltersTitle: "Actieve filters",
        desc: "Filters op inkomende berichten beheren",
        headerItem: "Actieve filters",
        parametersTitle: "Parameters",
        placeholderList: {
          parameter: "Parameter toevoegen aan het filter",
          action: "Actie toevoegen aan het filter"
        },

        title: "Filters"
      },
      folders: {
        deleteFolderDsc:
          "Wanneer je de map <code>{{folderName}}</code> verwijderd worden ook alle berichten in de map verwijderd.<br/><b>Dit is onomkeerbaar</b>.",
        deleteFoldersTitle: "Mappen verwijderen",
        desc: "Beheer en deel je persoonlijke mappen",
        descShared: "Gedeelde mappen beheren",
        headerItem: "Mappen",
        headerMsg: "Berichten",
        headerUnread: "Ongelezen",
        modifyFolder: "Folder aanpassen",
        newFolder: "Nieuwe map toevoegen",
        rootFolder: "-- Hoofdmap --",
        subscribe: "Inschrijven",
        title: "Mappen",
        titleShared: "Gedeelde mappen"
      },
      forward: {
        description: "Voeg maximaal {{max}} ontvangers toe.",
        headerItem: "Ontvangers",
        placeholderList: " ",
        title: "Doorsturen",
        titleSection: "Berichten doorsturen"
      },
      labels: {
        desc: "",
        headerItem: "Labels",
        deleteLabelTitle: "label verwijderen",
        deleteLabelTitleDsc: "Het verwijderen van labels verwijdert deze uit alle berichten.",
        placeholderList: " ",
        title: "Labels"
      },
      reading: {
        layout: {
          desc: "",
          title: "Lay-out"
        },
        readingSettings: {
          desc: "",
          title: "Leesinstellingen"
        },
        title: "Weergeven"
      },
      signature: {
        title: "Signature",
        default: "Default",
        dropFiles: "Drop here the image to add",
        management: {
          title: "Signature management"
        },
        onlyNewMessages: "Don't add signature on replies",
        placeholderList: " ",
        senderBcc: "Add sender in Bcc",
        setAsDefault: "Set as default sender",
        signatures: {
          desc:
            "Customize your email by choosing the name displayed, who receives the message, and setting a signature to add on all your messages.<br/>You can customize the display name and signature for all identities (alias groups) associated with your account.",
          headerItemSignature: "Display name",
          headerItemIdentity: "Identity",
          title: "Signatures and identities"
        }
      },
      title: "E-mail"
    },
    meeting: {
      title: "Vergadering",
      view: {
        desc: "",
        title: "Weergeven"
      }
    },
    news: {
      readMore: "Read more",
      title: "Webmail news"
    },
    shortcuts: {
      title: "Sneltoetsen"
    },
    title: "Instellingen",
    unsaved: "Er zijn niet-opgeslagen wijzigingen"
  },

  toast: {
    error: {
      alreadyPresent: "Waarschuwing, de bron bestaat al",
      avatarTooBig: "Waarschuwing, de avatar is te groot",
      compact: "Fout bij verkleinen van de map",
      connectionUnavailable: "Geen verbinding beschikbaar",
      delete: "Fout bij verwijderen",
      deleteDraft: "Fout bij het verwijderen van het concept",
      enqueued: "Instellingen worden bijgewerkt, probeer het later nog eens",
      fetchingAttachments: "Fout bij het ophalen van bijlages",
      filterActionsMissing: "Acties niet aanwezig",
      filterConditionsMissing: "Parameters ontbreken",
      filterNameMissing: "Filternaam ontbreekt",
      folderCreation: "Fout bij het aanmaken van een nieuwe map",
      folderCreationCharNotValid:
        "Fout bij het maken/hernoemen van een map, karakter niet geldig (/)",
      folderCreationNoName: "Fout bij het maken van de map, geef een geldige naam op",
      folderDelete: "Fout bij het verwijderen van de map",
      folderNotPresent: "Map niet aanwezig",
      folderRename: "Fout bij het hernoemen van de map, de gekozen naam komt al voor",
      folderSubscribe: "Er ging iets fout bij het inschrijven",
      generic: "Er ging iets mis",
      maxMembers: "Waarschuwing, maximaal aantal leden bereikt",
      messageCopy: "Fout bij het kopiëren van berichten",
      messageMove: "Fout bij het verplaatsen van berichten",
      messageNotFound: "Bericht niet gevonden",
      messageRemove: "Fout bij het verwijderen van berichten",
      missingParameters: "Ontbrekende parameters",
      notifications: "Fout bij het ontvangen van meldingen",
      notPermitted: "Bediening niet toegestaan",
      notValid: "Waarschuwing, ongeldige waarde",
      onlyOneHomeAddress: "Je kunt slechts één thuisadres invoeren",
      onlyOneWorkAddress: "Je kunt slechts één werkadres invoeren",
      save: "Fout bij het opslaan",
      saveAcl: "Fout bij het opslaan van mapmachtigingen",
      savingDraft: "Concept wordt opgeslagen",
      sendingMessage: "Fout bij het afleveren van berichten",
      serverDisconnection: "Server wordt bijgewerkt, probeer het later nog eens!",
      sessionExpired: "Sessie verlopen",
      smtpBlockedByAntispam: "Message not sent: blocked by antispam",
      smtpBlockedByAntivirus: "Message not sent: blocked by antivirus",
      smtpDisabled: "Message not sent: SMTP disabled on this account",
      smtpQuotaExceeded: "Bericht niet verzonden: verzonden bericht quota overschreden.",
      requestLimit: "Maximum aantal aanvragen overschreden, probeer het later nog eens.",
      requestError: "Er is een fout opgetreden, probeer het later nog eens.",
      updateFolders: "Er is een fout opgetreden bij het bijwerken van mappen",
      updateMessageEvent: "Fout bij het updaten van de status van het evenement",
      updatingParentFlags: "Fout bij het bijwerken van berichtenvlaggen"
    },
    info: {
      newVersionAvailable: {
        desc: "This operation is safe, your settings will not be modified.",
        title:
          "New features are available, reload the page to update the webmail to the lastest version."
      },
      serverReconnected: "Webmail weer online",
      sourceMail: "E-mailbron gekopieerd naar klembord"
    },
    progress: {
      newFolder: "Er worden nieuwe mappen aangemaakt...",
      operationInProgress: "In uitvoering..."
    },
    success: {
      compact: "Map met succes gecomprimeerd",
      copiedLink: "Link gekopieerd naar klembord",
      copiedMail: "Bericht succesvol gekopieerd",
      copiedMailAddress: "E-mailadres gekopieerd naar klembord",
      createFolder: "Folder is gemaakt",
      delete: "Succesvol verwijderd",
      disabledOtp: "OTP succesvol uitgezet",
      enabledOtp: "OTP succesvol aangezet",
      movedMail: "Bericht is verplaatst",
      otpDisabled: "OTP succesvol uitgezet",
      otpEnable: "OTP succesvol aangezet",
      save: "Succesvol opgeslagen",
      saveAcl: "Toestemmingen succesvol opgeslagen",
      saveDraft: "Map succesvol bijgewerkt",
      saveFolder: "Folder updated successfully",
      sentMail: "Bericht succesvol verzonden",
      subscribeFolder: "Abonnement succesvol bijgewerkt",
      updateMessageEvent: "Gebeurtenisstatus bijgewerkt"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "Hele dag",
      autoSaveContact:
        "Sla automatisch alle ontvangers op die niet in je adresboeken aanwezig zijn.",
      enableAutorespopnd: "Autoresponder inschakelen",
      enableAutorespopndInterval: "Datumbereik inschakelen",
      keepCopy: "Bewaar een lokale kopie van het bericht",
      showPriority: "Toon ontvangen berichtprioriteit",
      showThreads: "Toon berichten als conversatie",
      denyInvites: "Uitnodigingen voor afspraken voorkomen",
      enableDateSearch: "Zoeken op datum inschakelen",
      enableFullBody: "Zoeken op inhoud inschakelen",
      subscribeFolder: "Abonneer op map"
    }
  },

  wizard: {
    layout: {
      desc: "Customize your Webmail by choosing view settings that you prefer.",
      title: "View options"
    },
    wizardData: {
      desc: "",
      title: "Profiel voltooien"
    }
  }
};
