/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Contatto selezionato",
      notSelected: "Seleziona un contatto",
      sections: {
        cell: " Cellulare",
        home: "Casa",
        homeFax: "Fax casa",
        work: "Lavoro",
        workFax: "Fax lavoro"
      },
      selectContacts: "Contatti selezionati",
      selectedInfo: "Scegli un contatto dalla lista per visualizzare le informazioni."
    },
    contactsList: {
      emptyContacts: "Questa rubrica è vuota",
      emptyContactsInfo: "Aggiungi un contatto alla rubrica",
      foundContacts: "Trovati",
      notFound: "Nessun contatto trovato",
      notFoundInfo: "La ricerca non ha prodotto risultati, riprova utilizzando criteri diversi.",
      totalContacts: "Contatti"
    },
    contextualMenuLabels: {
      address: "Indirizzo",
      birthday: "Compleanno",
      cancel: "Annulla",
      delete: "Elimina",
      downloadVCard: "Download VCard",
      email: "Email",
      newContact: "Nuovo contatto",
      newList: "Nuova lista",
      note: "Note",
      phone: "Telefono",
      shareVCard: "Condividi",
      showVCard: "Visualizza VCard",
      sendAsAttachment: "Invia per email",
      sendMail: "Invia email",
      url: "Sito"
    },
    deleteContactDsc:
      "Eliminando il contatto <code>{{contact}}</code> tutte le informazioni relative saranno cancellate.<br/><b>L'operazione non è reversibile</b>.",
    deleteContactsDsc:
      "Eliminando i contatti selezionati tutte le informazioni relative saranno cancellate.<br/><b>L'operazione non è reversibile</b>.",
    deleteContactTitle: "Elimina contatto",
    deleteContactDscList:
      "Eliminando il contatto selezionato tutte le informazioni relative saranno cancellate.<br/><b>L'operazione non è reversibile</b>.",
    deleteContactsTitle: "Elimina contatti",

    deleteListDsc:
      "Eliminando la lista <code>{{contact}}</code> tutte le informazioni relative saranno cancellate.<br/><b>L'operazione non è reversibile</b>.",
    deleteListTitle: "Elimina lista",

    editContactModal: {
      title: "Modifica contatto"
    },
    editListModal: {
      title: "Modifica lista"
    },
    exportAddressBookModal: {
      desc:
        "Stai esportando la rubrica: <code>{{addressbookExport}}</code><br/>Scegli il formato con cui esportare tutti i contatti presenti",
      title: "Esporta rubrica"
    },
    import: {
      contactsInvalid: "Non validi:",
      contactsNr: "Totale contatti da importare:",
      contactsValid: "Validi:",
      desc:
        "Stai importando dei contatti nella rubrica: <code>{{addressbookImport}}</code><br/>È possibile importare contatti da altre app di posta utilizzando un file in formato vCard (<strong>.vcf</strong>) o <strong>.ldif</strong>.\u000AAd esempio, puoi esportare i contatti da Gmail o da Outlook e importarli in Qboxmail.\u000AL'importazione non sostituirà nessuno dei contatti già esistenti.",
      dropFiles: "Carica o rilascia qui il file da importare",
      error: "Non è stato possibile importare i contatti",
      importing:
        "Stiamo importando <b>{{contactsNumber}}</b> contatti nella rubrica: <code>{{addressbookImport}}</code>",
      loading: "Caricamento...",
      success: "Importazione avvenuta con successo!",
      title: "Importa contatti",
      uploadedFile: "Caricato",
      uploadingFile: "Caricamento"
    },
    list: {
      desc:
        "È possibile aggiungere fino a <b>150 membri</b> alla lista scegliendo tra i contatti presenti nella rubrica <code>{{addressbookName}}</code>.",
      members: "Membri lista"
    },
    messages: {
      newContactAddressBookBody:
        "Non è possibile inserire un contatto su <b>Tutti i contatti</b> o su <b>Rubrica aziendale</b>.<br/>Seleziona una rubrica personale o condivisa.",
      newContactAddressBookTitle: "Seleziona rubrica",
      resetListBody:
        "I membri della lista sono collegati alla rubrica selezionata. La modifica porterà alla cancellazione dei contatti inseriti. Proseguire ?",
      resetListTitle: "Cancellazione membri"
    },
    newAddressBookModal: {
      desc: "",
      title: "Crea / Modifica rubrica"
    },
    newContactModal: {
      title: "Crea contatto"
    },
    newListModal: {
      title: "Crea lista"
    },
    shareModal: {
      desc:
        "Stai condividendo la rubrica: <code>{{addressbookShare}}</code><br/>Seleziona i contatti con cui vuoi condividere la rubrica e assegna loro dei permessi di gestione.",
      title: "Condividi rubrica"
    },
    sidebar: {
      allContacts: "Tutti i contatti",
      deleteAddressbookDsc:
        "Eliminando la rubrica <code>{{addressbook}}</code> tutti i contatti presenti saranno cancellati.<br/><b>L'operazione non è reversibile</b>.",
      deleteAddressbookTitle: "Eliminare rubrica",
      globalAddressbook: "Rubrica aziendale",
      personalAddressbook: "Rubrica personale",
      sharedAddressbooks: "Rubriche condivise",
      userAddressbooks: "Rubriche utente"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "Non ci sono rubriche da sottoscrivere",
      title: "Sottoscrivi rubrica"
    }
  },

  app: {
    addressbook: "Rubrica",
    calendar: "Calendario",
    chat: "Chat",
    meeting: "Meeting",
    of: "di",
    webmail: "Webmail"
  },

  avatarEditorModal: {
    photoTitle: "Scatta foto",
    title: "Modifica avatar",
    zoom: "Zoom -/+"
  },

  buttons: {
    accept: "Accetta",
    add: "Aggiungi",
    addAction: "Aggiungi azione",
    addAddressbook: "Rubrica",
    addAsAttachment: "Allega",
    addCalendar: "Calendario",
    addField: "Aggiungi campo",
    addHolidayCalendar: "Calendario festività",
    addLabel: "Aggiungi etichetta",
    addInline: "Inserisci in linea",
    addParameter: "Aggiungi parametro",
    addSignature: "Aggiungi firma",
    addToList: "Aggiungi alla lista",
    apply: "Applica",
    backTo: "Indietro",
    backAddressbook: "Chiudi",
    cancel: "Annulla",
    changeImage: "Cambia immagine",
    changePassword: "Cambia password",
    checkFreeBusy: "Verifica disponibilità",
    close: "Chiudi",
    createFolder: "Nuova cartella",
    comeBackLater: "Torna più tardi",
    confirm: "Conferma",
    copyLink: "Copia link",
    decline: "Rifiuta",
    delete: "Elimina",
    deleteDraft: "Elimina bozza",
    details: "Dettagli",
    disabled: "Disabilitata",
    disableOTP: "Disattiva OTP",
    download: "Scarica",
    editLabel: "Modifica etichetta",
    editSignature: "Modifica firma",
    empty: "Svuota",
    enabled: "Abilitata",
    event: "Evento",
    export: "Esporta",
    goToMail: "Torna alla mail",
    goToOldWebmail: "Vai alla vecchia webmail",
    hide: "Nascondi",
    import: "Importa",
    label: "Etichetta",
    leftRotate: "Ruota a sinistra di 90 gradi",
    loadImage: "Carica immagine",
    logout: "Logout",
    makePhoto: "Scatta foto",
    markRead: "Letto",
    markSeen: "Marca",
    markUnread: "Non letto",
    message: "Messaggio",
    modify: "Modifica",
    new: "Nuovo",
    newContact: "Nuovo contatto",
    newEvent: "Nuovo evento",
    newMessage: "Nuovo messaggio",
    occurrence: "Ricorrenza",
    openMap: "Apri in mappa",
    reloadPage: "Ricarica pagina",
    removeImage: "Rimuovi immagine",
    rename: "Rinomina",
    reorder: "Riordina",
    reorderEnd: "Fine riordina",
    report: "Segnala",
    retryImport: "Ritenta",
    save: "Salva",
    saveDraft: "Salva bozza",
    saveNextAvailability: "Salva disponibilità proposta",
    search: "Cerca",
    send: "Invia",
    series: "Serie",
    setOtp: "Attiva OTP",
    share: "Condividi",
    show: "Mostra",
    showMore: "Altre opzioni",
    showUrlsAddressbook: "Mostra URL rubrica",
    showUrlsCalendar: "Mostra URL calendario",
    snooze: "Posponi",
    starred: "Segna",
    subscribe: "Sottoscrivi",
    subscribeAddressbook: "Sottoscrivi rubrica",
    subscribeCalendar: "Sottoscrivi calendario",
    tentative: "Forse",
    unsetOtp: "Disattiva OTP",
    unsubscribe: "Desottoscrivi",
    update: "Aggiorna",
    updateFilter: "Aggiorna filtro",
    viewMap: "Visualizza mappa"
  },

  buttonIcons: {
    activeSync: "Abilita ActiveSync",
    activeSyncNot: "Rimuovi ActiveSync",
    alignCenter: "Centra",
    alignLeft: "Allinea a sinistra",
    alignRight: "Allinea a destra",
    backgroundColor: "Colore sfondo",
    backTo: "Indietro",
    bold: "Grassetto",
    confirm: "Conferma",
    close: "Chiudi",
    delete: "Elimina",
    download: "Download",
    downloadVCard: "Download VCard",
    edit: "Modifica",
    emoji: "Emoji",
    forward: "Inoltra",
    fullscreen: "Schermo intero",
    fullscreenNot: "Chiudi schermo intero",
    imageAdd: "Inserisci immagine",
    indent: "Aumenta rientro",
    italic: "Corsivo",
    maintenance: "Manutenzione",
    maxmized: "Ingrandisci",
    minimized: "Riduci",
    more: "Altro...",
    moveDown: "Sposta sotto",
    moveUp: "Sposta sopra",
    next: "Successivo",
    orderList: "Elenco numerato",
    outdent: "Diminuisci rientro",
    pin: "Fissa",
    previous: "Precedente",
    print: "Stampa",
    removeFormatting: "Rimuovi formattazione",
    reply: "Rispondi",
    replyAll: "Rispondi a tutti",
    saveDraft: "Salva bozza",
    sendDebug: "Invia messaggio non visualizzabile",
    sendMail: "Scrivi mail",
    share: "Condividi",
    showSidebar: "Mostra / nascondi sidebar",
    showSourceCode: "Mostra sorgente",
    showVCard: "Visualizza VCard",
    spam: "Marca come Spam",
    spamNot: "Marca come No Spam",
    startChat: "Inizia chat",
    startMeeting: "Inizia meeting",
    strike: "Barrato",
    subscribe: "Sottoscrivi",
    textColor: "Colore testo",
    thanks: "Ringrazia",
    underline: "Sottolineato",
    unorderList: "Elenco puntato",
    unSubscribe: "Elimina sottoscrizione"
  },

  calendar: {
    deleteEvent: "Cancellazione evento",
    deleteEventDsc: "Procedere con la cancellazione di questo evento ?",
    event: {
      deleteEventOccurrenceTitle: "Eliminare evento o serie?",
      deleteEventOccurrenceDesc:
        "L'evento che vuoi eliminare fa parte di una serie. Eliminando il singolo evento gli altri presenti nella serie rimarranno invariati.",
      fromDay: "da",
      maxLabelsNumberTitle: "Troppe etichette!",
      maxLabelsNumberDsc:
        "Hai stato raggiunto il numero massimo di etichette associabili a un evento. Per aggiungere una nuova etichetta devi prima ruomevere una di quelle già presenti.",
      organizerTitle: "Creato da",
      pendingMsg: "Evento in attesa di conferma",
      repeatEvent: "Questo evento fa parte di una serie",
      saveEventOccurrenceTitle: "Visualizzare evento o serie?",
      saveEventOccurrenceDesc:
        "L'evento che vuoi visualizzare o modificare fa parte di una serie. Modificando il singolo evento gli altri presenti nella serie rimarranno invariati.",
      toDay: "a"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "Primo/a",
      WeekDayNumberInMonth_2: "Secondo/a",
      WeekDayNumberInMonth_3: "Terzo/a",
      WeekDayNumberInMonth_4: "Quarto/a",
      WeekDayNumberInMonth_5: "Quinto/a",
      never: "Mai",
      after: "Dopo;ricorrenze",
      inDate: "In data",
      end: "Termina ricorrenza"
    },
    filterView: {
      listmonth: "Agenda",
      day: "Giorno",
      month: "Mese",
      week: "Settimana",
      workWeek: "Sett. lavorativa",
      year: "Anno"
    },
    import: {
      contactsInvalid: "Non validi:",
      contactsNr: "Totale eventi da importare:",
      contactsValid: "Validi:",
      dropFiles: "Carica o rilascia qui il file da importare",
      error: "Non è stato possibile importare gli eventi",
      importing:
        "Stiamo importando <b>{{eventsNumber}}</b> eventi nel calendario: <code>{{calendarImport}}</code>",
      info:
        "Stai importando degli eventi nel calendario: <code>{{calendarImport}}</code><br/>È possibile importare eventi da altre app di posta utilizzando un file in formato ICS. Ad esempio, puoi esportare gli eventi da Gmail o da Outllook in formato ICS e importarli in Qboxmail.<br/><b>L'importazione non sostituirà nessuno degli eventi già esistenti</b>.",
      loading: "Caricamento...",
      success: "Tutti i tuoi eventi sono stati importati con successo!",
      title: "Importa eventi",
      uploadedFile: "Caricato",
      uploadingFile: "Caricamento"
    },
    newCalendarModal: {
      desc: "",
      title: "Crea / Modifica calendario"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Allegati",
        placeholderList: " "
      },
      desc: "",
      dropFiles: "Carica o rilascia qui il file da allegare",
      freeBusy: {
        legend: {
          event: "Evento",
          busy: "Occupato",
          waiting: "In attesa",
          availability: "Prossima disponibilità"
        }
      },
      hideDetails: "Nascondi dettagli",
      participantsList: {
        headerItem: "Partecipanti",
        answeredHeaderItem: "Con risposta",
        notAnsweredHeaderItem: "in attesa di risposta",
        placeholderList: " "
      },
      showDetails: "Mostra dettagli",
      tabAttachments: "Allegati",
      tabDetails: "Dettagli",
      tabParticipants: "Partecipanti",
      tabSeries: "Ripetizioni",
      tabSettings: "Impostazioni",
      title: "Crea evento",
      titleEdit: {
        event: "Evento",
        series: "Serie"
      },
      titleFreebusy: "Disponibilità partecipanti"
    },
    nextAvailability: "Prossima disponibilità",
    searchEvents: {
      notFound: "Nessun evento trovato",
      notFoundInfo: "La ricerca non ha prodotto risultati, riprova utilizzando criteri diversi"
    },
    shareModal: {
      desc:
        "Stai condividendo il calendario: <code>{{calendarShare}}</code><br/>Seleziona i contatti con cui vuoi condividere il calendario e assenga loro dei permessi di gestione.",
      title: "Condividi calendario"
    },
    sidebar: {
      labels: "Etichette",
      sharedCalendars: "Calendari condivisi",
      userCalendars: "Calendari utente",
      deleteCalendarDsc:
        "Eliminando il calendario <code>{{calendar}}</code> tutti gli eventi presenti saranno cancellati.<br/><b>L'operazione non è reversibile</b>.",
      deleteCalendarTitle: "Eliminare calendario"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "Non ci sono calendari da sottoscrivere",
      title: "Sottoscrivi calendario"
    },
    today: "Oggi"
  },

  circularLoopBar: {
    uploading: "Caricamento"
  },

  comingSoon: "Sviluppo in corso...",

  contactImport: {
    title: "Ricerca contatti in corso",
    desc:
      "Stiamo importando i tuoi contatti. L'operazione potrebbe richiedere del tempo, ti preghiamo di attendere."
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Risposto",
      "\\\\Deleted": "Cancellato",
      "\\\\Draft": "Bozza",
      "\\\\Flagged": "Contrassegnato",
      "\\\\Seen": "Letto"
    },
    antispamFilterTypes: {
      whitelist_from: "Consenti da",
      whitelist_to: "Consenti verso"
    },
    calendarMinutesInterval: {
      "15": "15 minuti",
      "30": "30 minuti",
      "60": "60 minuti"
    },
    calendarNewEventFreeBusy: {
      busy: "Occupato",
      free: "Disponibile",
      outofoffice: "Fuori sede",
      temporary: "Provvisorio"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Accetta",
      DECLINED: "Rifiuta",
      TENTATIVE: "Forse"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Presidente",
      "REQ-PARTICIPANT": "Obbligatorio",
      "OPT-PARTICIPANT": "Non obbligatorio",
      "NON-PARTICIPANT": "Per conoscenza"
    },
    calendarNewEventPriority: {
      "0": "Nessuna",
      "1": "Alta",
      "5": "Media",
      "9": "Bassa"
    },
    calendarNewEventTypes: {
      public: "Pubblico",
      confidential: "Confidenziale",
      private: "Privato"
    },
    calendarStartWeekDay: {
      saturday: "Sabato",
      sunday: "Domenica",
      monday: "Lunedi"
    },
    calendarView: {
      year: "Anno",
      month: "Mese",
      week: "Settimana",
      workWeek: "Settimana lavorativa",
      day: "Giorno",
      listmonth: "Lista"
    },
    calendarEventFilterBy: {allEvents: "Tutti", nextEvents: "Tutti i prossimi eventi"},
    calendarEventSearchIn: {title: "Titolo/etichette/luogo", all: "Contenuto intero"},
    cancelSendTimer: {
      "0": "Disattivato",
      "5000": "5 secondi",
      "10000": "10 secondi",
      "20000": "20 secondi",
      "30000": "30 secondi"
    },
    contactNameFormat: {
      firstName: "Nome, Cognome",
      lastName: "Cognome, Nome"
    },
    contactsOrder: {
      firstName: "Nome",
      lastName: "Cognome"
    },
    countries: {
      AD: "Andorra",
      AE: "Emirati Arabi",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "Samoa Americana",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Isole Åland",
      AZ: "Azerbaijan",
      BA: "Bosnia Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgio",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius e Saba",
      BR: "Brasile",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Bielorussia",
      BZ: "Belize",
      CA: "Canada",
      CC: "Isole Cocos (Keeling)",
      CD: "Repubblica Democratica del Congo",
      CF: "Repubblica Centro Africana",
      CG: "Repubblica del Congo",
      CH: "Svizzera",
      CL: "Cile",
      CM: "Cameroun",
      CN: "Cina",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Capo Verde",
      CW: "Curaçao",
      CX: "Isola di Natale",
      CY: "Cipro",
      CZ: "Repubblica Ceca",
      DE: "Germania",
      DK: "Danimarca",
      DM: "Dominica",
      DO: "Repubblica Dominicana",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spagna",
      ET: "Etiopia",
      FI: "Finlandia",
      FO: "Isole Faroe",
      FR: "Francia",
      GA: "Gabon",
      GB: "Inghilterra",
      GD: "Grenada",
      GF: "Guiana Francese",
      GG: "Guernsey",
      GI: "Gibilterra",
      GL: "Groenlandia",
      GP: "Guadeloupe",
      GQ: "Guinea Equatoriale",
      GR: "Grecia",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croazia",
      HT: "Haiti",
      HU: "Ungheria",
      IE: "Irlanda",
      IM: "Isola di Man",
      IS: "Islanda",
      IT: "Italia",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Giappone",
      KE: "Kenya",
      KR: "Korea del Sud",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lituania",
      LU: "Lussemburgo",
      LV: "Lettonia",
      MC: "Monaco",
      MD: "Moldavia",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "Macedonia del nord",
      MQ: "Martinica",
      MT: "Malta",
      MW: "Malawi",
      MX: "Messico",
      MZ: "Mozambico",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Olanda",
      NO: "Norvegia",
      NZ: "Nuova Zelanda",
      PA: "Panama",
      PE: "Perú",
      PH: "Filippine",
      PL: "Polonia",
      PT: "Portogallo",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Svezia",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovenia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "Sudan del sud",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turchia",
      TZ: "Tanzania",
      UA: "Ucraina",
      UG: "Uganda",
      US: "Stati Uniti",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "Sud Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Chiedi sempre conferma di consegna",
      never: "Non chiedere mai conferma di consegna"
    },
    eventAlarmPostponeTime: {
      "60": "1 minuto",
      "300": "5 minuti",
      "900": "15 minuti",
      "1800": "30 minuti",
      "3600": "1 ora",
      "7200": "2 ore",
      "86400": "1 giorno",
      "604800": "1 settimana"
    },

    calendarTaskStatus: {
      "needs-action": "Richiede un'azione",
      completed: "Completato il",
      "in-process": "In esecuzione",
      cancelled: "Cancellato"
    },

    feedbackAreas: {
      addressbookSidebar: "Lista rubriche",
      contactList: "Lista contatti",
      contactView: "Visualizzazione contatto",
      mailSearch: "Ricerca messaggi mail",
      mailSidebar: "Lista cartelle mail",
      messageList: "Lista messaggi",
      messageView: "Visualizzazione messaggio",
      newMessage: "Composizione nuovo messaggio",
      other: "Altro...",
      settings: "Impostazioni"
    },
    filterMail: {
      all: "Tutti",
      byDate: "Data",
      byFrom: "Da",
      bySize: "Dimensione",
      bySubject: "Oggetto",
      flagged: "Contrassegnati",
      filter: "Filtri",
      orderBy: "Ordina",
      seen: "Letti",
      unseen: "Non letti"
    },
    filtersActions: {
      discard: "Cancella",
      fileinto: "Sposta messaggio in",
      fileinto_copy: "Copia messaggio in",
      keep: "Conserva messaggio",
      redirect: "Reindirizza messaggio in",
      redirect_copy: "Invia una copia a",
      reject: "Respingi con il seguente messaggio",
      addflag: "Contrassegna messaggio come",
      setkeyword: "Applica etichetta",
      stop: "Smetti di processare i filtri"
    },
    filtersDateOperators: {
      gt: "dopo",
      is: "è uguale a",
      lt: "prima",
      notis: "non è uguale a"
    },
    filtersEnvelopeSubtypes: {From: "Da", To: "A"},
    filtersDateSubTypes: {date: "Data", time: "Ora", weekday: "Giorno della settimana"},
    filtersDateWeekdays: {
      monday: "Lunedì",
      tuesday: "Martedì",
      wednesday: "Mercoledì",
      thursday: "Giovedì",
      friday: "Venerdì",
      saturday: "Sabato",
      sunday: "Domenica"
    },
    filtersRules: {
      Bcc: "Ccn",
      body: "Corpo",
      Cc: "Cc",
      currentdate: "Data di arrivo",
      envelope: "Messaggio",
      From: "Mittente",
      other: "Altra intestazione",
      size: "Dimensione",
      Subject: "Oggetto",
      To: "Destinatario"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "è maggiore di", under: "è minore di"},
    filtersStringOperators: {
      contains: "contiene",
      exists: "esiste",
      is: "è uguale a",
      notcontains: "non contiene",
      notexists: "non esiste",
      notis: "non è uguale a"
    },
    languages: {
      de: "Tedesco",
      en: "Inglese",
      it: "Italiano",
      nl: "Olandese",
      sv: "Svedese",
      es: "Spagnolo",
      pt: "Portoghese"
    },
    matchTypes: {
      allof: "Soddisfa tutti i parametri",
      anyof: "Soddisfa uno qualunque dei parametri"
    },
    newAllDayEventAlert: {
      never: "Mai",
      eventDate: "Giorno dell'evento",
      "-PT17H": "Il giorno prima, alle ore 9",
      "-P6DT17H": "Una settimana prima, alle ore 9"
    },
    newEventAlert: {
      never: "Mai",
      eventHour: "Ora evento",
      "-PT5M": "5 minuti prima",
      "-PT15M": "15 minuti prima",
      "-PT30M": "30 minuti prima",
      "-PT1H": "1 ora prima",
      "-PT2H": "2 ore prima",
      "-PT12H": "12 ore prima",
      "-P1D": "1 giorno prima",
      "-P7D": "1 settimana prima"
    },
    newEventRepeatDayWeek: {
      MO: "Lunedì",
      TU: "Martedì",
      WE: "Mercoledì",
      TH: "Giovedì",
      FR: "Venerdì",
      SA: "Sabato",
      SU: "Domenica"
    },
    newEventRepeatFrequency: {
      NEVER: "Mai",
      DAILY: "Ogni giorno",
      WEEKLY: "Ogni settimana",
      MONTHLY: "Ogni mese",
      YEARLY: "Ogni anno"
    },
    newEventRepeatMonths: {
      "1": "Gennaio",
      "2": "Febbraio",
      "3": "Marzo",
      "4": "Aprile",
      "5": "Maggio",
      "6": "Giugno",
      "7": "Luglio",
      "8": "Agosto",
      "9": "Settembre",
      "10": "Ottobre",
      "11": "Novembre",
      "12": "Dicembre"
    },
    orderSelector: {
      byLastname: "Cognome",
      byMail: "Email",
      byName: "Nome",
      byOrg: "Azienda",
      orderBy: "Ordina",
      showOnlyLists: "Visualizza solo liste"
    },
    readConfirmations: {
      always: "Chiedi sempre conferma di lettura",
      never: "Non chiedere mai conferma di lettura"
    },
    readingConfirmation: {
      always: "Sempre",
      ask: "Chiedi sempre",
      never: "Mai"
    },
    selectMail: {
      all: "Seleziona tutto",
      cancel: "Annulla",
      modify: "Modifica",
      none: "Deseleziona tutto"
    },
    shareOptionsCalendar: {
      None: "Nessuno",
      DAndTViewer: "Vedi data e ora",
      Viewer: "Vedi tutto",
      Responder: "Rispondi a",
      Modifier: "Modifica"
    },
    showImages: {
      always: "Sempre",
      contacts: "Contatti e fidati",
      never: "Mai"
    },
    themes: {
      dark: "Dark",
      light: "Light"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Visualizzazione a colonne",
      viewHalf: "Visualizzazione con anteprima",
      viewFull: "Visualizzazione a lista"
    },
    viewModeCompact: {
      viewColumns: "Colonne",
      viewHalf: "Anteprima",
      viewFull: "Lista"
    }
  },

  dropdownMenu: {
    activesync: "Sinc. ActiveSync",
    delete: "Elimina",
    download: "Download",
    edit: "Modifica",
    exportContacts: "Esporta contatti",
    importContacts: "Importa contatti",
    importEvents: "Importa eventi",
    remove: "Rimuovi",
    rename: "Rinomina",
    share: "Condividi",
    syncNo: "Non sincronizzare",
    syncYes: "Sincronizza",
    unsubscribe: "Rim. sottoscrizione"
  },

  dropdownUser: {
    feedback: "Feedback",
    logout: "Logout",
    settings: "Impostazioni"
  },

  errors: {
    emailDomainAlreadyPresent: "Indirizzo email o dominio già presente",
    emptyField: "Campo non compilato",
    invalidEmail: "Email non valida",
    invalidEmailDomain: "Indirizzo email o dominio non valido",
    invalidPassword: "Password non valida",
    invalidPasswordFormat: "Formato non valido, inserire almeno 8 caratteri alfanumerici",
    invalidValue: "Valore non valido",
    mandatory: "Campo obbligatorio",
    notAllowedCharacter: "Carattere non consentito",
    notChanged: "Campo non modificato",
    permissionDenied: "Permesso negato",
    tooLong: "Campo troppo lungo"
  },

  feedback: {
    disclaimer:
      "La tua opinione è importante! Aiutaci a risolvere i problemi e migliorare il servizio.",
    labels: {
      feedbackAreas: "Su quale area vuoi lasciare un feedback?",
      note: "Lascia un messaggio (opzionale)"
    },
    msgError: {
      title: "Non è stato possibile inviare il tuo feedback",
      desc:
        "Per favore riprova più tardi, per noi è importante sapere cosa pensi della nostra webmail. Il tuo feedback ci aiuterà a creare un'esperienza migliore per te e per tutti i nostri utenti."
    },
    msgSuccess: {
      title: "Grazie per il tuo feedback!",
      desc:
        "Apprezziamo molto il tempo che hai impiegato per aiutarci a migliorare la nostra webmail. Il tuo feedback ci aiuterà a creare un'esperienza migliore per te e per tutti i nostri utenti."
    },
    privacy:
      "I dati raccolti verranno trattati nel rispetto della normativa vigente sulla Privacy.",
    title: "Feedback"
  },

  inputs: {
    labels: {
      addAddress: "Aggiungi indirizzo",
      addDomain: "Aggiungi indirizzo o dominio",
      addLabel: "Nome etichetta",
      addMembers: "Aggiungi membri",
      addParticipant: "Aggiungi partecipante",
      addressbookName: "Nome rubrica",
      addUser: "Aggiungi utente",
      alert: "Avviso",
      alert2: "Secondo avviso",
      at: "in",
      calendar: "Calendario",
      calendarBusinessDayEndHour: "Fine giornata lavorativa",
      calendarBusinessDayStartHour: "Inizio giornata lavorativa",
      calendarColor: "Colore calendario",
      calendarMinutesInterval: "Divisione oraria",
      calendarName: "Nome calendario",
      calendarView: "Visualizzazione di default",
      company: "Società",
      confidentialEvents: "Eventi confidenziali",
      confirmNewPassword: "Conferma password",
      contactNameFormat: "Formato nome contatto",
      contactsOrder: "Ordinamento contatti",
      country: "Nazione",
      dateFormat: "Formato data",
      defaultAddressbook: "Rubrica default",
      defaultCalendar: "Calendario default",
      displayName: "Nome visual.",
      email: "E-mail",
      endDate: "Data fine",
      event: "Evento",
      eventDailyRepetition: "Ogni;giorno/i",
      eventDailyWorkdayRepetition: "Ogni giorno feriale",
      eventMonthlyLastDayRepetition: "Ultimo giorno del mese, ogni;mese/i",
      eventMonthlyRepetition: "Il giorno {{DayNumber}}, ogni;mese/i",
      eventMonthlyWeekDayRepetition: "{{WeekDayNumber}}, ogni;mese/i",
      eventMonthlyLastWeekDayRepetition: "Ultimo/a {{WeekDay}}, ogni;mese/i",
      eventPartecipation: "Stato partecipazione",
      eventPriority: "Priorità evento",
      eventType: "Tipo evento",
      eventWeeklyRepetition: "Ogni;settimana/e, il giorno:",
      eventYearlyLastDayRepetition: "Ultimo giorno di {{Month}}, ogni;anno/i",
      eventYearlyRepetition: "Il giorno {{DayNumber}} di {{Month}}, ogni;anno/i",
      eventYearlyWeekDayRepetition: "{{WeekDayNumber}} di {{Month}}, ogni;anno/i",
      eventYearlyLastWeekDayRepetition: "Ultimo/a {{WeekDay}} di {{Month}}, ogni;anno/i",
      exportAsVcard: "Esporta in formato VCard",
      exportAsLdiff: "Esporta in formato LDIF",
      faxHome: "Fax casa",
      faxWork: "Fax lavoro",
      filter: "Filtra",
      filterName: "Nome filtro",
      firstName: "Nome",
      folderName: "Nome cartella",
      freeBusy: "Mostra come",
      from: "Da",
      fromDate: "Dal giorno",
      home: "Casa",
      hour: "Ora",
      includeFreeBusy: "Includi nel free-busy",
      label: "Etichetta",
      labelColor: "Colore etichetta",
      language: "Lingua",
      lastName: "Cognome",
      listName: "Nome lista",
      location: "Luogo",
      loginAddressbook: "Rubrica iniziale",
      mailAccount: "Account",
      mobile: "Cellulare",
      newAddressBook: "Nuova rubrica",
      newAllDayEventAlert: "Alert nuovi eventi intera giornata",
      newCalendar: "Nuovo calendario",
      newEventAlert: "Alert nuovi eventi",
      newForward: "Aggiungi destinatario alla lista",
      newPassword: "Nuova password",
      nickname: "Nickname",
      object: "Oggetto",
      oldPassword: "Password attuale",
      parentFolder: "Percorso",
      phone: "Telefono",
      privateEvents: "Eventi privati",
      publicEvents: "Eventi pubblici",
      readingConfirmation: "Invia conferma di lettura",
      recoveryEmail: "E-mail di recupero",
      renameAddressbook: "Rinomina rubrica",
      repeatEvery: "Ripeti ogni",
      reportBugNote: "Descrizione problema",
      reportBugPhoneNumber: "Numero di telefono",
      searchAddressbookToSubscribe: "Cerca rubrica da sottoscrivere",
      searchCalendarToSubscribe: "Cerca calendario da sottoscrivere",
      searchIn: "Cerca in",
      selectCalendar: "Seleziona calendario",
      selectIdentity: "Seleziona identità",
      selectList: "Seleziona lista",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "Con chi vuoi condividere la rubrica?",
      shareToCalendar: "Con chi vuoi condividere il calendario?",
      shareToFolder: "Con chi vuoi condividere la cartella?",
      showImages: "Mostra immagini",
      signatureName: "Nome visualizzato",
      signatureText: "Testo firma",
      startDate: "Data inizio",
      team: "Team",
      theme: "Tema",
      timeFormat: "Formato ora",
      timezone: "Time zone",
      title: "Titolo",
      to: "A",
      untilDate: "Al giorno",
      weekStart: "Inizio settimana",
      work: "Lavoro"
    },
    placeholder: {
      addressbookName: "Nome rubrica",

      birthday: "Compleanno",
      city: "Città",
      country: "Paese",
      company: "Società",
      email: "Email",
      firstName: "Nome",
      lastName: "Cognome",
      newFolderName: "Nuovo nome cartella",
      newNameAddressbook: "Nuovo nome rubrica",
      newPassword: "Almeno 8 caratteri alfanumerici",
      note: "Note",
      phone: "Telefono",
      postCode: "CAP",
      members: "Membri",
      region: "Provincia",
      role: "Ruolo",
      select: "Seleziona",
      search: "Cerca in:",
      searchActivity: "Cerca attività",
      searchEvent: "Cerca evento",
      snooze: "Posponi",
      street: "Via"
    },
    hint: {
      cancelSendMessage:
        "Seleziona il tempo disponibile per annullare l'invio di un messaggio, se decidi di non voler inviare l'email."
    }
  },

  labels: {
    attendees: "Partecipanti",
    default: "Predefinito",
    forDomain: "Per dominio",
    withAllUsers: "Con tutti gli utenti",
    calendarAdvancedSearch: "Dove vuoi cercare?",
    clearAdvancedFilters: "Pulisci filtri",
    clearFields: "Pulisci campi",
    holidayCalendar: "Festività",
    searchResults: "Risultati ricerca"
  },

  loaderFullscreen: {
    description: "I contenuti stanno arrivando, per favore attendi!",
    descriptionFolder: "Le tue cartelle stanno arrivando, per favore attendi!",
    title: "Caricamento in corso"
  },

  mail: {
    confirmMailLabel: "OK, confermo!",
    contextualMenuLabels: {
      addAddressBook: "Aggiungi a rubrica",
      addEvent: "Crea evento",
      alarm: "Alarm",
      backTo: "Indietro",
      cancel: "Annulla",
      close: "Chiudi",
      confirm: "Conferma",
      copyIn: "Copia in",
      copyThreadIn: "Copia conversazione",
      delete: "Elimina",
      deletePerm: "Elimina definitivamente",
      deleteThread: "Elimina conversazione",
      deliveryNotification: "Conferma di consegna",
      download: "Download",
      editAsNew: "Modifica come nuovo",
      emptySpam: "Marca Spam come letto",
      emptyTrash: "Svuota cestino",
      forward: "Inoltra",
      import: "Importa messaggio",
      label: "Etichetta",
      labelsManagement: "Gestione etichette",
      labelThread: "Etichetta conversazione",
      markAllAsRead: "Marca tutti come letti",
      markAs: "Marca come",
      markThreadAs: "Marca conversazione",
      manageFolders: "Gestisci cartelle",
      modify: "Modifica",
      more: "Altro",
      moveThreadTo: "Sposta conversazione",
      moveTo: "Sposta in",
      newSubFolder: "Crea sottocartella",
      plainText: "Modalità testo semplice",
      print: "Stampa",
      priorityHigh: "Priorità alta",
      readed: "Letto",
      readedNot: "Non letto",
      readingNotification: "Conferma di lettura",
      rename: "Rinomina",
      reply: "Rispondi",
      replyToAll: "Rispondi a tutti",
      restore: "Ripristina",
      restoreThreadIn: "Ripristina conversazione",
      restoreIn: "Ripristina in",
      sendAsAttachment: "Inoltra come allegato",
      showSourceCode: "Mostra sorgente",
      spam: "Spam",
      spamNot: "No Spam",
      starred: "Contrassegna",
      starredNot: "Rimuovi contrassegno",
      thanks: "Ringrazia",
      thread: "Azioni conversazione"
    },
    deleteMailDsc:
      "Se non è attivo un sistema di archiviazione, il messaggio verrà eliminato definitivamente.<br/><b>L'operazione non è reversibile</b>.",
    deleteMailTitle: "Elimina messaggio",
    deleteMailsDsc:
      "Se non è attivo un sistema di archiviazione, i messaggi verranno eliminati definitivamente.<br/><b>L'operazione non è reversibile<b>.",
    deleteMailsTitle: "Elimina messaggi selezionati",
    dropFiles: "Rilascia qui i file da allegare",
    emptyTrashDsc:
      "Se non è attivo un sistema di archiviazione, svuotando il cestino tutti i messaggi saranno eliminati in maniera <b>irreversibile</b>.<br/>Dopo <b>30 giorni</b> i messaggi vengono eliminati automaticamente.",
    emptyTrashTitle: "Svuota cestino",
    import: {
      messagesInvalid: "Non validi:",
      messagesNr: "Totale messaggi da importare:",
      messagesValid: "Validi:",
      desc:
        "Stai importando dei messaggi nella cartella: <code>{{folderImport}}</code><br/>È possibile importare messaggi email da altre app di posta utilizzando un file in formato eml (<strong>.eml</strong>).",
      dropFiles: "Carica o rilascia qui il file da importare",
      error: "Non è stato possibile importare i messaggi",
      importing:
        "Stiamo importando <b>{{messagesNumber}}</b> messaggi nella cartella: <code>{{folderImport}}</code>",
      loading: "Caricamento...",
      success: "Importazione avvenuta con successo!",
      title: "Importa messaggi",
      uploadedFile: "Caricato",
      uploadingFile: "Caricamento"
    },
    markSpamReadDsc:
      "Marca tutti i messaggi presenti nella cartella come letti.<br/>Dopo <b>30 giorni</b> i messaggi vengono eliminati automaticamente.",
    markSpamReadTitle: "Marca messaggi come letti",
    maxLabelsNumberTitle: "Troppe etichette!",
    maxLabelsNumberDsc:
      "Hai stato raggiunto il numero massimo di etichette associabili a un messaggio. Per aggiungere una nuova etichetta devi prima ruomevere una di quelle già presenti.",
    message: {
      add: "Aggiungi",
      addToAddressBook: "Aggiungi contatto a rubrica",
      attachedMessage: "Messaggio allegato",
      attachments: "allegati",
      bugMsg: "Non visualizzi il messaggio?",
      cc: "Cc:",
      date: "Data:",
      draftsSelectedInfo: "Scegli un messaggio dalla lista e continua a scrivere.",
      downloadAll: "Scarica tutti",
      downloadAttachments: "Scarica allegati",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Modifica partecipazione",
      eventActionAccept: "Accetta",
      eventActionDecline: "Declina",
      eventActionTentative: "Forse",
      eventAddToCalendar: "Evento non presente nel calendario",
      eventAttendees: "Partecipanti",
      eventAttendeeStatusAccepted: "Evento accettato",
      eventAttendeeStatusDeclined: "Evento rifiutato",
      eventAttendeeStatusTentative: "Tentativo di partecipazione",
      eventCounterProposal: "Questo messaggio contiene una richiesta di modifica",
      eventDeleted: "Evento eliminato",
      eventInvitation: "Invito all'evento:",
      eventLocationEmpty: "Non specificata",
      eventModify: "Modifica",
      eventNeedAction: "Conferma partecipazione a questo evento",
      eventNotNeedAction: "Evento già gestito",
      eventOutDated: "L'evento è stato modificato",
      eventStatusAccepted: "Accettato",
      eventStatusDeclined: "Rifiutato",
      eventStatusTentative: "Tentativo",
      eventUpdate: "Aggiorna evento",
      folderSelectedInfo: "Scegli un messaggio dalla lista e scopri il suo contenuto.",
      from: "Da:",
      hideAllAttachments: "Nascondi",
      hugeMessage:
        "Contenuto troppo grande per essere visualizzato interamente, fai clic sul link in basso per visualizzare il codice sorgente del messaggio.",
      hugeMessageNotice: "Messaggio troncato",
      inboxSelectedInfo: "Scegli un messaggio dalla lista e scopri chi ti ha scritto.",
      loadImages: "Carica immagini",
      localAttachments: "Sono presenti altri allegati dentro l'evento",
      message: "messaggio",
      messageForwarded: "Messaggio inoltrato",
      messages: "messaggi",
      messagesQuota: "Numero messaggi:",
      newMessageinThread: "C'è un nuovo messaggio",
      notSelected: "Seleziona un messaggio",
      oneSelectMessages: "Messaggio selezionato",
      oneSelectThreads: "Conversazione selezionata",
      readNotificationIgnore: "Ignora",
      readNotificationLabel: "{{Sender}} ha richiesto conferma di lettura del messaggio.",
      readNotificationLabelSimple: "Richista conferma di lettura messaggio.",
      readNotificationResponse:
        "<P>Il messaggio<BR><BR>&nbsp;&nbsp;&nbsp; A:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Oggetto:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Inviato:&nbsp; %SENDDATE%<BR><BR>&egrave; stato letto il giorno %READDATE%.</P>",
      readNotificationSend: "Conferma",
      readNotificationSubjectPrefix: "Letto:",
      replyTo: "Rispondi a:",
      sentSelectedInfo: "Scegli un messaggio dalla lista e vedi a chi hai scritto.",
      showAllAttachments: "Mostra altri",
      showMoreThread: "Mostra altro",
      selectMessages: "Messaggi selezionati",
      selectThreads: "Conversazioni selezionate",
      sender: "Mittente:",
      spamNotSelected: "Mantieni la cartella in ordine",
      spamSelectedInfo: "Verifica i messaggi e marca lo spam come letto.",
      subject: "Oggetto:",
      to: "A:",
      trashNotSelected: "Controlla il cestino",
      trashSelectedInfo: "Puoi recuperare i messaggi dal cestino entro 30 giorni.",
      updating: "Modifica in corso",
      yesterday: "Ieri",
      wrote: "ha scritto:"
    },
    messagesList: {
      allMessagesLoaded: "Tutti i messaggi sono stati caricati",
      ctaSpamInfo: "Segna tutti come letti",
      ctaTrashInfo: "Svuota cestino",
      folderEmpty: "Questa cartella è vuota",
      folderInfo: "Avvia una conversazione o sposta un messaggio.",
      foundMessages: "Trovati",
      moveMessage: "Sposta messaggio",
      moveMessages: "Sposta {{NumMessages}} messaggi",
      notFound: "Nessun messaggio trovato",
      notFoundInfo: "La ricerca non ha prodotto risultati, riprova utilizzando criteri diversi.",
      pullDownToRefresh: "Trascina per aggiornare",
      releaseToRefresh: "Rilascia per aggiornare",
      searchInfo: "messaggi trovati",
      selectMessages: "Selezionati",
      showMoreThread: "Mostra altro",
      spamEmpty: "Tutto lo spam è scappato via!",
      spamInfo: "I messaggi in spam vengono eliminati automaticamente dopo 30 giorni.",
      trashEmpty: "Il cestino è vuoto",
      trashInfo: "I messaggi nel cestino vengono eliminati automaticamente dopo 30 giorni.",
      totalMessages: "Messaggi"
    },
    newMessage: {
      addAttach: "Aggiungi allegato",
      addImgeDsc:
        "Puoi inserire le immagini direttamente in linea, nel corpo del messaggio o allegarle al messaggio.",
      addImgeTitle: "Inserisci immagini",
      advanced: "Avanzate",
      bcc: "Ccn",
      cc: "Cc",
      cancelSend: "Invio mail cancellato correttamente",
      delete: "Elimina",
      deleted: "Messaggio eliminato correttamente",
      deleteInProgress: "Cancellazione messaggio in corso",
      discardChanges: "Scarta modifiche",
      fontSize: {
        smaller: "Molto piccolo",
        small: "Piccolo",
        normal: "Normale",
        medium: "Medio",
        big: "Grande",
        bigger: "Molto grande"
      },
      from: "Da",
      hide: "Nascondi",
      invalidMails: "Non tutti gli indirizzi email sono validi, rimuovi quelli errati e riprova",
      moreAttachments: "Altri",
      noDraftChanged: "Nessuna modifica da salvare",
      object: "Oggetto",
      saveDraftDsc:
        "Il messaggio non è stato inviato e contiene modifiche non salvate. Puoi salvarlo come bozza e riprendere a scrivere più tardi.",
      saveDraftTitle: "Salvare messaggio come bozza?",
      savedDraft: "Bozza salvata!",
      savingDraft: "Salvataggio in corso",
      sendInProgress: "Invio messaggio in corso",
      sent: "Messaggio inviato correttamente",
      sizeExceeded:
        "Allegati troppo grandi. La dimensione massima di invio è di {{messageMaxSize}} MB.",
      to: "A",
      toMore: "Altri",
      windowSubject: "Nuovo messaggio"
    },
    reportBugMessage:
      "Se non visualizzi correttamente il messaggio puoi aiutarci a risolvere il problema e migliorare il servizio. Potremmo aver bisogno di contattarti, inserisci il tuo numero telefonico e descrivi il problema.<br/><br/>Procedendo con la segnalazione il tuo messaggio potrà essere visualizzato dal nostro team di sviluppo al fine di migliorare il servizio.<br/><b>Le informazioni NON saranno condivise con terze parti e i dati verranno trattati nel rispetto della normativa vigente sulla Privacy</b>.",
    reportBugTitle: "Segnala messaggio",
    searchMail: {
      attachments: "Allegati",
      cleanFilters: "Pulisci filtri",
      marked: "Contrassegnati"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Crea etichetta"
      },
      createFolder: {
        desc: "",
        title: "Crea cartella"
      },
      modifyFolder: {
        desc: "",
        title: "Modifica cartella"
      },
      restoreFolder: {
        desc: "",
        title: "Ripristina cartella"
      },
      folder: "Cartella",
      folders: "Cartelle personali",
      labels: "Etichette",
      newFoderName: "Nuovo nome cartella",
      occupiedSpace: "Spazio usato",
      sharedFolders: "Cartelle condivise",
      unlimitedSpace: "Spazio illimitato"
    },
    thanksMailLabel: "OK, grazie!",
    tooManyCompositionsDsc:
      "Hai raggiunto il numero massimo di nuovi messaggi che puoi comporre contemporaneamente. Completa almeno un messaggio di quelli che stai scrivendo per avviare una nuova composizone.",
    tooManyCompositionsTitle: "Finisci di scrivere!"
  },

  mailboxes: {
    drafts: "Bozze",
    inbox: "Ricevuti",
    more: "Altro",
    sent: "Inviati",
    shared: "Cartelle condivise",
    spam: "Spam",
    trash: "Cestino"
  },

  navbar: {
    addressbook: "Rubrica",
    calendar: "Calendario",
    chat: "Chat",
    mail: "Mail",
    meeting: "Meeting",
    news: "Novità"
  },

  newsTags: {
    news: "Novità",
    suggestions: "Suggerimenti",
    fix: "Correzioni",
    comingSoon: "In arrivo",
    beta: "Beta"
  },

  pageLogout: {
    title: "Logout in corso",
    desc: ""
  },

  pageIndexing: {
    title: "Il tuo account è in fase di indicizzazione",
    desc: "L'operazione potrebbe richiedere del tempo, ti preghiamo di attendere."
  },

  pageNotFound: {
    pageNotFound: "Qualcosa è andato storto",
    pageNotFoundInfo: "La pagina che stai cercando non è disponibile, non perdere la speranza!"
  },

  powered: "powered by Qboxmail",
  poweredCollaborationWith: "in collaborazione con ",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Conferma di consegna messaggio",
      askReadConfirmation: "Conferma di lettura messaggio",
      defaultView: "Visualizzazione predefinita",
      newEventType: "Tipologia nuovi eventi",
      newEventFreeBusy: "Mostra disponibilità nuovi eventi"
    }
  },

  shareOptions: {
    delete: "Cancellazione",
    read: "Lettura",
    update: "Modifica",
    write: "Scrittura"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Consenti inserimento eventi in questo calendario",
    canEraseObjects: "Consenti cancellazione eventi in questo calendario"
  },

  shortcuts: {
    addressbook: {
      newContact: "Nuovo contatto",
      editContact: "Modifica contatto",
      deleteContact: "Elimina contatto"
    },
    calendar: {
      newEvent: "Nuovo evento",
      editEvent: "Modifica evento",
      deleteEvent: "Elimina evento",
      moveRangeNext: "Visualizza intervallo successivo",
      moveRangePrevious: "Visualizza intervallo precedente"
    },
    general: {
      switchModuls: "Cambia moduli",
      refresh: "Ricarica pagina",
      tabForward: "Vai avanti tra gli elementi cliccabili",
      tabBack: "Torna indietro tra gli elementi cliccabili",
      closeModal: "Chiudi modali",
      openSettings: "Apri impostazioni",
      copy: "Copia elemeno selezionato",
      past: "Incolla elemeno selezionato",
      cut: "Taglia elemeno selezionato"
    },
    mail: {
      confirm: "Conferma",
      deleteMessage: "Elimina messaggio",
      deleteSelectMessage: "Elimina messaggi selezionati",
      editAsNew: "Modifica come nuovo",
      empityTrash: "Svuota cestino",
      expandThread: "Espandi thread",
      extendSelectDown: "Estendi selezione verso il basso nell'elenco dei messaggi",
      extendSelectUp: "Estendi la selezione nell'elenco dei messaggi",
      forward: "Inoltra",
      markRead: "Marca come letto/non letto",
      moveDown: "Muoviti in basso nell'elenco dei messaggi",
      moveUp: "Muoviti in alto nell'elenco dei messaggi",
      newMessage: "Nuovo messaggio",
      openSelectMessage: "Apri messaggio selezionato",
      printMessage: "Stampa messaggio",
      reduceThread: "Riduci thread",
      reply: "Rispondi",
      replyAll: "Rispondi a tutti",
      thanks: "Ringrazia"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Salvataggio automatico"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Rubriche",
          placeholderList: " ",
          title: "Rubriche personali"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Rubriche sottoscritte",
          placeholderList: " ",
          title: "Rubriche condivise"
        },
        title: "Rubriche"
      },
      title: "Rubrica",
      view: {
        desc: "",
        title: "Visualizzazione"
      }
    },
    calendar: {
      activities: {title: "Attività"},
      events: {title: "Eventi", desc: "", alertTitle: "Alerts", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Gestione inviti",
        msgFromHeader: "Accetta inviti solo da",
        placeholderList: " "
      },
      labels: {title: "Etichette", desc: ""},
      labelsTask: {title: "Etichette attività", desc: ""},
      management: {
        title: "Calendari",
        holidayCalendars: {
          title: "Calendari festività",
          desc: "",
          headerItem: "Calendari",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Calendari personali",
          desc: "",
          headerItem: "Calendari",
          placeholderList: " "
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Calendari sottoscritti",
          placeholderList: " ",
          title: "Calendari condivisi"
        },
        includeInFreeBusy: "Includi in Free/Busy",
        showActivities: "Mostra attività",
        showAlarms: "Mostra allarmi",
        syncWithActivesync: "Sincronizza con ActiveSync",
        sendMeMailOnMyEdit: "Inviami una mail quando modifico il calendario",
        sendMeMailOnOtherEdit: "Inviami una mail quando qualcuno modifica il calendario",
        sendMailOnEditTo: "Quando modifico questo calendario invia una mail a"
      },
      view: {
        showBusyOutsideWorkingHours: "Mostra come occupato fuori ore lavorative",
        showWeekNumbers: "Mostra numero settimane",
        title: "Visualizzazione"
      },
      title: "Calendario"
    },
    chat: {
      title: "Chat",
      view: {
        title: "Visualizzazione"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "Email utente",
        generalSettings: {
          desc: "",
          title: "Impostazioni generali"
        },
        personalInfo: {
          desc: "",
          title: "Informazioni personali"
        },
        title: "Profilo"
      },
      security: {
        changepassword: {
          info: "Password di almeno 8 caratteri alfanumerici",
          desc: "",
          resetpassword: "Imposta nuova password",
          resetpassworddisabled: "Il cambio password è disabilitato su questo account",
          resetpasswordexpired: "La tua password è scaduta, impostane una nuova",
          title: "Cambio password"
        },
        otp: {
          cantDisableOtp: "L'autenticazione a 2 fattori non è disattivabile su questo account",
          desc: "",
          disableOtp: "Disattiva autenticazione a 2 fattori",
          hideParams: "Nascondi parametri",
          instructionDisableOtp:
            "Inserisci il codice generato dall'applicazione nel form sottostante per disattivare l'autenticazione a 2 fattori per questo account.",
          instructionsSetOtp:
            "Scansione il QRCode con l'applicazione e inserisci il codice generato nel form sottostante. Oppure inserisci manualmente i parametri per l'attivazione.",
          labelEnterCode: "Inserisci il codice generato dall'applicazione",
          paramAccount: "Account",
          paramCode: "Codice",
          paramType: "Basato sull'ora",
          paramTypeVal: "Attivo",
          params: "Inserisci il codice manualmente",
          paramsInstruction: "Inserisci i seguenti parametri nell'app",
          setOtp: "Attiva autenticazione a 2 fattori",
          title: "Attiva OTP",
          titleDisableOtp: "Disattiva OTP",
          titleSetOtp: "Attiva OTP"
        },
        sessions: {
          browser: "Browser:",
          currentSession: "Corrente",
          device: "Dispositivo:",
          desc: "",
          ip: "IP:",
          os: "OS:",
          removeAll: "Termina tutte",
          removeSession: "Termina",
          startdate: "Inizio:",
          title: "Sessioni aperte"
        },
        title: "Sicurezza"
      },
      title: "Generali"
    },
    mail: {
      accounts: {desc: "", title: "Accounts"},
      antispam: {
        blacklist: {
          desc:
            "Aggiungi un indirizzo o un dominio alla lista (es. 'nome@dominio' oppure '*@dominio')",
          msgNoHeader: "Non consentire messaggi da (From)",
          title: "Black list"
        },
        deleteRuleDsc: "Elimina la regola impostata su <code>{{ruleEmail}}</code>.",
        deleteRuleTitle: "Elimina regola antispam",
        placeholderList: " ",
        title: "Antispam",
        whitelist: {
          desc:
            "Aggiungi un indirizzo o un dominio alla lista (es. 'name@dominio' oppure '*@dominio')",
          msgFromHeader: "Consenti messaggi da (From)",
          msgToHeader: "Consenti messaggi a (To)",
          title: "White list"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Messaggio",
        placholdeSubject: "Oggetto",
        placholderTextarea: "Messaggio",
        title: "Autorisponditore"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Annulla invio email"
        },
        confirmations: {
          desc: "",
          title: "Conferme lettura / consegna"
        },
        title: "Composizione"
      },
      delegations: {desc: "", title: "Deleghe"},
      filters: {
        actionsTitle: "Azioni",
        activeFiltersTitle: "Filtri attivi",
        desc: "Gestisci i filtri sui messaggi in entrata",
        headerItem: "Filtri attivi",
        parametersTitle: "Parametri",
        placeholderList: {
          parameter: "Aggiungi un parametro al filtro",
          action: "Aggiungi un'azione al filtro"
        },
        title: "Filtri"
      },
      folders: {
        deleteFolderDsc:
          "Eliminando la cartella <code>{{folderName}}</code> verranno eliminati tutti i messaggi in esse contenuti.<br/><b>L'operazione non è reversibile</b>.",
        deleteFoldersTitle: "Elimina cartella",
        desc: "Gestisci e condividi le tue cartelle personali",
        descShared: "Gestisci le tue cartelle condivise",
        headerItem: "Cartelle",
        headerMsg: "Messaggi",
        headerUnread: "Non letti",
        modifyFolder: "Modifica cartella",
        newFolder: "Aggiungi nuova cartella",
        rootFolder: "-- Radice --",
        subscribe: "Sottoscrivi",
        title: "Cartelle",
        titleShared: "Cartelle condivise"
      },
      forward: {
        description: "Selezione max {{max}} destinatari.",
        headerItem: "Destinatari",
        placeholderList: " ",
        title: "Inoltri",
        titleSection: "Inoltra messaggi"
      },
      labels: {
        desc: "",
        headerItem: "Etichette",
        deleteLabelTitle: "Elimina etichetta",
        deleteLabelTitleDsc: "Eliminando l'etichettta questa verrà rimossa da tutti i messaggi.",
        placeholderList: " ",
        title: "Etichette"
      },
      reading: {
        layout: {
          desc: "",
          title: "Layout"
        },
        readingSettings: {
          desc: "",
          title: "Impostazioni lettura"
        },
        title: "Visualizzazione"
      },
      signature: {
        title: "Firma",
        default: "Pred.",
        dropFiles: "Rilascia qui l'immagine da inserire",
        management: {
          title: "Gestione firma"
        },
        onlyNewMessages: "Non aggiungere firma alle risposte",
        placeholderList: " ",
        senderBcc: "Aggiungi mittente in Ccn",
        setAsDefault: "Imposta come mittente predefinito",
        signatures: {
          desc:
            "Personalizza le email che invii scegliendo il nome visualizzato, da chi riceve il messaggio, ed impostando una firma che sarà inserita in tutti i tuoi messaggi.<br/>Puoi personalizzare il nome visualizzato e la firma per tutte le identità (gruppi di alias) associate al tuo account.",
          headerItemSignature: "Nome visualizzato",
          headerItemIdentity: "Identità",
          title: "Firme e identità"
        }
      },
      title: "Mail"
    },
    meeting: {
      title: "Meeting",
      view: {
        desc: "",
        title: "Visualizzazione"
      }
    },
    news: {
      readMore: "Leggi di più",
      title: "Novità webmail"
    },
    shortcuts: {
      title: "Scorciatoie da tastiera"
    },
    title: "Impostazioni",
    unsaved: "Ci sono modifiche non salvate"
  },

  toast: {
    error: {
      alreadyPresent: "Attenzione, la risorsa esiste già",
      avatarTooBig: "Attenzione, dimensione avatar troppo grande",
      compact: "Errore durante la compattazione cartella",
      connectionUnavailable: "Connessione assente",
      delete: "Errore nella cancellazione",
      deleteDraft: "Errore nella cancellazione della bozza",
      enqueued: "Impostazioni in aggiornamento, riprovare",
      fetchingAttachments: "Errore durante il recupero allegati",
      filterActionsMissing: "Azioni non presenti",
      filterConditionsMissing: "Parametri non presenti",
      filterNameMissing: "Nome filtro mancante",
      folderCreation: "Errore creazione nuova cartella",
      folderCreationCharNotValid: "Errore creazione/modifica cartella, carattere non valido (/)",
      folderCreationNoName: "Errore creazione cartella, specificare un nome",
      folderDelete: "Errore eliminazione cartella",
      folderNotPresent: "Cartella non presente",
      folderRename: "Errore rinominazione cartella, cartella già presente",
      folderSubscribe: "Errore sottoscrizione cartella",
      generic: "Qualcosa è andato storto",
      maxMembers: "Attenzione, numero massimo di membri raggiunto",
      messageCopy: "Errore durante la copia dei messaggi",
      messageMove: "Errore durante lo spostamento dei messaggi",
      messageNotFound: "Messaggio non trovato",
      messageRemove: "Errore durante la cancellazione dei messaggi",
      missingParameters: "Parametri non presenti",
      notifications: "Errore nella ricezione delle notifiche",
      notPermitted: "Non si hanno i permessi necessari per completare l'operazione",
      notValid: "Attenzione, valore non valido",
      onlyOneHomeAddress: "È possibile inserire un solo indirizzo di casa",
      onlyOneWorkAddress: "È possibile inserire un solo indirizzo di lavoro",
      save: "Errore durante il salvataggio",
      saveAcl: "Errore durante il salvataggio dei permessi sulla cartella",
      savingDraft: "È in corso un salvataggio bozza, riprova al termine",
      sendingMessage: "Errore durante l'invio del messaggio",
      serverDisconnection: "Server in aggiornamento, riprova più tardi!",
      sessionExpired: "Sessione scaduta",
      smtpBlockedByAntispam: "Messaggio non inviato: bloccato da antispam",
      smtpBlockedByAntivirus: "Messaggio non inviato: bloccato da antivirus",
      smtpDisabled: "Messaggio non inviato: SMTP disattivato su questo account",
      smtpQuotaExceeded: "Messaggio non inviato: superamento quota invio messaggi",
      requestLimit: "Limite di richieste raggiunto, riprova più tardi.",
      requestError: "Si è verificato un errore durante la richiesta, riprovare più tardi",
      updateFolders: "Si è verificato un errore durante l'aggiornamento delle cartelle",
      updateMessageEvent: "Errore nella ricezione dell'evento aggiornato",
      updatingParentFlags: "Errore nell'aggiornamento dei flags del messaggio"
    },
    info: {
      newVersionAvailable: {
        desc: "L'operazione non è distruttiva, le tue impostazioni rimarranno invariate.",
        title:
          "Nuove funzionalità disponibili, ricarica la pagina per aggiornare la webmail all'ultima versione."
      },
      serverReconnected: "Webmail di nuovo online",
      sourceMail: "Sorgente mail copiata negli appunti"
    },
    progress: {
      newFolder: "Creazione nuova cartella in corso...",
      operationInProgress: "Operazione in corso..."
    },
    success: {
      compact: "Cartella compattata con successo",
      copiedLink: "Collegamento copiato negli appunti",
      copiedMail: "Mail copiata con successo",
      copiedMailAddress: "Indirizzo mail copiato negli appunti",
      createFolder: "Cartella creata con successo",
      delete: "Eliminazione avvenuta con successo",
      disabledOtp: "OTP disattivato con successo",
      enabledOtp: "OTP attivato con successo",
      movedMail: "Mail spostata con successo",
      otpDisabled: "OTP disattivato con successo",
      otpEnable: "OTP attivato con successo",
      save: "Salvataggio avvenuto con successo",
      saveAcl: "Permessi modificati con successo",
      saveDraft: "Bozza salvata con successo",
      saveFolder: "Cartella modificata con successo",
      sentMail: "Mail inviata con successo",
      subscribeFolder: "Sottoscrizione modificata con successo",
      updateMessageEvent: "Stato evento aggiornato"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "Tutto il giorno",
      autoSaveContact: "Salva automaticamente i destinatari mail non presenti in rubrica",
      enableAutorespopnd: "Abilita autorisponditore",
      enableAutorespopndInterval: "Abilita intervallo",
      keepCopy: "Mantieni copia del messaggio in locale",
      showPriority: "Mostra priorità messaggi ricevuti",
      showThreads: "Mostra mail come conversazioni",
      denyInvites: "Impedisci inviti ad appuntamenti",
      enableDateSearch: "Abilita la ricerca per data",
      enableFullBody: "Cerca anche nel corpo del messaggio",
      subscribeFolder: "Sottoscrivi cartella"
    }
  },

  wizard: {
    layout: {
      desc:
        "Personalizza la tua Webmail scegliendo le impostazioni di visualizzazione che prefresici.",
      title: "Opzioni visualizzazione"
    },
    wizardData: {
      desc: "Personalizza il tuo profilo aggiungendo un’immagine e completando i dati mancanti.",
      title: "Completa profilo"
    }
  }
};
