/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Selected contact",
      notSelected: "Select a contact",
      sections: {
        cell: " Mobile",
        home: "Home",
        homeFax: "Fax home",
        work: "Work",
        workFax: "Fax work"
      },
      selectContacts: "Selected contacts",
      selectedInfo: "Select a contact from the list to view the details."
    },
    contactsList: {
      emptyContacts: "This address book is empty",
      emptyContactsInfo: "Add a contact to the address book",
      foundContacts: "Found",
      notFound: "No contacts found",
      notFoundInfo: "The search did not produce any result, try again using different criteria.",
      totalContacts: "Contacts"
    },
    contextualMenuLabels: {
      address: "Address",
      birthday: "Birthday",
      cancel: "Cancel",
      delete: "Delete",
      downloadVCard: "Download VCard",
      email: "Email",
      newContact: "New contact",
      newList: "New list",
      note: "Note",
      phone: "Phone",
      shareVCard: "Share",
      showVCard: "Show VCard",
      sendAsAttachment: "Send by email",
      sendMail: "Send email",
      url: "URL"
    },
    deleteContactDsc:
      "By deleting the contact <code>{{contact}}</code> all the related information will be deleted also.<br/><b>The operation is not reversible</b>.",
    deleteContactsDsc:
      "By deleting selected contacts all the their information will be deleted also.<br/><b>The operation is not reversible</b>.",
    deleteContactTitle: "Delete contact",
    deleteContactDscList:
      "By deleting selected contact all the information will be deleted also.<br/><b>The operation is not reversible</b>.",
    deleteContactsTitle: "Delete contacts",

    deleteListDsc:
      "By deleting the list <code>{{contact}}</code> all the related information will be deleted also.<br/><b>The operation is not reversible</b>.",
    deleteListTitle: "Delete list",

    editContactModal: {
      title: "Edit contact"
    },
    editListModal: {
      title: "Edit list"
    },
    exportAddressBookModal: {
      desc:
        "You are exporting the address book: <code>{{addressbookExport}}</code><br/>Please choose the format for exporting all the contacts present",
      title: "Export address book"
    },
    import: {
      contactsInvalid: "Not valid:",
      contactsNr: "Total contacts to be imported:",
      contactsValid: "Valid:",
      desc:
        "You are importing contacts in the address book: <code>{{addressbookImport}}</code><br/>You can import contacts from other email apps using a vCard (<strong>.vcf</strong>) or <strong>.ldif</strong> file format.\u000AFor example, you can export contacts from Gmail or from Outlook and import them into Qboxmail.\u000AThe import will not replace any of the existing contacts.",
      dropFiles: "Upload or drop here the file to be imported",
      error: "Contacts could not be imported",
      importing:
        "We are importing <b>{{contactsNumber}}</b> event in the calendar: <code>{{addressbookImport}}</code>",
      loading: "Loading...",
      success: "Import successfull!",
      title: "Import contacts",
      uploadedFile: "Uploaded",
      uploadingFile: "Uploading"
    },
    list: {
      desc:
        "It is possible add up to <b>150 members</b> to the list by choosing from the contacts in the address book <code>{{addressbookName}}</code>.",
      members: "List members"
    },
    messages: {
      newContactAddressBookBody:
        "You can not add a contact in <b>All contacts</b> or in <b>Organization contacts</b>.<br/>Select a personal or shared address book.",
      newContactAddressBookTitle: "Select address book",
      resetListBody:
        "List's members are related to the address book selected. This change will reset the members list. Continue ?",
      resetListTitle: "Members deletion"
    },
    newAddressBookModal: {
      desc: "",
      title: "Add / Edit address book"
    },
    newContactModal: {
      title: "Add contact"
    },
    newListModal: {
      title: "Add list"
    },
    shareModal: {
      desc:
        "You are sharing the address book: <code>{{addressbookShare}}</code><br/>Select the contacts you want to share the address book with and allow them to manage.",
      title: "Share address book"
    },
    sidebar: {
      allContacts: "All contacts",
      deleteAddressbookDsc:
        "By deleting the address book <code>{{addressbook}}</code> all the contacts present will be deleted.<br/><b>The operation is not reversible</b>.",
      deleteAddressbookTitle: "Delete address book",
      globalAddressbook: "Organization contacts",
      personalAddressbook: "Personal contacts",
      sharedAddressbooks: "Shared address books",
      userAddressbooks: "User address book"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "There are no address books to be subscribed to",
      title: "Subscribe to the address book"
    }
  },

  app: {
    addressbook: "Address book",
    calendar: "Calendar",
    chat: "Chat",
    meeting: "Meeting",
    of: "of",
    webmail: "Webmail"
  },

  avatarEditorModal: {
    photoTitle: "Take a picture",
    title: "Edit avatar",
    zoom: "Zoom -/+"
  },

  buttons: {
    accept: "Accept",
    add: "Add",
    addAction: "Add action",
    addAddressbook: "Address book",
    addAsAttachment: "Attach",
    addCalendar: "Calendar",
    addField: "Add field",
    addHolidayCalendar: "Holiday calendar",
    addLabel: "Add label",
    addInline: "Add in line",
    addParameter: "Add parameter",
    addSignature: "Add signature",
    addToList: "Add to list",
    apply: "Apply",
    backTo: "Back",
    backAddressbook: "Close",
    cancel: "Cancel",
    changeImage: "Change image",
    changePassword: "Change password",
    checkFreeBusy: "Check availability",
    close: "Close",
    createFolder: "New folder",
    comeBackLater: "Come back later",
    confirm: "Confirm",
    copyLink: "Copy link",
    decline: "Declines",
    delete: "Delete",
    deleteDraft: "Delete draft",
    details: "Details",
    disabled: "Disabled",
    disableOTP: "Disable OTP",
    download: "Download",
    editLabel: "Edit label",
    editSignature: "Edit signature",
    empty: "Empty",
    enabled: "Enabled",
    event: "Event",
    export: "Export",
    goToMail: "Return to mail",
    goToOldWebmail: "Go to old webmail",
    import: "Import",
    hide: "Hide",
    label: "Label",
    leftRotate: "Left rotate 90 degrees",
    loadImage: "Upload image",
    logout: "Logout",
    makePhoto: "Take a picture",
    markRead: "Read",
    markSeen: "Mark",
    markUnread: "Unread",
    message: "Message",
    modify: "Edit",
    new: "New",
    newContact: "New contact",
    newEvent: "New event",
    newMessage: "New message",
    occurrence: "Occurrence",
    openMap: "Open in map",
    reloadPage: "Reload page",
    removeImage: "Remove image",
    rename: "Rename",
    reorder: "Reorder",
    reorderEnd: "End reorder",
    report: "Report",
    retryImport: "Retry",
    save: "Save",
    saveDraft: "Save draft",
    saveNextAvailability: "Save suggested availability",
    search: "Search",
    send: "Send",
    series: "Series",
    setOtp: "Set OTP",
    share: "Share",
    show: "Show",
    showMore: "Other options",
    showUrlsAddressbook: "Show address book URLS",
    showUrlsCalendar: "Show calendar URLS",
    snooze: "Snooze",
    starred: "Star",
    subscribe: "Subscribe",
    subscribeAddressbook: "Subscribe address book",
    subscribeCalendar: "Subscribe calendar",
    tentative: "Maybe",
    unsetOtp: "Disable OTP",
    unsubscribe: "Unsubscribe",
    update: "Update",
    updateFilter: "Update filter",
    viewMap: "View map"
  },

  buttonIcons: {
    activeSync: "Enable ActiveSync",
    activeSyncNot: "Disable ActiveSync",
    alignCenter: "Align center",
    alignLeft: "Align left",
    alignRight: "Align right",
    backgroundColor: "Background color",
    backTo: "Back",
    bold: "Bold",
    confirm: "Confirm",
    close: "Close",
    delete: "Delete",
    download: "Download",
    downloadVCard: "Download VCard",
    edit: "Edit",
    emoji: "Emoji",
    forward: "Forward",
    fullscreen: "Fullscreen",
    fullscreenNot: "Windowed mode",
    imageAdd: "Add image",
    indent: "Increase indent",
    italic: "Italic",
    maintenance: "Maintenance",
    maxmized: "Enlarge",
    minimized: "Reduce",
    more: "More",
    moveDown: "Move down",
    moveUp: "Move up",
    next: "Next",
    orderList: "Ordered list",
    outdent: "Decrease indent",
    pin: "Pin",
    previous: "Previous",
    print: "Print",
    removeFormatting: "Remove formatting",
    reply: "Reply",
    replyAll: "Reply to all",
    saveDraft: "Save draft",
    sendDebug: "Send message that can not be viewed",
    sendMail: "Send mail",
    share: "Share",
    showSidebar: "Show / hide sidebar",
    showSourceCode: "Show source code",
    showVCard: "Show VCard",
    spam: "Mark as Spam",
    spamNot: "Mark as Not Spam",
    startChat: "Start chat",
    startMeeting: "Start meeting",
    strike: "Strike",
    subscribe: "Subscribe",
    textColor: "Text color",
    thanks: "Thanks",
    underline: "Underline",
    unorderList: "Unordered list",
    unSubscribe: "Unsubscribe"
  },

  calendar: {
    deleteEvent: "Delete event",
    deleteEventDsc: "Proceed with the deletion of the selected event ?",
    event: {
      deleteEventOccurrenceTitle: "Delete event or series?",
      deleteEventOccurrenceDesc:
        "The event you want to delete has several occurrencies. Deleting the single occurrence will leave others unaltered.",
      fromDay: "from",
      maxLabelsNumberTitle: "Too many labels!",
      maxLabelsNumberDsc:
        "You have reached the maximum number of labels that can be associated with an event. To add a new label you must first turn one of those already present.",
      organizerTitle: "Created by",
      pendingMsg: "Event awaiting confirmation",
      repeatEvent: "This event is part of recurring events",
      saveEventOccurrenceTitle: "Show event or recurring events?",
      saveEventOccurrenceDesc:
        "The event you want to view or edit has several occurrences. Editing the single occurrence will leave others unaltered.",
      toDay: "to"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "First",
      WeekDayNumberInMonth_2: "Second",
      WeekDayNumberInMonth_3: "Third",
      WeekDayNumberInMonth_4: "Fourth",
      WeekDayNumberInMonth_5: "Fifth",
      never: "Never",
      after: "After;recurrencies",
      inDate: "On date",
      end: "End repetition"
    },
    filterView: {
      listmonth: "Agenda",
      day: "Day",
      month: "Month",
      week: "Week",
      workWeek: "Work week",
      year: "Year"
    },
    import: {
      contactsInvalid: "Not valid:",
      contactsNr: "Total events to be imported:",
      contactsValid: "Valid:",
      dropFiles: "Upload or drop here the file to be imported",
      error: "Events could not be imported",
      importing:
        "We are importing <b>{{eventsNumber}}</b> event in the calendar: <code>{{calendarImport}}</code>",
      info:
        "You are importing events in the calendar: <code>{{calendarImport}}</code><br/>You can import events from other email apps using a ICS file format. For example, you can export events from Gmail or from Outlook in ICS format and import them into Qboxmail.<br/><b>The import will not replace any of the existing events</b>.",
      loading: "Loading...",
      success: "All your events have been successfully imported!",
      title: "Import events",
      uploadedFile: "Uploaded",
      uploadingFile: "Uploading"
    },
    newCalendarModal: {
      desc: "",
      title: "Add / Edit calendar"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Attachments",
        placeholderList: " "
      },
      desc: "",
      dropFiles: "Upload or drop here the file to attach",
      freeBusy: {
        legend: {
          event: "Event",
          busy: "Busy",
          waiting: "Waiting",
          availability: "Next availability"
        }
      },
      hideDetails: "Hide details",
      participantsList: {
        headerItem: "Participants",
        answeredHeaderItem: "Answered",
        notAnsweredHeaderItem: "Waiting for an answer",
        placeholderList: " "
      },
      showDetails: "Show details",
      tabAttachments: "Attachments",
      tabDetails: "Details",
      tabParticipants: "Participants",
      tabSeries: "Repetitions",
      tabSettings: "Settings",
      title: "Create event",
      titleEdit: {
        event: "Event",
        series: "Series"
      },
      titleFreebusy: "Attendees free/busy"
    },
    nextAvailability: "Next availability",
    searchEvents: {
      notFound: "No events found",
      notFoundInfo: "The search did not produce results, try again using different criteria."
    },
    shareModal: {
      desc:
        "You are sharing the calendar: <code>{{calendarShare}}</code><br/>Select the contacts you want to share the calendar with and allow them to manage.",
      title: "Share calendar"
    },
    sidebar: {
      labels: "Labels",
      sharedCalendars: "Shared calendars",
      userCalendars: "User calendars",
      deleteCalendarDsc:
        "By deleting the calendar <code>{{calendar}}</code> all the events present will be deleted.<br/><b>The operation is not reversible</b>.",
      deleteCalendarTitle: "Delete calendar"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "There are no calendars to be subscribed to",
      title: "Subscribe to the calendar"
    },
    today: "Today"
  },

  circularLoopBar: {
    uploading: "Uploading"
  },

  comingSoon: "Development in progress...",

  contactImport: {
    title: "Searching contacts",
    desc: "We are importing your old contacts. The operation may take some time, please wait."
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Answered",
      "\\\\Deleted": "Deleted",
      "\\\\Draft": "Draft",
      "\\\\Flagged": "Marked",
      "\\\\Seen": "Read"
    },
    antispamFilterTypes: {
      whitelist_from: "Allow from",
      whitelist_to: "Allow to"
    },
    calendarMinutesInterval: {
      "15": "15 minutes",
      "30": "30 minutes",
      "60": "60 minutes"
    },
    calendarNewEventFreeBusy: {
      busy: "Busy",
      free: "Free",
      outofoffice: "Out of office",
      temporary: "Temporary"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Accept",
      DECLINED: "Decline",
      TENTATIVE: "Maybe"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Chairman",
      "REQ-PARTICIPANT": "Requested",
      "OPT-PARTICIPANT": "Not requested",
      "NON-PARTICIPANT": "For information purpose"
    },
    calendarNewEventPriority: {
      "0": "None",
      "1": "High",
      "5": "Medium",
      "9": "Low"
    },
    calendarNewEventTypes: {
      public: "Public",
      confidential: "Confidential",
      private: "Private"
    },
    calendarStartWeekDay: {
      saturday: "Saturday",
      sunday: "Sunday",
      monday: "Monday"
    },
    calendarView: {
      year: "Year",
      month: "Month",
      week: "Week",
      workWeek: "Work week",
      day: "Day",
      listmonth: "List"
    },
    calendarEventFilterBy: {allEvents: "All", nextEvents: "All next events"},
    calendarEventSearchIn: {title: "Title/label/place", all: "Full content"},
    cancelSendTimer: {
      "0": "Disabled",
      "5000": "5 seconds",
      "10000": "10 seconds",
      "20000": "20 seconds",
      "30000": "30 seconds"
    },
    contactNameFormat: {
      firstName: "First name, Last name",
      lastName: "Last name, First name"
    },
    contactsOrder: {
      firstName: "First name",
      lastName: "Last name"
    },
    countries: {
      AD: "Andorra",
      AE: "United Arab Emirates",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "American Samoa",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Åland Islands",
      AZ: "Azerbaijan",
      BA: "Bosnia and Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgium",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius and Saba",
      BR: "Brasil",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Belarus",
      BZ: "Belize",
      CA: "Canada",
      CC: "Cocos (Keeling) Islands",
      CD: "Democratic Republic of the Congo",
      CF: "Central African Republic",
      CG: "Republic of the Congo",
      CH: "Switzerland",
      CL: "Chile",
      CM: "Cameroun",
      CN: "China",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Cabo Verde",
      CW: "Curaçao",
      CX: "Christmas Island",
      CY: "Cyprus",
      CZ: "Czech Republic",
      DE: "Germany",
      DK: "Denmark",
      DM: "Dominica",
      DO: "Dominican Republic",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spain",
      ET: "Ethiopia",
      FI: "Finland",
      FO: "Faroe Islands",
      FR: "France",
      GA: "Gabon",
      GB: "United Kingdom",
      GD: "Grenada",
      GF: "French Guiana",
      GG: "Guernsey",
      GI: "Gibraltar",
      GL: "Greenland",
      GP: "Guadeloupe",
      GQ: "Equatorial Guinea",
      GR: "Greece",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croatia",
      HT: "Haiti",
      HU: "Hungary",
      IE: "Ireland",
      IM: "Isle of Man",
      IS: "Island",
      IT: "Italy",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Japan",
      KE: "Kenya",
      KR: "South Korea",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lithuania",
      LU: "Luxembourg",
      LV: "Latvia",
      MC: "Monaco",
      MD: "Moldova",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "North Macedonia",
      MQ: "Martinique",
      MT: "Malta",
      MW: "Malawi",
      MX: "Mexico",
      MZ: "Mozambique",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Netherlands",
      NO: "Norway",
      NZ: "New Zealand",
      PA: "Panama",
      PE: "Perú",
      PH: "Philippines",
      PL: "Poland",
      PT: "Portugal",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Sweden",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovakia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "South Sudan",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turkey",
      TZ: "Tanzania",
      UA: "Ukraine",
      UG: "Uganda",
      US: "United States of America",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "South Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Always ask for delivery confirmation",
      never: "Never ask for delivery confirmation"
    },
    eventAlarmPostponeTime: {
      "60": "for 1 minute",
      "300": "for 5 minutes",
      "900": "for 15 minutes",
      "1800": "for 30 minutes",
      "3600": "for 1 hour",
      "7200": "for 2 hours",
      "86400": "for 1 day",
      "604800": "for 1 week"
    },

    calendarTaskStatus: {
      "needs-action": "Needs action",
      completed: "Completed",
      "in-process": "In progress",
      cancelled: "Cancelled"
    },

    feedbackAreas: {
      addressbookSidebar: "Address book list",
      contactList: "Contacts list",
      contactView: "Contact display",
      mailSearch: "Search messages",
      mailSidebar: "Folders list",
      messageList: "Messages list",
      messageView: "Message display",
      newMessage: "New message composition",
      other: "More",
      settings: "Settings"
    },
    filterMail: {
      all: "All",
      byDate: "Date",
      byFrom: "From",
      bySize: "Size",
      bySubject: "Subject",
      flagged: "Marked",
      filter: "Filters",
      orderBy: "Order by",
      seen: "Read",
      unseen: "Unread"
    },
    filtersActions: {
      discard: "Remove",
      addflag: "Mark message as",
      setkeyword: "Add label",
      fileinto: "Move message in",
      fileinto_copy: "Copy message in",
      keep: "Keep message",
      redirect: "Redirect message to",
      redirect_copy: "Send copy to",
      reject: "Reject with following message",
      stop: "Stop processing filters"
    },
    filtersDateOperators: {
      gt: "After",
      is: "is equal to",
      lt: "Before",
      notis: "is not equal to"
    },
    filtersEnvelopeSubtypes: {From: "From", To: "To"},
    filtersDateSubTypes: {date: "Date", time: "Time", weekday: "Week day"},
    filtersDateWeekdays: {
      monday: "Monday",
      tuesday: "Tuesday",
      wednesday: "Wednesday",
      thursday: "Thursday",
      friday: "Friday",
      saturday: "Saturday",
      sunday: "Sunday"
    },
    filtersRules: {
      Bcc: "Bcc",
      body: "Body",
      Cc: "Cc",
      currentdate: "Check-in date",
      envelope: "Message",
      From: "Sender",
      other: "Other header",
      size: "Size",
      Subject: "Subject",
      To: "Recipient"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "is bigger than", under: "is less than"},
    filtersStringOperators: {
      contains: "contains",
      exists: "exists",
      is: "is equal to",
      notcontains: "not contain",
      notexists: "not exists",
      notis: "is not equal to"
    },
    languages: {
      de: "German",
      en: "English",
      it: "Italian",
      nl: "Dutch",
      sv: "Swedish",
      es: "Spanish",
      pt: "Portuguese"
    },
    matchTypes: {
      allof: "Satisfy all parameters",
      anyof: "Satisfy at least one parameter"
    },
    newAllDayEventAlert: {
      never: "Never",
      eventDate: "Event date",
      "-PT17H": "One day before, at 9am",
      "-P6DT17H": "One week before, at 9am"
    },
    newEventAlert: {
      never: "Never",
      eventHour: "Event start time",
      //P[n]Y[n]M[n]DT[n]H[n]M[n]S
      "-PT5M": "5 minutes before",
      "-PT15M": "15 minutes before",
      "-PT30M": "30 minutes before",
      "-PT1H": "1 hour before",
      "-PT2H": "2 hours before",
      "-PT12H": "12 hours before",
      "-P1D": "1 day before",
      "-P7D": "1 week before"
    },
    newEventRepeatDayWeek: {
      MO: "Monday",
      TU: "Tuesday",
      WE: "Wednesday",
      TH: "Thursday",
      FR: "Friday",
      SA: "Saturday",
      SU: "Sunday"
    },
    newEventRepeatFrequency: {
      NEVER: "Never",
      DAILY: "Daily",
      WEEKLY: "Weekly",
      MONTHLY: "Monthly",
      YEARLY: "Yearly"
    },
    newEventRepeatMonths: {
      "1": "January",
      "2": "February",
      "3": "March",
      "4": "April",
      "5": "May",
      "6": "June",
      "7": "July",
      "8": "August",
      "9": "Settembre",
      "10": "October",
      "11": "November",
      "12": "December"
    },
    orderSelector: {
      byLastname: "Last name",
      byMail: "Email",
      byName: "First name",
      byOrg: "Company",
      orderBy: "Order by",
      showOnlyLists: "Show only lists"
    },
    readConfirmations: {
      always: "Always ask for reading confirmation",
      never: "Never ask for reading confirmation"
    },
    readingConfirmation: {
      always: "Always",
      ask: "Always ask",
      never: "Never"
    },
    selectMail: {
      all: "Select all",
      cancel: "Cancel",
      modify: "Edit",
      none: "Deselect all"
    },
    shareOptionsCalendar: {
      None: "None",
      DAndTViewer: "View date and time",
      Viewer: "View all",
      Responder: "Answer at",
      Modifier: "Modify"
    },
    showImages: {
      always: "Always",
      contacts: "Contacts and trusted",
      never: "Never"
    },
    themes: {
      dark: "Dark",
      light: "Light"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Layout 3 columns",
      viewHalf: "Layout with preview",
      viewFull: "Layout as list"
    },
    viewModeCompact: {
      viewColumns: "Columns",
      viewHalf: "Preview",
      viewFull: "List"
    }
  },

  dropdownMenu: {
    activesync: "ActiveSync",
    delete: "Delete",
    download: "Download",
    edit: "Edit",
    exportContacts: "Export contacts",
    importContacts: "Import contacts",
    importEvents: "Import events",
    remove: "Remove",
    rename: "Rename",
    share: "Share",
    syncNo: "Not synchronize",
    syncYes: "Synchronize",
    unsubscribe: "Unsubscribe"
  },

  dropdownUser: {
    feedback: "Feedback",
    logout: "Logout",
    settings: "Settings"
  },

  errors: {
    emailDomainAlreadyPresent: "Email address or domain already present",
    emptyField: "Empty field",
    invalidEmail: "Email not valid",
    invalidEmailDomain: "Email address or domain not valid",
    invalidPassword: "Not valid password",
    invalidPasswordFormat: "Invalid format, enter at least 8 alphanumeric characters",
    invalidValue: "Not valid value",
    mandatory: "Required field",
    notAllowedCharacter: "Not allowed character",
    notChanged: "Not modified field",
    permissionDenied: "Permission denied",
    tooLong: "Too long field"
  },

  feedback: {
    disclaimer: "Your opinion is important! Help us to solve issues and improve the service.",
    labels: {
      feedbackAreas: "On which area you want to leave feedback?",
      note: "Leave a message (optional)"
    },
    msgError: {
      title: "Your feedback could not be sent",
      desc:
        "Please try again later, it is important for us to know what you think of our webmail. Your feedback will help us create a better experience for you and all our users."
    },
    msgSuccess: {
      title: "Thanks for your feedback!",
      desc:
        "We really appreciate the time you took to help us improve our webmail. Your feedback will help us create a better experience for you and for all our users."
    },
    privacy:
      "The collected data will be processed in compliance with the current legislation on Privacy.",
    title: "Feedback"
  },

  inputs: {
    labels: {
      addAddress: "Add address",
      addDomain: "Add address or domain",
      addLabel: "Label name",
      addMembers: "Add members",
      addParticipant: "Add participant",
      addressbookName: "Address book",
      addUser: "Add user",
      alert: "Alert",
      alert2: "Second alert",
      at: "at",
      calendar: "Calendar",
      calendarBusinessDayEndHour: "Working day end",
      calendarBusinessDayStartHour: "Working day start",
      calendarColor: "Calendar color",
      calendarMinutesInterval: "Calendar time interval",
      calendarName: "Calendar name",
      calendarView: "Default view",
      company: "Company",
      confidentialEvents: "Confidential events",
      confirmNewPassword: "Confirm password",
      contactNameFormat: "Format contact name",
      contactsOrder: "Contacts order",
      country: "Country",
      dateFormat: "Date format",
      defaultAddressbook: "Default address book",
      defaultCalendar: "Default calendar",
      displayName: "Disp. name",
      email: "E-mail",
      endDate: "End date",
      event: "Event",
      eventDailyRepetition: "Every;day/s",
      eventDailyWorkdayRepetition: "Every week day",
      eventMonthlyLastDayRepetition: "Last day of month, every;month/s",
      eventMonthlyLastWeekDayRepetition: "Last {{WeekDay}}, every;month/s",
      eventMonthlyRepetition: "On {{DayNumber}}, every;month/s",
      eventMonthlyWeekDayRepetition: "On {{WeekDayNumber}}, every;month/s",
      eventPartecipation: "Partecipation status",
      eventPriority: "Event priority",
      eventType: "Event type",
      eventWeeklyRepetition: "Every;week/s, on:",
      eventYearlyLastDayRepetition: "Last day of {{Month}}, every;year/s",
      eventYearlyLastWeekDayRepetition: "Last {{WeekDay}} of {{Month}}, every;year/s",
      eventYearlyRepetition: "On {{DayNumber}} of {{Month}}, every;year/s",
      eventYearlyWeekDayRepetition: "On {{WeekDayNumber}} of {{Month}}, every;year/s",
      exportAsVcard: "Export in VCard format",
      exportAsLdiff: "Export in LDIF format",
      faxHome: "Fax home",
      faxWork: "Fax work",
      filter: "Filter by",
      filterName: "Filter name",
      firstName: "First name",
      folderName: "Folder name",
      freeBusy: "Show as",
      from: "From",
      fromDate: "From date",
      home: "Home",
      hour: "Time",
      includeFreeBusy: "Include in free-busy",
      label: "Label",
      labelColor: "Label color",
      language: "Language",
      lastName: "Last name",
      listName: "List name",
      location: "Location",
      loginAddressbook: "Initial address book",
      mailAccount: "Account",
      mobile: "Mobile",
      newAddressBook: "New address book",
      newAllDayEventAlert: "New all day events alert",
      newCalendar: "New calendar",
      newEventAlert: "New events alert",
      newForward: "Add recipient to list",
      newPassword: "New password",
      nickname: "Nickname",
      object: "Subject",
      oldPassword: "Current password",
      parentFolder: "Path",
      phone: "Phone",
      privateEvents: "Private events",
      publicEvents: "Public events",
      readingConfirmation: "Send reading confirmation",
      recoveryEmail: "Recovery e-mail",
      renameAddressbook: "Rename address book",
      repeatEvery: "Repeat every",
      reportBugNote: "Problem description",
      reportBugPhoneNumber: "Phone number",
      searchAddressbookToSubscribe: "Search an address book to be subscribed",
      searchCalendarToSubscribe: "Search a calendar to be subscribed",
      searchIn: "Search in",
      selectCalendar: "Select calendar",
      selectIdentity: "Select identity",
      selectList: "Select list",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "With who you want to share the address book?",
      shareToCalendar: "With who you want to share the calendar?",
      shareToFolder: "Who you want to share the folder with?",
      showImages: "Show images",
      signatureName: "Display name",
      signatureText: "Signature text",
      startDate: "Start date",
      team: "Team",
      theme: "Theme",
      timeFormat: "Time format",
      timezone: "Time zone",
      title: "Title",
      to: "To",
      untilDate: "Until day",
      weekStart: "First day of the week",
      work: "Work"
    },
    placeholder: {
      addressbookName: "Address book name",
      birthday: "Birthday",
      city: "City",
      country: "Country",
      company: "Company",
      email: "Email",
      firstName: "First name",
      lastName: "Last name",
      members: "Members",
      newFolderName: "New folder name",
      newNameAddressbook: "New address book name",
      newPassword: "At least 8 alphanumeric characters",
      note: "Note",
      phone: "Phone",
      postCode: "Postcode",
      region: "Region",
      role: "Role",
      select: "Select",
      search: "Search in:",
      searchActivity: "Search activity",
      searchEvent: "Search event",
      snooze: "Snooze",
      street: "Street"
    },
    hint: {
      cancelSendMessage:
        "Select the time available to retract a message during sending, if you decide you don't want to send the email."
    }
  },

  labels: {
    attendees: "Attendees",
    default: "Default",
    forDomain: "On domain",
    withAllUsers: "With all users",
    calendarAdvancedSearch: "Where do you want to search?",
    clearAdvancedFilters: "Clear filters",
    clearFields: "Clear fields",
    holidayCalendar: "Holiday",
    searchResults: "Search results"
  },

  loaderFullscreen: {
    description: "The contents are coming, please wait.",
    descriptionFolder: "Your folders are coming, please wait.",
    title: "Loading"
  },

  mail: {
    confirmMailLabel: "OK, I confirm!",
    contextualMenuLabels: {
      addAddressBook: "Add to address book",
      addEvent: "Add event",
      alarm: "Alarm",
      backTo: "Back",
      cancel: "Cancel",
      close: "Close",
      confirm: "Confirm",
      copyIn: "Copy in",
      copyThreadIn: "Copy thread",
      delete: "Delete",
      deletePerm: "Permanently delete",
      deleteThread: "Delete thread",
      deliveryNotification: "Delivery confirmation",
      download: "Download",
      editAsNew: "Edit as new",
      emptySpam: "Mark Spam read",
      emptyTrash: "Empty trash",
      forward: "Forward",
      import: "Import mail",
      label: "Label",
      labelsManagement: "Manage labels",
      labelThread: "Label thread",
      markAllAsRead: "Mark all as read",
      markAs: "Mark as",
      markThreadAs: "Mark thread",
      manageFolders: "Manage folders",
      modify: "Edit",
      more: "More",
      moveThreadTo: "Move thread",
      moveTo: "Move to",
      newSubFolder: "Add subfolder",
      plainText: "Plain text mode",
      print: "Print",
      priorityHigh: "High priority",
      readed: "Read",
      readedNot: "Unread",
      readingNotification: "Reading confirmation",
      rename: "Rename",
      reply: "Reply",
      replyToAll: "Reply to all",
      restore: "Restore",
      restoreThreadIn: "Restore thread",
      restoreIn: "Restore in",
      sendAsAttachment: "Forward as attachment",
      showSourceCode: "Show source code",
      spam: "Spam",
      spamNot: "Not Spam",
      starred: "Starred",
      starredNot: "Unstarred",
      thanks: "Thanks",
      thread: "Thread actions"
    },
    deleteMailDsc:
      "If a storage system is not active, the message will be permanently deleted.<br/><b>The operation is not reversible</b>.",
    deleteMailTitle: "Delete message",
    deleteMailsDsc:
      "If a storage system is not active, the messages will be permanently deleted.<br/><b>The operation is not reversible<b>.",
    deleteMailsTitle: "Delet selected messages",
    dropFiles: "Release here the files to attach",
    emptyTrashDsc:
      "If a storage system is not active, emptying the trash, all messages will be eliminated <b>irreversibly</b>.<br/>After <b>30 days</b> the messages are deleted automatically.",
    emptyTrashTitle: "Empty trash",
    import: {
      messagesInvalid: "Not valid:",
      messagesNr: "Total messages to be imported:",
      messagesValid: "Valid:",
      desc:
        "You are importing messages in folder: <code>{{folderImport}}</code><br/>You can import messages from other email apps using a eml file (<strong>.eml</strong>).",
      dropFiles: "Upload or drop here the file to be imported",
      error: "Messages could not be imported",
      importing:
        "We are importing <b>{{messagesNumber}}</b> messages in folder: <code>{{folderImport}}</code>",
      loading: "Loading...",
      success: "Import successfull!",
      title: "Import messages",
      uploadedFile: "Uploaded",
      uploadingFile: "Uploading"
    },
    markSpamReadDsc:
      "Mark all the messages in the folder as read.<br/>After <b>30 days</b> the messages are deleted automatically.",
    markSpamReadTitle: "Mark messages as read",
    maxLabelsNumberTitle: "Too many labels!",
    maxLabelsNumberDsc:
      "You have reached the maximum number of labels that can be associated with an message. To add a new label you must first remove one of those already present.",
    message: {
      add: "Add",
      addToAddressBook: "Add contact to address book",
      attachedMessage: "Attached message",
      attachments: "attachments",
      bugMsg: "Don't see the message?",
      cc: "Cc:",
      date: "Date:",
      draftsSelectedInfo: "Select a message from the list and continue to write.",
      downloadAll: "Download all",
      downloadAttachments: "Download attachments",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Edit partecipation status",
      eventActionAccept: "Accept",
      eventActionDecline: "Decline",
      eventActionTentative: "Maybe",
      eventAddToCalendar: "This event is not present in your calendar",
      eventAttendees: "Attendees",
      eventAttendeeStatusAccepted: "Event accepted",
      eventAttendeeStatusDeclined: "Event declined",
      eventAttendeeStatusTentative: "Tentative of partecipation",
      eventCounterProposal: "This message contains a counter proposal for the event",
      eventDeleted: "Event deleted",
      eventInvitation: "Event invitation:",
      eventLocationEmpty: "Not specified",
      eventModify: "Edit",
      eventNeedAction: "Confrim event partecipation",
      eventNotNeedAction: "Event already processed",
      eventOutDated: "The event has been modified",
      eventStatusAccepted: "Accepted",
      eventStatusDeclined: "Declined",
      eventStatusTentative: "Tentative",
      eventUpdate: "Update event",
      folderSelectedInfo: "Select a message from the list and discover its contents.",
      from: "From:",
      hideAllAttachments: "Hide",
      hugeMessage:
        "Content too big to display entirely, click the link below to view the message source code.",
      hugeMessageNotice: "Message truncated",
      inboxSelectedInfo: "Select a message from the list and find out who wrote you.",
      loadImages: "Download images",
      localAttachments: "There are additional attachments inside the event",
      message: "message",
      messageForwarded: "Forwarded message",
      messages: "messages",
      messagesQuota: "Used messages quota:",
      newMessageinThread: "There is a new message",
      notSelected: "Select a message",
      oneSelectMessages: "Selected message",
      oneSelectThreads: "Seleted thread",
      readNotificationIgnore: "Ignore",
      readNotificationLabel: "{{Sender}} asked reading confirmation for the message.",
      readNotificationLabelSimple: "Asked reading confirmation for message.",
      readNotificationResponse:
        "<P>Message<BR><BR>&nbsp;&nbsp;&nbsp; A:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Subject:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Sent:&nbsp; %SENDDATE%<BR><BR>it was read the day %READDATE%.</P>",
      readNotificationSend: "Confirm",
      readNotificationSubjectPrefix: "Read:",
      replyTo: "Reply to:",
      sender: "Sender:",
      sentSelectedInfo: "Select a message from the list and see who you wrote.",
      showAllAttachments: "Show other",
      showMoreThread: "Show more",
      selectMessages: "Selected messages",
      selectThreads: "Selected thread",
      spamNotSelected: "Keep the folder in order",
      spamSelectedInfo: "Check messages and mark spam as read.",
      subject: "Subject:",
      to: "To:",
      trashNotSelected: "Check the trash",
      trashSelectedInfo: "You can recover messages from the trash within 30 days.",
      updating: "Edit in progress",
      yesterday: "Yesterday",
      wrote: "wrote:"
    },
    messagesList: {
      allMessagesLoaded: "All messages have been uploaded",
      ctaSpamInfo: "Mark all as read",
      ctaTrashInfo: "Empty trash",
      folderEmpty: "This folder is empty",
      folderInfo: "Start a conversation or move a message.",
      foundMessages: "Found",
      moveMessage: "Move message",
      moveMessages: "Move {{NumMessages}} messages",
      notFound: "No messages found",
      notFoundInfo: "The search did not produce results, try again using different criteria.",
      pullDownToRefresh: "Drag to update",
      releaseToRefresh: "Release to update",
      searchInfo: "messages found",
      selectMessages: "Selected",
      showMoreThread: "Show more",
      spamEmpty: "All spam has run away!",
      spamInfo: "Messages in spam are automatically deleted after 30 days.",
      trashEmpty: "The trash is empty",
      trashInfo: "Messages in trash are deleted automatically after 30 days.",
      totalMessages: "Messages"
    },
    newMessage: {
      addAttach: "Add attachment",
      addImgeDsc:
        "You can insert images directly inline, in the body of the message or attach them to the message.",
      addImgeTitle: "Insert images",
      advanced: "Advanced",
      bcc: "Bcc",
      cc: "Cc",
      cancelSend: "Sending mail successfully canceled",
      delete: "Delete",
      deleted: "Message deleted successfully",
      deleteInProgress: "Message deletion in progress",
      discardChanges: "Discard changes",
      fontSize: {
        smaller: "Smaller",
        small: "Small",
        normal: "Normal",
        medium: "Medium",
        big: "Big",
        bigger: "Bigger"
      },
      from: "From",
      hide: "Hide",
      invalidMails: "Invalid mail address is present, check addresses and try again",
      moreAttachments: "Others",
      noDraftChanged: "No change to save",
      object: "Subject",
      saveDraftDsc:
        "The message has not been sent and contains unsaved changes. You can save it as a draft and resume writing later.",
      saveDraftTitle: "Save message as draft?",
      savedDraft: "Draft saved!",
      savingDraft: "Save in progress",
      sendInProgress: "Send message in progress",
      sent: "Message sent successfully",
      sizeExceeded: "Attachments too large. The maximum sending size is of {{messageMaxSize}} MB.",
      to: "To",
      toMore: "Others",
      windowSubject: "New message"
    },
    reportBugMessage:
      "If you do not see the message correctly, you can help us to solve the problem and improve the service. We might need to contact you, please insert your phone number and describe the problem you are facing.<br/><br/>Proceeding with the feedback, your message can be viewed by our development team in order to improve the service.<br/><b>The information will NOT be shared with third parties and the data will be processed in compliance with current legislation on Privacy</b>.",
    reportBugTitle: "Report message",
    searchMail: {
      attachments: "Attachments",
      cleanFilters: "Clean filters",
      marked: "Starred"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Create label"
      },
      createFolder: {
        desc: "",
        title: "Create folder"
      },
      modifyFolder: {
        desc: "",
        title: "Edit folder"
      },
      restoreFolder: {
        desc: "",
        title: "Restore folder"
      },
      folder: "Folders",
      folders: "Personal folders",
      labels: "Labels",
      newFoderName: "New folder name",
      occupiedSpace: "Used space",
      sharedFolders: "Shared folders",
      unlimitedSpace: "Unlimited space"
    },
    thanksMailLabel: "OK, thanks!",
    tooManyCompositionsDsc:
      "You have reached the maximum number of new messages that you can write at the same time. Complete at least one message from the ones you are writing to start a new composition.",
    tooManyCompositionsTitle: "Finish writing!"
  },

  mailboxes: {
    drafts: "Drafts",
    inbox: "Inbox",
    more: "More",
    sent: "Sent",
    shared: "Shared folders",
    spam: "Spam",
    trash: "Trash"
  },

  navbar: {
    addressbook: "Contacts",
    calendar: "Calendar",
    chat: "Chat",
    mail: "Mail",
    meeting: "Meeting",
    news: "News"
  },

  newsTags: {
    news: "News",
    suggestions: "Tips",
    fix: "Fix",
    comingSoon: "Coming soon",
    beta: "Beta"
  },

  pageLogout: {
    title: "Logout in progress",
    desc: ""
  },

  pageIndexing: {
    title: "Your account is indexing",
    desc: "The operation may take some time, please wait."
  },

  pageNotFound: {
    pageNotFound: "Something went wrong",
    pageNotFoundInfo: "The page you are looking for is not available, do not lose hope!"
  },

  powered: "powered by Qboxmail",
  poweredCollaborationWith: "in collaboration with ",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Delivery message confirmation",
      askReadConfirmation: "Reading message confirmation",
      defaultView: "Default layout",
      newEventType: "New events type",
      newEventFreeBusy: "Show new events availability"
    }
  },

  shareOptions: {
    delete: "Deletion",
    read: "Reading",
    update: "Editing",
    write: "Writing"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Allow to insert events in this calendar",
    canEraseObjects: "Allow to delete events in this calendar"
  },

  shortcuts: {
    addressbook: {
      newContact: "New contact",
      editContact: "Edit contact",
      deleteContact: "Delete contact"
    },
    calendar: {
      newEvent: "New event",
      editEvent: "Edit event",
      deleteEvent: "Delete event",
      moveRangeNext: "View next interval",
      moveRangePrevious: "View previous interval"
    },
    general: {
      switchModuls: "Switch modules",
      refresh: "Refresh page",
      tabForward: "Move forward among the clickable elements",
      tabBack: "Move back among the clickable elements",
      closeModal: "Close modal",
      openSettings: "Open settings",
      copy: "Copy selected item",
      past: "Paste selected item",
      cut: "Cut selected item"
    },
    mail: {
      confirm: "Confirm",
      deleteMessage: "Delete message",
      deleteSelectMessage: "Delete selected messages",
      editAsNew: "Edit as new",
      empityTrash: "Empty trash",
      expandThread: "Expand thread",
      extendSelectDown: "Estendi selezione verso il basso nell'elenco dei messaggi",
      extendSelectDown: "Extend selection down on messages list",
      extendSelectUp: "Extend selection up on messages list",
      forward: "Forward",
      markRead: "Mark as read/unread",
      moveDown: "Move down on messages list",
      moveUp: "Move up on messages list",
      newMessage: "New message",
      openSelectMessage: "Open selected message",
      printMessage: "Print message",
      reduceThread: "Reduce thread",
      reply: "Reply",
      replyAll: "Reply all",
      thanks: "Thanks"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Automatic save"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Address book",
          placeholderList: " ",
          title: "Personal address book"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Subscribed address book",
          placeholderList: " ",
          title: "Shared address book"
        },
        title: "Address books"
      },
      title: "Contacts",
      view: {
        desc: "",
        title: "View"
      }
    },
    calendar: {
      activities: {title: "Activities"},
      events: {title: "Events", desc: "", alertTitle: "Alerts", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Invitations management",
        msgFromHeader: "Allow invitations only from",
        placeholderList: " "
      },
      labels: {title: "Labels", desc: ""},
      labelsTask: {title: "Task labels", desc: ""},
      management: {
        title: "Calendars",
        holidayCalendars: {
          title: "Holiday calendars",
          desc: "",
          headerItem: "Calendars",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Personal calendars",
          desc: "",
          headerItem: "Calendars",
          placeholderList: " "
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Subscribed calendars",
          placeholderList: " ",
          title: "Shared calendars"
        },
        includeInFreeBusy: "Include in Free/Busy",
        showActivities: "Show activities",
        showAlarms: "Show alarms",
        syncWithActivesync: "Synchronize using ActiveSync",
        sendMeMailOnMyEdit: "Send me an email when I modify the calendar",
        sendMeMailOnOtherEdit: "Send me an email when someone modifies the calendar",
        sendMailOnEditTo: "When I modify send an email to"
      },
      view: {
        showBusyOutsideWorkingHours: "Show as busy outside working hours",
        showWeekNumbers: "Show week number",
        title: "View"
      },
      title: "Calendar"
    },
    chat: {
      title: "Chat",
      view: {
        title: "View"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "User email",
        generalSettings: {
          desc: "",
          title: "General settings"
        },
        personalInfo: {
          desc: "",
          title: "Personal information"
        },
        title: "Profile"
      },
      security: {
        changepassword: {
          info: "Password of at least 8 alphanumeric characters",
          desc: "",
          resetpassword: "Set new password",
          resetpassworddisabled: "Password change is disabled on this account",
          resetpasswordexpired: "Your password is expired, set a new one",
          title: "Change password"
        },
        otp: {
          cantDisableOtp: "Two-factor authentication can't be disabled on this account",
          desc: "",
          disableOtp: "Disable two-factor authentication",
          hideParams: "Hide parameters",
          instructionDisableOtp:
            "Enter the code generated by the application in the form below to disable two-factor authentication for this account.",
          instructionsSetOtp:
            "Scan the QRCode with the application and enter the code generated in the form below. Or enter the parameters for manually activation.",
          labelEnterCode: "Enter code generated by the application",
          paramAccount: "Account",
          paramCode: "Code",
          paramType: "Time based",
          paramTypeVal: "Active",
          params: "Enter code manually",
          paramsInstruction: "Enter the following parameters in the application",
          setOtp: "Enable two-factor authentication",
          title: "Set OTP",
          titleDisableOtp: "Disable OTP",
          titleSetOtp: "Enable OTP"
        },
        sessions: {
          browser: "Browser:",
          currentSession: "Current",
          device: "Device:",
          desc: "",
          ip: "IP:",
          os: "OS:",
          removeAll: "Remove all",
          removeSession: "Remove",
          startdate: "Start:",
          title: "Open sessions"
        },
        title: "Security"
      },
      title: "General"
    },
    mail: {
      accounts: {desc: "", title: "Accounts"},
      antispam: {
        blacklist: {
          desc: "Add address or domain to list (es. 'name@domain' or '*@domain')",
          msgNoHeader: "Not allow messages from",
          title: "Black list"
        },
        deleteRuleDsc: "Delete rule set to <code>{{ruleEmail}}</code>.",
        deleteRuleTitle: "Delete antispam rule",
        placeholderList: " ",
        title: "Antispam",
        whitelist: {
          desc: "Add address or domain to list (es. 'name@domain' or '*@domain')",
          msgFromHeader: "Allow messages from",
          msgToHeader: "Allow messages to",
          title: "White list"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Message",
        placholdeSubject: "Subject",
        placholderTextarea: "Message text",
        title: "Autoresponder"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Cancel send email"
        },
        confirmations: {
          desc: "",
          title: "Reading / delivery confirmation"
        },
        title: "Composition"
      },
      delegations: {desc: "", title: "Delegation"},
      filters: {
        actionsTitle: "Actions",
        activeFiltersTitle: "Active filters",
        desc: "Manage filters on incoming messages",
        headerItem: "Active filters",
        parametersTitle: "Parameters",
        placeholderList: {
          parameter: "Add parameter to the filter",
          action: "Add action to the filter"
        },

        title: "Filters"
      },
      folders: {
        deleteFolderDsc:
          "Deleting the folder  <code>{{folderName}}</code> will delete all the messages contained in them.<br/><b>The operation is not reversible</b>.",
        deleteFoldersTitle: "Delete folders",
        desc: "Manage and share your personal folders",
        descShared: "Manage your shared folders",
        headerItem: "Folders",
        headerMsg: "Messages",
        headerUnread: "Unread",
        modifyFolder: "Edit folder",
        newFolder: "Add new folder",
        rootFolder: "-- Root --",
        subscribe: "Subscribe",
        title: "Folders",
        titleShared: "Shared folders"
      },
      forward: {
        description: "Add max {{max}} recipients.",
        headerItem: "Recipients",
        placeholderList: " ",
        title: "Forward",
        titleSection: "Forward messages"
      },
      labels: {
        desc: "",
        headerItem: "Labels",
        deleteLabelTitle: "Delete label",
        deleteLabelTitleDsc: "Deleting labels will remove it from all messages.",
        placeholderList: " ",
        title: "Labels"
      },
      reading: {
        layout: {
          desc: "",
          title: "Layout"
        },
        readingSettings: {
          desc: "",
          title: "Reading settings"
        },
        title: "View"
      },
      signature: {
        title: "Signature",
        default: "Default",
        dropFiles: "Drop here the image to add",
        management: {
          title: "Signature management"
        },
        onlyNewMessages: "Don't add signature on replies",
        placeholderList: " ",
        senderBcc: "Add sender in Bcc",
        setAsDefault: "Set as default sender",
        signatures: {
          desc:
            "Customize your email by choosing the name displayed, who receives the message, and setting a signature to add on all your messages.<br/>You can customize the display name and signature for all identities (alias groups) associated with your account.",
          headerItemSignature: "Display name",
          headerItemIdentity: "Identity",
          title: "Signatures and identities"
        }
      },
      title: "Mail"
    },
    meeting: {
      title: "Meeting",
      view: {
        desc: "",
        title: "View"
      }
    },
    news: {
      readMore: "Read more",
      title: "Webmail news"
    },
    shortcuts: {
      title: "Keyboard shortcuts"
    },

    title: "Settings",
    unsaved: "There are unsaved changes"
  },

  toast: {
    error: {
      alreadyPresent: "Warning, the resource already exists",
      avatarTooBig: "Warning, avatar size too big",
      compact: "Error compacting folder",
      connectionUnavailable: "No connection available",
      delete: "Error on deletion",
      deleteDraft: "Error on draft deletion",
      enqueued: "Updating settings, try again",
      fetchingAttachments: "Error fetching attachments",
      filterActionsMissing: "Actions not present",
      filterConditionsMissing: "Parameters not present",
      filterNameMissing: "Filter name missing",
      folderCreation: "Error creating new folder",
      folderCreationCharNotValid: "Error creating/renaming folder, character not valid (/)",
      folderCreationNoName: "Error creating folder, you need to specify a valid name",
      folderDelete: "Error deleting folder",
      folderNotPresent: "Folder not present",
      folderRename: "Error renaming folder, already present",
      folderSubscribe: "Error subscribing folder",
      generic: "Something went wrong",
      maxMembers: "Warning, maximum number of members achieved",
      messageCopy: "Error copying messages",
      messageMove: "Error moving messages",
      messageNotFound: "Message not found",
      messageRemove: "Error removing messages",
      missingParameters: "Missing parameters",
      notifications: "Error getting notifications",
      notPermitted: "Operation not permitted",
      notValid: "Warning, invalid value",
      onlyOneHomeAddress: "You can insert only one home address",
      onlyOneWorkAddress: "You can insert only one work address",
      save: "Error saving",
      saveAcl: "Errore saving folder permissions",
      savingDraft: "Draft save in progress, please try again at the end",
      sendingMessage: "Error on message delivery",
      serverDisconnection: "Server being updated, please try again later!",
      sessionExpired: "Session expired",
      smtpBlockedByAntispam: "Message not sent: blocked by antispam",
      smtpBlockedByAntivirus: "Message not sent: blocked by antivirus",
      smtpDisabled: "Message not sent: SMTP disabled on this account",
      smtpQuotaExceeded: "Message not sent: sent message quota exceeded",
      requestLimit: "Request number limit exceeded, try again later",
      requestError: "Request error has occurred, try again later",
      updateFolders: "An error occurred on folders update",
      updateMessageEvent: "Error getting event status update",
      updatingParentFlags: "Error updating message flags"
    },
    info: {
      newVersionAvailable: {
        desc: "This operation is safe, your settings will not be modified.",
        title:
          "New features are available, reload the page to update the webmail to the lastest version."
      },
      serverReconnected: "Webmail back online",
      sourceMail: "Email source copied to clipboard"
    },
    progress: {
      newFolder: "New folder creating in progress...",
      operationInProgress: "Operation in progress..."
    },
    success: {
      compact: "Folder compacted successfully",
      copiedLink: "Link copied to clipboard",
      copiedMail: "Message successfully copied",
      copiedMailAddress: "Email address copied to clipboard",
      createFolder: "Folder created successfully",
      delete: "Deleted successful",
      disabledOtp: "OTP disabled successfully",
      enabledOtp: "OTP enabled successfully",
      movedMail: "Message moved successfully",
      otpDisabled: "OTP successfully disabled",
      otpEnable: "OTP successfully enabled",
      save: "Saved successfully",
      saveAcl: "Permissions saved successfully",
      saveDraft: "Draft saved successfully",
      saveFolder: "Folder updated successfully",
      sentMail: "Message sent successfully",
      subscribeFolder: "Subscription updated successfully",
      updateMessageEvent: "Event status updated"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "All day",
      autoSaveContact: "Save automatically all recipients not present in your address books",
      enableAutorespopnd: "Enable autoresponder",
      enableAutorespopndInterval: "Enable date range",
      keepCopy: "Keep copy of the message locally",
      showPriority: "Show received message priority",
      showThreads: "Show messages as thread",
      denyInvites: "Prevent invitations to appointments",
      enableDateSearch: "Enable date search",
      enableFullBody: "Enable full body search",
      subscribeFolder: "Subscribe folder"
    }
  },

  wizard: {
    layout: {
      desc: "Customize your Webmail by choosing view settings that you prefer.",
      title: "View options"
    },
    wizardData: {
      desc: "Customize your profile by adding an image and completing the missing data",
      title: "Complete profile"
    }
  }
};
