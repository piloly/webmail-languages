/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Contato selecionado",
      notSelected: "Selecionar um contato",
      sections: {
        cell: " Celular",
        home: "Casa",
        homeFax: "Fax residencial",
        work: "Trabalho",
        workFax: "Fax profissional"
      },
      selectContacts: "Contatos selecionados",
      selectedInfo: "Selecione um contato para ver os detalhes."
    },
    contactsList: {
      emptyContacts: "Esta lista de contatos está vazia",
      emptyContactsInfo: "Adicionar um contato a lista de contatos",
      foundContacts: "Encontrado",
      notFound: "Nenhum contato encontrado",
      notFoundInfo:
        "A busca não produziu nenhum resultado, tente novamente usando diferentes critérios",
      totalContacts: "Contatos"
    },
    contextualMenuLabels: {
      address: "Endereço",
      birthday: "Data de nascimento",
      cancel: "Cancelar",
      delete: "Deletar",
      downloadVCard: "Baixar VCard",
      email: "Email",
      newContact: "Novo contato",
      newList: "Nova lista",
      note: "Nota",
      phone: "Telefone",
      shareVCard: "Compartilhar",
      showVCard: "Mostrar VCard",
      sendAsAttachment: "Enviar por email",
      sendMail: "Enviar email",
      url: "URL"
    },
    deleteContactDsc:
      "Deletando o contato <code>{{contact}}</code> todas as informações relacionadas também serão deletadas.<br/><b>A operação não é reversível</b>.",
    deleteContactsDsc:
      "Deletando os contatos selecionados, todas as suas informações também serão deletadas.<br/><b>A operação não é reversível</b>.",
    deleteContactTitle: "Deletar contato",
    deleteContactDscList:
      "Deletando os contatos selecionados, todas as suas informações também serão deletadas.<br/><b>A operação não é reversível</b>.",
    deleteContactsTitle: "Deletar contatos",

    deleteListDsc:
      "Deletando a lista <code>{{contact}}</code> todas as informações relacionadas também serão deletadas.<br/><b>A operação não é reversível</b>.",
    deleteListTitle: "Deletar lista",

    editContactModal: {
      title: "Editar contato"
    },
    editListModal: {
      title: "Editar lista"
    },
    exportAddressBookModal: {
      desc:
        "Você está exportando a lista de contatos: <code>{{addressbookExport}}</code><br/>Por favor, escolha o formato para exportar todos os contatos presentes",
      title: "Exportar lista de contatos"
    },
    import: {
      contactsInvalid: "Não é valido:",
      contactsNr: "Total de contatos a serem importados:",
      contactsValid: "Válido:",
      desc:
        "Você está importando contatos para a lista de contatos: <code>{{addressbookImport}}</code><br/>Você pode importar contatos de outros aplicativos de email usando um VCard ou (<strong>.vcf</strong>) ou <strong>.ldif</strong> em formato de arquivo.\u000APor exemplo, você pode exportar contatos do Gmail ou Outlook e importá-los para o Qboxmail.\u000A A importação não vai substituir nenhum contato existente.",
      dropFiles: "Carregue ou arraste aqui os arquivos a serem importados",
      error: "Contato não pode ser importado",
      importing:
        "Estamos importando <b>{{contactsNumber}}</b> eventos no calendário: <code>{{addressbookImport}}</code>",
      loading: "Carregando...",
      success: "Importação concluída com sucesso!",
      title: "Importar contatos",
      uploadedFile: "Carregadas",
      uploadingFile: "Carregando"
    },
    list: {
      desc:
        "É possível adicionar até <b>150 membros</b> à lista selecionando entre os contatos da lista de contatos <code>{{addressbookName}}</code>.",
      members: "Membros da lista"
    },
    messages: {
      newContactAddressBookBody:
        "Você não pode adicionar um contato em <b>Todos os contatos</b> ou em <b>Contatos corporativos</b>.<br/>Selecione uma agenda pessoal ou compartilhe a lista de contatos",
      newContactAddressBookTitle: "Selecione a lista de contatos",
      resetListBody:
        "Os membros da lista são relacionados a lista de contatos selecionada. Esta mudança vai reconfigurar a lista de membros, deseja continuar?",
      resetListTitle: "Eliminação de membros"
    },
    newAddressBookModal: {
      desc: "",
      title: "Adicionar / editar catálogo de endereços"
    },
    newContactModal: {
      title: "Adicionar contato"
    },
    newListModal: {
      title: "Adicionar lista"
    },
    shareModal: {
      desc:
        "Você está compartilhando a lista de contatos: <code>{{addressbookShare}}</code><br/>Selecione os contatos com os quais você gostaria de compartilhar a lista de contatos e permiti-los gerenciar.",
      title: "Compartilhar lista de contatos"
    },
    sidebar: {
      allContacts: "Todos os contatos",
      deleteAddressbookDsc:
        "Deletando a lista de contatos <code>{{addressbook}}</code> todos os contatos presentes também serão deletados.<br/><b>A operação não é reversível</b>.",
      deleteAddressbookTitle: "Deletar lista de contatos",
      globalAddressbook: "Contatos corporativos",
      personalAddressbook: "Contatos pessoais",
      sharedAddressbooks: "Lista de contatos compartilhada",
      userAddressbooks: "Lista de contatos do usuário"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "Não há lista de contatos a ser subscrita",
      title: "Subscrever lista de contatos"
    }
  },

  app: {
    addressbook: "Lista de contatos",
    calendar: "Calendário",
    chat: "Conversas",
    meeting: "Reunião",
    of: "De",
    webmail: "Correio Eletrônico"
  },

  avatarEditorModal: {
    photoTitle: "Tirar foto",
    title: "Editar Avatar",
    zoom: "Zoom -/+"
  },

  buttons: {
    accept: "Aceitar",
    add: "Adicionar",
    addAction: "Adicionar ação",
    addAddressbook: "Lista de contatos",
    addAsAttachment: "Anexar",
    addCalendar: "Calendário",
    addField: "Adicionar campo",
    addHolidayCalendar: "Holiday calendar",
    addLabel: "Adicionar etiqueta",
    addInline: "Adicionar à fila",
    addParameter: "Adicionar parâmetro",
    addSignature: "Adiconar assinatura",
    addToList: "Adicionar à lista",
    apply: "Aplicar",
    backTo: "Voltar",
    backAddressbook: "Fechar",
    cancel: "Cancelar",
    changeImage: "Mudar imagem",
    changePassword: "Mudar senha",
    checkFreeBusy: "Verificar disponibilidade",
    close: "Fechar",
    createFolder: "Nova pasta",
    comeBackLater: "Retornar mais tarde",
    confirm: "Confirmar",
    copyLink: "Copiar link",
    decline: "Recusar",
    delete: "Deletar",
    deleteDraft: "Deletar rascunho",
    details: "Detalhes",
    disabled: "Desativar",
    disableOTP: "Desativar OTP",
    download: "Baixar",
    editLabel: "Editar etiqueta",
    editSignature: "Ediatr assinatura",
    empty: "Vazio",
    enabled: "Ativar",
    event: "Evento",
    export: "Exportar",
    goToMail: "Retornar ao email",
    goToOldWebmail: "Go to old webmail",
    import: "Importar",
    hide: "Esconder",
    label: "Etiqueta",
    leftRotate: "Girar 90 graus anti-horário",
    loadImage: "Carregar imagem",
    logout: "Encerrar seção",
    makePhoto: "Tirar foto",
    markRead: "Ler",
    markSeen: "Marcar",
    markUnread: "Não lido",
    message: "Mensagem",
    modify: "Editar",
    new: "Novo",
    newContact: "Novo contato",
    newEvent: "Novo evento",
    newMessage: "Nova mensagem",
    occurrence: "Ocorrência",
    openMap: "Abrir no mapa",
    reloadPage: "Recarregar página",
    removeImage: "Remover imagem",
    rename: "Renomear",
    reorder: "Reordenar",
    reorderEnd: "Fim do reordenamento",
    report: "Reportar",
    retryImport: "Tentar novamente",
    save: "Salvar",
    saveDraft: "Salvar rascunho",
    saveNextAvailability: "Salvar disponibilidade sugerida",
    search: "Buscar",
    send: "Enviar",
    series: "Séries",
    setOtp: "Configurar OTP",
    share: "Compartilhar",
    show: "Mostrar",
    showMore: "Outras opções",
    showUrlsAddressbook: "Mostrar URLS da lista de contatos",
    showUrlsCalendar: "Mostrar URLS do calendário",
    snooze: "Silenciar",
    starred: "Estrela",
    subscribe: "Inscrever",
    subscribeAddressbook: "Subscrever lista de contatos",
    subscribeCalendar: "Subscrever calendário",
    tentative: "Talvez",
    unsetOtp: "Desabilitar OTP",
    unsubscribe: "Cancelar inscrição",
    update: "Atualizar",
    updateFilter: "Atualizar filtro",
    viewMap: "Ver mapa"
  },

  buttonIcons: {
    activeSync: "Ativar ActiveSync",
    activeSyncNot: "Desativar ActiveSync",
    alignCenter: "Alinhar centro",
    alignLeft: "Alinhar à esquerda",
    alignRight: "Alinhar à direta",
    backgroundColor: "Cor de fundo",
    backTo: "Voltar",
    bold: "Negrito",
    confirm: "Confirmar",
    close: "Fechar",
    delete: "Deletar",
    download: "Baixar",
    downloadVCard: "Baixar VCard",
    edit: "Editar",
    emoji: "Emoji",
    forward: "Encaminhar",
    fullscreen: "Tela cheia",
    fullscreenNot: "Modo janela",
    imageAdd: "Adicionar imagem",
    indent: "Aumentar recuo",
    italic: "Itálico",
    maintenance: "Manutenção",
    maxmized: "Aumentar",
    minimized: "Reduzir",
    more: "Mais",
    moveDown: "Mover para baixo",
    moveUp: "Mover para cima",
    next: "Próximo",
    orderList: "Ordenar lista",
    outdent: "Diminuir recuo",
    pin: "Marcar",
    previous: "Anterior",
    print: "Imprimir",
    removeFormatting: "Remover formatação",
    reply: "Responder",
    replyAll: "Responder a todos",
    saveDraft: "Salvar rascunho",
    sendDebug: "Mendar mensagem que não pode ser visualizada",
    sendMail: "Enviar email",
    share: "Compartilhar",
    showSidebar: "Mostrar/Esconder barra lateral",
    showSourceCode: "Mostrar código fonte",
    showVCard: "Mostrar VCard",
    spam: "Marcar como Spam",
    spamNot: "Marcar como não sendo Spam",
    startChat: "Começar conversa",
    startMeeting: "Começar reunião",
    strike: "Atingir",
    subscribe: "Se inscrever",
    textColor: "Cor do texto",
    thanks: "Obrigado",
    underline: "Sublinhar",
    unorderList: "Desordenar lista",
    unSubscribe: "Cancelar inscrição"
  },

  calendar: {
    deleteEvent: "Deletar evento",
    deleteEventDsc: "Continuar com a eliminação do evento selecionado?",
    event: {
      deleteEventOccurrenceTitle: "Deletar evento ou série?",
      deleteEventOccurrenceDesc:
        "O evento que você quer deletar tem várias ocorrências. Deletar esta ocorrência deixara as outras inalteradas",
      fromDay: "De",
      maxLabelsNumberTitle: "Excesso de etiquetas!",
      maxLabelsNumberDsc:
        "Você atingiu o número máximo de etiquetas que podem ser associadas a um evento. Para adicionar novas etiquetas você deve primeiro transformar uma das já existentes.",
      organizerTitle: "Criado por",
      pendingMsg: "Evento esperando confirmação",
      repeatEvent: "Esse evento faz parte dos eventos recorrentes",
      saveEventOccurrenceTitle: "Mostrar eventos ou eventos recorrentes?",
      saveEventOccurrenceDesc:
        "O evento que você quer visualizar ou editar tem várias ocorrências. Editar uma única ocorrência deixará as demais inalteradas",
      toDay: "Para"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "Primeiro",
      WeekDayNumberInMonth_2: "Segundo",
      WeekDayNumberInMonth_3: "Terceiro",
      WeekDayNumberInMonth_4: "Quarto",
      WeekDayNumberInMonth_5: "Quinto",
      never: "Nunca",
      after: "Depois;recorrência",
      inDate: "Na data",
      end: "Fim da repetição"
    },
    filterView: {
      listmonth: "Agenda",
      day: "Dia",
      month: "Mês",
      week: "Semana",
      workWeek: "Semana de trabalho",
      year: "Ano"
    },
    import: {
      contactsInvalid: "Não válido:",
      contactsNr: "Total de eventos a serem importados:",
      contactsValid: "Válido:",
      dropFiles: "Carregue ou arraste aqui os arquivos a serem importados",
      error: "Evento não pode ser importado",
      importing:
        "Estamos importando <b>{{eventsNumber}}</b> eventos no calendário: <code>{{calendarImport}}</code>",
      info:
        "Você está importando eventos no calendário: <code>{{calendarImport}}</code><br/>Você pode importar eventos de outros aplicativos usando um arquivo de formato ICS. Por exemplo, você pode exportar eventos do Gmail ou Outlook em formato ICS e importá-los para Qboxmail.<br/><b>A importação não substituirá nenhum dos eventos existentes</b>.",
      loading: "Carregando...",
      success: "Todos os eventos foram importados com sucesso!",
      title: "Importar eventos",
      uploadedFile: "Carregadas",
      uploadingFile: "Carregando"
    },
    newCalendarModal: {
      desc: "",
      title: "Adicionar / Editar calendário"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Anexos",
        placeholderList: " "
      },
      desc: "",
      dropFiles: "Carregue ou arraste aqui arquivos para anexar",
      freeBusy: {
        legend: {
          event: "Eventos confidenciais",
          busy: "Ocupado",
          waiting: "Esperando",
          availability: "Próxima disponibilidade"
        }
      },
      hideDetails: "Esconder detalhes",
      participantsList: {
        headerItem: "Participantes",
        answeredHeaderItem: "Respondido",
        notAnsweredHeaderItem: "Esperando resposta",
        placeholderList: " "
      },
      showDetails: "Mostrar detalhes",
      tabAttachments: "Anexos",
      tabDetails: "Detalhes",
      tabParticipants: "Participantes",
      tabSeries: "Repetições",
      tabSettings: "Configurações",
      title: "Criar evento",
      titleEdit: {
        event: "Evento",
        series: "Séries"
      },
      titleFreebusy: "Assistentes Livres/Ocupados"
    },
    nextAvailability: "Próxima disponibilidade",
    searchEvents: {
      notFound: "Nenhum evento encontrado",
      notFoundInfo:
        "A busca não produziu nenhum resultado, tente novamente usando diferentes critérios"
    },
    shareModal: {
      desc:
        "Você está compartilhando o calendário: <code>{{calendarShare}}</code><br/>Selecione os contatos com os quais você deseja compartilhar o calendário e permita-os gerenciar.",
      title: "Compartilhar calendário"
    },
    sidebar: {
      labels: "Etiquetas",
      sharedCalendars: "Calendários compartilhados",
      userCalendars: "Calendários do usuário",
      deleteCalendarDsc:
        "Deletando o calendário <code>{{calendar}}</code> todos os eventos presentes serão deletados.<br/><b>A operação não é reversível</b>.",
      deleteCalendarTitle: "Deletar calendário"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "Não há calendários para se inscrever",
      title: "Inscrever-se ao calendário"
    },
    today: "Hoje"
  },

  circularLoopBar: {
    uploading: "Carregando"
  },

  comingSoon: "Desenvolvimento em andamento...",

  contactImport: {
    title: "Buscando contatos",
    desc: "Estamos importanto seus contatos antigos. Isso pode levar algum tempo, por favor espere."
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Respondido",
      "\\\\Deleted": "Deletado",
      "\\\\Draft": "Rascunho",
      "\\\\Flagged": "Marcado",
      "\\\\Seen": "Ler"
    },
    antispamFilterTypes: {
      whitelist_from: "Permitir de",
      whitelist_to: "Permitir para"
    },
    calendarMinutesInterval: {
      "15": "15 minutos",
      "30": "30 minutos",
      "60": "60 minutos"
    },
    calendarNewEventFreeBusy: {
      busy: "Ocupado",
      free: "Disponível",
      outofoffice: "Fora do escritório",
      temporary: "Temporário"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Aceitar",
      DECLINED: "Rejeitar",
      TENTATIVE: "Talvez"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Chairman",
      "REQ-PARTICIPANT": "Pedidos",
      "OPT-PARTICIPANT": "Não pedidos",
      "NON-PARTICIPANT": "Por motivos de informação"
    },
    calendarNewEventPriority: {
      "0": "Nenhum",
      "1": "Alto",
      "5": "Médio",
      "9": "Baixo"
    },
    calendarNewEventTypes: {
      public: "Público",
      confidential: "Confidencial",
      private: "Privado"
    },
    calendarStartWeekDay: {
      saturday: "Sábado",
      sunday: "Domingo",
      monday: "Segunda-feira"
    },
    calendarView: {
      year: "Ano",
      month: "Mês",
      week: "Semana",
      workWeek: "Semana de trabalho",
      day: "Dia",
      listmonth: "Lista"
    },
    calendarEventFilterBy: {allEvents: "Todos", nextEvents: "Todos os próximos eventos"},
    calendarEventSearchIn: {title: "Título/Etiqueta/Local", all: "Conteúdo completo"},
    cancelSendTimer: {
      "0": "Disabled",
      "5000": "5 seconds",
      "10000": "10 seconds",
      "20000": "20 seconds",
      "30000": "30 seconds"
    },
    contactNameFormat: {
      firstName: "Nome, Sobrenome",
      lastName: "Sobrenome, Nome"
    },
    contactsOrder: {
      firstName: "Nome",
      lastName: "Sobrenome"
    },
    countries: {
      AD: "Andorra",
      AE: "United Arab Emirates",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "American Samoa",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Åland Islands",
      AZ: "Azerbaijan",
      BA: "Bosnia and Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgium",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius and Saba",
      BR: "Brasil",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Belarus",
      BZ: "Belize",
      CA: "Canada",
      CC: "Cocos (Keeling) Islands",
      CD: "Democratic Republic of the Congo",
      CF: "Central African Republic",
      CG: "Republic of the Congo",
      CH: "Switzerland",
      CL: "Chile",
      CM: "Cameroun",
      CN: "China",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Cabo Verde",
      CW: "Curaçao",
      CX: "Christmas Island",
      CY: "Cyprus",
      CZ: "Czech Republic",
      DE: "Germany",
      DK: "Denmark",
      DM: "Dominica",
      DO: "Dominican Republic",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spain",
      ET: "Ethiopia",
      FI: "Finland",
      FO: "Faroe Islands",
      FR: "France",
      GA: "Gabon",
      GB: "United Kingdom",
      GD: "Grenada",
      GF: "French Guiana",
      GG: "Guernsey",
      GI: "Gibraltar",
      GL: "Greenland",
      GP: "Guadeloupe",
      GQ: "Equatorial Guinea",
      GR: "Greece",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croatia",
      HT: "Haiti",
      HU: "Hungary",
      IE: "Ireland",
      IM: "Isle of Man",
      IS: "Island",
      IT: "Italy",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Japan",
      KE: "Kenya",
      KR: "South Korea",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lithuania",
      LU: "Luxembourg",
      LV: "Latvia",
      MC: "Monaco",
      MD: "Moldova",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "North Macedonia",
      MQ: "Martinique",
      MT: "Malta",
      MW: "Malawi",
      MX: "Mexico",
      MZ: "Mozambique",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Netherlands",
      NO: "Norway",
      NZ: "New Zealand",
      PA: "Panama",
      PE: "Perú",
      PH: "Philippines",
      PL: "Poland",
      PT: "Portugal",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Sweden",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovakia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "South Sudan",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turkey",
      TZ: "Tanzania",
      UA: "Ukraine",
      UG: "Uganda",
      US: "United States of America",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "South Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Sempre pedir confirmação de recebimento",
      never: "Nunca pedir confirmação de recebimento"
    },
    eventAlarmPostponeTime: {
      "60": "por 1 minuto",
      "300": "por 5 minutos",
      "900": "por 15 minutos",
      "1800": "por 30 minutos",
      "3600": "por 1 hora",
      "7200": "por 2 horas",
      "86400": "por 1 dia",
      "604800": "por 1 semana"
    },

    calendarTaskStatus: {
      "needs-action": "Necessita ação",
      completed: "Finalizado",
      "in-process": "Em andamento",
      cancelled: "Cancelado"
    },

    feedbackAreas: {
      addressbookSidebar: "Listagem de listas de contatos",
      contactList: "Lista de contatos",
      contactView: "Exibição do contato",
      mailSearch: "Procurar mensagem",
      mailSidebar: "Lista de pastas",
      messageList: "Lista de mensagens",
      messageView: "Exibição da mensagem",
      newMessage: "Nova composição de mensagens",
      other: "Mais",
      settings: "Configurações"
    },
    filterMail: {
      all: "Todos",
      byDate: "Data",
      byFrom: "De",
      bySize: "Tamanho",
      bySubject: "Assunto",
      flagged: "Marcado",
      filter: "Filtros ativos",
      orderBy: "Ordenar por",
      seen: "Lido",
      unseen: "Não lido"
    },
    filtersActions: {
      discard: "Remover",
      addflag: "Marcar mensagem como",
      setkeyword: "Adicionar etiqueta",
      fileinto: "Mover mensagem para",
      fileinto_copy: "Copiar mensagem para",
      keep: "Guardar mensagem",
      redirect: "Redirecionar mensagem para",
      redirect_copy: "Mandar cópia para",
      reject: "Rejeitar com a seguinte mensagem",
      stop: "Parar filtragem"
    },
    filtersDateOperators: {
      gt: "Depois",
      is: "é igual a",
      lt: "Antes de",
      notis: "não é igual a"
    },
    filtersEnvelopeSubtypes: {From: "De", To: "Para"},
    filtersDateSubTypes: {date: "Data", time: "Hora", weekday: "Dia da semana"},
    filtersDateWeekdays: {
      monday: "Segunda-feira",
      tuesday: "Terça-feira",
      wednesday: "Quarta-feira",
      thursday: "Quinta-feira",
      friday: "Sexta-feira",
      saturday: "Sábado",
      sunday: "Domingo"
    },
    filtersRules: {
      Bcc: "Cco",
      body: "Corpo",
      Cc: "Cc",
      currentdate: "Data de entrada",
      envelope: "Mensagem",
      From: "Origem",
      other: "Outros responsáveis",
      size: "Tamanho",
      Subject: "Assunto",
      To: "Destino"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "é mais que", under: "é menos que"},
    filtersStringOperators: {
      contains: "contém",
      exists: "existe",
      is: "é igual a",
      notcontains: "não contém",
      notexists: "não existe",
      notis: "não é igual a"
    },
    languages: {
      de: "Alemão",
      en: "Inglês",
      it: "Italiano",
      nl: "Holandês",
      sv: "Suéco",
      es: "Espanhol",
      pt: "Português"
    },
    matchTypes: {
      allof: "Satisfaz todos os parâmetros",
      anyof: "Satisfaz pelo menos um parâmetro"
    },
    newAllDayEventAlert: {
      never: "Nunca",
      eventDate: "Data do evento",
      "-PT17H": "Um dia antes, às 9h",
      "-P6DT17H": "Uma semana antes, às 9h"
    },
    newEventAlert: {
      never: "Nunca",
      eventHour: "Na hora de início do evento",
      //P[n]Y[n]M[n]DT[n]H[n]M[n]S
      "-PT5M": "5 minutos antes",
      "-PT15M": "15 minutos antes",
      "-PT30M": "30 minutos antes",
      "-PT1H": "1 hora antes",
      "-PT2H": "2 horas antes",
      "-PT12H": "12 horas antes",
      "-P1D": "1 dia antes",
      "-P7D": "1 semana antes"
    },
    newEventRepeatDayWeek: {
      MO: "Segunda-feira",
      TU: "Terça-feira",
      WE: "Quarta-feira",
      TH: "Quinta-feira",
      FR: "Sexta-feira",
      SA: "Sábado",
      SU: "Domingo"
    },
    newEventRepeatFrequency: {
      NEVER: "Nunca",
      DAILY: "Diariamente",
      WEEKLY: "Semanalmente",
      MONTHLY: "Mensalmente",
      YEARLY: "Anualmente"
    },
    newEventRepeatMonths: {
      "1": "Janeiro",
      "2": "Fevereiro",
      "3": "Março",
      "4": "Abril",
      "5": "Maio",
      "6": "Junho",
      "7": "Julho",
      "8": "Agosto",
      "9": "Setembro",
      "10": "Outubro",
      "11": "Novembro",
      "12": "Dezembro"
    },
    orderSelector: {
      byLastname: "Sobrenome",
      byMail: "Email",
      byName: "Nome",
      byOrg: "Companhia",
      orderBy: "Ordenar por",
      showOnlyLists: "Mostrar apenas listas"
    },
    readConfirmations: {
      always: "Sempre pedir confirmação de leitura",
      never: "Nunca pedir confirmação de leitura"
    },
    readingConfirmation: {
      always: "Sempre",
      ask: "Sempre pedir",
      never: "Nunca"
    },
    selectMail: {
      all: "Selecionar tudo",
      cancel: "Cancelar",
      modify: "Editar",
      none: "Deselecionar tudo"
    },
    shareOptionsCalendar: {
      None: "Nenhum",
      DAndTViewer: "Visualizar data e hora",
      Viewer: "Visualizar tudo",
      Responder: "Responder as",
      Modifier: "Modificar"
    },
    showImages: {
      always: "Sempre",
      contacts: "Contatos e de confiança",
      never: "Nunca"
    },
    themes: {
      dark: "Escuro",
      light: "Claro"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Disposição em 3 colunas",
      viewHalf: "Disposição como pré-visualização",
      viewFull: "Disposição como lista"
    },
    viewModeCompact: {
      viewColumns: "Colunas",
      viewHalf: "Visualização",
      viewFull: "Lista"
    }
  },

  dropdownMenu: {
    activesync: "ActiveSync",
    delete: "Deletar",
    download: "Baixar",
    edit: "Editar",
    exportContacts: "Exportar contatos",
    importContacts: "Importar contatos",
    importEvents: "Importar eventos",
    remove: "Remover",
    rename: "Renomear",
    share: "Compartilhar",
    syncNo: "Não sincronizar",
    syncYes: "Sincronizar",
    unsubscribe: "Cancelar inscrição"
  },

  dropdownUser: {
    feedback: "Realimentar",
    logout: "Encerrar seção",
    settings: "Configurações"
  },

  errors: {
    emailDomainAlreadyPresent: "Endereço de email ou domínio já presente",
    emptyField: "Campo em branco",
    invalidEmail: "Email inválido",
    invalidEmailDomain: "Endereço de email ou domínio inválido",
    invalidPassword: "Senha inválida",
    invalidPasswordFormat: "Formato inválido, inserir ao menos 8 caracteres alfanuméricos",
    invalidValue: "Valor inválido",
    mandatory: "Campo obrigatório",
    notAllowedCharacter: "Caracter inválido",
    notChanged: "Campo inalterado",
    permissionDenied: "Permissão negada",
    tooLong: "Campo muito longo"
  },

  feedback: {
    disclaimer:
      "Sua opinião é importante! Nos ajude a resolver os problemas e melhorar nossos serviços",
    labels: {
      feedbackAreas: "Em qual área você deseja deixar sua sugestão?",
      note: "Deixe uma mensagem (opcional)"
    },
    msgError: {
      title: "Sua sugestão não pode ser enviada",
      desc:
        "Por favor, tente novamente mais tarde, é importante para nós sabermos o que você acha do nosso webmail. Suas críticas e sugestões nos ajudarão a criar uma experiência melhor para todos os nossos usuários."
    },
    msgSuccess: {
      title: "Muito obrigado pela sua resposta!",
      desc:
        "Nós agradecemos o tempo que você investiu nos ajudando a melhorar nosso correio eletrônico. Suas críticas e sugestões nos ajudarão a criar uma experiência melhor para todos os nossos usuários."
    },
    privacy:
      "As informações coletadas serão processadas confidencialmente e levando em consideração toda a legislação de confidencialidade vigente.",
    title: "Críticas e sugestões"
  },

  inputs: {
    labels: {
      addAddress: "Adicionar endereço",
      addDomain: "Adicionar endereço ou domínio",
      addLabel: "Nome da etiqueta",
      addMembers: "Adicionar membros",
      addParticipant: "Adicionar participantes",
      addressbookName: "Lista de contatos",
      addUser: "Adicionar usuário",
      alert: "Alerta",
      alert2: "Segundo Alerta",
      at: "Às",
      calendar: "Calendário",
      calendarBusinessDayEndHour: "Dia de trabalho final",
      calendarBusinessDayStartHour: "Dia de trabalho inicial",
      calendarColor: "Cor do calendário",
      calendarMinutesInterval: "Tempo de intervalo do calendário",
      calendarName: "Nome do calendário",
      calendarView: "Visualização padrão",
      company: "Empresa",
      confidentialEvents: "Eventos confidenciais",
      confirmNewPassword: "Confirmar senha",
      contactNameFormat: "Formato do nome do contato",
      contactsOrder: "Ordem do contato",
      country: "Country",
      dateFormat: "Formato da data",
      defaultAddressbook: "Lista de contatos padrão",
      defaultCalendar: "Calendário padrão",
      displayName: "Nome do dispositivo",
      email: "Email",
      endDate: "Data final",
      event: "Evento",
      eventDailyRepetition: "Todos;os dias",
      eventDailyWorkdayRepetition: "Cada dia da semana",
      eventMonthlyLastDayRepetition: "Último dia do mês, todos;os meses",
      eventMonthlyLastWeekDayRepetition: "Último {{WeekDay}}, todos;os meses",
      eventMonthlyRepetition: "No {{DayNumber}}, todo;mês",
      eventMonthlyWeekDayRepetition: "No {{WeekDayNumber}}, todo;mês",
      eventPartecipation: "Status de participação",
      eventPriority: "Prioridade do evento",
      eventType: "Tipo de evento",
      eventWeeklyRepetition: "Todo;mês, no:",
      eventYearlyLastDayRepetition: "Último dia de {{Month}}, todo;ano",
      eventYearlyLastWeekDayRepetition: "Último {{WeekDay}} do {{Month}}, todos;os anos",
      eventYearlyRepetition: "Em {{DayNumber}} de {{Month}}, todos;os anos",
      eventYearlyWeekDayRepetition: "No {{WeekDayNumber}} de {{Month}}, todos;os anos",
      exportAsVcard: "Exportar em formato VCard",
      exportAsLdiff: "Eportar em formato LDIF",
      faxHome: "Fax residencial",
      faxWork: "Fax profissional",
      filter: "Filtrar por",
      filterName: "Filtrar nomes",
      firstName: "Nome",
      folderName: "Nome da pasta",
      freeBusy: "Mostrar como",
      from: "De",
      fromDate: "A partir da data",
      home: "Casa",
      hour: "Hora",
      includeFreeBusy: "Incluir em livre-ocupado",
      label: "Etiqueta",
      labelColor: "Cor de etiqueta",
      language: "Idioma",
      lastName: "Sobrenome",
      listName: "Lista de nomes",
      location: "Localização",
      loginAddressbook: "Lista de contatos inicial",
      mailAccount: "Conta",
      mobile: "Celular",
      newAddressBook: "Nova lista de contatos",
      newAllDayEventAlert: "Novo alerta de eventos de dia todo",
      newCalendar: "Novo calendário",
      newEventAlert: "Novo alerta de eventos",
      newForward: "Adicionar destinatário à lista",
      newPassword: "Nova senha",
      nickname: "Apelido",
      object: "Assunto",
      oldPassword: "Senha atual",
      parentFolder: "Caminho",
      phone: "Telefone",
      privateEvents: "Eventos privados",
      publicEvents: "Eventos públicos",
      readingConfirmation: "Enviar confirmação de leitura",
      recoveryEmail: "Email de recuperação",
      renameAddressbook: "Renomear lista de contatos",
      repeatEvery: "Repetir cada",
      reportBugNote: "Descrição do problema",
      reportBugPhoneNumber: "Número do telefone",
      searchAddressbookToSubscribe: "Buscar uma lista de contatos para ser subscrita",
      searchCalendarToSubscribe: "Buscar um calendário para ser subscrito",
      searchIn: "Buscar em",
      selectCalendar: "Selecionar calendário",
      selectIdentity: "Selecionar identidade",
      selectList: "Selecionar lista",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "Com quem você deseja compartilhar a lista de contatos?",
      shareToCalendar: "Com quem você deseja compartilhar o calendário?",
      shareToFolder: "Com quem você deseja compartilhar a pasta?",
      showImages: "Mostrar imagem",
      signatureName: "Nome de exibição",
      signatureText: "Texto de assinatura",
      startDate: "Data de início",
      team: "Equipe",
      theme: "Tema",
      timeFormat: "Formato de hora",
      timezone: "Fuso horário",
      title: "Título",
      to: "Para",
      untilDate: "Até a data",
      weekStart: "Primeiro dia da semana",
      work: "Trabalho"
    },
    placeholder: {
      addressbookName: "Nome da lista de contatos",
      birthday: "Data de nascimento",
      city: "Cidade",
      country: "País",
      company: "Empresa",
      email: "Email",
      firstName: "Nome",
      lastName: "Sobrenome",
      members: "Membros",
      newFolderName: "Nome da nova pasta",
      newNameAddressbook: "Nome da nova lista de contatos",
      newPassword: "Pelos menos 8 caracteres alfanuméricos",
      note: "Nota",
      phone: "Telefone",
      postCode: "Código postal",
      region: "Região",
      role: "Função",
      select: "Selecionar",
      search: "Buscar em",
      searchActivity: "Buscar atividade",
      searchEvent: "Buscar evento",
      snooze: "Silenciar",
      street: "Rua"
    },
    hint: {
      cancelSendMessage:
        "Select the time available to retract a message during sending, if you decide you don't want to send the email."
    }
  },

  labels: {
    attendees: "Participantes",
    default: "Padrão",
    forDomain: "On domain",
    withAllUsers: "Com todos os usuários",
    calendarAdvancedSearch: "Onde você quer buscar?",
    clearAdvancedFilters: "Limpar filtros",
    clearFields: "Limpar campos",
    holidayCalendar: "Holiday",
    searchResults: "Resultados da busca"
  },

  loaderFullscreen: {
    description: "Os conteúdos estão chegando, por favor espere.",
    descriptionFolder: "Suas pastas estão chegando, por favor espere,",
    title: "Carregando"
  },

  mail: {
    confirmMailLabel: "Ok, eu confirmo!",
    contextualMenuLabels: {
      addAddressBook: "Adicionar à lista de contatos",
      addEvent: "Adicionar evento",
      alarm: "Alarme",
      backTo: "Voltar",
      cancel: "Cancelar",
      close: "Fechar",
      confirm: "Confirmar",
      copyIn: "Copiar em",
      copyThreadIn: "Copiar tópico",
      delete: "Deletar",
      deletePerm: "Deletar permanentemente",
      deleteThread: "Deletar tópico",
      deliveryNotification: "Confirmação de entrega",
      download: "Baixar",
      editAsNew: "Editar como novo",
      emptySpam: "Marcar Spam como lido",
      emptyTrash: "Esvaziar lixeira",
      forward: "Encaminhar",
      import: "Importar email",
      label: "Etiqueta",
      labelsManagement: "Gerenciar etiquetas",
      labelThread: "Tópico da etiqueta",
      markAllAsRead: "Marcar tudo como lido",
      markAs: "Marcar como",
      markThreadAs: "Marcar tópico",
      manageFolders: "Gerenciar pastas",
      modify: "Editar",
      more: "Mais",
      moveThreadTo: "Mais tópicos",
      moveTo: "Mover para",
      newSubFolder: "Adcionar sub-pasta",
      plainText: "Modo de texto sem formatação",
      print: "Imprimir",
      priorityHigh: "Alta prioridade",
      readed: "Lido",
      readedNot: "Não lido",
      readingNotification: "Confirmação de leitura",
      rename: "Renomear",
      reply: "Responder",
      replyToAll: "Responder a todos",
      restore: "Restaurar",
      restoreThreadIn: "Restaurar tópico",
      restoreIn: "Restaurar em",
      sendAsAttachment: "Encaminhar como anexo",
      showSourceCode: "Mostrar code de origem",
      spam: "Spam",
      spamNot: "Não é Spam",
      starred: "Favorito",
      starredNot: "Desmarcar como favorito",
      thanks: "Obrigado",
      thread: "Ações de tópico"
    },
    deleteMailDsc:
      "Se um sistema de armazenamento não está ativo, a mensagem será permanentemente deletada.<br/><b>The operation is not reversible</b>.",
    deleteMailTitle: "Deletar mensagem",
    deleteMailsDsc:
      "Se um sistema de armazenamento não está ativo, a mensagem será permanentemente deletada.<br/><b>The operation is not reversible<b>.",
    deleteMailsTitle: "Deletar mensagens selecionadas",
    dropFiles: "Solte aqui os arquivos para anexar",
    emptyTrashDsc:
      "Se um sistema de armazenamento não está ativo, esvaziando a lixeira todas as mensagens serão eliminadas <b>irreversivelmente</b>.<br/>Após <b>30 dias</b> todas as mensagens são deletadas automaticamente.",
    emptyTrashTitle: "Esvaziar lixeira",
    import: {
      messagesInvalid: "Inválido:",
      messagesNr: "Total de mensagens a serem importadas:",
      messagesValid: "Válido:",
      desc:
        "Você está importando mensagens na pasta: <code>{{folderImport}}</code><br/>Você pode importar mensagens de outros aplicativos de email usando arquivos eml (<strong>.eml</strong>).",
      dropFiles: "Carregue ou arraste aqui os arquivos a serem importados",
      error: "A mensagem não pode ser importada",
      importing:
        "Estamos importando <b>{{messagesNumber}}</b> mensagens na pasta: <code>{{folderImport}}</code>",
      loading: "Carregando...",
      success: "Importado com sucesso!",
      title: "Importar mensagens",
      uploadedFile: "Carregadas",
      uploadingFile: "Carregando"
    },
    markSpamReadDsc:
      "Marcar todas as mensagens na pasta como lidas.<br/>Após <b>30 dias</b> as mensagens serão deletadas automaticamente.",
    markSpamReadTitle: "Marcar mensagens como lidas",
    maxLabelsNumberTitle: "Número excessivo de etiquetas!",
    maxLabelsNumberDsc:
      "Você atingiu o número máximo de etiquetas que podem ser associadas a uma mensagem. Para adicionar uma nova etiqueta você deve remover uma dentre as já existentes.",
    message: {
      add: "Adicionar",
      addToAddressBook: "Adicionar contato à lista de contatos",
      attachedMessage: "Anexar mensagem",
      attachments: "Anexos",
      bugMsg: "Não consegue visualizar a mensagem?",
      cc: "Cc:",
      date: "Data:",
      draftsSelectedInfo: "Selecione uma mensagem da lista e continue para escrever",
      downloadAll: "Baixar tudo",
      downloadAttachments: "Baixar anexos",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Editar status dos participantes",
      eventActionAccept: "Aceitar",
      eventActionDecline: "Rejeitar",
      eventActionTentative: "Talvez",
      eventAddToCalendar: "Esse evento não está presente no seu calendário",
      eventAttendees: "Participantes",
      eventAttendeeStatusAccepted: "Evento aceito",
      eventAttendeeStatusDeclined: "Evento rejeitado",
      eventAttendeeStatusTentative: "Participação incerta",
      eventCounterProposal: "Essa mensagem contém uma contra-proposta para o evento",
      eventDeleted: "Evento deletado",
      eventInvitation: "Confite de evento:",
      eventLocationEmpty: "Não especificado",
      eventModify: "Editar status dos participantes",
      eventNeedAction: "Confirmar participação no evento",
      eventNotNeedAction: "Evento já ocorrido",
      eventOutDated: "O evento foi modificado",
      eventStatusAccepted: "Aceito",
      eventStatusDeclined: "Rejeitado",
      eventStatusTentative: "Provisório",
      eventUpdate: "Atualizar evento",
      folderSelectedInfo: "Selecione uma mensagem da lista e descubra quem lhe escreveu.",
      from: "De:",
      hideAllAttachments: "Esconder",
      hugeMessage:
        "Conteúdo muito grande para ser exibido inteiramente, clique no link abaixo para visualizar o código fonte da mensagem.",
      hugeMessageNotice: "Mensagem truncada",
      inboxSelectedInfo: "Selecione uma mensagem da lista e veja a quem você escreveu.",
      loadImages: "Baixar imagem",
      localAttachments: "There are additional attachments inside the event",
      message: "Mensagem",
      messageForwarded: "Mensagem encaminhada",
      messages: "Mensagens",
      messagesQuota: "Cota de mensagens usada:",
      newMessageinThread: "Você tem uma nova mensagem",
      notSelected: "Selecione uma mensagem",
      oneSelectMessages: "Mensagem selecionada",
      oneSelectThreads: "Tópico selecionado",
      readNotificationIgnore: "Ignorar",
      readNotificationLabel: "{{Sender}} pediu confirmação de letura para esta mensagem.",
      readNotificationLabelSimple: "Confirmação de leitura da mensagem pedida.",
      readNotificationResponse:
        "<P>Message<BR><BR>&nbsp;&nbsp;&nbsp; A:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Subject:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Sent:&nbsp; %SENDDATE%<BR><BR>it was read the day %READDATE%.</P>",
      readNotificationSend: "Confirmar",
      readNotificationSubjectPrefix: "Ler:",
      replyTo: "Responder para:",
      sender: "Remetente:",
      sentSelectedInfo: "Selecione uma mensagem da lista e veja a quem você escreveu.",
      showAllAttachments: "Mostrar outros",
      showMoreThread: "Mostrar mais",
      selectMessages: "Mensagens selecionadas",
      selectThreads: "Tópico selecionado",
      spamNotSelected: "Manter a pasta em ordem",
      spamSelectedInfo: "Verificar mensagens e marcar spam como lidos.",
      subject: "Assunto:",
      to: "Para:",
      trashNotSelected: "Verificar lixeira",
      trashSelectedInfo: "Você pode recuperar mensagens da lixeira em 30 dias.",
      updating: "Edição em andamento",
      yesterday: "Ontem",
      wrote: "escrito:"
    },
    messagesList: {
      allMessagesLoaded: "Todas as mensagens foram carregadas",
      ctaSpamInfo: "Marcar tudo como lido",
      ctaTrashInfo: "Esvaziar lixeira",
      folderEmpty: "Esta pasta está vazia",
      folderInfo: "Começar conversa ou mover uma mensagem.",
      foundMessages: "Encontrado",
      moveMessage: "Mover mensagem",
      moveMessages: "Mover {{NumMessages}} mensagens",
      notFound: "Nenhuma mensagem encontrada",
      notFoundInfo:
        "A pesquisa não produziu resultados, tente novamente usando diferentes critérios",
      pullDownToRefresh: "Arraste para atualizar",
      releaseToRefresh: "Solte para atualizar",
      searchInfo: "Mensagem encontrada",
      selectMessages: "Selecionada",
      showMoreThread: "Mostrar mais",
      spamEmpty: "Todos os Spams desapareceram!",
      spamInfo: "Mensagens na caixa de spam são automaticamente deletadas após 30 dias.",
      trashEmpty: "A lixeira está vazia",
      trashInfo: "Mensagens na lixeira são deletadas automaticamente depois de 30 dias.",
      totalMessages: "Mensagens"
    },
    newMessage: {
      addAttach: "Adicionar anexo",
      addImgeDsc:
        "Você pode inserir imagens diretamente entre as linhas no corpo da mensagem ou anexá-las à mensagem.",
      addImgeTitle: "Inserir imagem",
      advanced: "Avançado",
      bcc: "Cco",
      cc: "Cc",
      cancelSend: "Envio de email cancelado com sucesso",
      delete: "Deletar",
      deleted: "Mensagem deletada com sucesso",
      deleteInProgress: "Deletando mensagem",
      discardChanges: "Descartar alterações",
      fontSize: {
        smaller: "Smaller",
        small: "Small",
        normal: "Normal",
        medium: "Medium",
        big: "Big",
        bigger: "Bigger"
      },
      from: "De",
      hide: "Esconder",
      invalidMails:
        "Um endereço de email é inválido, verifique os endereços de email e tente novamente",
      moreAttachments: "Outros",
      noDraftChanged: "Sem alterações para salvar",
      object: "Assunto",
      saveDraftDsc:
        "A mensagem não foi enviada e contém alterações não salvas. Você pode salvá-la como rascunho e continuar escrevendo mais tarde",
      saveDraftTitle: "Salvar mensagem como rascunho?",
      savedDraft: "Rascunho salvo!",
      savingDraft: "Salvando",
      sendInProgress: "Envio de mensagem em andamento",
      sent: "Mensagem enviada com sucesso",
      sizeExceeded:
        "Anexo muito grande. O tamanho máximo de arquivo opara envio é de {{messageMaxSize}} MB.",
      to: "Para",
      toMore: "Outros",
      windowSubject: "Nova mensagem"
    },
    reportBugMessage:
      "Se você não consegue visualizar a mensagem corretamente, você pode nos ajudar a resolver o problema e melhorar nosso serviço. Talvez nós precisemos contatá-lo, por favor, insira seu número de telefone e descreva o problema que está enfrentando.<br/><br/>Continuando com suas criticas e sugestões, sua mensagem poderá ser visualizada pelos nossa equipe de desenvolvimento para podermos melhorar nosso serviço.<br/><b>As informações NÃO serão compartilhadas com terceiros e serão tratadas de acordo com a legislação de privacidade vigente.</b>.",
    reportBugTitle: "Denunciar mensagem",
    searchMail: {
      attachments: "Anexos",
      cleanFilters: "Limpar filtros",
      marked: "Favorito"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Criar Etiqueta"
      },
      createFolder: {
        desc: "",
        title: "Criar pasta"
      },
      modifyFolder: {
        desc: "",
        title: "Editar pasta"
      },
      restoreFolder: {
        desc: "",
        title: "Restaurar pasta"
      },
      folder: "Pastas",
      folders: "Pastas pessoais",
      labels: "Etiquetas",
      newFoderName: "Nome da nova pasta",
      occupiedSpace: "Espaço utilizado",
      sharedFolders: "Pastas compartilhadas",
      unlimitedSpace: "Espaço ilimitado"
    },
    thanksMailLabel: "Ok, obrigado!",
    tooManyCompositionsDsc:
      "Você atingiu o número máximo de novas mensagens que você pode escrever ao mesmo tempo.Termine pelo menos uma mensagem dentre as mensagens que você está escrevendo para iniciar uma nova.",
    tooManyCompositionsTitle: "Escrita terminada!"
  },

  mailboxes: {
    drafts: "Rascunhos",
    inbox: "Caixa de entrada",
    more: "Mais",
    sent: "Enviada",
    shared: "Pastas compartilhadas",
    spam: "Spam",
    trash: "Lixeira"
  },

  navbar: {
    addressbook: "Contatos",
    calendar: "Calendário",
    chat: "Conversas",
    mail: "Email",
    meeting: "Reunião",
    news: "Novidades"
  },

  newsTags: {
    news: "Novidades",
    suggestions: "Dicas",
    fix: "Reparar",
    comingSoon: "Em breve",
    beta: "Beta"
  },

  pageLogout: {
    title: "Encerrando seção",
    desc: ""
  },

  pageIndexing: {
    title: "Sua conta está indexada",
    desc: "Esta operação pode demorar algum tempo, por favor, espere."
  },

  pageNotFound: {
    pageNotFound: "Algo deu errado",
    pageNotFoundInfo:
      "A página que você está procurando não está disponível, não perca as esperanças!"
  },

  powered: "impulsionado por Qboxmail",
  poweredCollaborationWith: "em colaboração com ",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Confirmação de entrega de mensagem",
      askReadConfirmation: "Confirmação de leitura de mensagem",
      defaultView: "Exibição padrão",
      newEventType: "Tipo de novos eventos",
      newEventFreeBusy: "Mostrar novos eventos disponíveis"
    }
  },

  shareOptions: {
    delete: "Eliminação",
    read: "Lendo",
    update: "Editando",
    write: "Escrevendo"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Permitir inserção de eventos no calendário",
    canEraseObjects: "Permitir exclusão de eventos do calendário"
  },

  shortcuts: {
    addressbook: {
      newContact: "Novo contato",
      editContact: "Editar contato",
      deleteContact: "Deletar contato"
    },
    calendar: {
      newEvent: "Novo evento",
      editEvent: "Editar evento",
      deleteEvent: "Deletar evento",
      moveRangeNext: "Visualizar próximo intervalo",
      moveRangePrevious: "Visualizar intervalo anterior"
    },
    general: {
      switchModuls: "Mudar módulos",
      refresh: "Atualizar página",
      tabForward: "Avançar entre os elementos clicáveis",
      tabBack: "Volte entre os elementos clicáveis",
      closeModal: "Fechar módulo",
      openSettings: "Abrir configurações",
      copy: "Copiar itens selecionados",
      past: "Colar itens selecionados",
      cut: "Recortar itens selecionados"
    },
    mail: {
      confirm: "Confirmar",
      deleteMessage: "Deletar mensagem",
      deleteSelectMessage: "Deletar mensagens selecionadas",
      editAsNew: "Editar como novo",
      empityTrash: "Esvaziar lixeira",
      expandThread: "Expandir tópico",
      extendSelectDown: "Extender seleção para baixo na lista de mensagens",
      extendSelectDown: "Extender seleção para baixo na lista de mensagens",
      extendSelectUp: "Extender seleção para cima na lista de mensagens",
      forward: "Encaminhar",
      markRead: "Marcar como lida/não lida",
      moveDown: "Mover para baixo na lista de mensagens",
      moveUp: "Mover para cima na lista de mensagens",
      newMessage: "Nova mensagem",
      openSelectMessage: "Abrir mensagem selecionada",
      printMessage: "Imprimir mensagem",
      reduceThread: "Reduzir tópico",
      reply: "Responder",
      replyAll: "Responder à todos",
      thanks: "Obrigado"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Salvar automaticamente"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Lista de contatos",
          placeholderList: " ",
          title: "Lista de contatos pessoal"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Lista de contatos inscrita",
          placeholderList: " ",
          title: "Compartilhar lista de contatos"
        },
        title: "Listas de contatos"
      },
      title: "Contatos",
      view: {
        desc: "",
        title: "Visualizar"
      }
    },
    calendar: {
      activities: {title: "Atividades"},
      events: {title: "Eventos", desc: "", alertTitle: "Alertas", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Gerenciamento de convites",
        msgFromHeader: "Permitir convites apenas de",
        placeholderList: " "
      },
      labels: {title: "Etiquetas", desc: ""},
      labelsTask: {title: "Etiquetas de tarefa", desc: ""},
      management: {
        title: "Calendários",
        holidayCalendars: {
          title: "Calendário de férias",
          desc: "",
          headerItem: "Calendários",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Calendário pessoal",
          desc: "",
          headerItem: "Calendários",
          placeholderList: " "
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Calendários inscritos",
          placeholderList: " ",
          title: "Calendários compartilhados"
        },
        includeInFreeBusy: "Incluir em Livre/Ocupado",
        showActivities: "Mostrar atividades",
        showAlarms: "Mostrar alarmes",
        syncWithActivesync: "Sincronizar usando ActiveSync",
        sendMeMailOnMyEdit: "Mandar-me um email quando eu modificar o calendário",
        sendMeMailOnOtherEdit: "Mandar-me um email quando alguém modificar o calendário",
        sendMailOnEditTo: "Quando eu modificar, mandar um email para"
      },
      view: {
        showBusyOutsideWorkingHours: "Mostrar como ocupado fora do horário de trabalho",
        showWeekNumbers: "Mostrar número da semana",
        title: "Visualizar"
      },
      title: "Calendário"
    },
    chat: {
      title: "Conversas",
      view: {
        title: "Visualizar"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "Email do usuário",
        generalSettings: {
          desc: "",
          title: "Configurações gerais"
        },
        personalInfo: {
          desc: "",
          title: "Informações pessoais"
        },
        title: "Perfil"
      },
      security: {
        changepassword: {
          info: "Senha com pelo menos 8 caracteres alfanuméricos",
          desc: "",
          resetpassword: "Configurar nova senha",
          resetpassworddisabled: "Mudança de senha está desativada nesta conta",
          resetpasswordexpired: "Sua senha expirou, configure uma nova senha",
          title: "Mudar senha"
        },
        otp: {
          cantDisableOtp: "Autentificação de dois fatores não pode ser desabilitada nesta conta",
          desc: "",
          disableOtp: "Desabilitar autentificação de dois fatores",
          hideParams: "Ocultar parâmetros",
          instructionDisableOtp:
            "Informar o código gerado pelo aplicativo na forma abaixo para desabilitar autentificação de dois fatores para essa conta.",
          instructionsSetOtp:
            "Escaneie o QRCode pelo aplicativo e informe o código gerado no fomato abaixo. Ou informe os parâmetros para ativar  manualmente.",
          labelEnterCode: "Informe o código gerado pelo aplicativo",
          paramAccount: "Conta",
          paramCode: "Código",
          paramType: "Base de tempo",
          paramTypeVal: "Ativo",
          params: "Informe o código manualmente",
          paramsInstruction: "Informe os seguintes parâmetros no aplicativo",
          setOtp: "Habilitar autentificação de dois fatores",
          title: "Configurar OTP",
          titleDisableOtp: "Desabilitar OTP",
          titleSetOtp: "Habilitar OTP"
        },
        sessions: {
          browser: "Navegador:",
          currentSession: "Atual",
          device: "Aparelho:",
          desc: "",
          ip: "IP:",
          os: "OS:",
          removeAll: "Remover tudo",
          removeSession: "Remover",
          startdate: "Iniciar:",
          title: "Abrir seção"
        },
        title: "Segurança"
      },
      title: "Geral"
    },
    mail: {
      accounts: {desc: "", title: "Contas"},
      antispam: {
        blacklist: {
          desc: "Adicionar endereço ou domínio à lista (ex. 'nome@domínio' ou '*@domínio')",
          msgNoHeader: "Não permitir mensagens de",
          title: "Lista negra"
        },
        deleteRuleDsc: "Deletar conjunto de regras de <code>{{ruleEmail}}</code>.",
        deleteRuleTitle: "Deletar regra antispam",
        placeholderList: " ",
        title: "Antispam",
        whitelist: {
          desc: "Adicionar endereço ou domínio à lista (ex. 'nome@domínio' ou '*@domínio')",
          msgFromHeader: "Permitir mensagens de",
          msgToHeader: "Permitir mensagens para",
          title: "Lista branca"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Mensagem",
        placholdeSubject: "Assunto",
        placholderTextarea: "Texto da mensagem",
        title: "Autoresposta"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Cancel send email"
        },
        confirmations: {
          desc: "",
          title: "Confirmação de leitura / entrega"
        },
        title: "Composição"
      },
      delegations: {desc: "", title: "Delegação"},
      filters: {
        actionsTitle: "Ações",
        activeFiltersTitle: "Filtros ativos",
        desc: "Gerenciar filtros em mensagens recebidas",
        headerItem: "Filtros ativos",
        parametersTitle: "Parâmetros",
        placeholderList: {
          parameter: "Adicionar parâmetros ao filtro",
          action: "Adicionar ações ao filtro"
        },

        title: "Filtros ativos"
      },
      folders: {
        deleteFolderDsc:
          "Deletar a pasta  <code>{{folderName}}</code> deletará as mensagens contidas.<br/><b>A operação não é reversível</b>.",
        deleteFoldersTitle: "Deletar pastas",
        desc: "Gerenciar e compartilhar suas pastas pessoais",
        descShared: "Gerenciar suas pastas compartilhadas",
        headerItem: "Pastas",
        headerMsg: "Mensagens",
        headerUnread: "Não lido",
        modifyFolder: "Editar pasta",
        newFolder: "Adicionar nova pasta",
        rootFolder: "-- Origem --",
        subscribe: "Inscrever",
        title: "Pastas",
        titleShared: "Pastas compartilhadas"
      },
      forward: {
        description: "Adicionar máximo {{max}} destinatários",
        headerItem: "Destinatários",
        placeholderList: " ",
        title: "Encaminhar",
        titleSection: "Encaminhar mensagens"
      },
      labels: {
        desc: "",
        headerItem: "Etiquetas",
        deleteLabelTitle: "Deletar etiqueta",
        deleteLabelTitleDsc: "Deletar etiquetas irá removê-las de todas as mensagens",
        placeholderList: " ",
        title: "Etiquetas"
      },
      reading: {
        layout: {
          desc: "",
          title: "Disposição"
        },
        readingSettings: {
          desc: "",
          title: "Configurações de leitura"
        },
        title: "Visualizar"
      },
      signature: {
        title: "Assinatura",
        default: "Default",
        dropFiles: "Arraste aqui uma imagem para adicioná-la",
        management: {
          title: "Signature management"
        },
        onlyNewMessages: "Não adicione assinatura nas respostas",
        placeholderList: " ",
        senderBcc: "Adicionar remetente a Cco",
        setAsDefault: "Set as default sender",
        signatures: {
          desc:
            "Customize your email by choosing the name displayed, who receives the message, and setting a signature to add on all your messages.<br/>You can customize the display name and signature for all identities (alias groups) associated with your account.",
          headerItemSignature: "Nome de exibição",
          headerItemIdentity: "Identidade",
          title: "Assinaturas e identidades"
        }
      },
      title: "Email"
    },
    meeting: {
      title: "Reunião",
      view: {
        desc: "",
        title: "Visualizar"
      }
    },
    news: {
      readMore: "Leia mais",
      title: "Novidades do correio eletrônico"
    },
    shortcuts: {
      title: "Atalhos"
    },

    title: "Configurações",
    unsaved: "Existem alterações não salvas"
  },

  toast: {
    error: {
      alreadyPresent: "Atenção, o recurso já existe",
      avatarTooBig: "Atenção, tamanho do avatar muito grande",
      compact: "Erro ao compactar a pasta",
      connectionUnavailable: "Sem conexão disponível",
      delete: "Erro ao deletar",
      deleteDraft: "Erro ao deletar rascunho",
      enqueued: "Atualizando configurações, tente novamente",
      fetchingAttachments: "Erro ao anexar",
      filterActionsMissing: "Ação inexistente",
      filterConditionsMissing: "Parâmetro inexistente",
      filterNameMissing: "Falta nome do filtro",
      folderCreation: "Erro ao criar uma nova pasta",
      folderCreationCharNotValid: "Erro ao criar/renomear uma pasta, caractere inválido (/)",
      folderCreationNoName: "Erro ao criar pasta, você precisa informar um nome válido",
      folderDelete: "Erro ao deletar pasta",
      folderNotPresent: "Pasta inexistente",
      folderRename: "Erro ao renomear pasta, pasta já existe",
      folderSubscribe: "Erro ao subscrever pasta",
      generic: "Algo deu errado",
      maxMembers: "Atenção, número máximo de membros atingido",
      messageCopy: "Erro ao copiar mensagem",
      messageMove: "Erro ao mover mensagem",
      messageNotFound: "Mensagem não encontrada",
      messageRemove: "Erro ao remover mensagem",
      missingParameters: "Parâmetros faltando",
      notifications: "Erro ao receber notificações",
      notPermitted: "Operação não permitida",
      notValid: "Atenção, valor inválido",
      onlyOneHomeAddress: "Você pode informar apenas um endereço residencial",
      onlyOneWorkAddress: "Você pode informar apenas um endereço profissional",
      save: "Erro ao salvar",
      saveAcl: "Erro ao salvar permissões de pasta",
      savingDraft: "Salvando rascunho, por favor tente novamente ao final",
      sendingMessage: "Erro na entrega da mensagem",
      serverDisconnection: "Servidor sendo atualizado, por favor tente novamente mais tarde!",
      sessionExpired: "Sua sessão expirou",
      smtpBlockedByAntispam: "Mensagem não enviada: bloqueada pelo antispam",
      smtpBlockedByAntivirus: "Message not sent: blocked by antivirus",
      smtpDisabled: "Mensagem não enviada: SMTP desativado nesta conta",
      smtpQuotaExceeded: "Mensagem não enviada: cota de envio de mensagens excedida",
      requestLimit: "Número limite de pedidos excedido, tente novamente mais tarde",
      requestError: "Ocorreu um erro no pedido, tente novamente mais tarde",
      updateFolders: "Ocorreu um erro na atualização dos arquivos",
      updateMessageEvent: "Erro ao obter atualização dos status do evento",
      updatingParentFlags: "Erro ao atualizar os indicadores da mensagem"
    },
    info: {
      newVersionAvailable: {
        desc: "Esta operação é segura, suas configurações não serão modificadas",
        title:
          "Novos recursos estão disponíveis, recarregue a página para atualizar o correio eletrônico para a última versão"
      },
      serverReconnected: "Correio eletrônico online de volta",
      sourceMail: "Fonte do email copiado para área de transferência"
    },
    progress: {
      newFolder: "Criando nova pasta",
      operationInProgress: "Operação em andamento"
    },
    success: {
      compact: "Pasta compactada com sucesso",
      copiedLink: "Link copiado para área de transferência",
      copiedMail: "Mensagem copiada com sucesso",
      copiedMailAddress: "Endereço de email copiado para área de transferência",
      createFolder: "Pasta criada com sucesso",
      delete: "Deletado com sucesso",
      disabledOtp: "Desativação de OTP ocorrida com sucesso",
      enabledOtp: "Ativação de OTP ocorrida com sucesso",
      movedMail: "Mensagem movida com sucesso",
      otpDisabled: "OTP desabilitado com sucesso",
      otpEnable: "OTP habilitado com sucesso",
      save: "Salvo com sucesso",
      saveAcl: "Permissões salvas com sucesso",
      saveDraft: "Rascunho salvo com sucesso",
      saveFolder: "Pasta atualizada com sucesso",
      sentMail: "Mensagem enviada com sucesso",
      subscribeFolder: "Inscrição atualizada com sucesso",
      updateMessageEvent: "Status de evento atualizado"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "Dia inteiro",
      autoSaveContact:
        "Salvar automaticamente todos os destinatários que não constem na sua lista de contatos",
      enableAutorespopnd: "Ativar autoresposta",
      enableAutorespopndInterval: "Ativar faixa de data",
      keepCopy: "Manter localmente uma cópia da mensagem",
      showPriority: "Mostrar prioridade da mensagem recebida",
      showThreads: "Mostrar mensagem como tópico",
      denyInvites: "Prevenir convites para compromissos",
      enableDateSearch: "Ativar busca de datas",
      enableFullBody: "Ativar pesquisa completa",
      subscribeFolder: "Subscrever pasta"
    }
  },

  wizard: {
    layout: {
      desc:
        "Customizar seu correio eletrônico escolhendo as suas preferências de configurações de visualização",
      title: "Opções de visualização"
    },
    wizardData: {
      desc: "Customizar seu perfil adicionando imagens e preenchendo dados que estão faltando",
      title: "Completar perfil"
    }
  }
};
