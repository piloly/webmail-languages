/*********************************************************************************************************
 * English Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * All rights reserved. No part of this publication may be reproduced, distributed, or transmitted in any
 * form or by any means, including photocopying, recording, or other electronic or mechanical
 * methods, without the prior written permission of the publisher, except in the case of brief quotations
 * embodied in critical reviews and certain other noncommercial uses permitted by copyright law. For
 * permission requests, write to the publisher at the address below.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *
 * Italiano Copyright (C) 2017-2019 by Qboxmail Srl
 *
 * Tutti i diritti riservati. Nessuna parte di questa pubblicazione può essere riprodotta, memorizzata in
 * sistemi di recupero o trasmessa in qualsiasi forma o attraverso qualsiasi mezzo elettronico, meccanico,
 * mediante fotocopiatura, registrazione o altro, senza l'autorizzazione del possessore del copyright salvo
 * nel caso di brevi citazioni a scopo critico o altri usi non commerciali consentiti dal copyright. Per le
 * richieste di autorizzazione, scrivere all'editore al seguente indirizzo.
 *
 * Qboxmail Srl - IT02338120971
 * https://www.qboxmail.it - info@qboxmail.it
 *********************************************************************************************************/

module.exports = {
  addressbook: {
    contact: {
      oneSelectContacts: "Ausgewählter Kontakt",
      notSelected: "Kontakt auswählen",
      sections: {
        cell: " Handy",
        home: "Telefon Privat",
        homeFax: "Fax Privat",
        work: "Telefon Arbeit",
        workFax: "Fax Arbeit"
      },
      selectContacts: "Ausgewählte Kontakte",
      selectedInfo:
        "Wählen Sie einen Kontakt aus der Liste aus, um dessen Informationen anzuzeigen."
    },
    contactsList: {
      emptyContacts: "Dieses Adressbuch ist leer",
      emptyContactsInfo: "Fügen Sie Ihrem Adressbuch einen Kontakt hinzu",
      foundContacts: "Gefundene Kontakte",
      notFound: "Keinen Kontakt gefunden",
      notFoundInfo:
        "Die Suche ergab keine Ergebnisse. Ändern Sie bitte die Suchkriterien und versuchen Sie es anschließend erneut.",
      totalContacts: "Kontakte"
    },
    contextualMenuLabels: {
      address: "Adresse",
      birthday: "Geburtstag",
      cancel: "Beenden",
      delete: "Löschen",
      downloadVCard: "Download VCard",
      email: "E-Mail",
      newContact: "Neuer Kontakt",
      newList: "Neues Adressbuch",
      note: "Notizen",
      phone: "Telefon",
      shareVCard: "Teilen",
      showVCard: "VCard anzeigen",
      sendAsAttachment: "Als E-Mail versenden",
      sendMail: "E-Mail senden",
      url: "Webseite"
    },
    deleteContactDsc:
      "Durch das Löschen des Kontaktes <code>{{contact}}</code> werden alle zugehörigen Informationen gelöscht.<br/><b>Dieser Vorgang kann nicht rückgängig gemacht werden.</b>.",
    deleteContactsDsc:
      "Durch das Löschen der ausgewählen Kontakte werden alle zugehörigen Informationen gelöscht.<br/><b>Dieser Vorgang kann nicht rückgängig gemacht werden.</b>",
    deleteContactTitle: "Kontakte löschen",
    deleteContactDscList:
      "Durch das Löschen des ausgewählten Kontaktes werden alle zugehörigen Informationen gelöscht.<br/><b>Dieser Vorgang kann nicht rückgängig gemacht werden.</b>",
    deleteContactsTitle: "Kontakt löschen",

    deleteListDsc:
      "Durch das Löschen der Liste <code>{{contact}}</code> werden alle zugehörigen Informationen gelöscht.<br/><b>Dieser Vorgang kann nicht rückgängig gemacht werden.</b>",
    deleteListTitle: "Liste löschen",

    editContactModal: {
      title: "Kontakt ändern"
    },
    editListModal: {
      title: "Adressliste ändern"
    },
    exportAddressBookModal: {
      desc:
        "Sie exportieren das Adressbuch: <code>{{addressbookExport}}</code><br/>Bitte wählen Sie das Format für den Export aller vorhandenen Kontakte.",
      title: "Adressbuch exportieren"
    },
    import: {
      contactsInvalid: "Ungültige Kontakte:",
      contactsNr: "Gesamtzahl der zu importierenden Kontakte:",
      contactsValid: "Gültige Kontakte:",
      desc:
        "Sie importieren Kontakte in das Adressbuch: <code>{{addressbookImport}}</code><br/>Sie können Kontakte aus anderen E-Mail-Programmen in den Formaten vCard (<strong>.vcf</strong>) oder <strong>.ldif</strong> importieren.\u000AZum Beispiel können Sie Kontakte aus Gmail oder Outlook exportieren und in Qboxmail importieren.\u000ADurch das Importieren werden keine vorhandenen Kontakte ersetzt.",
      dropFiles:
        "Laden Sie die zu importierende Datei hier hoch oder fügen Sie sie mittels Drag & Drop hier ein.",
      error: "Kontakte konnten nicht importiert werden",
      importing:
        "Wir importieren <b>{{contactsNumber}}</b> Kontakte in das Adressbuch: <code>{{addressbookImport}}</code>",
      loading: "Lädt...",
      success: "Der Import wurde erfolgreich abgeschlossen!",
      title: "Kontakte importieren",
      uploadedFile: "Hochgeladen",
      uploadingFile: "Wird hochgeladen"
    },
    list: {
      desc:
        "Es ist möglich, bis zu <b>150 Mitglieder</b> zu einer Liste hinzuzufügen, indem Sie aus den Kontakten im Adressbuch <code>{{addressbookName}}</code> auswählen.",
      members: "Mitglieder der Liste"
    },
    messages: {
      newContactAddressBookBody:
        "Der Kontakt kann nicht im Adressbuch <b>Alle Kontakte</b> oder im <b>Firmenadressbuch</b> hinzugefügt werden.<br/>Bitte wählen Sie ein persönliches oder freigegebenes Adressbuch aus.",
      newContactAddressBookTitle: "Adressbuch auswählen",
      resetListBody:
        "Listenmitglieder sind mit dem ausgewählten Adressbuch verknüpft. Die Änderung führt zum Löschen der eingebenen Kontakte. Trotzdem fortfahren ?",
      resetListTitle: "Mitglieder löschen"
    },
    newAddressBookModal: {
      desc: "",
      title: "Adressbuch erstellen / bearbeiten"
    },
    newContactModal: {
      title: "Kontakt erstellen"
    },
    newListModal: {
      title: "Liste erstellen"
    },
    shareModal: {
      desc:
        "Sie teilen das Adressbuch: <code>{{addressbookShare}}</code><br/>Wählen Sie die Kontakte aus, für die Sie das Adressbuch freigeben möchten und weisen Sie ihnen Verwaltungsberechtigungen zu.",
      title: "Adressbuch teilen"
    },
    sidebar: {
      allContacts: "Alle Kontakte",
      deleteAddressbookDsc:
        "Durch das Löschen des Adressbuchs <code>{{addressbook}}</code> werden auch enthaltenen Kontakte gelöscht.<br/><b>Dieser Vorgang kann nicht rückgängig gemacht werden.</b>",
      deleteAddressbookTitle: "Adressbuch löschen",
      globalAddressbook: "Firmenadressbuch",
      personalAddressbook: "Persönliches Adressbuch",
      sharedAddressbooks: "Freigegebene Adressbücher",
      userAddressbooks: "Benutzeradressbuch"
    },
    subscribeModal: {
      desc: "",
      noAddressbook: "Keine freigegebenen Adressbücher vorhanden",
      title: "Freigegebenes Adressbuch abonnieren"
    }
  },

  app: {
    addressbook: "Adressbuch",
    calendar: "Kalender",
    chat: "Chat",
    meeting: "Meeting",
    of: "von",
    webmail: "Webmail"
  },

  avatarEditorModal: {
    photoTitle: "Machen Sie ein Foto",
    title: "Avatar ändern",
    zoom: "Zoom -/+"
  },

  buttons: {
    accept: "Annehmen",
    add: "Hinzufügen",
    addAction: "Aktion hinzufügen",
    addAddressbook: "Adressbuch",
    addAsAttachment: "Dateien anhängen",
    addCalendar: "Kalender",
    addField: "Feld hinzufügen",
    addHolidayCalendar: "Holiday calendar",
    addLabel: "Label hinzufügen",
    addInline: "In Zeile hinzufügen",
    addParameter: "Parameter hinzufügen",
    addSignature: "Signatur hinzufügen",
    addToList: "Zur Liste hinzufügen",
    apply: "Anwenden",
    backTo: "Zurück",
    backAddressbook: "Schließen",
    cancel: "Beenden",
    changeImage: "Bild ändern",
    changePassword: "Passwort ändern",
    checkFreeBusy: "Verfügbarkeit prüfen",
    close: "Schließen",
    createFolder: "Neuer Ordner",
    comeBackLater: "Später zurückkommen",
    confirm: "Bestätigen",
    copyLink: "Link kopieren",
    decline: "Ablehnen",
    delete: "Löschen",
    deleteDraft: "Entwurf löschen",
    details: "Details",
    disabled: "Deaktiviert",
    disableOTP: "OTP deaktivieren",
    download: "Donwload",
    editLabel: "Label ändern",
    editSignature: "Signatur ändern",
    empty: "Leeren",
    enabled: "Aktiviert",
    event: "Ereignis",
    export: "Export",
    goToMail: "Zurück zur Mail",
    goToOldWebmail: "Zur alten Webmail-Oberfläche",
    hide: "Verstecken",
    import: "Importieren",
    label: "Label",
    leftRotate: "90 Grad nach links drehen",
    loadImage: "Bild hochladen",
    logout: "Abmelden",
    makePhoto: "Foto machen",
    markRead: "Gelesen",
    markSeen: "Markieren",
    markUnread: "Ungelesen",
    message: "Nachricht",
    modify: "Ändern",
    new: "Neu",
    newContact: "Neuer Kontakt",
    newEvent: "Neues Ereignis",
    newMessage: "Neue Nachricht",
    occurrence: "Wiederholung",
    openMap: "In Karte öffnen",
    reloadPage: "Seite neu laden",
    removeImage: "Foto entfernen",
    rename: "Umbenennen",
    reorder: "Sortierung anpassen",
    reorderEnd: "Sortierung abschließen",
    report: "Report",
    retryImport: "Erneut versuchen",
    save: "Speichern",
    saveDraft: "Entwurf speichern",
    saveNextAvailability: "Vorgeschlagene Verfügbarkeit speichern",
    search: "Suchen",
    send: "Senden",
    series: "Serie",
    setOtp: "OTP aktivieren",
    share: "Teilen",
    show: "Anzeigen",
    showMore: "Weitere Optionen",
    showUrlsAddressbook: "Adressbuch-URL anzeigen",
    showUrlsCalendar: "Kalender-URL anzeigen",
    snooze: "Schlummermodus",
    starred: "Markieren",
    subscribe: "Abonnieren",
    subscribeAddressbook: "Adressbuch abonnieren",
    subscribeCalendar: "Kalender abonnieren",
    tentative: "Vielleicht",
    unsetOtp: "OTP deaktivieren",
    unsubscribe: "Abmelden",
    update: "Aktualisieren",
    updateFilter: "Filter aktualisieren",
    viewMap: "Karte anzeigen"
  },

  buttonIcons: {
    activeSync: "ActiveSync aktivieren",
    activeSyncNot: "ActiveSync deaktivieren",
    alignCenter: "Zentriert",
    alignLeft: "Linksbündig",
    alignRight: "Rechtsbündig",
    backgroundColor: "Hintergrundfarbe",
    backTo: "Zurück",
    bold: "Fett",
    confirm: "Bestätigen",
    close: "Schließen",
    delete: "Löschen",
    download: "Download",
    downloadVCard: "Download VCard",
    edit: "Bearbeiten",
    emoji: "Emoji",
    forward: "Weiterleiten",
    fullscreen: "Fullscreen",
    fullscreenNot: "Fullscreen minimieren",
    imageAdd: "Bild einfügen",
    indent: "Einzug vergrößern",
    italic: "Kursiv",
    maintenance: "Wartung",
    maxmized: "Vergrößern",
    minimized: "Verkleinern",
    more: "Mehr",
    moveDown: "Nach unten",
    moveUp: "Nach oben",
    next: "Nächster",
    orderList: "Nummerierte Liste",
    outdent: "Einzug verringern",
    pin: "Fixieren",
    previous: "Vorherige",
    print: "Drucken",
    removeFormatting: "Formatierung entfernen",
    reply: "Antworten",
    replyAll: "Allen antworten",
    saveDraft: "Entwurf speichern",
    sendDebug: "Nachricht senden, die nicht angezeigt werden kann",
    sendMail: "E-Mail senden",
    share: "Teilen",
    showSidebar: "Seitenleiste ein- / ausblenden",
    showSourceCode: "Quellcode anzeigen",
    showVCard: "VCard anzeigen",
    spam: "Als Spam markieren",
    spamNot: "Kein Spam",
    startChat: "Chat starten",
    startMeeting: "Meeting starten",
    strike: "Durchsteichen",
    subscribe: "Abonnieren",
    textColor: "Schriftfarbe",
    thanks: "Vielen Dank",
    underline: "Unterstichen",
    unorderList: "Liste mit Aufzählungszeichen",
    unSubscribe: "Abmelden"
  },

  calendar: {
    deleteEvent: "Ereignis löschen",
    deleteEventDsc: "Mit dem Löschen des ausgewählten Ereignisses fortfahren ?",
    event: {
      deleteEventOccurrenceTitle: "Ereignis oder Serienereignis löschen",
      deleteEventOccurrenceDesc:
        "Das Ereignis, das Sie löschen möchten, ist Teil einer Serie. Durch das Löschen des einzelnen Ereignisses bleiben die anderen Ereignisse der Serie unverändert.",
      fromDay: "von",
      maxLabelsNumberTitle: "Zu viele Labels!",
      maxLabelsNumberDsc:
        "Sie haben die maximale Anzahl von Labels erreicht, die einem Ereignis zugeordnet werden können. Um ein neues Label hinzufügen zu können, müssen Sie zuerst ein zugeordnetes Label entfernen.",
      organizerTitle: "Erstellt von",
      pendingMsg: "Unbestätigtes Ereignis",
      repeatEvent: "Dieses Ereignis ist Teil eines Serienereignisses",
      saveEventOccurrenceTitle: "Ereignis oder Serienereignis anzeigen?",
      saveEventOccurrenceDesc:
        "Das Ereignis, das Sie anzeigen oder bearbeiten möchten, ist Teil einer Serie. Durch das Ändern des einzelnen Ereignisses bleiben die anderen Ereignisse der Serie unverändert.",
      toDay: "bis"
    },
    eventRepetition: {
      WeekDayNumberInMonth_1: "Erste",
      WeekDayNumberInMonth_2: "Zweite",
      WeekDayNumberInMonth_3: "Dritte",
      WeekDayNumberInMonth_4: "Vierte",
      WeekDayNumberInMonth_5: "Fünfte",
      never: "Nie",
      after: "Nach;Wiederholungen",
      inDate: "Am",
      end: "Wiederholung beenden"
    },
    filterView: {
      listmonth: "Agenda",
      day: "Tag",
      month: "Monat",
      week: "Woche",
      workWeek: "Arbeitswoche",
      year: "Jahr"
    },
    import: {
      contactsInvalid: "Ungültig:",
      contactsNr: "Anzahl der zu importierenden Ereignisse:",
      contactsValid: "Gültig:",
      dropFiles:
        "Laden Sie die zu importierende Datei hier hoch oder fügen Sie sie mittels Drag & Drop hier ein",
      error: "Die Ereignisse konnten nicht importiert werden",
      importing:
        "Wir importieren <b>{{eventsNumber}}</b> Ereignisse in den Kalender: <code>{{calendarImport}}</code>",
      info:
        "Sie importieren Ereignisse in den Kalender: <code> {{calendarImport}} </ code> <br/> Sie können Ereignisse aus anderen E-Mail-Programmen mittels einer ICS-Datei importieren. Zum Beispiel können Sie Ereignisse aus Gmail oder Outlook exportieren und in Qboxmail importieren. <br/> <b> Durch das Importieren werden keine vorhandenen Ereignisse ersetzt.</ b>",
      loading: "Lädt...",
      success: "Alle Ereignisse wurden erfolgreich importiert!",
      title: "Ereignisse importieren",
      uploadedFile: "Hochgeladen",
      uploadingFile: "Wird hochgeladen"
    },
    newCalendarModal: {
      desc: "",
      title: "Kalender erstellen / ändern"
    },
    newEvent: {
      attachmentsList: {
        headerItem: "Anhänge",
        placeholderList: " "
      },
      desc: "",
      dropFiles: "Dateien anhängen",
      freeBusy: {
        legend: {
          event: "Ereignis",
          busy: "Besetzt",
          waiting: "Provisorisch",
          availability: "Verfügbarkeit"
        }
      },
      hideDetails: "Detais ausblenden",
      participantsList: {
        headerItem: "Teilnehmer",
        answeredHeaderItem: "Zugesagt",
        notAnsweredHeaderItem: "Ausstehend",
        placeholderList: " "
      },
      showDetails: "Details anzeigen",
      tabAttachments: "Anhänge",
      tabDetails: "Details",
      tabParticipants: "Teilnehmer",
      tabSeries: "Wiederholungen",
      tabSettings: "Einstellungen",
      title: "Ereignis erstellen",
      titleEdit: {
        event: "Ereignis",
        series: "Serie"
      },
      titleFreebusy: "Verfügbarkeit der Teilnehmer"
    },
    nextAvailability: "Nächste Verfügbarkeit",
    searchEvents: {
      notFound: "Kein Ereignis gefunden",
      notFoundInfo:
        "Es konnte kein passendes Ereignis gefunden werden. Ändern Sie bitte die Suchkriterien und versuchen Sie es anschließend erneut."
    },
    shareModal: {
      desc:
        "Sie teilen den Kalender: <code>{{calendarShare}}</code><br/>Wählen Sie die Kontakte aus, für die Sie den Kalender freigeben möchten und weisen Sie ihnen Verwaltungsberechtigungen zu.",
      title: "Kalender freigeben"
    },
    sidebar: {
      labels: "Labels",
      sharedCalendars: "Freigegebene Kalender",
      userCalendars: "Benutzerkalender",
      deleteCalendarDsc:
        "Durch das Löschen des Kalenders <code>{{calendar}}</code> werden auch alle vorhandenen Ereignisse gelöscht.<br/><b>Dieser Vorgang kann nicht rückgängig gemacht werden.</b>",
      deleteCalendarTitle: "Kalender löschen"
    },
    subscribeModal: {
      desc: "",
      noCalendar: "Es gibt keinen freigegebenen Kalender den Sie annehmen müssen",
      title: "Freigegebenen Kalender annehmen"
    },
    today: "Heute"
  },

  circularLoopBar: {
    uploading: "Lädt"
  },

  comingSoon: "In Ausarbeitung...",

  contactImport: {
    title: "Kontakte suchen",
    desc:
      "Wir importieren Ihre Kontakte. Dies kann einige Zeit dauern. Wir bitten Sie um ein wenig Geduld."
  },

  datasets: {
    actionsFlags: {
      "\\\\Answered": "Beantwortet",
      "\\\\Deleted": "Gelöscht",
      "\\\\Draft": "Entwurf",
      "\\\\Flagged": "Markiert",
      "\\\\Seen": "Gelesen"
    },
    antispamFilterTypes: {
      whitelist_from: "Erlauben von",
      whitelist_to: "Erlauben an"
    },
    calendarMinutesInterval: {
      "15": "15 Minuten",
      "30": "30 Minuten",
      "60": "60 Minuten"
    },
    calendarNewEventFreeBusy: {
      busy: "Besetzt",
      free: "Verfügbar",
      outofoffice: "Außer Haus",
      temporary: "Provisorisch"
    },
    calendarNewEventPartecipation: {
      ACCEPTED: "Annehmen",
      DECLINED: "Ablehnen",
      TENTATIVE: "Vielleicht"
    },
    calendarNewEventParticipationRoles: {
      CHAIR: "Vorsitzender",
      "REQ-PARTICIPANT": "Obligatorisch",
      "OPT-PARTICIPANT": "Nicht obligatorisch",
      "NON-PARTICIPANT": "Zu Informationszwecken"
    },
    calendarNewEventPriority: {
      "0": "Keine",
      "1": "Hoch",
      "5": "Mittel",
      "9": "Niedrig"
    },
    calendarNewEventTypes: {
      public: "Öffentlich",
      confidential: "Vertraulich",
      private: "Privat"
    },
    calendarStartWeekDay: {
      saturday: "Samstag",
      sunday: "Sonntag",
      monday: "Montag"
    },
    calendarView: {
      year: "Jahr",
      month: "Monat",
      week: "Woche",
      workWeek: "Arbeitswoche",
      day: "Tag",
      listmonth: "Liste"
    },
    calendarEventFilterBy: {allEvents: "Alle", nextEvents: "Zukünftige Ereignissse"},
    calendarEventSearchIn: {title: "Titel/Label/Ort", all: "Gesamten Inhalt"},
    cancelSendTimer: {
      "0": "Deaktiviert",
      "5000": "5 Sekunden",
      "10000": "10 Sekunden",
      "20000": "20 Sekunden",
      "30000": "30 Sekunden"
    },
    contactNameFormat: {
      firstName: "Vorname, Nachname",
      lastName: "Nachname, Vorname"
    },
    contactsOrder: {
      firstName: "Vorname",
      lastName: "Nachname"
    },
    countries: {
      AD: "Andorra",
      AE: "United Arab Emirates",
      AG: "Antigua & Barbuda",
      AI: "Anguilla",
      AL: "Albania",
      AM: "Armenia",
      AO: "Angola",
      AR: "Argentina",
      AS: "American Samoa",
      AT: "Austria",
      AU: "Australia",
      AW: "Aruba",
      AX: "Åland Islands",
      AZ: "Azerbaijan",
      BA: "Bosnia and Herzegovina",
      BB: "Barbados",
      BD: "Bangladesh",
      BE: "Belgium",
      BF: "Burkina Faso",
      BG: "Bulgaria",
      BH: "Bahrain",
      BI: "Burundi",
      BJ: "Bénin",
      BL: "Saint Barthélemy",
      BM: "Bermuda",
      BN: "Brunei",
      BO: "Bolivia",
      BQ: "Bonaire, Sint Eustatius and Saba",
      BR: "Brasil",
      BS: "Bahamas",
      BW: "Botswana",
      BY: "Belarus",
      BZ: "Belize",
      CA: "Canada",
      CC: "Cocos (Keeling) Islands",
      CD: "Democratic Republic of the Congo",
      CF: "Central African Republic",
      CG: "Republic of the Congo",
      CH: "Switzerland",
      CL: "Chile",
      CM: "Cameroun",
      CN: "China",
      CO: "Colombia",
      CR: "Costa Rica",
      CU: "Cuba",
      CV: "Cabo Verde",
      CW: "Curaçao",
      CX: "Christmas Island",
      CY: "Cyprus",
      CZ: "Czech Republic",
      DE: "Germany",
      DK: "Denmark",
      DM: "Dominica",
      DO: "Dominican Republic",
      EC: "Ecuador",
      EE: "Estonia",
      ES: "Spain",
      ET: "Ethiopia",
      FI: "Finland",
      FO: "Faroe Islands",
      FR: "France",
      GA: "Gabon",
      GB: "United Kingdom",
      GD: "Grenada",
      GF: "French Guiana",
      GG: "Guernsey",
      GI: "Gibraltar",
      GL: "Greenland",
      GP: "Guadeloupe",
      GQ: "Equatorial Guinea",
      GR: "Greece",
      GT: "Guatemala",
      GU: "Guam",
      GY: "Guyana",
      HN: "Honduras",
      HR: "Croatia",
      HT: "Haiti",
      HU: "Hungary",
      IE: "Ireland",
      IM: "Isle of Man",
      IS: "Island",
      IT: "Italy",
      JE: "Jersey",
      JM: "Jamaica",
      JP: "Japan",
      KE: "Kenya",
      KR: "South Korea",
      LI: "Lichtenstein",
      LS: "Lesotho",
      LT: "Lithuania",
      LU: "Luxembourg",
      LV: "Latvia",
      MC: "Monaco",
      MD: "Moldova",
      ME: "Montenegro",
      MG: "Madagascar",
      MK: "North Macedonia",
      MQ: "Martinique",
      MT: "Malta",
      MW: "Malawi",
      MX: "Mexico",
      MZ: "Mozambique",
      NA: "Namibia",
      NI: "Nicaragua",
      NL: "Netherlands",
      NO: "Norway",
      NZ: "New Zealand",
      PA: "Panama",
      PE: "Perú",
      PH: "Philippines",
      PL: "Poland",
      PT: "Portugal",
      PY: "Paraguay",
      RE: "Réunion",
      RO: "Romania",
      RS: "Serbia",
      RU: "Russia",
      RW: "Rwanda",
      SE: "Sweden",
      SG: "Singapore",
      SH: "Saint Helena",
      SI: "Slovenia",
      SJ: "Svalbard & Jan Mayen",
      SK: "Slovakia",
      SM: "San Marino",
      SO: "Somalia",
      SS: "South Sudan",
      SV: "El Salvador",
      TG: "Togo",
      TO: "Tonga",
      TR: "Turkey",
      TZ: "Tanzania",
      UA: "Ukraine",
      UG: "Uganda",
      US: "United States of America",
      UY: "Uruguay",
      VA: "Città del Vaticano",
      VE: "Venezuela",
      VN: "Vietnam",
      XK: "Kosovo",
      YT: "Mayotte",
      ZA: "South Africa",
      ZM: "Zambia",
      ZW: "Zimbabwe"
    },
    deliveryNotifications: {
      always: "Immer eine Empfangsbestätigung anfordern",
      never: "Nie eine Empfangsbestätigung anfordern"
    },
    eventAlarmPostponeTime: {
      "60": "1 Minute",
      "300": "5 Minuten",
      "900": "15 Minuten",
      "1800": "30 Minuten",
      "3600": "1 Stunde",
      "7200": "2 Stunden",
      "86400": "1 Tag",
      "604800": "1 Woche"
    },

    calendarTaskStatus: {
      "needs-action": "Aktion erforderlich",
      completed: "Abgeschlossen",
      "in-process": "In Bearbeitung",
      cancelled: "Gelöscht"
    },

    feedbackAreas: {
      addressbookSidebar: "Adressbuchliste",
      contactList: "Kontaktliste",
      contactView: "Kontakt anzeigen",
      mailSearch: "Nach E-Mail suchen",
      mailSidebar: "Ordnerliste anzeigen",
      messageList: "E-Mail-Liste",
      messageView: "E-Mail anzeigen",
      newMessage: "Neue E-Mail erstellen",
      other: "Mehr...",
      settings: "Einstellungen"
    },
    filterMail: {
      all: "Alle",
      byDate: "Datum",
      byFrom: "Von",
      bySize: "Größe",
      bySubject: "Betreff",
      flagged: "Markiert",
      filter: "Filter",
      orderBy: "Sortieren nach",
      seen: "Gelesen",
      unseen: "Ungelesen"
    },
    filtersActions: {
      discard: "Löschen",
      fileinto: "Verschieben nach",
      fileinto_copy: "Kopieren nach",
      keep: "E-Mail behalten",
      redirect: "E-Mail weiterleiten an",
      redirect_copy: "Kopie senden an ",
      reject: "Mit folgender Meldung ablehnen",
      addflag: "Markieren als",
      setkeyword: "Label hinzufügen",
      stop: "Filtereinstellungen beenden"
    },
    filtersDateOperators: {
      gt: "Nach",
      is: "Gleich",
      lt: "Vorher",
      notis: "ist nicht gleich"
    },
    filtersEnvelopeSubtypes: {From: "Von", To: "An"},
    filtersDateSubTypes: {date: "Datum", time: "Uhrzeit", weekday: "Wochentag"},
    filtersDateWeekdays: {
      monday: "Montag",
      tuesday: "Dienstag",
      wednesday: "Mittwoch",
      thursday: "Donnerstag",
      friday: "Freitag",
      saturday: "Samstag",
      sunday: "Sonntag"
    },
    filtersRules: {
      Bcc: "Bcc",
      body: "Text",
      Cc: "Cc",
      currentdate: "Erhalten am",
      envelope: "Nachricht",
      From: "Absender",
      other: "Anderer Header",
      size: "Größe",
      Subject: "Betreff",
      To: "Empfänger"
    },
    filtersSizeMUnits: {B: "Bytes", K: "Kilobytes", M: "Megabytes"},
    filtersSizeOperators: {over: "ist größer als", under: "ist kleiner als"},
    filtersStringOperators: {
      contains: "enhält",
      exists: "existiert",
      is: "entspricht",
      notcontains: "enthält nicht",
      notexists: "existiert nicht",
      notis: "ist ungleich zu"
    },
    languages: {
      de: "Deutsch",
      en: "Englisch",
      it: "Italienisch",
      nl: "Holländisch",
      sv: "Schwedisch",
      es: "Spanisch",
      pt: "Portugiesisch"
    },
    matchTypes: {
      allof: "Erfüllt alle Parameter",
      anyof: "Erfüllt einen der Parameter"
    },
    newAllDayEventAlert: {
      never: "Nie",
      eventDate: "Am Ereignistag",
      "-PT17H": "Einen Tag vorher - um 9:00 Uhr",
      "-P6DT17H": "Eine Woche vorher - um 9:00 Uhr"
    },
    newEventAlert: {
      never: "Nie",
      eventHour: "Zum Ereigniszeitpunkt",
      "-PT5M": "5 Minuten vorher",
      "-PT15M": "15 Minuten vorher",
      "-PT30M": "30 Minuten vorher",
      "-PT1H": "1 Stunde vorher",
      "-PT2H": "2 Stunden vorher",
      "-PT12H": "12 Stunden vorher",
      "-P1D": "1 Tag vorher",
      "-P7D": "1 Woche vorher"
    },
    newEventRepeatDayWeek: {
      MO: "Montag",
      TU: "Dienstag",
      WE: "Mittwoch",
      TH: "Donnerstag",
      FR: "Freitag",
      SA: "Samstag",
      SU: "Sonntag"
    },
    newEventRepeatFrequency: {
      NEVER: "Nie",
      DAILY: "Täglich",
      WEEKLY: "Wöchentlich",
      MONTHLY: "Monatlich",
      YEARLY: "Jährlich"
    },
    newEventRepeatMonths: {
      "1": "Januar",
      "2": "Februar",
      "3": "März",
      "4": "April",
      "5": "Mai",
      "6": "Juni",
      "7": "Juli",
      "8": "August",
      "9": "September",
      "10": "Oktober",
      "11": "November",
      "12": "Dezember"
    },
    orderSelector: {
      byLastname: "Nachname",
      byMail: "E-Mail",
      byName: "Vorname",
      byOrg: "Firma",
      orderBy: "Sortieren nach",
      showOnlyLists: "Listenansicht"
    },
    readConfirmations: {
      always: "Immer eine Lesebestätigung anfordern",
      never: "Nie eine Lesebestätigung anfordern"
    },
    readingConfirmation: {
      always: "Immer",
      ask: "Immer nachfragen",
      never: "Nie"
    },
    selectMail: {
      all: "Alle markieren",
      cancel: "Löschen",
      modify: "Ändern",
      none: "Nicht markieren"
    },
    shareOptionsCalendar: {
      None: "Keiner",
      DAndTViewer: "Datum und Uhrzeit anzeigen",
      Viewer: "Alle ansehen",
      Responder: "Antworten",
      Modifier: "Ändern"
    },
    showImages: {
      always: "Immer",
      contacts: "Nur von Kontakten und vertrauenswürdigen Quellen",
      never: "Nie"
    },
    themes: {
      dark: "Dark Modus",
      light: "Light Modus"
    },
    timeFormats: {
      "12h": "12h",
      "24h": "24h"
    },
    viewMode: {
      viewColumns: "Spaltenansicht",
      viewHalf: "Vorschau",
      viewFull: "Listenansicht"
    },
    viewModeCompact: {
      viewColumns: "Spalten",
      viewHalf: "Vorschau",
      viewFull: "Liste"
    }
  },

  dropdownMenu: {
    activesync: "ActiveSync",
    delete: "Löschen",
    download: "Download",
    edit: "Ändern",
    exportContacts: "Kontakte exportieren",
    importContacts: "Kontakte importieren",
    importEvents: "Ereignisse importieren",
    remove: "Entfernen",
    rename: "Umbenennen",
    share: "Teilen",
    syncNo: "Nicht synchronisieren",
    syncYes: "Synchronisieren",
    unsubscribe: "Anmelden"
  },

  dropdownUser: {
    feedback: "Feedback",
    logout: "Abmelden",
    settings: "Einstellungen"
  },

  errors: {
    emailDomainAlreadyPresent: "E-Mail-Adresse oder Domain bereits vorhanden",
    emptyField: "Leeres Feld",
    invalidEmail: "Ungültige E-Mail-Adresse",
    invalidEmailDomain: "Ungültige Domain oder E-Mail-Adresse",
    invalidPassword: "Ungültiges Passwort",
    invalidPasswordFormat: "Ungültiges Format, geben Sie mindestens 8 alphanumerische Zeichen ein",
    invalidValue: "Ungültiger Wert",
    mandatory: "Pflichtfled",
    notAllowedCharacter: "Nicht erlaubtes Zeichen",
    notChanged: "Unverändertes Feld",
    permissionDenied: "Erlaubnis verweigert",
    tooLong: "Zu langes Feld"
  },

  feedback: {
    disclaimer:
      "Ihre Meinung ist wichtig! Helfen Sie uns, Probleme zu lösen und den Service zu verbessern.",
    labels: {
      feedbackAreas: "Zu welchem Bereich möchten Sie uns ein Feedback geben?",
      note: "Hinterlassen Sie hier Ihre Nachricht (optional)"
    },
    msgError: {
      title: "Leider konnte Ihre Nachricht nicht gesendet werden",
      desc:
        "Bitte versuchen Sie es später noch einmal. Ihr Feedback hilft uns dabei, eine bessere Nutzererfahrung für Sie und alle unsere Nutzer zu schaffen."
    },
    msgSuccess: {
      title: "Vielen Dank für Ihre Nachricht!",
      desc:
        "Wir freuen uns sehr über Ihre Nachricht. Ihr Feedback hilft uns dabei, eine bessere Nutzererfahrung für Sie und alle unsere Nutzer zu schaffen."
    },
    privacy: "Die erhobenen Daten werden gemäß den geltenden Datenschutzbestimmungen verarbeitet.",
    title: "Feedback"
  },

  inputs: {
    labels: {
      addAddress: "Adresse hinzufügen",
      addDomain: "Adresse oder Domain hinzufügen",
      addLabel: "Labelname",
      addMembers: "Mitglieder hinzufügen",
      addParticipant: "Teilnehmer hinzufügen",
      addressbookName: "Adressbuch",
      addUser: "Nutzer hinzufügen",
      alert: "Hinweis",
      alert2: "2. Hinweis",
      at: "in",
      calendar: "Kalender",
      calendarBusinessDayEndHour: "Arbeitsende",
      calendarBusinessDayStartHour: "Arbeitsbeginn",
      calendarColor: "Kalenderfarbe",
      calendarMinutesInterval: "Zeitintervall",
      calendarName: "Kalendername",
      calendarView: "Standardansicht",
      company: "Firma",
      confidentialEvents: "Private Ereignisse",
      confirmNewPassword: "Kennwort bestätigen",
      contactNameFormat: "Form des Kontaktnamens",
      contactsOrder: "Kontakte sortieren",
      country: "Country",
      dateFormat: "Datumsformat",
      defaultAddressbook: "Standardadressbuch",
      defaultCalendar: "Standardkalender",
      displayName: "Angezeigter Name",
      email: "E-Mail",
      endDate: "Enddatum",
      event: "Ereignis",
      eventDailyRepetition: "Jeden;Tag",
      eventDailyWorkdayRepetition: "Jeden Wochentag",
      eventMonthlyLastDayRepetition: "Monatlich, am letzten Tag des Monats",
      eventMonthlyRepetition: "Am {{DayNumber}} jeden Monats",
      eventMonthlyWeekDayRepetition: "{{WeekDayNumber}}jeden Monats",
      eventMonthlyLastWeekDayRepetition: "Letzter {{WeekDay}} des Monats",
      eventPartecipation: "Teilnahmestatus",
      eventPriority: "Ereignispriorität",
      eventType: "Ereignistyp",
      eventWeeklyRepetition: "Wöchentlich, am:",
      eventYearlyLastDayRepetition: "Letzter Tag im {{Month}}, jährlich",
      eventYearlyRepetition: "Jeden {{DayNumber}} im {{Month}}, jährlich",
      eventYearlyWeekDayRepetition: "{{WeekDayNumber}} im {{Month}}, jährlich",
      eventYearlyLastWeekDayRepetition: "Letzter {{WeekDay}} im {{Month}}, jährlich",
      exportAsVcard: "Im VCard-Format exportieren",
      exportAsLdiff: "Im LDIF-Format exportieren",
      faxHome: "Fax Haus",
      faxWork: "Fax Arbeit",
      filter: "Filter",
      filterName: "Filtername",
      firstName: "Name",
      folderName: "Ordnername",
      freeBusy: "Anzeigen als",
      from: "Von",
      fromDate: "Ab Datum",
      home: "Home",
      hour: "Uhrzeit",
      includeFreeBusy: "Frei/besetzt einschließen",
      label: "Label",
      labelColor: "Labelfarbe",
      language: "Sprache",
      lastName: "Nachname",
      listName: "Listenname",
      location: "Ort",
      loginAddressbook: "Erstes Adressbuch",
      mailAccount: "Account",
      mobile: "Handy",
      newAddressBook: "Neues Adressbuch",
      newAllDayEventAlert: "Benachrichtigung für ganztägige Ereignisse",
      newCalendar: "Neuer Kalender",
      newEventAlert: "Benachrichtigung für neue Ereignisse",
      newForward: "Empfänger zur Liste hinzufügen",
      newPassword: "Neues Passwort",
      nickname: "Spitzname",
      object: "Betreff",
      oldPassword: "Aktuelles Passwort",
      parentFolder: "Pfad",
      phone: "Telefon",
      privateEvents: "Private Ereignisse",
      publicEvents: "Öffentliche Ereignisse",
      readingConfirmation: "Lesebestätigung senden",
      recoveryEmail: "Wiederherstellungs-E-Mail",
      renameAddressbook: "Adressbuch umbenennen",
      repeatEvery: "Wiederhole jeden",
      reportBugNote: "Problembeschreibung",
      reportBugPhoneNumber: "Telefonnummer",
      searchAddressbookToSubscribe: "Suchen Sie ein zu abonnierendes Adressbuch",
      searchCalendarToSubscribe: "Suchen Sie einen zu abonnierenden Kalender",
      searchIn: "Suche in",
      selectCalendar: "Kalender auswählen",
      selectIdentity: "Identität auswählen",
      selectList: "Liste auswählen",
      shareCalendarCalDAVURL: "CalDAV URL",
      shareCalendarWebDavICSURL: "WebDAV ICS URL",
      shareCalendarWebDavXMLURL: "WebDAV XML URL",
      shareToAddressbook: "Mit wem möchten Sie das Adressbuch teilen?",
      shareToCalendar: "Mit wem möchten Sie den Kalender teilen?",
      shareToFolder: "Mit wem möchten Sie den Ordner teilen",
      showImages: "Bilder anzeigen",
      signatureName: "Angezeigter Name",
      signatureText: "Unterschriftstext",
      startDate: "Startdatum",
      team: "Team",
      theme: "Thema",
      timeFormat: "Zeitforma",
      timezone: "Zeitzone",
      title: "Titel",
      to: "An",
      untilDate: "Bis zum",
      weekStart: "Wochenanfang",
      work: "Arbeit"
    },
    placeholder: {
      addressbookName: "Adressbuchname",

      birthday: "Geburtstag",
      city: "Ort",
      country: "Land",
      company: "Unternehmen",
      email: "E-Mail",
      firstName: "Name",
      lastName: "Nachname",
      newFolderName: "Neuer Ordnername",
      newNameAddressbook: "Neuer Adressbuchname",
      newPassword: "Mindestens 8 alphanumerische Zeichen",
      note: "Notiz",
      phone: "Telefon",
      postCode: "PLZ",
      members: "Mitglieder",
      region: "Provinz",
      role: "Rolle",
      select: "Auswählen",
      search: "Suchen in:",
      searchActivity: "Suchaktivität",
      searchEvent: "Ereignis suchen",
      snooze: "Schlummermodus",
      street: "Straße"
    },
    hint: {
      cancelSendMessage:
        "Wählen Sie die Rückruffrist aus, um eine Nachricht während des Sendens zurückzuziehen."
    }
  },

  labels: {
    attendees: "Teilnehmer",
    default: "Standard",
    forDomain: "On domain",
    withAllUsers: "Mit allen Benutzern",
    calendarAdvancedSearch: "Wo möchten Sie suchen?",
    clearAdvancedFilters: "Filter löschen",
    clearFields: "Felder löschen",
    holidayCalendar: "Holiday",
    searchResults: "Suchergebnisse"
  },

  loaderFullscreen: {
    description: "Inhalte werden geladen. Bitte warten!",
    descriptionFolder: "Ordner werden geladen. Bitte warten!",
    title: "Lädt..."
  },

  mail: {
    confirmMailLabel: "OK, bestätige!",
    contextualMenuLabels: {
      addAddressBook: "Zum Adressbuch hinzufügen",
      addEvent: "Ereignis hinzufügen",
      alarm: "Benachrichtigung",
      backTo: "Zurück",
      cancel: "Abbrechen",
      close: "Schließen",
      confirm: "Bestätigen",
      copyIn: "Kopieren nach",
      copyThreadIn: "Konversation kopieren",
      delete: "Löschen",
      deletePerm: "Unwiderruflich löschen",
      deleteThread: "Konversation löschen",
      deliveryNotification: "Empfangsbestätigung",
      download: "Download",
      editAsNew: "Als neu bearbeiten",
      emptySpam: "SPAM als gelesen markieren",
      emptyTrash: "Papierkorb leeren",
      forward: "Weiterleiten",
      import: "Nachricht importieren",
      label: "Label",
      labelsManagement: "Label verwalten",
      labelThread: "Label hinzufügen",
      markAllAsRead: "Alle als gelesen markieren",
      markAs: "Markieren als",
      markThreadAs: "Konversation markieren",
      manageFolders: "Ordner verwalten",
      modify: "Bearbeiten",
      more: "Mehr",
      moveThreadTo: "Konversation verschieben",
      moveTo: "Verschieben nach",
      newSubFolder: "Unterordner erstellen",
      plainText: "Textmodus",
      print: "Drucken",
      priorityHigh: "Wichtig",
      readed: "Gelesen",
      readedNot: "Ungelesen",
      readingNotification: "Lesebestätigung",
      rename: "Umbenennen",
      reply: "Antworten",
      replyToAll: "Allen antworten",
      restore: "Wiederherstellen",
      restoreThreadIn: "Konversation wiederherstellen",
      restoreIn: "Wiederherstellen nach",
      sendAsAttachment: "Als Anhang weiterleiten",
      showSourceCode: "Quellcode anzeigen",
      spam: "Spam",
      spamNot: "Kein Spam",
      starred: "Markieren",
      starredNot: "Markierung entfernen",
      thanks: "Vielen Dank",
      thread: "Konversationsaktion"
    },
    deleteMailDsc:
      "Wenn die Archivfunktion nicht aktiv ist, wird die Nachricht unwiderruflich gelöscht.<br/><b>Diese Aktion kann nicht rückgängig gemacht werden.</b>",
    deleteMailTitle: "Nachricht löschen",
    deleteMailsDsc:
      "Wenn die Archivfunktion nicht aktiv ist, werden die Nachricht unwiderruflich gelöscht.<br/><b>Diese Aktion kann nicht rückgängig gemacht werden.<b>",
    deleteMailsTitle: "Ausgewählte Nachrichten löschen",
    dropFiles: "Dateien anhängen",
    emptyTrashDsc:
      "Wenn die Archivfunktion nicht aktiv ist, werden beim entleeren des Papierkorbs alle Nachrichten <b>unwiderruflich gelöscht.</b><br/>Nachrichten, die länger als <b>30 Tage</b> im Papierkorb waren, werden automatisch gelöscht.",
    emptyTrashTitle: "Papierkorb entleeren",
    import: {
      messagesInvalid: "Ungültig: ",
      messagesNr: "Gesamtzahl der zu importierenden Nachrichten:",
      messagesValid: "Gültig:",
      desc:
        "Sie importieren Nachrichten in den Ordner: <code>{{folderImport}}</code><br/>Mithilfe einer eml-Datei (<strong>.eml</strong>) können Sie E-Mails auch aus anderen E-Mailprogrammen importieren.",
      dropFiles:
        "Laden Sie die zu importierende Datei hier hoch oder fügen Sie sie mittels Drag & Drop hier ein",
      error: "Die Nachrichten konnten nicht importiert werden",
      importing:
        "Wir importieren <b>{{messagesNumber}}</b> Nachrichten in den Ordner: <code>{{folderImport}}</code>",
      loading: "Lädt...",
      success: "Import erfolgreich abgeschlossen!",
      title: "Neue Nachrichten importieren",
      uploadedFile: "Hochgeladen",
      uploadingFile: "Wird hochgeladen"
    },
    markSpamReadDsc:
      "Alle im Ordner enthaltenen Nachrichten als gelesen markieren.<br/>Nach <b>30 Tagen</b> werden die Nachrichten automatisch gelöscht.",
    markSpamReadTitle: "Nachrichten als gelesen markieren",
    maxLabelsNumberTitle: "Zu viele Labels!",
    maxLabelsNumberDsc:
      "Sie haben die maximale Anzahl von Labels erreicht, die einer Nachricht zugeordnet werden können. Um ein neues Label hinzufügen zu können, müssen Sie zuerst ein zugeordnetes Label entfernen.",
    message: {
      add: "Hinzufügen",
      addToAddressBook: "Kontakt zum Adressbuch hinzufügen",
      attachedMessage: "Angehängte Nachricht",
      attachments: "Anhänge",
      bugMsg: "Kannst du die Nachricht nicht sehen?",
      cc: "Cc:",
      date: "Datum:",
      draftsSelectedInfo: "Wählen Sie eine Nachricht aus der Liste aus und schreiben Sie weiter.",
      downloadAll: "Alles herunterladen",
      downloadAttachments: "Anhänge herunterladen",
      editPartecipationStatusDecription: "",
      editPartecipationStatusTitle: "Teilnahmestatus bearbeiten",
      eventActionAccept: "Akteptieren",
      eventActionDecline: "Ablehnen",
      eventActionTentative: "Vielleicht",
      eventAddToCalendar: "Dieses Ereignis ist in Ihrem Kalender nicht vorhanden.",
      eventAttendees: "Teilnehmer",
      eventAttendeeStatusAccepted: "Ereignis angenommen",
      eventAttendeeStatusDeclined: "Ereignis abgelehnt",
      eventAttendeeStatusTentative: "Vorläufige Teilnahme",
      eventCounterProposal: "Diese Nachricht enthält einen Änderungsvorschlag",
      eventDeleted: "Ereignis gelöscht",
      eventInvitation: "Einladung zum Ereignis:",
      eventLocationEmpty: "Nicht definiert",
      eventModify: "Bearbeiten",
      eventNeedAction: "Bestätigen Sie die Teilnahme an diesem Ereignis",
      eventNotNeedAction: "Ereignis bereits verarbeitet",
      eventOutDated: "Das Ereignis wurde geändert",
      eventStatusAccepted: "Akzeptiert",
      eventStatusDeclined: "Abgelehnt",
      eventStatusTentative: "Vorläufig",
      eventUpdate: "Ereignis aktualisieren",
      folderSelectedInfo:
        "Wählen Sie eine Nachricht aus der Liste aus und ermittelns Sie deren Inhalt.",
      from: "Von:",
      hideAllAttachments: "Ausblenden",
      hugeMessage:
        "Der Inhalt ist zu groß, um vollständig angezeigt zu werden. Klicken Sie auf den folgenden Link, um den Quellcode der Nachricht anzuzeigen.",
      hugeMessageNotice: "Nachricht abgeschnitten",
      inboxSelectedInfo:
        "Wählen Sie eine Nachricht aus der Liste aus und finden Sie heraus, wer Ihnen geschrieben hat.",
      loadImages: "Bilder herunterladen",
      localAttachments: "Es gibt zusätzliche Anhänge innerhalb des Ereignisses.",
      message: "Nachricht",
      messageForwarded: "Weitergeleitete Nachricht",
      messages: "Nachrichten",
      messagesQuota: "Anzahl der Nachrichten:",
      newMessageinThread: "Sie haben eine neue Nachricht erhalten",
      notSelected: "Nachricht auswählen",
      oneSelectMessages: "Ausgewählte Nachricht",
      oneSelectThreads: "Ausgewählte Konversation",
      readNotificationIgnore: "Ignorieren",
      readNotificationLabel: "{{Sender}} hat eine Lesebestätigung angeforder.",
      readNotificationLabelSimple: "Lesebestätigung für Nachricht anfordern.",
      readNotificationResponse:
        "<P>Die Nachricht<BR><BR>&nbsp;&nbsp;&nbsp; An:&nbsp; %TO%<BR>&nbsp;&nbsp;&nbsp; Betreff:&nbsp; %SUBJECT%<BR>&nbsp;&nbsp;&nbsp; Gesendet:&nbsp; %SENDDATE%<BR><BR>& wurde am %READDATE% gelesen.</P>",
      readNotificationSend: "Bestätigen",
      readNotificationSubjectPrefix: "Gelesen:",
      replyTo: "Antwort an:",
      sentSelectedInfo:
        "Wählen Sie eine Nachricht aus der Liste aus und sehen Sie, wem Sie geschrieben haben.",
      showAllAttachments: "Mehr anzeigen",
      showMoreThread: "Mehr anzeigen",
      selectMessages: "Ausgewählte Nachrichten",
      selectThreads: "Ausgewählte Konversation",
      sender: "Absender:",
      spamNotSelected: "Halten Sie den Ordner in Ordnung",
      spamSelectedInfo: "Überprüfen Sie die Nachrichten und markierne Sie Spam-Mails als gelesen",
      subject: "Betreff:",
      to: "An:",
      trashNotSelected: "Überprüfen Sie den Papierkorb",
      trashSelectedInfo:
        "Nachrichten können innerhalb von 30 Tagen aus dem Papierkorb wiederhergestellt werden.",
      updating: "Bearbeitung läuft",
      yesterday: "Gestern",
      wrote: "hat geschrieben:"
    },
    messagesList: {
      allMessagesLoaded: "Alle Nachrichten wurden hochgeladen",
      ctaSpamInfo: "Alle als gelesen markieren",
      ctaTrashInfo: "Papierkorb entleeren",
      folderEmpty: "Dieser Ornder ist leer",
      folderInfo: "Starten Sie eine Konversation oder verschieben Sie eine Nachricht.",
      foundMessages: "Gefunden",
      moveMessage: "Nachricht verschieben",
      moveMessages: "{{NumMessages}} Nachrichten verschieben",
      notFound: "Keine Nachricht gefunden",
      notFoundInfo:
        "Die Suche ergab keine Ergebnisse. Ändern Sie bitte die Suchkriterien und versuchen Sie es anschließend erneut.",
      pullDownToRefresh: "Zum Aktualisieren ziehen",
      releaseToRefresh: "Zum Aktualisieren loslassen",
      searchInfo: "Gefundene Nachrichten",
      selectMessages: "Ausgewählt",
      showMoreThread: "Mehr anzeigen",
      spamEmpty: "Alle Spam-Mails wurden entfernt!",
      spamInfo: "Spam-Mails werden nach 30 Tagen automatisch gelöscht.",
      trashEmpty: "Der Papierkorb ist leer",
      trashInfo: "Nachrichen, die im Papierkorb sind, werden nach 30 Tagen automatisch gelöscht.",
      totalMessages: "Nachrichten"
    },
    newMessage: {
      addAttach: "Anhang anhängen",
      addImgeDsc: "Bilder können direkt im Mailtext oder als Anhang eingefügt werden.",
      addImgeTitle: "Bilder einfügen",
      advanced: "Erweitert",
      bcc: "Bcc",
      cc: "Cc",
      cancelSend: "Senden der E-Mail erfolgreich abgebrochen",
      delete: "Löschen",
      deleted: "Nachricht erfolgreich gelöscht",
      deleteInProgress: "Nachricht wird gerade gelöscht",
      discardChanges: "Änderungen verwerfen",
      fontSize: {
        smaller: "Sehr klein",
        small: "Klein",
        normal: "Normal",
        medium: "Medium",
        big: "Groß",
        bigger: "Sehr groß"
      },
      from: "Von",
      hide: "Verstecken",
      invalidMails:
        "Es ist eine ungültige E-Mail-Adresse vorhanden. Überprüfen Sie die Adressen und versuchen Sie es erneut.",
      moreAttachments: "Mehr",
      noDraftChanged: "Es gibt keien Änderungen die gespeichert werden müssen",
      object: "Betreff",
      saveDraftDsc:
        "Die Nachricht wurde nicht gesendet und enthält nicht gespeicherte Änderungen. Sie können die Nachricht als Entwurf speichern und zu einem späteren Zeitpunkt mit dem Schreiben fortfahren.",
      saveDraftTitle: "Nachricht als Entwurf speichern?",
      savedDraft: "Als Entwurf gespeichert!",
      savingDraft: "Wird gespeichert...",
      sendInProgress: "Nachricht wird gesendet",
      sent: "Nachricht wurde erfolgreich gesendet",
      sizeExceeded: "Die Anhänge sind zu groß. Die maximale Größe beträgt {{messageMaxSize}} MB.",
      to: "An",
      toMore: "Andere",
      windowSubject: "Neue Nachricht"
    },
    reportBugMessage:
      "Wenn Sie die Nachricht nicht korrekt sehen können, bitten wir Sie uns zu kontaktieren, damit wir das Problem lösen und dadurch den Service verbessern können. Bitte geben Sie uns Ihre Telefonnummer und beschreiben Sie kurz das Problem, mit dem Sie konfrontiert sind.<br/><br/>Wenn Sie mit dem Feedback fortfahren, kann Ihre Nachricht on unserem Entwicklerteam bearbeitet werden.<br/><b>Die von Ihnen hinterlegten Informationen werden NICHT an Dritte weitergegeben und die Daten werden gemäß unseren Datenschutzbestimmungen behandelt.</b>",
    reportBugTitle: "Nachricht melden",
    searchMail: {
      attachments: "Anhänge",
      cleanFilters: "Filter zurücksetzen",
      marked: "Markiert"
    },
    sidebar: {
      createLabel: {
        desc: "",
        title: "Label erstellen"
      },
      createFolder: {
        desc: "",
        title: "Ordner erstellen"
      },
      modifyFolder: {
        desc: "",
        title: "Ordner bearbeiten"
      },
      restoreFolder: {
        desc: "",
        title: "Ordner wiederherstellen"
      },
      folder: "Ordner",
      folders: "Persönliche Ordner",
      labels: "Labels",
      newFoderName: "Neuer Ordnername",
      occupiedSpace: "Belegter Speicherplatz",
      sharedFolders: "Freigegebene Ordner",
      unlimitedSpace: "Unbegrenzter Speicherplatz"
    },
    thanksMailLabel: "OK, Vielen Dank!",
    tooManyCompositionsDsc:
      "Sie haben die maximale Anzahl neuer Nachrichten erreicht, die Sie gleichzeitig verfassen können. Vervollständigen Sie mindestens eine Nachricht von denen, die Sie schreiben, damit Sie eine neue Nachricht erstellen können.",
    tooManyCompositionsTitle: "Beenden Sie das Schreiben!"
  },

  mailboxes: {
    drafts: "Entwürfe",
    inbox: "Posteingang",
    more: "Mehr",
    sent: "Gesendet",
    shared: "Freigegebene Ordner",
    spam: "Spam",
    trash: "Papierkorb"
  },

  navbar: {
    addressbook: "Adressbuch",
    calendar: "Kalender",
    chat: "Chat",
    mail: "E-Mail",
    meeting: "Meeting",
    news: "News"
  },

  newsTags: {
    news: "News",
    suggestions: "Tipps",
    fix: "Korrekturen",
    comingSoon: "Coming soon",
    beta: "Beta"
  },

  pageLogout: {
    title: "Abmeldung läuft",
    desc: ""
  },

  pageIndexing: {
    title: "Ihr Konto wird indiziert",
    desc: "Diese Operation kann einige Zeit dauern. Bitte warten."
  },

  pageNotFound: {
    pageNotFound: "Etwas ist schief gelaufen",
    pageNotFoundInfo: "Die von Ihnen gesuchte Seite ist leider nicht verfügbar."
  },

  powered: "powered by Qboxmail",
  poweredCollaborationWith: "in Zusammenarbeit mit ",

  radioButtons: {
    labels: {
      askDeliveryConfirmation: "Empfangsbestätigung anfragen",
      askReadConfirmation: "Lesebestätigung anfragen",
      defaultView: "Standardansicht",
      newEventType: "Neuer Ereignistyp",
      newEventFreeBusy: "Verfügbarbkeit für neue Ereignisse anzeigen"
    }
  },

  shareOptions: {
    delete: "Löschen",
    read: "Lesen",
    update: "Bearbeiten",
    write: "Schreiben"
  },

  shareOptionsCalendar: {
    canCreateObjects: "Ereignisse in diesem Kalender einfügen lassen",
    canEraseObjects: "Ereignisse in diesem Kalender löchen lassen"
  },

  shortcuts: {
    addressbook: {
      newContact: "Neuer Kontakt",
      editContact: "Kontakt bearbeiten",
      deleteContact: "Kontakt löschen"
    },
    calendar: {
      newEvent: "Neues Ereignis",
      editEvent: "Ereignis bearbeiten",
      deleteEvent: "Ereignis löschen",
      moveRangeNext: "Nächstes Intervall anzeigen",
      moveRangePrevious: "Vorheriges Intervall anzeigen"
    },
    general: {
      switchModuls: "Module wechseln",
      refresh: "Seite aktualisieren",
      tabForward: "Zwischen klickbaren Elementen vorwärts gehen",
      tabBack: "Zwischen klickbaren Elementen rückwärts gehen",
      closeModal: "Lightbox schließen",
      openSettings: "Einstellungen öffnen",
      copy: "Ausgewähltes Element kopieren",
      past: "Ausgewähltes Element einfügen",
      cut: "Ausgewähltes Element ausschneiden"
    },
    mail: {
      confirm: "Bestätigen",
      deleteMessage: "Nachricht löschen",
      deleteSelectMessage: "Ausgewählte Nachricht löschen",
      editAsNew: "Als neu bearbeiten",
      empityTrash: "Papierkorb entleeren",
      expandThread: "Konversation erweitern",
      extendSelectDown: "Auswahl in der Nachrichtenliste nach unten erweitern",
      extendSelectUp: "Auswahl in der Nachrichtenliste erweitern",
      forward: "Weiterleiten",
      markRead: "Als gelesen/ungelesen markieren",
      moveDown: "Innerhalb der Nachrichtenliste nach unten bewegen",
      moveUp: "Innerhalb der Nachrichtenliste nach oben bewegen",
      newMessage: "Neue Nachricht",
      openSelectMessage: "Ausgewählte Nachricht öffnen",
      printMessage: "Nachricht drucken",
      reduceThread: "Konversation minimieren",
      reply: "Antworten",
      replyAll: "Allen Antworten",
      thanks: "Vielen Dank"
    }
  },

  settings: {
    addressbook: {
      management: {
        automaticSave: {
          desc: "",
          title: "Automatisches Speichern"
        },
        personalAddressbooks: {
          desc: "",
          headerItem: "Adressbuch",
          placeholderList: " ",
          title: "Persönliches Adressbuch"
        },
        sharedAddressbooks: {
          desc: "",
          headerItem: "Abonnierte Adressbücher",
          placeholderList: " ",
          title: "Freigegebene Adressbücher"
        },
        title: "Adressbücher"
      },
      title: "Kontakt",
      view: {
        desc: "",
        title: "Anzeigen"
      }
    },
    calendar: {
      activities: {title: "Aktivitäten"},
      events: {title: "Ereignisse", desc: "", alertTitle: "Warnungen", alertDesc: ""},
      invitations: {
        desc: "",
        title: "Einladungen verwalten",
        msgFromHeader: "Akzeptieren Sie Einladungen nur von",
        placeholderList: " "
      },
      labels: {title: "Labels", desc: ""},
      labelsTask: {title: "Labelbeschreibung", desc: ""},
      management: {
        title: "Kalender",
        holidayCalendars: {
          title: "Holiday calendars",
          desc: "",
          headerItem: "Calendars",
          placeholderList: " "
        },
        personalCalendars: {
          title: "Persönliche Kalender",
          desc: "",
          headerItem: "Kalender",
          placeholderList: " "
        },
        sharedCalendars: {
          desc: "",
          headerItem: "Abonnierte Kalender",
          placeholderList: " ",
          title: "Freigegebene Kalender"
        },
        includeInFreeBusy: "In Verfügbar / Beschäftigt einschließen",
        showActivities: "Aktivität anzeigen",
        showAlarms: "Hinweise anzeigen",
        syncWithActivesync: "Mit ActiveSync synchronisieren",
        sendMeMailOnMyEdit: "Senden Sie mir eine E-Mail, wenn ich den Kalender ändere",
        sendMeMailOnOtherEdit:
          "Senden Sie mir eine E-Mail, wenn jemand anderes den Kalender ändert",
        sendMailOnEditTo: "Wenn ich diesen Kaeldner ändere, sende eine E-Mail an"
      },
      view: {
        showBusyOutsideWorkingHours: "Außerhalb der Arbeitszeiten als besetzt anzeigen",
        showWeekNumbers: "Kalenderwoche anzeigen",
        title: "Darstellung"
      },
      title: "Kalender"
    },
    chat: {
      title: "Chat",
      view: {
        title: "Darstellung"
      }
    },
    general: {
      profile: {
        avatar: {
          desc: "",
          title: "Avatar"
        },
        email: "Benutzer E-Mail",
        generalSettings: {
          desc: "",
          title: "Allgemeine Einstellungen"
        },
        personalInfo: {
          desc: "",
          title: "Persönliche Informationen"
        },
        title: "Profil"
      },
      security: {
        changepassword: {
          info: "Passwort mit mindestens 8 alphanumerischen Zeichen",
          desc: "",
          resetpassword: "Neues Passwort erstellen",
          resetpassworddisabled: 'Die Funktion "Kennwort ändern" ist für dieses Konto deaktiviert.',
          resetpasswordexpired: "Ihr Passwort ist abgelaufen, bitte erstellen Sie ein neues",
          title: "Passwort ändern"
        },
        otp: {
          cantDisableOtp:
            "Die 2-Faktor-Authentifizierung kann für dieses Konto nicht deaktiviert werden",
          desc: "",
          disableOtp: "Deaktivieren Sie die 2-Faktor-Authentifizierung",
          hideParams: "Parameter ausblenden",
          instructionDisableOtp:
            "Geben Sie den von der Anwendung generierten Code in das folgende Formular ein, um die 2-Faktor-Authentifizierung für dieses Konto zu deaktivieren.",
          instructionsSetOtp:
            "Scannen Sie den QR-Code mit der Anwendung und geben Sie den generierten Code in das folgende Formular ein. Alternativ dazu können Sie die entsprechenenden Paramter für die Aktivierung manuell eingeben.",
          labelEnterCode: "Geben Sie den von der Anwendung generierten Code ein",
          paramAccount: "Account",
          paramCode: "Code",
          paramType: "Zeitbasiert",
          paramTypeVal: "Aktiv",
          params: "Code manuell einfügen",
          paramsInstruction: "Fügen Sie folgende Paramteter in die App ein",
          setOtp: "Aktivieren Sie die 2-Faktor-Authentifizierung",
          title: "OTP Einstellungen",
          titleDisableOtp: "OTP deaktivieren",
          titleSetOtp: "OTP aktivieren"
        },
        sessions: {
          browser: "Browser:",
          currentSession: "Aktuelle Sitzung",
          device: "Gerät:",
          desc: "",
          ip: "IP:",
          os: "OS:",
          removeAll: "Alle entfernen",
          removeSession: "Beenden",
          startdate: "Start:",
          title: "Offene Sitzungen"
        },
        title: "Sicherheit"
      },
      title: "Allgemein"
    },
    mail: {
      accounts: {desc: "", title: "Accounts"},
      antispam: {
        blacklist: {
          desc: "Adresse oder Domain zur Liste hinzufügen (z.B.: 'name@domain' oder '*@domain')",
          msgNoHeader: "Nachrichten von nicht zulassen (From)",
          title: "Black list"
        },
        deleteRuleDsc: "Hinterlegte Regel für <code>{{ruleEmail}}</code> löschen.",
        deleteRuleTitle: "Antispam-Regel löschen",
        placeholderList: " ",
        title: "Antispam",
        whitelist: {
          desc: "Adresse oder Domain zur Liste hinzufügen (z.B.: 'name@domain' oder '*@domain')",
          msgFromHeader: "Nachrichten von zulassen (From)",
          msgToHeader: "Nachrichten an zulassen (To)",
          title: "White list"
        }
      },
      autoresponder: {
        desc: "",
        msg: "Nachricht",
        placholdeSubject: "Betreff",
        placholderTextarea: "Nachricht",
        title: "Autoresponder"
      },
      compose: {
        cancelSend: {
          desc: "",
          title: "Senden der E-Mail abbrechen"
        },
        confirmations: {
          desc: "",
          title: "Lese- / Empfangsbestätigungen"
        },
        title: "Zusammensetzung"
      },
      delegations: {desc: "", title: "Delegierung"},
      filters: {
        actionsTitle: "Aktionen",
        activeFiltersTitle: "Aktive Filter",
        desc: "Filter für eingehende Nachrichten verwalten",
        headerItem: "Aktive Filter",
        parametersTitle: "Parameter",
        placeholderList: {
          parameter: "Parameter zum Filter hinzufügen",
          action: "Aktion zum Filter hinzufügen"
        },
        title: "Filter"
      },
      folders: {
        deleteFolderDsc:
          "Durch das Entfernen des Ordners <code>{{folderName}}</code> werden alle darin enthaltenen Nachrichten gelöscht.<br/><b>Diese Aktion kann nicht rückgängig gemacht werden.</b>.",
        deleteFoldersTitle: "Ordner löschen",
        desc: "Verwalten und teilen Sie ihre persönlichen Ordner",
        descShared: "Verwalten Sie Ihre freigegebenen Ordner",
        headerItem: "Ordner",
        headerMsg: "Nachrichten",
        headerUnread: "Ungelesen",
        modifyFolder: "ordner bearbeiten",
        newFolder: "Neuen Ordner erstellen",
        rootFolder: "-- Root --",
        subscribe: "Abonnieren",
        title: "Ordner",
        titleShared: "Freigegebene Ordner"
      },
      forward: {
        description: "Maximal {{max}} Empfänger auswählen",
        headerItem: "Empfänger",
        placeholderList: " ",
        title: "Weiterleiten",
        titleSection: "Nachricht weiterleiten"
      },
      labels: {
        desc: "",
        headerItem: "Labels",
        deleteLabelTitle: "Label löschen",
        deleteLabelTitleDsc: "Durch das Löschen des Labels wird es aus allen Nachrichten entfernt.",
        placeholderList: " ",
        title: "Labels"
      },
      reading: {
        layout: {
          desc: "",
          title: "Layout"
        },
        readingSettings: {
          desc: "",
          title: "Leseeinstellungen"
        },
        title: "Darstellung"
      },
      signature: {
        title: "Signatur",
        default: "Standardsignatur",
        dropFiles: "Bild hier hochladen",
        management: {
          title: "Einstellungen der Signatur"
        },
        onlyNewMessages: "Bei Antworten keine Signatur einfügen",
        placeholderList: " ",
        senderBcc: "Absender in Bcc hinzufügen",
        setAsDefault: "Set as default sender",
        signatures: {
          desc:
            "Personalisieren Sie die von Ihnen gesendeten E-Mails, indem Sie den Absendername definieren und eine persönliche Signatur erstellen.<br/>Der Absendername sowie die Signatur kann für alle Identitäten (Aliasgruppen), welche Ihrem Konto zugeordnet sind, angepasst werden.",
          headerItemSignature: "Absendername",
          headerItemIdentity: "Identität",
          title: "Signatur & Identität"
        }
      },
      title: "Mail"
    },
    meeting: {
      title: "Meeting",
      view: {
        desc: "",
        title: "Darstellung"
      }
    },
    news: {
      readMore: "Weiterlesen",
      title: "Webmail-News"
    },
    shortcuts: {
      title: "Tastaturkürzel"
    },
    title: "Einstellungen",
    unsaved: "Es gibt nicht gespeicherte Änderungen"
  },

  toast: {
    error: {
      alreadyPresent: "Achtung, die Ressource ist bereits vorhanden",
      avatarTooBig: "Achtung, die Avatargröße ist zu groß",
      compact: "Fehler beim Komprimieren des Ordners",
      connectionUnavailable: "Keine Verbindung",
      delete: "Fehler beim Löschen",
      deleteDraft: "Fehler beim Löschen des Entwurfs",
      enqueued: "Versuchen Sie es erneut, um die Einstellungen zu aktualisieren",
      fetchingAttachments: "Fehler biem Abrufen von Anhängen",
      filterActionsMissing: "Aktionen nicht vorhanden",
      filterConditionsMissing: "Parameter nicht vorhanden",
      filterNameMissing: "Filtername fehlt",
      folderCreation: "Fehler beim Erstellen eines neuen Ordners",
      folderCreationCharNotValid:
        "Fehler beim Erstellen/Ändern des Ordners. Es wird ein ungültiges Zeichen verwendet (/)",
      folderCreationNoName:
        "Fehler beim Erstellen des Ordners. Sie müssen einen gültigen Namen angeben",
      folderDelete: "Fehler beim Löschen des Ordners",
      folderNotPresent: "Ordner nicht vorhanden",
      folderRename: "Fehler beim Umbenennen des Ordners. Ordner bereits vorhanden",
      folderSubscribe: "Fehler beim Abonnieren des Ordners",
      generic: "Ein Fehler ist aufgetreten",
      maxMembers: "Achtung, maximale Mitgliederzahl erreicht",
      messageCopy: "Fehler beim Kopieren von Nachrichten",
      messageMove: "Fehler beim Verschieben von Nachrichten",
      messageNotFound: "Nachricht nicht gefunden",
      messageRemove: "Fehler beim Löschen von Nachrichten",
      missingParameters: "Fehlende Parameter",
      notifications: "Fehler beim Empfangen von Benachrichtigungen",
      notPermitted:
        "Sie verfügen nicht über die erforderlichen Berechtigungen, um den Vorgang abzuschließen",
      notValid: "Achtung, ungültiger Wert",
      onlyOneHomeAddress: "Es kann nur eine Privatadresse hinterlegt werden",
      onlyOneWorkAddress: "Es kann nur eine Arbeitsadresse hinterlegt werden",
      save: "Fehler beim Speichern",
      saveAcl: "Fehler beim Speichern der Berechtigungen für den Ordner",
      savingDraft: "Entwurf wird gerade gespeichert. Bitte versuchen Sie es in Kürze erneut",
      sendingMessage: "Fehler bei der Nachrichtenübermittlung",
      serverDisconnection: "Server wird aktualisiert. Bitte versuchen Sie es später erneut!",
      sessionExpired: "Sitzung abgelaufen",
      smtpBlockedByAntispam: "Die Nachricht wurde nicht gesendet: durch Antispam blockiert",
      smtpBlockedByAntivirus: "Die Nachricht wurde nicht gesendet: durch Antivirus blockiert",
      smtpDisabled: "Die Nachricht wurde nicht gesendet: SMTP ist für dieses Konto deaktiviert",
      smtpQuotaExceeded:
        "Die Nachricht wurde nicht gesendet: Das Kontingent für das Senden von Nachrichten wurde überschritten",
      requestLimit: "Anfragelimit erreicht. Bitte versuchen Sie es später erneut.",
      requestError:
        "Während der Anfrage ist ein Fehler aufgetreten. Bitte versuchen Sie es später erneut",
      updateFolders: "Bei der Ordneraktualisierung ist ein Fehler aufgetreten",
      updateMessageEvent: "Fehler beim Abrufen der Aktualisierung von Ereignissen",
      updatingParentFlags: "Fehler beim Aktualisieren der Nachrichtenmarkierung"
    },
    info: {
      newVersionAvailable: {
        desc: "Dieser Vorgang ist sicher, Ihre Einstellungen bleiben unverändert.",
        title:
          "Neue Funktionen sind verfügbar. Laden Sie die Seite neu, um Ihr Webmail auf die neueste Version zu aktualisieren."
      },
      serverReconnected: "Webmail wieder online",
      sourceMail: "E-Mail-Quellcode in Zwischenablage kopiert"
    },
    progress: {
      newFolder: "Neuer Ordner wird erstellt...",
      operationInProgress: "In Bearbeitung..."
    },
    success: {
      compact: "Ordner erfolgreich komprimiert",
      copiedLink: "Link in Zwischenablage kopiert",
      copiedMail: "Nachricht erfolgreich kopiert",
      copiedMailAddress: "E-Mail-Adresse in Zwischenablage kopiert",
      createFolder: "Ordner erfolgreich erstellt",
      delete: "Erfolgreich gelöscht",
      disabledOtp: "OTP erfolgreich deaktiviert",
      enabledOtp: "OTP erfolgreich aktiviert",
      movedMail: "Nachricht erfolgreich verschoben",
      otpDisabled: "OTP erfolgreich deaktiviert",
      otpEnable: "OTP erfolrgreich aktiviert",
      save: "Erfolgreich gespeichert",
      saveAcl: "Berechtigungen erfolgreich geändert",
      saveDraft: "Entwurf erfolgreich gespeichert",
      saveFolder: "Ordner erfolgreich bearbeitet",
      sentMail: "Nachricht erfolgreich gesendet",
      subscribeFolder: "Abonnement erfolgreich aktualisiert",
      updateMessageEvent: "Ereignisstatus aktualisiert"
    }
  },

  toggles: {
    labels: {
      allDayEvent: "Ganztägig",
      autoSaveContact:
        "Alle E-Mail-Empfänger die nicht in den Adressbüchern enthalten sind automatisch speichern",
      enableAutorespopnd: "Autoresponder aktivieren",
      enableAutorespopndInterval: "Nur in diesem Zeitraum senden",
      keepCopy: "Kopie der Nachricht lokal speichern",
      showPriority: "Priorität der empfangenen Nachrichten anzeigen",
      showThreads: "Konversationsansicht aktivieren",
      denyInvites: "Einladungen zu Terminen blockieren",
      enableDateSearch: "Datumssuche aktivieren",
      enableFullBody: "Nachrichtentext in Suche berücksichtigen",
      subscribeFolder: "Ordner abonnieren"
    }
  },

  wizard: {
    layout: {
      desc:
        "Personalisieren Sie Ihr Webmail, indem Sie die gewünschten Darstellungseinstellungen vornehmen.",
      title: "Darstellungsoptionen"
    },
    wizardData: {
      desc:
        "Personalisieren Sie Ihr Profil, indem Sie ein Bild hinzufügen und die fehlenden Daten vervollständigen.",
      title: "Profil"
    }
  }
};
